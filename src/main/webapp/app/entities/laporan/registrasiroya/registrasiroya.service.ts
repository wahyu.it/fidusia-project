import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { ISertifikatDTO } from 'app/shared/model/sertifikatDTO.model';
import { ICabangDTO } from 'app/shared/model/cabangDTO.model';

type EntityArrayResponseType = HttpResponse<ISertifikatDTO[]>;

@Injectable({ providedIn: 'root' })
export class RegistrasiRoyaService {
  	  public resourceUrlReport = SERVER_API_URL + 'api/sertifikats';
  	  public resourceUrlPreviewBerkas = SERVER_API_URL + 'api/file/sertifikat';
  	  public resourceUrlCabang = SERVER_API_URL + 'api/cabangs/getByLogin';
	
	  constructor(protected http: HttpClient) {}

	 /* query(req?: any): Observable<EntityArrayResponseType> {
	    const options = createRequestOption(req);
	    return this.http
	      .get<ILogBookDTO[]>(this.resourceUrlReport, { params: options, observe: 'response' })
	      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
	  }*/

	  search(req?: any): Observable<EntityArrayResponseType> {
	    const options = createRequestOption(req);
	    return this.http
	      .get<ISertifikatDTO[]>(this.resourceUrlReport +"/search-roya", { params: options, observe: 'response' })
	      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
	  }
	  
	  
	 viewSertifikat(ppkNomor: string): Observable<Blob> {
	    console.log(ppkNomor);
	    return this.http.get<Blob>(this.resourceUrlPreviewBerkas + (ppkNomor ? `?ppkNomor=${ppkNomor}` : ''), {
	      observe: 'body',
	      responseType: 'blob' as 'json'
	    });
	  }

// alternatif open blob new tab 
//	  viewSertifikat(ppkNomor: string): any {
//	    return this.http.get(this.resourceUrlPreviewBerkas + (ppkNomor ? `?ppkNomor=${ppkNomor}` : ''), { responseType: 'blob', observe: 'response'}).pipe(
//	      map((res: any) => {
//	        return new Blob([res.body], { type: 'application/pdf' });
//	      })
//	    );
//	  }

	  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
		    if (res.body) {
		      res.body.forEach((r: ISertifikatDTO) => {
		        r.tglSertifikat = r.tglSertifikat ? moment(r.tglSertifikat) : undefined;
		        r.ppkTanggal = r.ppkTanggal ? moment(r.ppkTanggal) : undefined;
		      });
		    }
	    return res;
	  }

	  cabangLogin(): Observable<ICabangDTO> {
	    return this.http.get<ICabangDTO>(this.resourceUrlCabang);
	  }
	
}