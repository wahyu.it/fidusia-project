import { Component, OnInit, ElementRef  } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { UploadWarkahService } from './uploadwarkah.service';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'jhi-warkah-dialog',
  templateUrl: './warkah-dialog.component.html',
  styleUrls: ['uploadwarkah.scss']
})
export class WarkahDialogComponent implements OnInit  {
	url !: any;
	filename !: string;
	image !: any;
	ext !: any;
	blob !: any;

  constructor(
	  public activeModal: NgbActiveModal, 
	  private eventManager: JhiEventManager,
      protected uploadBerkasService: UploadWarkahService,
      private sanitizer: DomSanitizer,
	  private playerRef: ElementRef
    ) {}

  clear(): void {
    this.activeModal.dismiss();
  }

  ngOnInit(): void {
	 this.uploadBerkasService.viewBerkas2(this.filename).subscribe(res => {
	  this.ext = this.filename.split(".")[1];
	  	 try{
            this.blob = new Blob([res], { type: "image/"+ this.ext});
            const imageUrl = window.URL.createObjectURL(this.blob);
            
   		   this.image = this.sanitizer.bypassSecurityTrustResourceUrl(imageUrl);
   		   
         } catch (ex) {
             console.log(ex);
         }
	 });
	  
  }
}
