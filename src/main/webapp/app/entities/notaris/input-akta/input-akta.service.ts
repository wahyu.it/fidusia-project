import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SERVER_API_URL } from 'app/app.constants';
import { Observable } from 'rxjs';
import { INotarisDTO } from 'app/shared/model/notarisDTO.model';
import { ITransaksiAktaDTO } from 'app/shared/model/transaksiAktaDTO.model';

@Injectable({ providedIn: 'root' })
export class InputAktaService {
  	  public resourceUrlMandatory= SERVER_API_URL + 'api/transaksi-akta/input/data-mandatory';
  	  public resourceUrl= SERVER_API_URL + 'api/transaksi-akta/order-notaris';
  	  public byLoginNotarisUrl= SERVER_API_URL + 'api/notarises/getByLogin';
	
	  constructor(protected http: HttpClient) {}

	  notarisLogin(): Observable<INotarisDTO> {
	    return this.http.get<INotarisDTO>(this.byLoginNotarisUrl);
	  }
	  
	   jmlOrder(): Observable<ITransaksiAktaDTO> {
	    return this.http.get<ITransaksiAktaDTO>(this.resourceUrl);
	  }
	  
	   dataMandatory(): Observable<ITransaksiAktaDTO> {
	    return this.http.get<ITransaksiAktaDTO>(this.resourceUrlMandatory);
	  }
	
}