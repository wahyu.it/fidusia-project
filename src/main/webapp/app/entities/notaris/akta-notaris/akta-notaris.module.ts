import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasimonitoringSharedModule } from 'app/shared/shared.module';
import { AktaNotarisComponent } from './akta-notaris.component';
import { aktaNotarisState } from './akta-notaris.route';

@NgModule({
  imports: [SisteminformasimonitoringSharedModule, RouterModule.forChild(aktaNotarisState)],
  declarations: [AktaNotarisComponent]
})
export class AktaNotarisModule {}
