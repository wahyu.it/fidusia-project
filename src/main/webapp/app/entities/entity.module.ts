import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
/* jhipster-needle-add-admin-module-import - JHipster will add admin modules imports here */

@NgModule({
  imports: [
    /* jhipster-needle-add-admin-module - JHipster will add admin modules here */
    RouterModule.forChild([
      {
        path: 'laporan/sertifikat',
        loadChildren: () => import('./Laporan/viewsertifikat/viewsertifikat.module').then(m => m.ViewSertifikatModule),
        data: {
          pageTitle: 'viewsertifikat.title'
        }
      },{
        path: 'laporan/registrasi-fidusia',
        loadChildren: () => import('./Laporan/registrasifidusia/registrasifidusia.module').then(m => m.RegistrasiFidusiaModule),
        data: {
          pageTitle: 'reportsertifikatfidusia.title'
        }
      },{
        path: 'laporan/upload-warkah',
        loadChildren: () => import('./Laporan/uploadwarkah/uploadwarkah.module').then(m => m.UploadWarkahModule),
        data: {
          pageTitle: 'reportsertifikatfidusia.title'
        }
      },{
        path: 'laporan/registrasi-roya',
        loadChildren: () => import('./Laporan/registrasiroya/registrasiroya.module').then(m => m.RegistrasiRoyaModule),
        data: {
          pageTitle: 'reportsertifikatroya.title'
        }
      },{
        path: 'leasing/user-manag',
        loadChildren: () => import('./leasing/user-manag/user-manag.module').then(m => m.UserManagModule),
        data: {
          pageTitle: 'userManagement.home.title'
        }
      },{
        path: 'uploadberkas',
        loadChildren: () => import('./leasing/uploadberkas/uploadberkas.module').then(m => m.UploadBerkasModule),
        data: {
          pageTitle: 'uploadberkas.title'
        }
      },{
        path: 'operation/verifyberkas',
        loadChildren: () => import('./operation/verifyberkas/verifyberkas.module').then(m => m.VerifyBerkasModule),
        data: {
          pageTitle: 'verifyberkas.page'
        }
      },{
        path: 'operation/verifykendaraan',
        loadChildren: () => import('./operation/verifykendaraan/verifykendaraan.module').then(m => m.VerifyKendaraanModule),
        data: {
          pageTitle: 'verifyberkas.page'
        }
      },{
        path: 'notaris/akta-notaris',
        loadChildren: () => import('./notaris/akta-notaris/akta-notaris.module').then(m => m.AktaNotarisModule),
        data: {
          pageTitle: 'akta.title'
        }
      },{
        path: 'notaris/cetak-akta',
        loadChildren: () => import('./notaris/cetak-akta/cetak-akta.module').then(m => m.CetakAktaModule),
        data: {
          pageTitle: 'akta.title'
        }
      },{
        path: 'notaris/biodata-notaris',
        loadChildren: () => import('./notaris/biodata-notaris/biodata-notaris.module').then(m => m.BiodataNotarisModule),
        data: {
          pageTitle: 'akta.title'
          }
       },{
        path: 'notaris/input-akta',
        loadChildren: () => import('./notaris/input-akta/input-akta.module').then(m => m.InputAktaModule),
        data: {
          pageTitle: 'akta.title'
        }
      }
      /* jhipster-needle-add-admin-route - JHipster will add admin routes here */
    ])
  ]
})

export class EntityRoutingModule {}
