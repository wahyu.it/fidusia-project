export const enum Bank {
  BCA = 'BCA',

  BNI = 'BNI',

  MANDIRI = 'MANDIRI'
}
