package com.fidusia.app.repository;

import com.fidusia.app.domain.Notaris;
import com.fidusia.app.domain.Saksi;
import com.fidusia.app.domain.TransaksiAkta;
import com.fidusia.app.domain.util.SaksiStatus;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Saksi entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TransaksiAktaRepository extends JpaRepository<TransaksiAkta, Long> {

	List<TransaksiAkta> findByNotarisAndTglOrder(Notaris notaris, LocalDate tglOrder);

	List<TransaksiAkta> findByNotarisAndTglOrderBetween(Notaris notaris, LocalDate startDate, LocalDate endDate);

	List<TransaksiAkta> findByNotarisAndTglOrderBetweenAndStatus(Notaris notaris, LocalDate startDate, LocalDate endDate, int i);

	List<TransaksiAkta> findByNotarisAndTglOrderAndStatus(Notaris notaris, LocalDate tglOrder, int i);

}
