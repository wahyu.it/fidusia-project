package com.fidusia.app.util;

import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.draw.DottedLineSeparator;

/**
 * Created by ataufani on 6/3/2017.
 */
public class CustomDashedLineSeparator extends DottedLineSeparator {
    protected float dash = 0;
    protected float phase = 0;

    public CustomDashedLineSeparator() {
        super();

        this.setDash(2.5f);
        this.setPhase(4f);
        this.setGap(2f);
        this.setOffset(3);
        this.setLineWidth(0.75f);
    }

    public float getDash() {
        return dash;
    }

    public float getPhase() {
        return phase;
    }

    public void setDash(float dash) {
        this.dash = dash;
    }

    public void setPhase(float phase) {
        this.phase = phase;
    }

    public void draw(PdfContentByte canvas, float llx, float lly, float urx, float ury, float y) {
        canvas.saveState();
        canvas.setLineWidth(lineWidth);
        canvas.setLineDash(dash, gap, phase);
        drawLine(canvas, llx, urx, y);
        canvas.restoreState();
    }
}
