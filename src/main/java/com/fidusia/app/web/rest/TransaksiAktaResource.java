package com.fidusia.app.web.rest;

import com.fidusia.app.domain.Notaris;
import com.fidusia.app.domain.Saksi;
import com.fidusia.app.domain.TransaksiAkta;
import com.fidusia.app.repository.SaksiRepository;
import com.fidusia.app.repository.TransaksiAktaRepository;
import com.fidusia.app.security.AuthoritiesConstants;
import com.fidusia.app.security.SecurityUtils;
import com.fidusia.app.service.CabangService;
import com.fidusia.app.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.fidusia.app.domain.Saksi}.
 */
@RestController
@RequestMapping("/api/transaksi-akta")
@Transactional
public class TransaksiAktaResource {

    private final Logger log = LoggerFactory.getLogger(TransaksiAktaResource.class);

    private static final String ENTITY_NAME = "saksi";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SaksiRepository saksiRepository;

    private final CabangService cabangService;

    private final TransaksiAktaRepository transakiAktaRepository;

    public TransaksiAktaResource(SaksiRepository saksiRepository, CabangService cabangService, TransaksiAktaRepository transakiAktaRepository) {
        this.saksiRepository = saksiRepository;
        this.cabangService = cabangService;
        this.transakiAktaRepository = transakiAktaRepository;
    }

    @PostMapping("")
    public ResponseEntity<Saksi> createSaksi(@RequestBody Saksi saksi) throws URISyntaxException {
        log.debug("REST request to save Saksi : {}", saksi);
        if (saksi.getId() != null) {
            throw new BadRequestAlertException("A new saksi cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Saksi result = saksiRepository.save(saksi);
        return ResponseEntity
            .created(new URI("/api/saksis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Saksi> updateSaksi(@PathVariable(value = "id", required = false) final Long id, @RequestBody Saksi saksi)
        throws URISyntaxException {
        log.debug("REST request to update Saksi : {}, {}", id, saksi);
        if (saksi.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, saksi.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!saksiRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Saksi result = saksiRepository.save(saksi);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, saksi.getId().toString()))
            .body(result);
    }

    @GetMapping("")
    public ResponseEntity<List<Saksi>> getAllSaksis(Pageable pageable) {
        log.debug("REST request to get a page of Saksis");
        Page<Saksi> page = saksiRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Saksi> getSaksi(@PathVariable("id") Long id) {
        log.debug("REST request to get Saksi : {}", id);
        Optional<Saksi> saksi = saksiRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(saksi);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteSaksi(@PathVariable("id") Long id) {
        log.debug("REST request to delete Saksi : {}", id);
        saksiRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
    
    @GetMapping("/order-notaris")
    public ResponseEntity<TransaksiAkta> getTransaksiAkta(
            @RequestParam(value = "tglorder", required = false) String tglorder) {
        log.debug("REST request to get a page of getTransaksiAkta");
        
        LocalDate tglOrder = tglorder != null ? LocalDate.parse(tglorder) : LocalDate.now();
        Notaris	notaris = cabangService.findNotarisByUserLogin();
        List<TransaksiAkta>	transaksiAktas = transakiAktaRepository.findByNotarisAndTglOrderAndStatus(notaris,tglOrder, 1);
        
        return ResponseEntity
                .ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, ""))
                .body(transaksiAktas.size()>0? transaksiAktas.get(0) : null);
    }
    
    @GetMapping("/input/data-mandatory")
    public ResponseEntity<TransaksiAkta> getDataMandatory() {
        log.debug("REST request to get a page of getTransaksiAkta");
        
//        LocalDate startDate = LocalDate.now().withDayOfMonth(1);
        LocalDate startDate = LocalDate.now();
        LocalDate endDate = LocalDate.now();
        Notaris	notaris = cabangService.findNotarisByUserLogin();
        List<TransaksiAkta>	transaksiAktas = transakiAktaRepository.findByNotarisAndTglOrderBetweenAndStatus(notaris, startDate, endDate,2);
        
        return ResponseEntity
                .ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, ""))
                .body(transaksiAktas.size()>0? transaksiAktas.get(transaksiAktas.size()-1) : null);
    }
}
