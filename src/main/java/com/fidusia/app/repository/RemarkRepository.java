package com.fidusia.app.repository;

import com.fidusia.app.domain.Remark;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Remark entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RemarkRepository extends JpaRepository<Remark, Long> {

	List<Remark> findByCategory(String upperCase);}
