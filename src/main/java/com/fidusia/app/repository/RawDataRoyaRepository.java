package com.fidusia.app.repository;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.fidusia.app.domain.RawDataRoya;
import com.fidusia.app.service.dto.SertifikatRoyaDTO;


public interface RawDataRoyaRepository extends JpaRepository<RawDataRoya, Long> {
	
	@Query("select r from  RawDataRoya r where r.desentralisasi=:desentralisasi and r.status in :statusList and r.duplicate= false")
	Page<RawDataRoya> findByDesentralisasiAndStatus(@Param("desentralisasi") Boolean desentralisasi, @Param("statusList") String[] statusList, Pageable pageable);
	
	@Query("select r from  RawDataRoya r where r.desentralisasi=:desentralisasi and r.status in :statusList and r.duplicate= false and r.tglOrder=:tglOrder")
	Page<RawDataRoya> findByDesentralisasiAndStatusAndTglOrder(@Param("desentralisasi") Boolean desentralisasi, @Param("statusList") String[] statusList,@Param("tglOrder") LocalDate tglOrder, Pageable pageable);
	
	@Query("select r from  RawDataRoya r where r.tglOrder=:tglOrder and r.status in :statusList and r.duplicate= false")
	Page<RawDataRoya> findByTglOrderAndStatus(@Param("tglOrder") LocalDate tglOrder, @Param("statusList") String[] statusList, Pageable pageable);
	
	@Query("select r from  RawDataRoya r where r.tglLunas between :tglStart and :tglEnd and r.status in :statusList and r.duplicate= false")
	Page<RawDataRoya> findByTglLunasAndStatus(@Param("tglStart") LocalDate tglStart, @Param("tglEnd") LocalDate tglEnd, @Param("statusList") String[] statusList, Pageable pageable);

	@Query("select r from  RawDataRoya r where r.uniqueKey=:uniqueKey and r.status in :statusList and r.duplicate= false")
	List<RawDataRoya> findByUniqueKeyAndStatus(@Param("uniqueKey") String uniqueKey, @Param("statusList") String[] statusList);

	@Query("select r from  RawDataRoya r where r.uniqueKey=:uniqueKey and r.status in ('SUCCESS','NOTFOUND') and r.duplicate= false")
	List<RawDataRoya> findByUniqueKeyAndStatusFinal(@Param("uniqueKey") String uniqueKey);

	@Query("select r from  RawDataRoya r where r.fileNama=:fileNama and r.status in :statusList and r.duplicate= false")
	List<RawDataRoya> findByFileNamaAndStatus(@Param("fileNama") String fileNama, @Param("statusList") String[] statusList);
	
	@Query("select r from  RawDataRoya r where r.resultSent=:resultSent and r.status in :statusList and r.duplicate= false")
	List<RawDataRoya> findByResultSentAndStatusNot(@Param("resultSent") Boolean resultSent, @Param("statusList") String[] status);

	@Query("select r from  RawDataRoya r where r.uniqueKey=:uniqueKey and r.status in :statusList and r.duplicate= false")
	List<RawDataRoya> findByUniqueKeyAndStatusIn(@Param("uniqueKey") String uniqueKey, @Param("statusList") String[] statusList);

//    @Query("SELECT new com.pitoesi.fidusia.service.dto.RawDataRoyaDTO(r.uniqueKey, COUNT(r.id))" +
//            "FROM RawDataRoya r " +
//            "GROUP BY r.uniqueKey HAVING COUNT(r.uniqueKey) > 1 ")
//    List<RawDataRoyaDTO> findDuplicateRoya();
//	
	List<RawDataRoya> findByUniqueKey(String uniqueKey);

	@Query("select r from  RawDataRoya r where r.uniqueKey=:uniqueKey ORDER BY r.tglOrder, r.uniqueKey ASC")
	List<RawDataRoya> findByUniqueKey2(@Param("uniqueKey") String uniqueKey);
	
	RawDataRoya findOneByUniqueKeyAndStatus(String uniqueKey, String status);

	@Query("select r from  RawDataRoya r where r.uniqueKey=:uniqueKey and r.status != 'SUCCESS'")
	RawDataRoya findOneByUniqueKeyAndStatusNotSuccess(@Param("uniqueKey") String uniqueKey);

	@Transactional
	@Modifying
	@Query("update RawDataRoya set resultSent=:resultSent, lastUpdateOn=:updateOn, lastUpdateBy=:updateBy where id=:id")
	int updateSentFlag(
			@Param("resultSent") Boolean resultSent,
			@Param("updateOn") ZonedDateTime updateOn,
			@Param("updateBy") String updateBy,
			@Param("id") Long id);
	
	long countByUniqueKey(String uniqueKey);

	Long countByDesentralisasiAndTglOrder(Boolean desentralisasi, LocalDate tglOrder);

	RawDataRoya findOneByUniqueKey(String ppkNomor);
    
	static final String SERTIFIKAT_ROYA_DTO  = "select new com.fidusia.app.service.dto.SertifikatRoyaDTO(r.id, r.noSertifikatOri, r.tglSertifikatOri, r.uniqueKey,"
			+ " r.tglOrder, r.tglLunas, r.namaNotarisOri, r.wilNotarisOri, r.royaTipe, r.nomorRoya, r.tglRoya, r.status, r.message, r.registrationId, r.desentralisasi) from RawDataRoya r ";
	
	@Query(SERTIFIKAT_ROYA_DTO + " WHERE r.tglOrder between :tglStart and :tglEnd")
	Page<SertifikatRoyaDTO> findByRoyaTglOrderBetween(
			@Param("tglStart") LocalDate tglStart, 
			@Param("tglEnd") LocalDate tglEnd,
			Pageable pageable);
	
	@Query(SERTIFIKAT_ROYA_DTO + " WHERE r.tglOrder between :tglStart and :tglEnd")
	Page<SertifikatRoyaDTO> findByAktaCabangAndAktaTglOrderBetween(
			@Param("tglStart") LocalDate tglStart, 
			@Param("tglEnd") LocalDate tglEnd,
			Pageable pageable);
	
	@Query(SERTIFIKAT_ROYA_DTO + " WHERE r.uniqueKey=:uniqueKey")
	Page<SertifikatRoyaDTO> findByUniqueKey(
			@Param("uniqueKey") String uniqueKey, 
			Pageable pageable);
	
}
