package com.fidusia.app.service.dto;

public class ResponseArrayGetDataDTO {
	
	private String adartDate;
	private String adartNotes;
	private String addressPic;
    private String agreementDate;
    private String akteAkhirDate;
    private String akteAkhirNotes;
    private String akteAwalDate;
    private String akteAwalNotes;
    private String alamatPenjamin;
    private String idNumberPic;
    private String jobPic;
    private String kecamatanPic;
    private String kelurahanPIC;
    private String kodeposPic;
    private String kotaPic;
    private String noAdart;
    private String npwpDate;
    private String npwpNotes;
    private String namaPic;
    private String nilaiPenjaminan;
    private String noAkteAkhir;
    private String noAkteAwal;
    private String noHpPic;
    private String noSiup;
    private String noTdp;
    private String siupDate;
    private String siupNotes;
    private String tdpDate;
    private String tdpNotes;
    private String agreementNumber;
    private String alamatDebitur;
    private String alamatPemegangBpkb;
    private String assetDescription;
    private String assetUsage;
    private String effectiveDate;
    private String endDate;
    private String fiduciaOnlineIdPersonal;
    private String fiduciaOnlineIdCompany;
    private String fiduciaType;
    private String iDType;
    private String iDTypePemegangBpkb;
    private String idNumber;
    private String idNumberPemegangBpkb;
    private String jenisCustomer;
    private String jenisKelamin;
    private String jenisKelaminPemegangBpkb;
    private String kecamatan;
    private String kecamatanPemegangBpkb;
    private String kelurahan;
    private String kelurahanPemegangBpkb;
    private String kewarganegaraan;
    private String kewarganegaraanPemegangBpkb;
    private String kodeCabang;
    private String kodepos;
    private String kodeposPemegangBpkb;
    private String kota;
    private String kotaPemegangBpkb;
    private String licensePlate;
    private String merkTypeKendaraan;
    private String namaCabang;
    private String namaDebitur;
    private String namaNotaris;
    private String namaPemegangBpkb;
    private String nilaiHutangPokok;
    private String nilaiOtr;
    private String nilaiObjekPenjaminan;
    private String noBpkb;
    private String noHp;
    private String noHPPemegangBpkb;
    private String noMesin;
    private String noRangka;
    private String noTelp;
    private String npwp;
    private String pekerjaan;
    private String pekerjaanPemegangBpkb;
    private String rtRw;
    private String rtRwPic;
    private String rtRwPemegangBpkb;
    private String spBpkbDate;
    private String startDate;
    private String statusPernikahan;
    private String statusPernikahanPemegangBpkb;
    private String supplier;
    private String tahunPembuatan;
    private String tanggalKontrak;
    private String tanggalLahir;
    private String tempatLahir;
    private String tempatTanggalLahirPemegangBpkb;
    private String usedNew;
    private String warna;
    private String aktaNo;
    private String aktaDate;
    
	public String getAdartDate() {
		return adartDate;
	}
	public void setAdartDate(String adartDate) {
		this.adartDate = adartDate;
	}
	public String getAdartNotes() {
		return adartNotes;
	}
	public void setAdartNotes(String adartNotes) {
		this.adartNotes = adartNotes;
	}
	public String getAddressPic() {
		return addressPic;
	}
	public void setAddressPic(String addressPic) {
		this.addressPic = addressPic;
	}
	public String getAgreementDate() {
		return agreementDate;
	}
	public void setAgreementDate(String agreementDate) {
		this.agreementDate = agreementDate;
	}
	public String getAkteAkhirDate() {
		return akteAkhirDate;
	}
	public void setAkteAkhirDate(String akteAkhirDate) {
		this.akteAkhirDate = akteAkhirDate;
	}
	public String getAkteAkhirNotes() {
		return akteAkhirNotes;
	}
	public void setAkteAkhirNotes(String akteAkhirNotes) {
		this.akteAkhirNotes = akteAkhirNotes;
	}
	public String getAkteAwalDate() {
		return akteAwalDate;
	}
	public void setAkteAwalDate(String akteAwalDate) {
		this.akteAwalDate = akteAwalDate;
	}
	public String getAkteAwalNotes() {
		return akteAwalNotes;
	}
	public void setAkteAwalNotes(String akteAwalNotes) {
		this.akteAwalNotes = akteAwalNotes;
	}
	public String getAlamatPenjamin() {
		return alamatPenjamin;
	}
	public void setAlamatPenjamin(String alamatPenjamin) {
		this.alamatPenjamin = alamatPenjamin;
	}
	public String getIdNumberPic() {
		return idNumberPic;
	}
	public void setIdNumberPic(String idNumberPic) {
		this.idNumberPic = idNumberPic;
	}
	public String getJobPic() {
		return jobPic;
	}
	public void setJobPic(String jobPic) {
		this.jobPic = jobPic;
	}
	public String getKecamatanPic() {
		return kecamatanPic;
	}
	public void setKecamatanPic(String kecamatanPic) {
		this.kecamatanPic = kecamatanPic;
	}
	public String getKelurahanPIC() {
		return kelurahanPIC;
	}
	public void setKelurahanPIC(String kelurahanPIC) {
		this.kelurahanPIC = kelurahanPIC;
	}
	public String getKodeposPic() {
		return kodeposPic;
	}
	public void setKodeposPic(String kodeposPic) {
		this.kodeposPic = kodeposPic;
	}
	public String getKotaPic() {
		return kotaPic;
	}
	public void setKotaPic(String kotaPic) {
		this.kotaPic = kotaPic;
	}
	public String getNoAdart() {
		return noAdart;
	}
	public void setNoAdart(String noAdart) {
		this.noAdart = noAdart;
	}
	public String getNpwpDate() {
		return npwpDate;
	}
	public void setNpwpDate(String npwpDate) {
		this.npwpDate = npwpDate;
	}
	public String getNpwpNotes() {
		return npwpNotes;
	}
	public void setNpwpNotes(String npwpNotes) {
		this.npwpNotes = npwpNotes;
	}
	public String getNamaPic() {
		return namaPic;
	}
	public void setNamaPic(String namaPic) {
		this.namaPic = namaPic;
	}
	public String getNilaiPenjaminan() {
		return nilaiPenjaminan;
	}
	public void setNilaiPenjaminan(String nilaiPenjaminan) {
		this.nilaiPenjaminan = nilaiPenjaminan;
	}
	public String getNoAkteAkhir() {
		return noAkteAkhir;
	}
	public void setNoAkteAkhir(String noAkteAkhir) {
		this.noAkteAkhir = noAkteAkhir;
	}
	public String getNoAkteAwal() {
		return noAkteAwal;
	}
	public void setNoAkteAwal(String noAkteAwal) {
		this.noAkteAwal = noAkteAwal;
	}
	public String getNoHpPic() {
		return noHpPic;
	}
	public void setNoHpPic(String noHpPic) {
		this.noHpPic = noHpPic;
	}
	public String getNoSiup() {
		return noSiup;
	}
	public void setNoSiup(String noSiup) {
		this.noSiup = noSiup;
	}
	public String getNoTdp() {
		return noTdp;
	}
	public void setNoTdp(String noTdp) {
		this.noTdp = noTdp;
	}
	public String getSiupDate() {
		return siupDate;
	}
	public void setSiupDate(String siupDate) {
		this.siupDate = siupDate;
	}
	public String getSiupNotes() {
		return siupNotes;
	}
	public void setSiupNotes(String siupNotes) {
		this.siupNotes = siupNotes;
	}
	public String getTdpDate() {
		return tdpDate;
	}
	public void setTdpDate(String tdpDate) {
		this.tdpDate = tdpDate;
	}
	public String getTdpNotes() {
		return tdpNotes;
	}
	public void setTdpNotes(String tdpNotes) {
		this.tdpNotes = tdpNotes;
	}
	public String getAgreementNumber() {
		return agreementNumber;
	}
	public void setAgreementNumber(String agreementNumber) {
		this.agreementNumber = agreementNumber;
	}
	public String getAlamatDebitur() {
		return alamatDebitur;
	}
	public void setAlamatDebitur(String alamatDebitur) {
		this.alamatDebitur = alamatDebitur;
	}
	public String getAlamatPemegangBpkb() {
		return alamatPemegangBpkb;
	}
	public void setAlamatPemegangBpkb(String alamatPemegangBpkb) {
		this.alamatPemegangBpkb = alamatPemegangBpkb;
	}
	public String getAssetDescription() {
		return assetDescription;
	}
	public void setAssetDescription(String assetDescription) {
		this.assetDescription = assetDescription;
	}
	public String getAssetUsage() {
		return assetUsage;
	}
	public void setAssetUsage(String assetUsage) {
		this.assetUsage = assetUsage;
	}
	public String getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getFiduciaOnlineIdPersonal() {
		return fiduciaOnlineIdPersonal;
	}
	public void setFiduciaOnlineIdPersonal(String fiduciaOnlineIdPersonal) {
		this.fiduciaOnlineIdPersonal = fiduciaOnlineIdPersonal;
	}
	public String getFiduciaType() {
		return fiduciaType;
	}
	public void setFiduciaType(String fiduciaType) {
		this.fiduciaType = fiduciaType;
	}
	public String getiDType() {
		return iDType;
	}
	public void setiDType(String iDType) {
		this.iDType = iDType;
	}
	public String getiDTypePemegangBpkb() {
		return iDTypePemegangBpkb;
	}
	public void setiDTypePemegangBpkb(String iDTypePemegangBpkb) {
		this.iDTypePemegangBpkb = iDTypePemegangBpkb;
	}
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	public String getIdNumberPemegangBpkb() {
		return idNumberPemegangBpkb;
	}
	public void setIdNumberPemegangBpkb(String idNumberPemegangBpkb) {
		this.idNumberPemegangBpkb = idNumberPemegangBpkb;
	}
	public String getJenisCustomer() {
		return jenisCustomer;
	}
	public void setJenisCustomer(String jenisCustomer) {
		this.jenisCustomer = jenisCustomer;
	}
	public String getJenisKelamin() {
		return jenisKelamin;
	}
	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}
	public String getJenisKelaminPemegangBpkb() {
		return jenisKelaminPemegangBpkb;
	}
	public void setJenisKelaminPemegangBpkb(String jenisKelaminPemegangBpkb) {
		this.jenisKelaminPemegangBpkb = jenisKelaminPemegangBpkb;
	}
	public String getKecamatan() {
		return kecamatan;
	}
	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}
	public String getKecamatanPemegangBpkb() {
		return kecamatanPemegangBpkb;
	}
	public void setKecamatanPemegangBpkb(String kecamatanPemegangBpkb) {
		this.kecamatanPemegangBpkb = kecamatanPemegangBpkb;
	}
	public String getKelurahan() {
		return kelurahan;
	}
	public void setKelurahan(String kelurahan) {
		this.kelurahan = kelurahan;
	}
	public String getKelurahanPemegangBpkb() {
		return kelurahanPemegangBpkb;
	}
	public void setKelurahanPemegangBpkb(String kelurahanPemegangBpkb) {
		this.kelurahanPemegangBpkb = kelurahanPemegangBpkb;
	}
	public String getKewarganegaraan() {
		return kewarganegaraan;
	}
	public void setKewarganegaraan(String kewarganegaraan) {
		this.kewarganegaraan = kewarganegaraan;
	}
	public String getKewarganegaraanPemegangBpkb() {
		return kewarganegaraanPemegangBpkb;
	}
	public void setKewarganegaraanPemegangBpkb(String kewarganegaraanPemegangBpkb) {
		this.kewarganegaraanPemegangBpkb = kewarganegaraanPemegangBpkb;
	}
	public String getKodeCabang() {
		return kodeCabang;
	}
	public void setKodeCabang(String kodeCabang) {
		this.kodeCabang = kodeCabang;
	}
	public String getKodepos() {
		return kodepos;
	}
	public void setKodepos(String kodepos) {
		this.kodepos = kodepos;
	}
	public String getKodeposPemegangBpkb() {
		return kodeposPemegangBpkb;
	}
	public void setKodeposPemegangBpkb(String kodeposPemegangBpkb) {
		this.kodeposPemegangBpkb = kodeposPemegangBpkb;
	}
	public String getKota() {
		return kota;
	}
	public void setKota(String kota) {
		this.kota = kota;
	}
	public String getKotaPemegangBpkb() {
		return kotaPemegangBpkb;
	}
	public void setKotaPemegangBpkb(String kotaPemegangBpkb) {
		this.kotaPemegangBpkb = kotaPemegangBpkb;
	}
	public String getLicensePlate() {
		return licensePlate;
	}
	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}
	public String getMerkTypeKendaraan() {
		return merkTypeKendaraan;
	}
	public void setMerkTypeKendaraan(String merkTypeKendaraan) {
		this.merkTypeKendaraan = merkTypeKendaraan;
	}
	public String getNamaCabang() {
		return namaCabang;
	}
	public void setNamaCabang(String namaCabang) {
		this.namaCabang = namaCabang;
	}
	public String getNamaDebitur() {
		return namaDebitur;
	}
	public void setNamaDebitur(String namaDebitur) {
		this.namaDebitur = namaDebitur;
	}
	public String getNamaNotaris() {
		return namaNotaris;
	}
	public void setNamaNotaris(String namaNotaris) {
		this.namaNotaris = namaNotaris;
	}
	public String getNamaPemegangBpkb() {
		return namaPemegangBpkb;
	}
	public void setNamaPemegangBpkb(String namaPemegangBpkb) {
		this.namaPemegangBpkb = namaPemegangBpkb;
	}
	public String getNilaiHutangPokok() {
		return nilaiHutangPokok;
	}
	public void setNilaiHutangPokok(String nilaiHutangPokok) {
		this.nilaiHutangPokok = nilaiHutangPokok;
	}
	public String getNilaiOtr() {
		return nilaiOtr;
	}
	public void setNilaiOtr(String nilaiOtr) {
		this.nilaiOtr = nilaiOtr;
	}
	public String getNilaiObjekPenjaminan() {
		return nilaiObjekPenjaminan;
	}
	public void setNilaiObjekPenjaminan(String nilaiObjekPenjaminan) {
		this.nilaiObjekPenjaminan = nilaiObjekPenjaminan;
	}
	public String getNoBpkb() {
		return noBpkb;
	}
	public void setNoBpkb(String noBpkb) {
		this.noBpkb = noBpkb;
	}
	public String getNoHp() {
		return noHp;
	}
	public void setNoHp(String noHp) {
		this.noHp = noHp;
	}
	public String getNoHPPemegangBpkb() {
		return noHPPemegangBpkb;
	}
	public void setNoHPPemegangBpkb(String noHPPemegangBpkb) {
		this.noHPPemegangBpkb = noHPPemegangBpkb;
	}
	public String getNoMesin() {
		return noMesin;
	}
	public void setNoMesin(String noMesin) {
		this.noMesin = noMesin;
	}
	public String getNoRangka() {
		return noRangka;
	}
	public void setNoRangka(String noRangka) {
		this.noRangka = noRangka;
	}
	public String getNoTelp() {
		return noTelp;
	}
	public void setNoTelp(String noTelp) {
		this.noTelp = noTelp;
	}
	public String getNpwp() {
		return npwp;
	}
	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}
	public String getPekerjaan() {
		return pekerjaan;
	}
	public void setPekerjaan(String pekerjaan) {
		this.pekerjaan = pekerjaan;
	}
	public String getPekerjaanPemegangBpkb() {
		return pekerjaanPemegangBpkb;
	}
	public void setPekerjaanPemegangBpkb(String pekerjaanPemegangBpkb) {
		this.pekerjaanPemegangBpkb = pekerjaanPemegangBpkb;
	}
	public String getRtRw() {
		return rtRw;
	}
	public void setRtRw(String rtRw) {
		this.rtRw = rtRw;
	}
	public String getRtRwPemegangBpkb() {
		return rtRwPemegangBpkb;
	}
	public void setRtRwPemegangBpkb(String rtRwPemegangBpkb) {
		this.rtRwPemegangBpkb = rtRwPemegangBpkb;
	}
	public String getSpBpkbDate() {
		return spBpkbDate;
	}
	public void setSpBpkbDate(String spBpkbDate) {
		this.spBpkbDate = spBpkbDate;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getStatusPernikahan() {
		return statusPernikahan;
	}
	public void setStatusPernikahan(String statusPernikahan) {
		this.statusPernikahan = statusPernikahan;
	}
	public String getStatusPernikahanPemegangBpkb() {
		return statusPernikahanPemegangBpkb;
	}
	public void setStatusPernikahanPemegangBpkb(String statusPernikahanPemegangBpkb) {
		this.statusPernikahanPemegangBpkb = statusPernikahanPemegangBpkb;
	}
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getTahunPembuatan() {
		return tahunPembuatan;
	}
	public void setTahunPembuatan(String tahunPembuatan) {
		this.tahunPembuatan = tahunPembuatan;
	}
	public String getTanggalKontrak() {
		return tanggalKontrak;
	}
	public void setTanggalKontrak(String tanggalKontrak) {
		this.tanggalKontrak = tanggalKontrak;
	}
	public String getTanggalLahir() {
		return tanggalLahir;
	}
	public void setTanggalLahir(String tanggalLahir) {
		this.tanggalLahir = tanggalLahir;
	}
	public String getTempatLahir() {
		return tempatLahir;
	}
	public void setTempatLahir(String tempatLahir) {
		this.tempatLahir = tempatLahir;
	}
	public String getTempatTanggalLahirPemegangBpkb() {
		return tempatTanggalLahirPemegangBpkb;
	}
	public void setTempatTanggalLahirPemegangBpkb(String tempatTanggalLahirPemegangBpkb) {
		this.tempatTanggalLahirPemegangBpkb = tempatTanggalLahirPemegangBpkb;
	}
	public String getUsedNew() {
		return usedNew;
	}
	public void setUsedNew(String usedNew) {
		this.usedNew = usedNew;
	}
	public String getWarna() {
		return warna;
	}
	public void setWarna(String warna) {
		this.warna = warna;
	}
	
	public String getFiduciaOnlineIdCompany() {
		return fiduciaOnlineIdCompany;
	}
	public void setFiduciaOnlineIdCompany(String fiduciaOnlineIdCompany) {
		this.fiduciaOnlineIdCompany = fiduciaOnlineIdCompany;
	}
	public String getRtRwPic() {
		return rtRwPic;
	}
	public void setRtRwPic(String rtRwPic) {
		this.rtRwPic = rtRwPic;
	}
	
	public String getAktaNo() {
		return aktaNo;
	}
	public void setAktaNo(String aktaNo) {
		this.aktaNo = aktaNo;
	}
	public String getAktaDate() {
		return aktaDate;
	}
	public void setAktaDate(String aktaDate) {
		this.aktaDate = aktaDate;
	}
	@Override
	public String toString() {
		return "ResponseArrayGetDataDTO [adartDate=" + adartDate + ", adartNotes=" + adartNotes + ", addressPic="
				+ addressPic + ", agreementDate=" + agreementDate + ", akteAkhirDate=" + akteAkhirDate
				+ ", akteAkhirNotes=" + akteAkhirNotes + ", akteAwalDate=" + akteAwalDate + ", akteAwalNotes="
				+ akteAwalNotes + ", alamatPenjamin=" + alamatPenjamin + ", idNumberPic=" + idNumberPic + ", jobPic="
				+ jobPic + ", kecamatanPic=" + kecamatanPic + ", kelurahanPIC=" + kelurahanPIC + ", kodeposPic="
				+ kodeposPic + ", kotaPic=" + kotaPic + ", noAdart=" + noAdart + ", npwpDate=" + npwpDate
				+ ", npwpNotes=" + npwpNotes + ", namaPic=" + namaPic + ", nilaiPenjaminan=" + nilaiPenjaminan
				+ ", noAkteAkhir=" + noAkteAkhir + ", noAkteAwal=" + noAkteAwal + ", noHpPic=" + noHpPic + ", noSiup="
				+ noSiup + ", noTdp=" + noTdp + ", siupDate=" + siupDate + ", siupNotes=" + siupNotes + ", tdpDate="
				+ tdpDate + ", tdpNotes=" + tdpNotes + ", agreementNumber=" + agreementNumber + ", alamatDebitur="
				+ alamatDebitur + ", alamatPemegangBpkb=" + alamatPemegangBpkb + ", assetDescription="
				+ assetDescription + ", assetUsage=" + assetUsage + ", effectiveDate=" + effectiveDate + ", endDate="
				+ endDate + ", fiduciaOnlineIdPersonal=" + fiduciaOnlineIdPersonal + ", fiduciaOnlineIdCompany="
				+ fiduciaOnlineIdCompany + ", fiduciaType=" + fiduciaType + ", iDType=" + iDType
				+ ", iDTypePemegangBpkb=" + iDTypePemegangBpkb + ", idNumber=" + idNumber + ", idNumberPemegangBpkb="
				+ idNumberPemegangBpkb + ", jenisCustomer=" + jenisCustomer + ", jenisKelamin=" + jenisKelamin
				+ ", jenisKelaminPemegangBpkb=" + jenisKelaminPemegangBpkb + ", kecamatan=" + kecamatan
				+ ", kecamatanPemegangBpkb=" + kecamatanPemegangBpkb + ", kelurahan=" + kelurahan
				+ ", kelurahanPemegangBpkb=" + kelurahanPemegangBpkb + ", kewarganegaraan=" + kewarganegaraan
				+ ", kewarganegaraanPemegangBpkb=" + kewarganegaraanPemegangBpkb + ", kodeCabang=" + kodeCabang
				+ ", kodepos=" + kodepos + ", kodeposPemegangBpkb=" + kodeposPemegangBpkb + ", kota=" + kota
				+ ", kotaPemegangBpkb=" + kotaPemegangBpkb + ", licensePlate=" + licensePlate + ", merkTypeKendaraan="
				+ merkTypeKendaraan + ", namaCabang=" + namaCabang + ", namaDebitur=" + namaDebitur + ", namaNotaris="
				+ namaNotaris + ", namaPemegangBpkb=" + namaPemegangBpkb + ", nilaiHutangPokok=" + nilaiHutangPokok
				+ ", nilaiOtr=" + nilaiOtr + ", nilaiObjekPenjaminan=" + nilaiObjekPenjaminan + ", noBpkb=" + noBpkb
				+ ", noHp=" + noHp + ", noHPPemegangBpkb=" + noHPPemegangBpkb + ", noMesin=" + noMesin + ", noRangka="
				+ noRangka + ", noTelp=" + noTelp + ", npwp=" + npwp + ", pekerjaan=" + pekerjaan
				+ ", pekerjaanPemegangBpkb=" + pekerjaanPemegangBpkb + ", rtRw=" + rtRw + ", rtRwPic=" + rtRwPic
				+ ", rtRwPemegangBpkb=" + rtRwPemegangBpkb + ", spBpkbDate=" + spBpkbDate + ", startDate=" + startDate
				+ ", statusPernikahan=" + statusPernikahan + ", statusPernikahanPemegangBpkb="
				+ statusPernikahanPemegangBpkb + ", supplier=" + supplier + ", tahunPembuatan=" + tahunPembuatan
				+ ", tanggalKontrak=" + tanggalKontrak + ", tanggalLahir=" + tanggalLahir + ", tempatLahir="
				+ tempatLahir + ", tempatTanggalLahirPemegangBpkb=" + tempatTanggalLahirPemegangBpkb + ", usedNew="
				+ usedNew + ", warna=" + warna + "]";
	}
}
