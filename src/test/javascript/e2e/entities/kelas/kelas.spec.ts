import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { KelasComponentsPage, KelasDeleteDialog, KelasUpdatePage } from './kelas.page-object';

const expect = chai.expect;

describe('Kelas e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let kelasComponentsPage: KelasComponentsPage;
  let kelasUpdatePage: KelasUpdatePage;
  let kelasDeleteDialog: KelasDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Kelas', async () => {
    await navBarPage.goToEntity('kelas');
    kelasComponentsPage = new KelasComponentsPage();
    await browser.wait(ec.visibilityOf(kelasComponentsPage.title), 5000);
    expect(await kelasComponentsPage.getTitle()).to.eq('sisteminformasiakademikApp.kelas.home.title');
    await browser.wait(ec.or(ec.visibilityOf(kelasComponentsPage.entities), ec.visibilityOf(kelasComponentsPage.noResult)), 1000);
  });

  it('should load create Kelas page', async () => {
    await kelasComponentsPage.clickOnCreateButton();
    kelasUpdatePage = new KelasUpdatePage();
    expect(await kelasUpdatePage.getPageTitle()).to.eq('sisteminformasiakademikApp.kelas.home.createOrEditLabel');
    await kelasUpdatePage.cancel();
  });

  it('should create and save Kelas', async () => {
    const nbButtonsBeforeCreate = await kelasComponentsPage.countDeleteButtons();

    await kelasComponentsPage.clickOnCreateButton();

    await promise.all([kelasUpdatePage.setNamaInput('nama'), kelasUpdatePage.jenjangSelectLastOption()]);

    expect(await kelasUpdatePage.getNamaInput()).to.eq('nama', 'Expected Nama value to be equals to nama');

    await kelasUpdatePage.save();
    expect(await kelasUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await kelasComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Kelas', async () => {
    const nbButtonsBeforeDelete = await kelasComponentsPage.countDeleteButtons();
    await kelasComponentsPage.clickOnLastDeleteButton();

    kelasDeleteDialog = new KelasDeleteDialog();
    expect(await kelasDeleteDialog.getDialogTitle()).to.eq('sisteminformasiakademikApp.kelas.delete.question');
    await kelasDeleteDialog.clickOnConfirmButton();

    expect(await kelasComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
