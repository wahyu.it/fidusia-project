package com.fidusia.app.repository;

import com.fidusia.app.domain.OrderData;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the OrderData entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrderDataRepository extends JpaRepository<OrderData, Long> {

    OrderData findOneByPpkNomor(String ppkNomor);
//    @Query("SELECT o " +
//            "FROM OrderData o " +
//            "LEFT JOIN FETCH o.berkas " +
//            "LEFT JOIN FETCH o.skSubstitusi " +
//            "LEFT JOIN FETCH o.cabang " +
//            "WHERE (o.status=0 OR o.status=1) AND o.nasabahJenis=:nasabahJenis AND o.ppkTanggal<=:ppkTanggal AND o.type=:type ORDER BY o.cabang.uploadPerforma DESC")
//    List<OrderData> findPendingOrderDataByNasabahJenisAndPpkTanggalAndType(@Param("nasabahJenis") Integer nasabahJenis, @Param("ppkTanggal") LocalDate ppkTanggal, Pageable pageable);

	List<OrderData> findByStatus(int value);

	List<OrderData> findByTanggal(LocalDate tanggal);

	List<OrderData> findByStatusOrderByAktaNoAsc(int value);

}
