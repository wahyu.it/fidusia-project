package com.fidusia.app.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Objects;


public class TransaksiAhuDTO implements Serializable {

    private String billId;

    private String kodeVoucher;

    private ZonedDateTime tanggalTransaksi;

    private LocalDate tanggalBayar;

    private String noSertifikat;

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public String getKodeVoucher() {
        return kodeVoucher;
    }

    public void setKodeVoucher(String kodeVoucher) {
        this.kodeVoucher = kodeVoucher;
    }

    public ZonedDateTime getTanggalTransaksi() {
        return tanggalTransaksi;
    }

    public void setTanggalTransaksi(ZonedDateTime tanggalTransaksi) {
        this.tanggalTransaksi = tanggalTransaksi;
    }

    public LocalDate getTanggalBayar() {
        return tanggalBayar;
    }

    public void setTanggalBayar(LocalDate tanggalBayar) {
        this.tanggalBayar = tanggalBayar;
    }

    public String getNoSertifikat() {
		return noSertifikat;
	}

	public void setNoSertifikat(String noSertifikat) {
		this.noSertifikat = noSertifikat;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TransaksiAhuDTO dto = (TransaksiAhuDTO) o;
        if (dto.billId == null || billId == null) {
            return false;
        }
        return Objects.equals(billId, dto.billId);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(billId);
    }

    @Override
    public String toString() {
        return "TransaksiAhuDTO{" +
                "billId='" + billId + "'" +
                ", kodeVoucher='" + kodeVoucher + "'" +
                ", tanggalTransaksi='" + tanggalTransaksi + "'" +
                ", tanggalBayar='" + tanggalBayar + "'" +
                ", noSertifikat='" + noSertifikat + "'" +
                '}';
    }
}
