package com.fidusia.app.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.fidusia.app.domain.Berkas;
import com.fidusia.app.domain.Cabang;
import com.fidusia.app.service.dto.BerkasDTO;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * Spring Data JPA repository for the Berkas entity.
 */
public interface BerkasRepository extends JpaRepository<Berkas,Long>, JpaSpecificationExecutor<Berkas> {

    Page<Berkas> findByCabang(Cabang cabang, Pageable pageable);

    Page<Berkas> findByCabangAndBerkasStatusNot(Cabang cabang, Integer berkasStatus, Pageable pageable);

    Berkas findOneByPpkNomor(String ppkNomor);

    Page<Berkas> findByUploadOnBetweenAndBerkasStatusNot(ZonedDateTime uploadFrom, ZonedDateTime uploadTo, Integer berkasStatus, Pageable pageable);

    Page<Berkas> findByUploadOnBetweenAndBerkasStatus(ZonedDateTime uploadFrom, ZonedDateTime uploadTo, Integer berkasStatus, Pageable pageable);

    Page<Berkas> findByTanggalPpkBetweenAndBerkasStatusNot(LocalDate tanggalPpkFrom, LocalDate tanggalPpkTo, Integer berkasStatus, Pageable pageable);

    Page<Berkas> findByTanggalPpkBetweenAndBerkasStatus(LocalDate tanggalPpkFrom, LocalDate tanggalPpkTo, Integer berkasStatus, Pageable pageable);

    Page<Berkas> findByCabangAndUploadOnBetweenAndBerkasStatusNot(Cabang cabang, ZonedDateTime uploadFrom, ZonedDateTime uploadTo, Integer berkasStatus, Pageable pageable);

    Page<Berkas> findByCabangAndUploadOnBetweenAndBerkasStatus(Cabang cabang, ZonedDateTime uploadFrom, ZonedDateTime uploadTo, Integer berkasStatus, Pageable pageable);

    Page<Berkas> findByCabangAndTanggalPpkBetweenAndBerkasStatusNot(Cabang cabang, LocalDate tanggalPpkFrom, LocalDate tanggalPpkTo, Integer berkasStatus, Pageable pageable);

    Page<Berkas> findByCabangAndTanggalPpkBetweenAndBerkasStatus(Cabang cabang, LocalDate tanggalPpkFrom, LocalDate tanggalPpkTo, Integer berkasStatus, Pageable pageable);

    Berkas findOneByCabangAndPpkNomor(Cabang cabang, String ppkNomor);

    Page<Berkas> findByPpkNomor(String ppkNomor, Pageable pageable);
    
    Berkas findByPpkNomor(String ppkNomor);
    
    Page<Berkas> findByPpkNomorIn(String ppkNomor, List<Integer> berkasStatusList, Pageable pageable);

    Page<Berkas> findByTanggalPpkAndBerkasStatusNot(LocalDate tanggalPpk, Integer berkasStatus, Pageable pageable);

    Page<Berkas> findByTanggalPpkAndBerkasStatus(LocalDate tanggalPpk, Integer berkasStatus, Pageable pageable);

    Page<Berkas> findByCabangAndTanggalPpkAndBerkasStatusNot(Cabang cabang, LocalDate tanggalPpk, Integer berkasStatus, Pageable pageable);

    Page<Berkas> findByCabangAndTanggalPpkAndBerkasStatus(Cabang cabang, LocalDate tanggalPpk, Integer berkasStatus, Pageable pageable);

    Page<Berkas> findByBerkasStatus(Integer berkasStatus, Pageable pageable);

    Page<Berkas> findByCabangAndBerkasStatus(Cabang cabang, Integer berkasStatus, Pageable pageable);
    
    Page<Berkas> findByNamaNasabah(String namaNasabah, Pageable pageable);
    
    Page<Berkas> findByCabangAndTanggalPpkBetweenAndBerkasStatusLessThan(Cabang cabang, LocalDate tanggalPpkFrom, LocalDate tanggalPpkTo, Integer berkasStatus, Pageable pageable);
    
    Page<Berkas> findByTanggalPpkBetweenAndBerkasStatusLessThan(LocalDate tanggalPpkFrom, LocalDate tanggalPpkTo, Integer berkasStatus, Pageable pageable);

    Page<Berkas> findByCabangAndUploadOnBetweenAndBerkasStatusLessThan(Cabang cabang, ZonedDateTime uploadFrom, ZonedDateTime uploadTo, Integer berkasStatus, Pageable pageable);

    Page<Berkas> findByUploadOnBetweenAndBerkasStatusLessThan(ZonedDateTime uploadFrom, ZonedDateTime uploadTo, Integer berkasStatus, Pageable pageable);
    
	Page<Berkas> findByPpkNomorAndCabang(String svalue, Cabang cabang, Pageable pageable);

    Page<Berkas> findByUploadOnBetweenAndBerkasStatusIn(ZonedDateTime uploadFrom, ZonedDateTime uploadTo, List<Integer> berkasStatusList, Pageable pageable);
    Page<Berkas> findByUploadOnBetweenAndBerkasStatusInAndNsbJenis(ZonedDateTime uploadFrom, ZonedDateTime uploadTo, List<Integer> berkasStatusList, Integer nsbJenis, Pageable pageable);
    Page<Berkas> findByUpdateOnBetweenAndBerkasStatusIn(ZonedDateTime uploadFrom, ZonedDateTime uploadTo, List<Integer> berkasStatusList, Pageable pageable);
    Page<Berkas> findByUpdateOnBetweenAndBerkasStatusInAndNsbJenis(ZonedDateTime uploadFrom, ZonedDateTime uploadTo, List<Integer> berkasStatusList, Integer nsbJenis, Pageable pageable);
    
//	static final String BERKAS_DTO  = "select new com.fidusia.app.service.dto.BerkasDTO(b.id, b.identitas, b.kk, b.ppk, b.skNasabah,"
//			+ " b.updateOn, b.uploadOn, b.ppkNomor, b.nsbJenis, c.id, b.namaNasabah, b.tanggalPpk, c.nama, r.id, r.name, b.orderType) from Berkas b"
//			+ " INNER JOIN Cabang c on c.id=b.cabang.id"
//			+ " LEFT JOIN Remark r on r.id=b.remark.id";
//
//	@Query(BERKAS_DTO + " WHERE b.tanggalPpk between :tglPpdFrom and :tglPpdTo AND b.berkasStatus in (0,1,2)")
//    Page<BerkasDTO> findByTanggalPpkBetweenAndBerkasStatusNotComplete(
//    		@Param("tglPpdFrom") LocalDate tglPpdFrom,
//    		@Param("tglPpdTo") LocalDate tglPpdTo,
//    		Pageable pageable);

	Page<BerkasDTO> findByPpkNomorAndAndMetodeKirim(String ppkNomor, Integer metodeKirim, Pageable pageable);

	Page<BerkasDTO> findByPpkNomorAndAndNsbJenis(String ppkNomor, Integer nsbJenis, Pageable pageable);

	Page<Berkas> findByUploadOnBetweenAndPartitionIdAndBerkasStatusInAndNsbJenis(ZonedDateTime uploadStart, ZonedDateTime uploadEnd, Integer partisiId, List<Integer> statusList, Integer statusNsbJns, Pageable pageable);

	Page<Berkas> findByCabangAndUploadOnBetweenAndPartitionIdAndBerkasStatusIn(Cabang cabang, ZonedDateTime sod,
			ZonedDateTime eod, int parseInt, List<Integer> statusList, Pageable pageable);

	Page<Berkas> findByCabangAndUploadOnBetweenAndBerkasStatusIn(Cabang cabang, ZonedDateTime sod, ZonedDateTime eod,
			List<Integer> statusList, Pageable pageable);

	Page<Berkas> findByUpdateOnBetweenAndPartitionIdAndBerkasStatusInAndNsbJenis(ZonedDateTime sod, ZonedDateTime eod,
			int parseInt, List<Integer> statusList, Integer statusNsbJns, Pageable pageable);

	Page<BerkasDTO> findByUpdateOnBetweenAndNsbJenisAndPartitionIdAndBerkasStatus(ZonedDateTime sod,
			ZonedDateTime eod, int value, int parseInt, int value2, Pageable pageable);

	Page<Berkas> findByCabangAndUpdateOnBetweenAndPartitionIdAndBerkasStatusIn(Cabang cabang, ZonedDateTime sod,
			ZonedDateTime eod, int parseInt, List<Integer> statusList, Pageable pageable);

	Page<BerkasDTO> findByCabangAndUpdateOnBetweenAndNsbJenisAndPartitionIdAndBerkasStatus(Cabang cabang,
			ZonedDateTime sod, ZonedDateTime eod, int value, int parseInt, int value2, Pageable pageable);

	Page<BerkasDTO> findByUpdateOnBetweenAndBerkasStatusAndNsbJenis(ZonedDateTime sod, ZonedDateTime eod, int value,
			int value2, Pageable pageable);

	Page<Berkas> findByCabangAndUpdateOnBetweenAndBerkasStatusIn(Cabang cabang, ZonedDateTime sod, ZonedDateTime eod,
			List<Integer> statusList, Pageable pageable);

	Page<BerkasDTO> findByUpdateOnBetweenAndMetodeKirimAndPartitionIdAndBerkasStatus(ZonedDateTime som,
			ZonedDateTime eom, int i, int parseInt, int value, Pageable pageable);

	Page<Berkas> findByUploadOnBetweenAndPartitionIdAndBerkasStatusIn(ZonedDateTime som, ZonedDateTime eom,
			int parseInt, List<Integer> statusList, Pageable pageable);

	Page<Berkas> findByCabangAndUpdateOnBetweenAndPartitionIdAndBerkasStatusInAndNsbJenis(Cabang cabang,
			ZonedDateTime som, ZonedDateTime eom, int parseInt, List<Integer> statusList, Integer statusNsbJns,
			Pageable pageable);

	Page<BerkasDTO> findByUpdateOnBetweenAndMetodeKirimAndBerkasStatus(ZonedDateTime som, ZonedDateTime eom, int i,
			int value, Pageable pageable);

	Page<BerkasDTO> findByCabangAndUpdateOnBetweenAndMetodeKirimAndPartitionIdAndBerkasStatus(Cabang cabang,
			ZonedDateTime som, ZonedDateTime eom, int i, int parseInt, int value, Pageable pageable);

	Page<Berkas> findByCabangAndUpdateOnBetweenAndBerkasStatusInAndNsbJenis(Cabang cabang, ZonedDateTime som,
			ZonedDateTime eom, List<Integer> statusList, Integer statusNsbJns, Pageable pageable);

	Page<BerkasDTO> findByCabangAndUpdateOnBetweenAndMetodeKirimAndBerkasStatus(Cabang cabang, ZonedDateTime som,
			ZonedDateTime eom, int i, int value, Pageable pageable);

	Page<BerkasDTO> findByCabangAndUpdateOnBetweenAndBerkasStatusAndNsbJenis(Cabang cabang, ZonedDateTime som,
			ZonedDateTime eom, int value, int value2, Pageable pageable);
	
	List<Berkas> findByBerkasStatus(Integer berkasStatus);
}
