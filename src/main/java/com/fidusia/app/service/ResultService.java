package com.fidusia.app.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.time.ZonedDateTime;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.fidusia.app.config.ApplicationProperties;
import com.fidusia.app.domain.Akta;
import com.fidusia.app.domain.OrderData;
import com.fidusia.app.domain.Ppk;
import com.fidusia.app.domain.Sertifikat;
import com.fidusia.app.repository.ApplicationConfigRepository;
import com.fidusia.app.repository.OrderDataRepository;
import com.fidusia.app.repository.SertifikatRepository;

@Service
public class ResultService {

    private final Logger log = LoggerFactory.getLogger(ResultService.class);

    private final ApplicationConfigRepository applicationConfigRepository;

    private final OrderDataRepository orderDataRepository;
    
    private final SertifikatRepository sertifikatRepository;
    
    private final ApplicationProperties applicationProperties;

    private final RawDataService rawDataService;
    
    private final Base64Service base64Service;

    private final static int connectTimeout = 60000 ;
    
    private final static int readTimeout = 60000 ;
    
    public ResultService(
    		SertifikatRepository sertifikatRepository,
    		OrderDataRepository orderDataRepository,
    		ApplicationConfigRepository applicationConfigRepository,
    		RawDataService rawDataService,
    		Base64Service base64Service,
			ApplicationProperties applicationProperties) {
    	this.applicationConfigRepository = applicationConfigRepository;
    	this.rawDataService = rawDataService;
    	this.base64Service = base64Service;
    	this.orderDataRepository = orderDataRepository;
    	this.sertifikatRepository = sertifikatRepository;
		this.applicationProperties = applicationProperties;
    }
    
    public void sendResult(Sertifikat sertifikat, String token) {
//    	log.debug("Start send result fidusia...");
    	OutputStreamWriter writer = null;
		BufferedReader reader = null;
		Akta akta = sertifikat.getAkta();
		Ppk ppk = akta.getPpk();
		OrderData orderData = orderDataRepository.findOneByPpkNomor(ppk.getNomor());
		
		String sertifikatDirectory = applicationProperties.getDomain().getBaseDirectory() + applicationProperties.getDirectory().getSertifikat();

        File savedFileDir = new File(sertifikatDirectory + sertifikat.getTglOrder());
        if(!savedFileDir.exists())
        	savedFileDir.mkdirs();

        String saveFilePath = savedFileDir.getPath() + "/" + ppk.getNomor() + "-sertifikat.pdf";
    	
		try {
			String fileSertifikatDoc = base64Service.encodedFile(saveFilePath);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("fiduciaOnlineId", orderData.getFidusiaOnlineId());
            jsonObject.put("agreementNumber", ppk.getNomor());
            jsonObject.put("notaryId", akta.getNotaris().getId().toString());
            jsonObject.put("aktaNo", akta.getNomor());
            jsonObject.put("aktaDate", akta.getTanggal());
            jsonObject.put("certificateNo", sertifikat.getNoSertifikat());
            jsonObject.put("certificateDate", sertifikat.getTglSertifikat().toLocalDate());
            jsonObject.put("invoiceNo", "-");
            jsonObject.put("invoiceDate", "-");
            jsonObject.put("notaryFee", "-");
            jsonObject.put("fiduciaRegistrationFee", sertifikat.getBiayaPnbp().toString());
            jsonObject.put("apToNotary", "-");
            jsonObject.put("tax", "-");
            jsonObject.put("ppnNotaryFee", "-");
            jsonObject.put("noSeriFakturPajak", "-");
            jsonObject.put("tanggalTerbitFaktur", "-");
            jsonObject.put("fileSertifikatDoc", fileSertifikatDoc);
            jsonObject.put("notaryFee", "-");
            jsonObject.put("billingID", sertifikat.getRegistrationId());
            
            String postData = jsonObject.toString();
//			log.debug("Post send data sertifikat :{}",postData);
    		
    		String url_mtf = applicationConfigRepository.findByCategoryAndName("APPLICATION_PROPERTIES", "URL_MTF").getValue();
//    		String token = rawDataService.requestToken();
    		
    		SSLContext ssl_ctx = SSLContext.getInstance("TLS");
			TrustManager[] trust_mgr = get_trust_mgr();
			ssl_ctx.init(null, trust_mgr, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());
			
			URL url = new URL(url_mtf + "api/agreements/result");
//			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty("Accept", "*/*");
			conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Authorization","Bearer " + token);
			conn.setConnectTimeout(connectTimeout);
			conn.setReadTimeout(readTimeout);
	        conn.setDoInput(true);
			conn.setDoOutput(true);
			
			writer = new OutputStreamWriter(conn.getOutputStream());
            writer.write(postData);
            writer.flush();
			
			int responseCode = conn.getResponseCode();
			
//			log.debug("responseCode: " + responseCode);
			
			if(responseCode == HttpStatus.OK.value()) {
				reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				
				String line;
				StringBuffer sb = new StringBuffer();
				while ((line = reader.readLine()) != null) {
					sb.append(line);
				}
				JSONObject result = new JSONObject(sb.toString());
				
				if ("success".equals(result.getString("status"))) {
		            log.debug("Send data to dinos success ppknomor {}", ppk.getNomor());
					sertifikat.setReportSubmitted(true);
					sertifikat.setUpdateOn(ZonedDateTime.now());
					
					sertifikatRepository.save(sertifikat);
				} else {
		            log.error("send data failed {} ", result.getString("message"));
				}
			} else if (responseCode == HttpStatus.FORBIDDEN.value()) {
				reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				
				String line;
				StringBuffer sb = new StringBuffer();
				while ((line = reader.readLine()) != null) {
					sb.append(line);
				}
				JSONObject result = new JSONObject(sb.toString());
				
				if ("FiduciaOnlineID Already Exists".equals(result.getString("message"))) {
					log.debug("FiduciaOnlineID Already Exists kontrak {}", ppk.getNomor());
				}
			} else {
	            log.error("send data failed {} ", responseCode);
//	            String line;
//
//				reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
//				StringBuffer sb = new StringBuffer();
//				while ((line = reader.readLine()) != null) {
//					sb.append(line);
//				}
////	            log.debug("result response: {}", sb.toString());
			}
			
		} catch (Exception e) {
            log.error("Failed service Get data token Job caused by {}", e.getMessage(), e);
		} finally {
			if(writer != null)
				try {
					writer.close();
				} catch (Exception e) { }
	
			if(reader != null)
				try {
					reader.close();
				} catch (IOException e) { }
		}
    	
    }

	private TrustManager[] get_trust_mgr() {
		TrustManager[] certs = new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String t) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String t) {
			}
		} };
		return certs;
	}

}
