import { Routes } from '@angular/router';
import { VerifikasiBerkasComponent } from './verifyberkas.component';
import { JhiResolvePagingParams } from 'ng-jhipster';

export const verifyBerkasState: Routes = [{
  path: '',
  component: VerifikasiBerkasComponent,
  resolve: {
	  pagingParams: JhiResolvePagingParams
  }
  }
];
