import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasimonitoringSharedModule } from 'app/shared/shared.module';
import { BiodataNotarisComponent } from './biodata-notaris.component';
import { biodataNotarisState } from './biodata-notaris.route';

@NgModule({
  imports: [SisteminformasimonitoringSharedModule, RouterModule.forChild(biodataNotarisState)],
  declarations: [BiodataNotarisComponent]
})
export class BiodataNotarisModule {}
