package com.fidusia.app.domain.util;

public enum AktaStatus {
	NEW(0), ORDERED(1), REGISTERED(2), CONFIRMED(3), PAID(4), INPROGRESS(5), CANCELLED(-1), FAILED(-2);
	
	private int value;
	
	AktaStatus (int value) {
		this.value = value;
	}

    public int value() {
        return value;
    }

    public static final Integer[] ACTIVE_STATUS = {AktaStatus.ORDERED.value, AktaStatus.REGISTERED.value};
    public static final Integer[] CANCEL_STATUS = {AktaStatus.CANCELLED.value};
}
