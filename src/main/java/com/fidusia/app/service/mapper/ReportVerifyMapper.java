package com.fidusia.app.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.fidusia.app.domain.ReportVerify;
import com.fidusia.app.domain.User;
import com.fidusia.app.service.dto.ReportVerifyDTO;

import java.util.List;

/**
 * Mapper for the entity ReportVerify and its DTO ReportVerifyDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ReportVerifyMapper {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "user.login", target = "userLogin")
    @Mapping(source = "user.firstName", target = "userFirstName")
    @Mapping(source = "user.lastName", target = "userLastName")
    ReportVerifyDTO reportVerifyToReportVerifyDTO(ReportVerify reportVerify);

    List<ReportVerifyDTO> reportVerifiesToReportVerifyDTOs(List<ReportVerify> reportVerifys);

    @Mapping(source = "userId", target = "user")
    ReportVerify reportVerifyDTOToReportVerify(ReportVerifyDTO reportVerifyDTO);

    List<ReportVerify> reportVerifyDTOsToReportVerifies(List<ReportVerifyDTO> reportVerifyDTOs);

    default User userFromId(Long id) {
        if (id == null) {
            return null;
        }
        User user = new User();
        user.setId(id);
        return user;
    }
}
