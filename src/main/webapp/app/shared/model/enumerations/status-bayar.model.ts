export const enum StatusBayar {
  LUNAS = 'LUNAS',

  BELUMBAYAR = 'BELUMBAYAR',

  NUNGGAK = 'NUNGGAK'
}
