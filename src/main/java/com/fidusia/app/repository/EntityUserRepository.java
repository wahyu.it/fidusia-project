package com.fidusia.app.repository;

import com.fidusia.app.domain.EntityUser;
import com.fidusia.app.service.dto.UserLeasingDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the EntityUser entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EntityUserRepository extends JpaRepository<EntityUser, Long> {

    EntityUser findOneByRecordStatusAndUserLogin(Integer recordStatus, String userLogin);

	static final String USER_LEASING_DTO  = "select new com.fidusia.app.service.dto.UserLeasingDTO(u.id, u.login, u.firstName, u.lastName, u.email, u.activated, u.langKey, u.createdBy, u.createdDate, u.lastModifiedBy, u.lastModifiedDate, c.id, c.nama, eu.role) from EntityUser eu"
			+ " INNER JOIN User u on u.id=eu.user.id"
			+ " LEFT JOIN Cabang c on c.id=eu.cabang.id";

	@Query(USER_LEASING_DTO + " WHERE eu.leasing.id IS NOT NULL")
    Page<UserLeasingDTO> findByUserAndLeasing(
    		Pageable pageable);
}
