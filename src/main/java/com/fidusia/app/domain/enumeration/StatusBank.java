package com.fidusia.app.domain.enumeration;

/**
 * The Bank enumeration.
 */
public enum StatusBank {
    ACTIVE, INACTIVE
}
