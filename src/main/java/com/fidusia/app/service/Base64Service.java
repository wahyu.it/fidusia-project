package com.fidusia.app.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class Base64Service {

    private final Logger log = LoggerFactory.getLogger(Base64Service.class);
    
    public Base64Service() {}
    
    public String encodedFile (String filePath) {
//    	log.debug("encodedFile: " + filePath);
    	
    	try {
        	byte[] input_file = Files.readAllBytes(Paths.get(filePath));

            byte[] encodedBytes = Base64.getEncoder().encode(input_file);
            String encodedString =  new String(encodedBytes);
            return encodedString;
            
		} catch (Exception e) {
	    	log.error("error encoded file: " + filePath);

	    	return null;
		}
    }
    
    public String decodedFile (String code, String baseDir, String namingFile) throws IOException {
//    	log.debug("encodedFile: " + baseDir + namingFile);
    	String encodedString =  new String(code);
        byte[] decodedBytes = Base64.getDecoder().decode(encodedString.getBytes());
        
        FileOutputStream fos;
		try {
			fos = new FileOutputStream(baseDir + namingFile);
	        fos.write(decodedBytes);
	        fos.flush();
	        fos.close();
	        return namingFile;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
    	
    	return null;
    }

}
