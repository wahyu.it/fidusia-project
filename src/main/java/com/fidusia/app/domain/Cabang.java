package com.fidusia.app.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Cabang.
 */
@Entity
@Table(name = "cabang")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Cabang implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "kode")
    private String kode;

    @Column(name = "nama")
    private String nama;

    @Column(name = "alamat")
    private String alamat;

    @Column(name = "kota")
    private String kota;

    @Column(name = "provinsi")
    private String provinsi;

    @Column(name = "pengadilan_negeri")
    private String pengadilanNegeri;

    @Column(name = "no_kontak")
    private String noKontak;

    @Column(name = "record_status")
    private Integer recordStatus;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "update_on")
    private ZonedDateTime updateOn;

    @Column(name = "start_tanggal")
    private LocalDate startTanggal;

    @ManyToOne(fetch = FetchType.LAZY)
    private Leasing leasing;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Cabang id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKode() {
        return this.kode;
    }

    public Cabang kode(String kode) {
        this.setKode(kode);
        return this;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return this.nama;
    }

    public Cabang nama(String nama) {
        this.setNama(nama);
        return this;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return this.alamat;
    }

    public Cabang alamat(String alamat) {
        this.setAlamat(alamat);
        return this;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getKota() {
        return this.kota;
    }

    public Cabang kota(String kota) {
        this.setKota(kota);
        return this;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getProvinsi() {
        return this.provinsi;
    }

    public Cabang provinsi(String provinsi) {
        this.setProvinsi(provinsi);
        return this;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public String getPengadilanNegeri() {
        return this.pengadilanNegeri;
    }

    public Cabang pengadilanNegeri(String pengadilanNegeri) {
        this.setPengadilanNegeri(pengadilanNegeri);
        return this;
    }

    public void setPengadilanNegeri(String pengadilanNegeri) {
        this.pengadilanNegeri = pengadilanNegeri;
    }

    public String getNoKontak() {
        return this.noKontak;
    }

    public Cabang noKontak(String noKontak) {
        this.setNoKontak(noKontak);
        return this;
    }

    public void setNoKontak(String noKontak) {
        this.noKontak = noKontak;
    }

    public Integer getRecordStatus() {
        return this.recordStatus;
    }

    public Cabang recordStatus(Integer recordStatus) {
        this.setRecordStatus(recordStatus);
        return this;
    }

    public void setRecordStatus(Integer recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getUpdateBy() {
        return this.updateBy;
    }

    public Cabang updateBy(String updateBy) {
        this.setUpdateBy(updateBy);
        return this;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public ZonedDateTime getUpdateOn() {
        return this.updateOn;
    }

    public Cabang updateOn(ZonedDateTime updateOn) {
        this.setUpdateOn(updateOn);
        return this;
    }

    public void setUpdateOn(ZonedDateTime updateOn) {
        this.updateOn = updateOn;
    }

    public LocalDate getStartTanggal() {
        return this.startTanggal;
    }

    public Cabang startDate(LocalDate startTanggal) {
        this.setStartTanggal(startTanggal);
        return this;
    }

    public void setStartTanggal(LocalDate startTanggal) {
        this.startTanggal = startTanggal;
    }

    public Leasing getLeasing() {
        return this.leasing;
    }

    public void setLeasing(Leasing leasing) {
        this.leasing = leasing;
    }

    public Cabang leasing(Leasing leasing) {
        this.setLeasing(leasing);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Cabang)) {
            return false;
        }
        return getId() != null && getId().equals(((Cabang) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Cabang{" +
            "id=" + getId() +
            ", kode='" + getKode() + "'" +
            ", nama='" + getNama() + "'" +
            ", alamat='" + getAlamat() + "'" +
            ", kota='" + getKota() + "'" +
            ", provinsi='" + getProvinsi() + "'" +
            ", pengadilanNegeri='" + getPengadilanNegeri() + "'" +
            ", noKontak='" + getNoKontak() + "'" +
            ", recordStatus=" + getRecordStatus() +
            ", updateBy='" + getUpdateBy() + "'" +
            ", updateOn='" + getUpdateOn() + "'" +
            ", startTanggal='" + getStartTanggal() + "'" +
            "}";
    }
}
