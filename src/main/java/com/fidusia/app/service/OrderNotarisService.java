package com.fidusia.app.service;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fidusia.app.domain.OrderData;
import com.fidusia.app.domain.Ppk;
import com.fidusia.app.domain.Saksi;
import com.fidusia.app.domain.SkSubstitusi;
import com.fidusia.app.domain.util.AktaStatus;
import com.fidusia.app.domain.util.OrderStatus;
import com.fidusia.app.domain.util.SaksiStatus;
import com.fidusia.app.repository.AktaRepository;
import com.fidusia.app.repository.OrderDataRepository;
import com.fidusia.app.repository.PpkRepository;
import com.fidusia.app.repository.SaksiRepository;
import com.fidusia.app.repository.SkSubtitusiRepository;
import com.fidusia.app.util.DateUtils;
import com.fidusia.app.domain.Akta;
import com.fidusia.app.domain.Notaris;

@Service
public class OrderNotarisService {
	
    private final Logger log = LoggerFactory.getLogger(AhuPendaftaranService.class);
	
	private final OrderDataRepository orderDataRepository;
	   
	   private final PpkRepository ppkRepository;
	   
	   private final AktaRepository aktaRepository;
	   
	   private final SaksiRepository saksiRepository;
	   
	   private final SkSubtitusiRepository skSubtitusiRepository;
	
	public OrderNotarisService(SkSubtitusiRepository skSubtitusiRepository, SaksiRepository saksiRepository, PpkRepository ppkRepository, OrderDataRepository orderDataRepository, AktaRepository aktaRepository) {
		this.orderDataRepository = orderDataRepository;
		this.ppkRepository = ppkRepository;
		this.aktaRepository = aktaRepository;
		this.saksiRepository = saksiRepository;
		this.skSubtitusiRepository = skSubtitusiRepository;
	}
	
	public String createOrder(Integer jmlOrder, Notaris notaris, LocalDate tglOrder, LocalDate tglAkta, String startNomor, String pukulAwal) {
		log.debug("create order notaris {} tglOrder {} sejumlah {}", notaris.getNama(), tglOrder, jmlOrder);
		
		List<OrderData> listOrder = orderDataRepository.findByStatus(OrderStatus.NEW.value());
		List<Saksi> saksiList = saksiRepository.findByRecordStatusAndNotaris(SaksiStatus.ACTIVE.value(), notaris);
		Saksi saksiOne = saksiList.get(0);
		Saksi saksiTwo = saksiList.get(1);
		Integer nomor = Integer.parseInt(startNomor);
		Integer loop = 1;
		SkSubstitusi  skSubstitusi = skSubtitusiRepository.findTopByOrderByTanggalDesc();
		ZonedDateTime pkAwal = DateUtils.parseTime(tglAkta, pukulAwal);
    	
    	for (OrderData orderData : listOrder) {
    		Ppk ppk = ppkRepository.findByNomor(orderData.getPpkNomor());
//    		format pukulAwal "08:00";
    		ZonedDateTime pkAkhir = pkAwal.plusMinutes(notaris.getSelisihPukul());
    		
    		Akta akta = new Akta();
    			akta.setCabang(orderData.getCabang());
    			akta.setNotaris(notaris);
    			akta.setOrderType(0);
    			akta.setNomor(nomor.toString());
    			akta.setKode(notaris.getId()+ nomor.toString());
    			akta.setPukulAwal(pkAwal);
    			akta.setPukulAkhir(pkAkhir);
    			akta.setPpk(ppk);
    			akta.setUpdateBy("system");
    			akta.setUpdateOn(ZonedDateTime.now());
    			akta.setPenerimaKuasa(notaris.getPenerimaKuasa());
    			akta.setSkSubstitusi(skSubstitusi);
    			akta.setSaksiOne(saksiOne);
    			akta.setSaksiTwo(saksiTwo);
    			akta.setStatus(AktaStatus.ORDERED.value());
    			akta.setTglOrder(tglOrder);
    			akta.setTanggal(tglAkta);
    		
    			aktaRepository.save(akta);
    			orderData.setStatus(2);
    			orderDataRepository.save(orderData);
    			
    			pkAwal = pkAwal.plusMinutes(notaris.getSelisihPukul());
    			nomor++;
    			if (loop == jmlOrder) return "complete";
    			loop++;
		}
    	
		return null;
	}
	
	public String createOrder(Notaris notaris, LocalDate tglOrder) {
		log.debug("create order notaris {} tglOrder {} ", notaris.getNama(), tglOrder);
		
		List<OrderData> listOrder = orderDataRepository.findByStatusOrderByAktaNoAsc(OrderStatus.NEW.value());
		List<Saksi> saksiList = saksiRepository.findByRecordStatusAndNotaris(SaksiStatus.ACTIVE.value(), notaris);
		Saksi saksiOne = saksiList.get(0);
		Saksi saksiTwo = saksiList.get(1);
		SkSubstitusi  skSubstitusi = skSubtitusiRepository.findTopByOrderByTanggalDesc();
    	
    	for (OrderData orderData : listOrder) {
    		Ppk ppk = ppkRepository.findByNomor(orderData.getPpkNomor());
    		ZonedDateTime pkAwal = orderData.getAktaDate();
    		ZonedDateTime pkAkhir = pkAwal.plusMinutes(notaris.getSelisihPukul());
    		
    		Akta akta = new Akta();
    			akta.setCabang(orderData.getCabang());
    			akta.setNotaris(notaris);
    			akta.setOrderType(0);
    			akta.setNomor(orderData.getAktaNo());
    			akta.setKode(notaris.getId() + "-"+ orderData.getAktaNo());
    			akta.setPukulAwal(pkAwal);
    			akta.setPukulAkhir(pkAkhir);
    			akta.setPpk(ppk);
    			akta.setUpdateBy("system");
    			akta.setUpdateOn(ZonedDateTime.now());
    			akta.setPenerimaKuasa(notaris.getPenerimaKuasa());
    			akta.setSkSubstitusi(skSubstitusi);
    			akta.setSaksiOne(saksiOne);
    			akta.setSaksiTwo(saksiTwo);
    			akta.setStatus(AktaStatus.ORDERED.value());
    			akta.setTglOrder(tglOrder);
    			akta.setTanggal(orderData.getAktaDate().toLocalDate());
    		
    			aktaRepository.save(akta);
    			orderData.setStatus(2);
    			orderDataRepository.save(orderData);
    			
    			pkAwal = pkAkhir;
		}
    	
		return null;
	}
}
