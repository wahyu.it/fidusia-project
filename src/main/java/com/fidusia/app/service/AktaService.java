package com.fidusia.app.service;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.fidusia.app.domain.Akta;
import com.fidusia.app.domain.Cabang;
import com.fidusia.app.domain.Notaris;
import com.fidusia.app.domain.OrderData;
import com.fidusia.app.domain.OrderNotaris;
import com.fidusia.app.domain.PenerimaKuasa;
import com.fidusia.app.domain.Ppk;
import com.fidusia.app.domain.Saksi;
import com.fidusia.app.domain.util.AktaStatus;
import com.fidusia.app.domain.util.RecordStatus;
import com.fidusia.app.repository.AktaRepository;
import com.fidusia.app.repository.OrderNotarisRepository;
import com.fidusia.app.repository.PpkRepository;
import com.fidusia.app.repository.SaksiRepository;
import com.fidusia.app.service.AktaService;

@Service
public class AktaService {

    private final Logger log = LoggerFactory.getLogger(AktaService.class);
    
    private final OrderNotarisRepository orderNotarisRepository;
    
    private final PpkRepository ppkRepository;
    
    private final SaksiRepository saksiRepository;
    
    private final AktaRepository aktaRepository;
    
    public AktaService(SaksiRepository saksiRepository, AktaRepository aktaRepository, OrderNotarisRepository orderNotarisRepository, PpkRepository ppkRepository) {
    	this.orderNotarisRepository = orderNotarisRepository;
    	this.ppkRepository = ppkRepository;
    	this.aktaRepository = aktaRepository;
    	this.saksiRepository = saksiRepository;
    }
    
	@Async
    public void generateAktaAsync(Notaris notaris, LocalDate tglOrder, LocalDate tanggalAkta , int startNomor, ZonedDateTime jamKerjaAwal) {
        log.debug("Request to generateAktaAsync notaris:{} - startNomor:{} - startPukul:{}",notaris.getNama() ,startNomor, jamKerjaAwal);
        
        List<OrderNotaris> orderNotarises = orderNotarisRepository.findByTanggalAndNotaris(tglOrder, notaris);
        ZonedDateTime jamRestAwal;
        ZonedDateTime jamRestAkhir;
        ZonedDateTime jamKerjaAkhir;
        long nomor = startNomor;
        int tmpJmlAkta = 0;

        if(tanggalAkta.getDayOfWeek().getValue() == DayOfWeek.SATURDAY.getValue()) {
            jamRestAwal = jamKerjaAwal.withHour(notaris.getJamRestAwalTwo().getHour()).withMinute(notaris.getJamRestAwalTwo().getMinute());
            jamRestAkhir = jamKerjaAwal.withHour(notaris.getJamRestAkhirTwo().getHour()).withMinute(notaris.getJamRestAkhirTwo().getMinute());
            jamKerjaAkhir = jamKerjaAwal.withHour(notaris.getJamKerjaAkhirTwo().getHour()).withMinute(notaris.getJamKerjaAkhirTwo().getMinute());
        } else {
            jamRestAwal = jamKerjaAwal.withHour(notaris.getJamRestAwalOne().getHour()).withMinute(notaris.getJamRestAwalOne().getMinute());
            jamRestAkhir = jamKerjaAwal.withHour(notaris.getJamRestAkhirOne().getHour()).withMinute(notaris.getJamRestAkhirOne().getMinute());
            jamKerjaAkhir = jamKerjaAwal.withHour(notaris.getJamKerjaAkhirOne().getHour()).withMinute(notaris.getJamKerjaAkhirOne().getMinute());
        }
        
        for (OrderNotaris orderNotaris : orderNotarises) {
        	OrderData orderData = orderNotaris.getOrderData();
    		PenerimaKuasa penerimaKuasa = notaris.getPenerimaKuasa();
    		Cabang cabang = orderData.getCabang();
        	String ppkNomor = orderData.getPpkNomor();

            ZonedDateTime pukulAwal = jamKerjaAwal;
            tmpJmlAkta++;
        	
        	if((pukulAwal.isAfter(jamRestAwal.minusMinutes(notaris.getSelisihPukul())) && pukulAwal.isBefore(jamRestAkhir))) {
                pukulAwal = jamRestAkhir;
            }

            ZonedDateTime pukulAkhir = pukulAwal.plusMinutes(notaris.getSelisihPukul());

        	
        	Ppk ppk = ppkRepository.findByNomor(ppkNomor);
            String kodePrefix = tanggalAkta.format(DateTimeFormatter.ofPattern("yyMM")) + "." + notaris.getId();
            String noAkta = String.format("%02d", nomor);
			Akta akta = new Akta()
					.tglOrder(tglOrder)
					.kode(kodePrefix + "." + noAkta)
					.nomor(noAkta)
					.tanggal(tanggalAkta)
					.pukulAwal(pukulAwal)
					.pukulAkhir(pukulAkhir)
					.status(AktaStatus.NEW.value())
					.ppk(ppk)
					.skSubstitusi(orderData.getSkSubstitusi())
					.updateOn(ZonedDateTime.now())
					.updateBy("" + notaris.getId())
					.orderType(orderData.getType());
			
			List<Saksi> saksis = saksiRepository.findTop2ByNotarisAndRecordStatus(notaris, RecordStatus.ACTIVE.value());
			if(saksis.size() > 0 && saksis.get(0) != null) {
				akta.setSaksiOne(saksis.get(0));

				if(saksis.get(0).getNotaris() != null) {
					notaris = saksis.get(0).getNotaris();
				}
			}
			if(saksis.size() > 1 && saksis.get(1) != null) {
				akta.setSaksiTwo(saksis.get(1));

				if(saksis.get(1).getNotaris() != null && notaris.getNama() == null) {
					notaris = saksis.get(1).getNotaris();
				}
			}
			akta.setCabang(cabang);
			akta.setNotaris(notaris);
			akta.setPenerimaKuasa(penerimaKuasa);

			akta = aktaRepository.save(akta);
			
		}
		
	}

}
