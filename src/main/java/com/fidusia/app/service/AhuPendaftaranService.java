package com.fidusia.app.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.net.URLEncoder;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.Loader;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.fidusia.app.config.ApplicationProperties;
import com.fidusia.app.domain.Akta;
import com.fidusia.app.domain.Badan;
import com.fidusia.app.domain.Cabang;
import com.fidusia.app.domain.Kendaraan;
import com.fidusia.app.domain.Leasing;
import com.fidusia.app.domain.Notaris;
import com.fidusia.app.domain.Orang;
import com.fidusia.app.domain.PenerimaKuasa;
import com.fidusia.app.domain.Ppk;
import com.fidusia.app.domain.Referensi;
import com.fidusia.app.domain.Sertifikat;
import com.fidusia.app.domain.util.NasabahJenis;
import com.fidusia.app.domain.util.SertifikatStatus;
import com.fidusia.app.repository.KendaraanRepository;
import com.fidusia.app.repository.ReferensiRepository;
import com.fidusia.app.repository.SertifikatRepository;
import com.fidusia.app.service.dto.TransaksiAhuDTO;
import com.fidusia.app.util.DateUtils;
import com.fidusia.app.util.ExceptionUtils;

@Service
public class AhuPendaftaranService {
	
    private final Logger log = LoggerFactory.getLogger(AhuPendaftaranService.class);
	
    private final static String konteRoyaUrl = "https://fidusia.ahu.go.id/roya.html";
	
	private final static DateTimeFormatter dateFormat =  DateTimeFormatter.ofPattern("dd-MM-yyyy");
	
    private final static String registrasiUrl = "https://fidusia.ahu.go.id/app/add_act.php";
	
    private final static String voucherUrl = "https://fidusia.ahu.go.id/app/form_cetak.php";
	
    private final static String daftarTransaksiUrl = "https://fidusia.ahu.go.id/daftar.html";
	
    private final static String sertifikatUrl = "https://fidusia.ahu.go.id/app/form_cetak_sertifikat_pdf.php";
	
    private final static String readDaftarUrl = "https://fidusia.ahu.go.id/app/nextPageDaftar.php";
    
    private final static String loadBakumUrl = "https://fidusia.ahu.go.id/app/load_bakum.php";
    
    private final static int BUFFER_SIZE = 4096;
    private final static int connectTimeout = 60000;
    private final static int readTimeout = 60000;
    private final static String host = "fidusia.ahu.go.id";
    private final static String origin = "https://fidusia.ahu.go.id";
    private final static String refererPendaftaran = "https://fidusia.ahu.go.id/pendaftaran.html";
    private final static String refererVoucher = "https://fidusia.ahu.go.id/daftar.html/#";
    private final static String refererSertifikat = "https://fidusia.ahu.go.id/daftar.html/#";
    private final static String refererReadDaftar = "https://fidusia.ahu.go.id/daftar.html";
	private final static String AHU_REG_TAGIHAN = "TAGIHAN : Rp";
	private final static String KODE_VOUCHER = "KODE VOUCHER :";
	private final static String NTB = "NOMOR INVOICE PAYMENT :";
	private final static String NTPN = "NTPN :";
    private final static String refererKonte = "https://fidusia.ahu.go.id/roya.html";
//	private final static String referePaymenMandiri = "https://fidusia.ahu.go.id/app/add_act_payment.php";
	private final static String referePaymenMandiri = "https://fidusia.ahu.go.id/app/add_act_payment.php";
    
	private final ReferensiRepository referensiRepository;
    
	private final SertifikatRepository sertifikatRepository;
	
	private final ReferensiService referensiService;
	
	private final KendaraanRepository kendaraanRepository;
    
    private final ApplicationProperties applicationProperties;
	
	public AhuPendaftaranService (
			KendaraanRepository kendaraanRepository, 
			ReferensiRepository referensiRepository, 
			SertifikatRepository sertifikatRepository, 
			ReferensiService referensiService,
			ApplicationProperties applicationProperties
			) {
		this.referensiRepository = referensiRepository;
		this.sertifikatRepository = sertifikatRepository;
		this.referensiService = referensiService;
		this.kendaraanRepository = kendaraanRepository;
		this.applicationProperties = applicationProperties;
	}
	
	
	public String generateRoyaKonte(String sessionId) throws Exception {
		BufferedReader reader = null;
		
		try {
			long startTimestamp = System.currentTimeMillis();

			SSLContext ssl_ctx = SSLContext.getInstance("TLS");
			TrustManager[] trust_mgr = get_trust_mgr();
			ssl_ctx.init(null, trust_mgr, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());

			URL url = new URL(konteRoyaUrl);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setConnectTimeout(connectTimeout);
			conn.setReadTimeout(readTimeout);
		    
			conn.setHostnameVerifier(new HostnameVerifier() {
				public boolean verify(String host, SSLSession sess) {
					if (host.equals("localhost"))
						return true;
					else
						return false;
				}
			});

			conn.setRequestProperty("Accept", "*/*");
			conn.setRequestProperty("Cookie", "PHPSESSID=" + sessionId);
			conn.setRequestProperty("Host", host);
			conn.setRequestProperty("Origin", origin);
			conn.setRequestProperty("Referer", refererKonte);
			conn.setRequestProperty("sec-ch-ua", "\"Chromium\";v=\"128\", \"Not;A=Brand\";v=\"24\", \"Google Chrome\";v=\"128\"");
			conn.setRequestProperty("sec-ch-ua-mobile", "?0");
			conn.setRequestProperty("sec-ch-ua-platform", "\"Windows\"");
			conn.setRequestProperty("sec-fetch-dest", "document");
			conn.setRequestProperty("sec-fetch-mode", "navigate");
			conn.setRequestProperty("sec-fetch-site", "same-origin");
			conn.setRequestProperty("sec-fetch-user", "?1");
			conn.setRequestProperty("upgrade-insecure-requests", "1");
			conn.setRequestProperty("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/128.0.0.0 Safari/537.36");

			conn.setDoOutput(true);
		    
		    int responseCode = conn.getResponseCode();
		    String responseMessage = conn.getResponseMessage();
			long endTimestamp = System.currentTimeMillis();
		    
		    if(responseCode == HttpURLConnection.HTTP_OK) {
			    reader = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
		        String line;
		        StringBuilder sb2 = new StringBuilder();
		        while ((line = reader.readLine()) != null) { 
		        	sb2.append(line);
		        }
		        
		        reader.close();
		        
		        String html = sb2.toString();
				Document document = Jsoup.parse(html);
				Element konte = document.getElementsByAttributeValue("name", "cekkonte").first();
				if(konte != null) {
				    reader.close();

					log.debug("generate konte SUCCESS in {}ms", (endTimestamp-startTimestamp));
					return konte.val();
				}
		    } else {
				log.debug("generate konte FAILED {}-{} {}ms", responseCode, responseMessage, (endTimestamp-startTimestamp));
			}
		    
		    reader.close();
			
			return null;
		} catch (Exception e) {
        	log.error("Error on generate konte ", e);

			if(reader != null)
				try {
					reader.close();
				} catch (IOException e2) { }
			
			throw e;
		}
	}

	public Sertifikat register(Sertifikat sertifikat, String sessionId) {
		Akta akta = sertifikat.getAkta();
		referensiService.reload();
		Ppk ppk = akta.getPpk();
		Integer nsbJenis = ppk.getNsbJenis();
		
		OutputStreamWriter writer = null;
		BufferedReader reader = null;
		String postData = null;
		
		try {
			if (nsbJenis == NasabahJenis.PERORANGAN.value()) {
				String prov = referensiService.valueOf("PROVINSI", ppk.getOrang().getProvinsiPemegangBpkb());
				String kota = referensiService.valueOf("KOTA", ppk.getOrang().getKotaPemegangBpkb());
				if (prov == null) {
					log.error(ppk.getNomor() + " Provinsi tidak ada di tabel referensi");
					return null;
				}
				
				if (kota == null) {
					log.error(ppk.getNomor() + " Kota tidak ada di tabel referensi");
					return null;
				}
			} else {
				String prov = referensiService.valueOf("PROVINSI", ppk.getBadan().getProvinsiPemegangBpkb());
				String kota = referensiService.valueOf("KOTA", ppk.getBadan().getKotaPemegangBpkb());
				if (prov == null) {
					log.error(ppk.getNomor() + " Provinsi tidak ada di tabel referensi");
					return null;
				}
				
				if (kota == null) {
					log.error(ppk.getNomor() + " Kota tidak ada di tabel referensi");
					return null;
				}
			}
			
			postData = createRequest2(akta, sessionId);
//			log.error("Register payload {}", postData);
		} catch (Exception e) {
			log.error("Error on mapping register payload", e);
		}
		
		try {
			long startTimestamp = System.currentTimeMillis();

			SSLContext ssl_ctx = SSLContext.getInstance("TLS");
			TrustManager[] trust_mgr = get_trust_mgr();
			ssl_ctx.init(null, trust_mgr, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());

			URL url = new URL(registrasiUrl);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setConnectTimeout(connectTimeout);
			conn.setReadTimeout(readTimeout);
		    
			conn.setHostnameVerifier(new HostnameVerifier() {
				public boolean verify(String host, SSLSession sess) {
					if (host.equals("localhost"))
						return true;
					else
						return false;
				}
			});

			conn.setRequestProperty("Accept", "*/*");
			conn.setRequestProperty("Accept-Encoding", "gzip, deflate, br");
			conn.setRequestProperty("Accept-Language", "en-US,en;q=0.9");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			conn.setRequestProperty("Cookie", "PHPSESSID=" + sessionId);
			conn.setRequestProperty("Host", host);
			conn.setRequestProperty("Origin", origin);
			conn.setRequestProperty("Referer", refererPendaftaran);
			conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
			conn.setRequestProperty("sec-ch-ua", "\"Chromium\";v=\"128\", \"Not;A=Brand\";v=\"24\", \"Google Chrome\";v=\"128\"");
			conn.setRequestProperty("sec-ch-ua-mobile", "?0");
			conn.setRequestProperty("sec-ch-ua-platform", "\"Windows\"");
			conn.setRequestProperty("sec-fetch-dest", "document");
			conn.setRequestProperty("sec-fetch-mode", "navigate");
			conn.setRequestProperty("sec-fetch-site", "same-origin");
			conn.setRequestProperty("sec-fetch-user", "?1");
			conn.setRequestProperty("upgrade-insecure-requests", "1");
			conn.setRequestProperty("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/128.0.0.0 Safari/537.36");

			
	        conn.setDoInput(true);
			conn.setDoOutput(true);
		    writer = new OutputStreamWriter(conn.getOutputStream());

		    writer.write(postData);
		    writer.flush();
		    
		    int responseCode = conn.getResponseCode();
		    String responseMessage = conn.getResponseMessage();
			long endTimestamp = System.currentTimeMillis();
		    
		    if(responseCode == HttpURLConnection.HTTP_OK) {
			    reader = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
		        String line;
		        StringBuilder sb = new StringBuilder();
		        while ((line = reader.readLine()) != null) { 
		        	sb.append(line);
		        }
		        
		        reader.close();
		        
		        String responsePayload = sb.toString();
		        
				String[] respTokens = responsePayload.split("\\^");
				if("1".equals(respTokens[0])) {
					sertifikat.setRegistrationId(respTokens[1]);
					log.debug("register SUCCESS {} {} in {}ms", akta.getPpk().getNomor(), respTokens[1], (endTimestamp-startTimestamp));
		        } else {
					log.debug("register FAILED {} {} {}ms", akta.getPpk().getNomor(), responsePayload, (endTimestamp-startTimestamp));
					log.debug(postData);
				}
		    } else {
				log.debug("register FAILED {} {}-{} {}ms", akta.getPpk().getNomor(), responseCode, responseMessage, (endTimestamp-startTimestamp));
				log.debug(postData);
			}
		} catch (Exception e) {
        	log.error("Error on register " + akta.getPpk().getNomor(), e);
			log.debug(postData);
		} finally {
			if(writer != null)
				try {
					writer.close();
				} catch (Exception e) { }

			if(reader != null)
				try {
					reader.close();
				} catch (IOException e) { }
		}
		
		return sertifikat;
	}
	
	public Sertifikat getSertifikat(String ppkNomor, LocalDate orderDate, String registrationId, String sessionId) {
		Sertifikat result = sertifikatRepository.findByAktaPpkNomor(ppkNomor).get(0);

		FileOutputStream outputStream = null;
		InputStream inputStream = null;
		
		String sertifikatDirectory = applicationProperties.getDomain().getBaseDirectory() + applicationProperties.getDirectory().getSertifikat();
		
		try {
			long startTimestamp = System.currentTimeMillis();

			SSLContext ssl_ctx = SSLContext.getInstance("TLS");
			TrustManager[] trust_mgr = get_trust_mgr();
			ssl_ctx.init(null, trust_mgr, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());
			
			String stringURL = sertifikatUrl + "?id=" + registrationId;
			URL url = new URL(stringURL);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setConnectTimeout(connectTimeout);
			conn.setReadTimeout(readTimeout);
		    
			conn.setHostnameVerifier(new HostnameVerifier() {
				public boolean verify(String host, SSLSession sess) {
					if (host.equals("localhost"))
						return true;
					else
						return false;
				}
			});

			conn.setRequestProperty("Accept", "*/*");
			conn.setRequestProperty("Content-Type", "application/pdf");
			conn.setRequestProperty("Cookie", "PHPSESSID=" + sessionId);
			conn.setRequestProperty("Host", host);
			conn.setRequestProperty("Origin", origin);
			conn.setRequestProperty("Referer", refererSertifikat);
			conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
			conn.setRequestProperty("sec-ch-ua", "\"Chromium\";v=\"128\", \"Not;A=Brand\";v=\"24\", \"Google Chrome\";v=\"128\"");
			conn.setRequestProperty("sec-ch-ua-mobile", "?0");
			conn.setRequestProperty("sec-ch-ua-platform", "\"Windows\"");
			conn.setRequestProperty("sec-fetch-dest", "document");
			conn.setRequestProperty("sec-fetch-mode", "navigate");
			conn.setRequestProperty("sec-fetch-site", "same-origin");
			conn.setRequestProperty("sec-fetch-user", "?1");
			conn.setRequestProperty("upgrade-insecure-requests", "1");
			conn.setRequestProperty("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/128.0.0.0 Safari/537.36");

		    
		    int responseCode = conn.getResponseCode();
		    String responseMessage = conn.getResponseMessage();
			
			if (responseCode == HttpURLConnection.HTTP_OK) {
	            inputStream = conn.getInputStream();

	            File savedFileDir = new File(sertifikatDirectory + orderDate);
	            if(!savedFileDir.exists())
	            	savedFileDir.mkdirs();
	            
	            String saveFilePath = savedFileDir.getPath() + "/" + ppkNomor + "-sertifikat.pdf";
	            File downloadFile = new File(saveFilePath);
	            if(downloadFile.exists()) {
	            	try {
	            		downloadFile.delete();
	            	} catch(Exception ex1) {}
	            }
	            
	            outputStream = new FileOutputStream(saveFilePath);
	 
	            int bytesRead = -1;
	            byte[] buffer = new byte[BUFFER_SIZE];
	            boolean read = false;
	            while ((bytesRead = inputStream.read(buffer)) != -1) {
	            	if(!read) read = true;
	            	outputStream.write(buffer, 0, bytesRead);
	            }
	            
				if(read) {
					outputStream.close();
					inputStream.close();

					String[] lines = readPdf(new File(saveFilePath));
					
					String noSertifikat = null;
					
					if(lines != null) {
						if(lines.length > 6) {
							String[] noSertifikatTokens = lines[9].trim().split(" ");
							noSertifikat = noSertifikatTokens.length >= 4 ? noSertifikatTokens[2] + " " + noSertifikatTokens[3] + " " + noSertifikatTokens[4] : null;
							result.setNoSertifikat(noSertifikat);

							String[] tglSertifikatTokens = lines[10].trim().split(" ");
							String tglSertifikat = tglSertifikatTokens.length > 6 ? tglSertifikatTokens[2] + " " + tglSertifikatTokens[6] : null;
							try {
								Date time = DateUtils.parse(tglSertifikat, "dd-MM-yyyy HH:mm:ss");
								result.setTglSertifikat(ZonedDateTime.ofInstant(time.toInstant(), ZoneId.systemDefault()));
								
							} catch (Exception e) {}

							result.setUpdateBy("AhuPendaftaranService.dowloadSertifikat");
							result.setUpdateOn(ZonedDateTime.now());
							result.setStatus(SertifikatStatus.DONE.value());
							sertifikatRepository.save(result);
							log.debug("getSertifikat SUCCESS {} {} {} -> {}, in {} ms.", ppkNomor, orderDate, registrationId, noSertifikat, System.currentTimeMillis()-startTimestamp);
						}
					}

					if(noSertifikat == null)
						log.debug("getSertifikat FAILED {} {} {} -> Content is empty or invalid, in {} ms.", ppkNomor, orderDate, registrationId, System.currentTimeMillis()-startTimestamp);
				} else {
					log.debug("getSertifikat FAILED {} {} {} -> {}, in {} ms.", ppkNomor, orderDate, registrationId, responseCode + " " + responseMessage, System.currentTimeMillis()-startTimestamp);
				}
	 
	            outputStream.close();
	        } else {
				log.debug("getSertifikat FAILED {} {} {} -> {}, in {} ms.", ppkNomor, orderDate, registrationId, responseCode + " " + responseMessage, System.currentTimeMillis()-startTimestamp);
	        }
		} catch (Exception e) {
        	log.error("getSertifikat ERROR " + ppkNomor + " " + orderDate + " " + ExceptionUtils.getMessage(e));
        	e.printStackTrace();
		} finally {
			if(outputStream != null)
				try {
					outputStream.close();
				} catch (Exception e) { }

			if(inputStream != null)
				try {
					inputStream.close();
				} catch (IOException e) { }
		}
		
		return result;
	}


	public String getVoucherFile(String sessionId, String registrasiId, String ppkNomor) {
		
		String buktiKocerDirectory = applicationProperties.getDomain().getBaseDirectory() + applicationProperties.getDirectory().getBuktiLunas();
		Sertifikat sertifikat = sertifikatRepository.findByAktaPpkNomor(ppkNomor).get(0);
		
		String voucherDirectory = buktiKocerDirectory + "/tagihan/";
		File savedFileDir = new File(voucherDirectory + sertifikat.getTglOrder());
		String saveFilePath = savedFileDir.getPath() + "/" + ppkNomor + "-voucher.pdf";
		File saveFile = new File(saveFilePath);
		
		String result = null;

		FileOutputStream outputStream = null;
		InputStream inputStream = null;
		try {
			long startTimestamp = System.currentTimeMillis();

			SSLContext ssl_ctx = SSLContext.getInstance("TLS");
			TrustManager[] trust_mgr = get_trust_mgr();
			ssl_ctx.init(null, trust_mgr, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());

			String stringURL = voucherUrl + "?id=" + registrasiId;
			URL url = new URL(stringURL);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setConnectTimeout(connectTimeout);
			conn.setReadTimeout(readTimeout);

			conn.setHostnameVerifier(new HostnameVerifier() {
				public boolean verify(String host, SSLSession sess) {
					if (host.equals("localhost"))
						return true;
					else
						return false;
				}
			});

			conn.setRequestProperty("Accept", "*/*");
			conn.setRequestProperty("Content-Type", "application/pdf");
			conn.setRequestProperty("Cookie", "PHPSESSID=" + sessionId);
			conn.setRequestProperty("Host", host);
			conn.setRequestProperty("Origin", origin);
			conn.setRequestProperty("Referer", refererVoucher);
			conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
			conn.setRequestProperty("sec-ch-ua", "\"Chromium\";v=\"128\", \"Not;A=Brand\";v=\"24\", \"Google Chrome\";v=\"128\"");
			conn.setRequestProperty("sec-ch-ua-mobile", "?0");
			conn.setRequestProperty("sec-ch-ua-platform", "\"Windows\"");
			conn.setRequestProperty("sec-fetch-dest", "document");
			conn.setRequestProperty("sec-fetch-mode", "navigate");
			conn.setRequestProperty("sec-fetch-site", "same-origin");
			conn.setRequestProperty("sec-fetch-user", "?1");
			conn.setRequestProperty("upgrade-insecure-requests", "1");
			conn.setRequestProperty("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/128.0.0.0 Safari/537.36");


			int responseCode = conn.getResponseCode();
			String responseMessage = conn.getResponseMessage();

			if (responseCode == HttpURLConnection.HTTP_OK) {
				inputStream = conn.getInputStream();
				if(!savedFileDir.exists()) savedFileDir.mkdirs();
				if(saveFile.exists()) FileUtils.deleteQuietly(saveFile);

				outputStream = new FileOutputStream(saveFile);

				int bytesRead = -1;
				byte[] buffer = new byte[BUFFER_SIZE];
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outputStream.write(buffer, 0, bytesRead);
				}

				outputStream.close();

				inputStream.close();

				String pdfPayload = null;
				String[] lines = null;

				PDDocument document = Loader.loadPDF(new File(saveFilePath));
				PDFTextStripperByArea stripper = new PDFTextStripperByArea();
				stripper.setSortByPosition(true);
				PDFTextStripper tStripper = new PDFTextStripper();
				pdfPayload = tStripper.getText(document);
				document.close();

				lines = pdfPayload.split("\\r?\\n");

					if(pdfPayload != null && pdfPayload.length() > 0) {
						for (String line : lines) {
							if(line.startsWith(AHU_REG_TAGIHAN)) {
								result = line.substring(AHU_REG_TAGIHAN.length()).trim().replaceAll("\\.", "");
							}
						}
					}
					sertifikat.setBiayaPnbp(Long.parseLong(result));
					
					sertifikatRepository.save(sertifikat);
				
				log.info("getVoucherFile SUCCESS {}, result {} in {}ms.", ppkNomor, result, System.currentTimeMillis()-startTimestamp);
			} else {
				log.warn("getVoucherFile FAILED {}, result {}-{} in {}ms.", ppkNomor, responseCode, responseMessage, System.currentTimeMillis()-startTimestamp);
			}
		} catch (Exception e) {
			log.error("Error on getVoucherFile " + ppkNomor, e);
			e.printStackTrace();
		} finally {
			if(outputStream != null)
				try {
					outputStream.close();
				} catch (Exception e) { }

			if(inputStream != null)
				try {
					inputStream.close();
				} catch (IOException e) { }
		}

		return result;
	}

	public String getBuktiLunasFile(String sessionId, String registrasiId, String ppkNomor) {
		
		String buktiKocerDirectory = applicationProperties.getDomain().getBaseDirectory() + applicationProperties.getDirectory().getBuktiLunas();
		Sertifikat sertifikat = sertifikatRepository.findByAktaPpkNomor(ppkNomor).get(0);
		
		String voucherDirectory = buktiKocerDirectory + "/buktilunas/";
		File savedFileDir = new File(voucherDirectory + sertifikat.getTglOrder());
		String saveFilePath = savedFileDir.getPath() + "/" + ppkNomor + "-buktilunas.pdf";
		File saveFile = new File(saveFilePath);

		FileOutputStream outputStream = null;
		InputStream inputStream = null;
		try {
			long startTimestamp = System.currentTimeMillis();

			SSLContext ssl_ctx = SSLContext.getInstance("TLS");
			TrustManager[] trust_mgr = get_trust_mgr();
			ssl_ctx.init(null, trust_mgr, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());

			String stringURL = voucherUrl + "?id=" + registrasiId + "&bukti=cetak";
			URL url = new URL(stringURL);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setConnectTimeout(connectTimeout);
			conn.setReadTimeout(readTimeout);

			conn.setHostnameVerifier(new HostnameVerifier() {
				public boolean verify(String host, SSLSession sess) {
					if (host.equals("localhost"))
						return true;
					else
						return false;
				}
			});

			conn.setRequestProperty("Accept", "*/*");
			conn.setRequestProperty("Content-Type", "application/pdf");
			conn.setRequestProperty("Cookie", "PHPSESSID=" + sessionId);
			conn.setRequestProperty("Host", host);
			conn.setRequestProperty("Origin", origin);
			conn.setRequestProperty("Referer", refererVoucher);
			conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
			conn.setRequestProperty("sec-ch-ua", "\"Chromium\";v=\"128\", \"Not;A=Brand\";v=\"24\", \"Google Chrome\";v=\"128\"");
			conn.setRequestProperty("sec-ch-ua-mobile", "?0");
			conn.setRequestProperty("sec-ch-ua-platform", "\"Windows\"");
			conn.setRequestProperty("sec-fetch-dest", "document");
			conn.setRequestProperty("sec-fetch-mode", "navigate");
			conn.setRequestProperty("sec-fetch-site", "same-origin");
			conn.setRequestProperty("sec-fetch-user", "?1");
			conn.setRequestProperty("upgrade-insecure-requests", "1");
			conn.setRequestProperty("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/128.0.0.0 Safari/537.36");


			int responseCode = conn.getResponseCode();
			String responseMessage = conn.getResponseMessage();

			if (responseCode == HttpURLConnection.HTTP_OK) {
				inputStream = conn.getInputStream();
				if(!savedFileDir.exists()) savedFileDir.mkdirs();
				if(saveFile.exists()) FileUtils.deleteQuietly(saveFile);

				outputStream = new FileOutputStream(saveFile);

				int bytesRead = -1;
				byte[] buffer = new byte[BUFFER_SIZE];
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outputStream.write(buffer, 0, bytesRead);
				}

				outputStream.close();

				inputStream.close();

				String pdfPayload = null;
				String[] lines = null;

				PDDocument document = Loader.loadPDF(new File(saveFilePath));
				PDFTextStripperByArea stripper = new PDFTextStripperByArea();
				stripper.setSortByPosition(true);
				PDFTextStripper tStripper = new PDFTextStripper();
				pdfPayload = tStripper.getText(document);
				document.close();

				lines = pdfPayload.split("\\r?\\n");
				String kodeVoucher = null;
				String ntpn = null;
				String ntb = null;

					if(pdfPayload != null && pdfPayload.length() > 0) {
						for (String line : lines) {
							if(line.startsWith(KODE_VOUCHER)) {
								kodeVoucher = line.substring(KODE_VOUCHER.length()).trim().replaceAll("\\.", "");
							}
							
							if(line.startsWith(NTPN)) {
								ntpn = line.substring(NTPN.length()).trim().replaceAll("\\.", "");
							}
						}
					}
					
					sertifikat.setKodeVoucher(kodeVoucher);
					sertifikat.setNoReferensiBni(ntpn);
					
					sertifikatRepository.save(sertifikat);
				
				log.info("getVoucherFile SUCCESS {}, kocer {}, ntpn {} in {}ms.", ppkNomor, kodeVoucher, ntpn, System.currentTimeMillis()-startTimestamp);
			} else {
				log.warn("getVoucherFile FAILED {}, result {}-{} in {}ms.", ppkNomor, responseCode, responseMessage, System.currentTimeMillis()-startTimestamp);
			}
		} catch (Exception e) {
			log.error("Error on getVoucherFile " + ppkNomor, e);
			e.printStackTrace();
		} finally {
			if(outputStream != null)
				try {
					outputStream.close();
				} catch (Exception e) { }

			if(inputStream != null)
				try {
					inputStream.close();
				} catch (IOException e) { }
		}

		return null;
	}
	
	public String getVoucherMandiri(String sessionId, String registrasiId) throws Exception {
		String result = null;
		OutputStreamWriter writer = null;
		BufferedReader reader = null;

		String konte = generateRoyaKonte(sessionId);
		
		StringBuilder jsonObject = new StringBuilder();
		jsonObject.append("konte=").append(konte).append("&");
		jsonObject.append("no_transaksi=").append(URLEncoder.encode(registrasiId, StandardCharsets.UTF_8.toString())).append("&");
		jsonObject.append("payment_method=").append(URLEncoder.encode("MANDIRI", StandardCharsets.UTF_8.toString()));
		
        
        String postData = jsonObject.toString();
		log.debug("Post data get-token :{}",postData);
		
		try {
			long startTimestamp = System.currentTimeMillis();

			SSLContext ssl_ctx = SSLContext.getInstance("TLS");
			TrustManager[] trust_mgr = get_trust_mgr();
			ssl_ctx.init(null, trust_mgr, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());

			String stringURL = referePaymenMandiri;
			URL url = new URL(stringURL);
		    log.debug("url:" + url);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
//			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Accept", "*/*");
			conn.setRequestProperty("Connection", "keep-alive");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			conn.setRequestProperty("Content-Lenght", "91");
			conn.setRequestProperty("Cookie", "PHPSESSID=" + sessionId);
			conn.setRequestProperty("Host", host);
			conn.setRequestProperty("Origin", origin);
			conn.setRequestProperty("Referer", refererReadDaftar);
			conn.setRequestProperty("X-Requested-With", "Ajax_Request");
			conn.setRequestProperty("sec-ch-ua", "\"Chromium\";v=\"128\", \"Not;A=Brand\";v=\"24\", \"Google Chrome\";v=\"128\"");
			conn.setRequestProperty("sec-ch-ua-mobile", "?0");
			conn.setRequestProperty("sec-ch-ua-platform", "\"Windows\"");
			conn.setRequestProperty("sec-fetch-dest", "document");
			conn.setRequestProperty("sec-fetch-mode", "navigate");
			conn.setRequestProperty("sec-fetch-site", "same-origin");
			conn.setRequestProperty("sec-fetch-user", "?1");
			conn.setRequestProperty("upgrade-insecure-requests", "1");
			conn.setRequestProperty("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/128.0.0.0 Safari/537.36");

			conn.setConnectTimeout(connectTimeout);
			conn.setReadTimeout(readTimeout);
			conn.setDoInput(true);
			conn.setDoOutput(true);

			writer = new OutputStreamWriter(conn.getOutputStream());
            writer.write(postData);
		    writer.flush();
//		    writer.close();
		    
			int responseCode = conn.getResponseCode();
			String responseMessage = conn.getResponseMessage();
			long endTimestamp = System.currentTimeMillis();
		    log.debug("responseCode:" + responseCode);

			if (responseCode == HttpURLConnection.HTTP_OK) {
				reader = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
		        String line;
		        StringBuilder sb = new StringBuilder();
		        while ((line = reader.readLine()) != null) { 
		        	sb.append(line);
		        }
		        reader.close();
		        String responsePayload = sb.toString();
		        
				String[] respTokens = responsePayload.split("\\^");
				if("2".equals(respTokens[0])) {
					String[] kocer = respTokens[1].split("/");
					if (kocer.length > 5) {
						result = kocer[6];
						log.info("getKodeVoucher SUCCESS {}, result {} in {}ms.", registrasiId, result, System.currentTimeMillis()-startTimestamp);
					  } 
					} else {
					log.debug("getKodeVoucher FAILED {} {} {}ms", registrasiId, responsePayload, (endTimestamp-startTimestamp));
				}

				
			} else {
				log.warn("getKodeVoucher FAILED {}, result {}-{} in {}ms.", registrasiId, responseCode, responseMessage, System.currentTimeMillis()-startTimestamp);
			}
		} catch (Exception e) {
            log.error("Failed service Get data token Job caused by {}", e.getMessage(), e);
		} finally {
			if(writer != null)
				try {
					writer.close();
				} catch (Exception e) { }
	
			if(reader != null)
				try {
					reader.close();
				} catch (IOException e) { }
		}
    	
		return result;
	}

	public Map<String, TransaksiAhuDTO> getVoucherFromTable(String sessionId) throws Exception {
		log.debug("Request to getVoucherFromTable");

		Map<String, TransaksiAhuDTO> resultMap = null;

		BufferedReader reader = null;
		try {
			SSLContext ssl_ctx = SSLContext.getInstance("TLS");
			TrustManager[] trust_mgr = get_trust_mgr();
			ssl_ctx.init(null, trust_mgr, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());

			String stringURL = daftarTransaksiUrl;
			URL url = new URL(stringURL);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setConnectTimeout(connectTimeout);
			conn.setReadTimeout(readTimeout);

			conn.setHostnameVerifier(new HostnameVerifier() {
				public boolean verify(String host, SSLSession sess) {
					if (host.equals("localhost"))
						return true;
					else
						return false;
				}
			});

			conn.setRequestProperty("Accept", "text/html");
			conn.setRequestProperty("Cookie", "PHPSESSID=" + sessionId);
			conn.setRequestProperty("Host", host);

			conn.setRequestProperty("sec-ch-ua", "\"Chromium\";v=\"128\", \"Not;A=Brand\";v=\"24\", \"Google Chrome\";v=\"128\"");
			conn.setRequestProperty("sec-ch-ua-mobile", "?0");
			conn.setRequestProperty("sec-ch-ua-platform", "\"Windows\"");
			conn.setRequestProperty("sec-fetch-dest", "document");
			conn.setRequestProperty("sec-fetch-mode", "navigate");
			conn.setRequestProperty("sec-fetch-site", "same-origin");
			conn.setRequestProperty("sec-fetch-user", "?1");
			conn.setRequestProperty("upgrade-insecure-requests", "1");
			conn.setRequestProperty("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/128.0.0.0 Safari/537.36");


			int responseCode = conn.getResponseCode();
			String responseMessage = conn.getResponseMessage();

			if(responseCode == HttpURLConnection.HTTP_OK) {
				resultMap = new HashedMap<>();

				reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				String line;
				StringBuilder sb = new StringBuilder();
				while ((line = reader.readLine()) != null) {
					sb.append(line);
				}

				reader.close();

				String html = sb.toString();
				Document document = Jsoup.parse(html);
				Element table = document.select("table").first();
				Element tbody = table.select("tbody").first();
				Elements rows = tbody.getElementsByTag("tr");

				for (int i = 0; i < rows.size(); i++) {
					Element row = rows.get(i);
					Elements cols = row.getElementsByTag("td");

					String noSertifikat = cols.get(3).text();
					if(noSertifikat == null || "".equals(noSertifikat)) {
						TransaksiAhuDTO transaksiAhu = new TransaksiAhuDTO();
						transaksiAhu.setKodeVoucher(cols.get(2).text());
						transaksiAhu.setTanggalTransaksi(DateUtils.parseZonedDateTime(cols.get(14).text(), DateUtils.FORMATTER_ID_LOCAL_DATETIME_V2));

						Elements spans = cols.get(15).getElementsByTag("span");
						Element span0 = spans.get(0).select("a").first();
						String span0_onclick = span0.attr("onclick");
						String billId = span0_onclick.substring(span0_onclick.indexOf("'") + 1, span0_onclick.lastIndexOf("'"));
						transaksiAhu.setBillId(billId);
						Element span3 = spans.get(3).select("span").last();
						transaksiAhu.setTanggalBayar(DateUtils.parseDate(span3.text(), DateUtils.FORMATTER_ID_LOCAL_DATE));

						resultMap.put(billId, transaksiAhu);
					}
				}

				log.debug("SUCCESS getVoucherFromTable with count {} -> {} {}", resultMap.size(), responseCode, responseMessage);
			} else {
				log.debug("FAILED getVoucherFromTable -> " + responseCode + " " + responseMessage);
			}
		} catch (Exception e) {
			log.error("FAILED getVoucherFromTable " + ExceptionUtils.getMessage(e));
			e.printStackTrace();
			throw e;
		} finally {
			if(reader != null)
				try {
					reader.close();
				} catch (Exception e) { }
		}

		return resultMap;
	}
	private List<BiayaPnbp> biayaPnbpList;
	
	public void updateBiayaPnbp() {
		List<Referensi> BiayaPnbpRefs = referensiRepository.findByCategory("BIAYA_PNBP");
		
		if(BiayaPnbpRefs != null) {
			biayaPnbpList = new ArrayList<>();
			
			for(Referensi conf : BiayaPnbpRefs) {
				String[] tokens = conf.getName().split("-");
				BiayaPnbp biayaPnbp = new BiayaPnbp(Long.parseLong(conf.getValue()), Long.parseLong(tokens[0]), Long.parseLong(tokens[1]));
				biayaPnbpList.add(biayaPnbp);
			}
		}
	}
	
	public List<BiayaPnbp> getBiayaPnbpList() {
		return this.biayaPnbpList;
	}
	
	public Long getBiayaPnbp(Long nilai) {
		if(biayaPnbpList!=null && !biayaPnbpList.isEmpty()) {
			for(BiayaPnbp b : biayaPnbpList) {
				if(nilai < b.maxNilai && nilai >= b.minNilai) {
					return b.biaya;
				}
			}
		}
		
		return null;
	}
	
	class BiayaPnbp {
		
		private Long biaya;
		
		private Long minNilai;
		
		private Long maxNilai;
		
		BiayaPnbp(Long biaya, Long minNilai, Long maxNilai) {
			this.biaya = biaya;
			this.minNilai = minNilai;
			this.maxNilai = maxNilai;
		}
	}

	private String createRequest2(Akta akta, String sessionid) throws Exception  {
		Notaris notaris = akta.getNotaris();
//		log.debug("notaris:{}", notaris.getId());
		String notarisWilKerja = referensiService.valueOf("WILAYAH_KERJA", notaris.getWilayahKerja());
		Ppk ppk = akta.getPpk();
		Badan badan = ppk.getBadan();
		Orang orang = ppk.getOrang();
		List<Kendaraan> kendaraans = kendaraanRepository.findByPpk(ppk);
//		log.debug("kendaraans:{}", kendaraans.size());
		PenerimaKuasa penerimaKuasa = akta.getPenerimaKuasa();
		Cabang cabang = akta.getCabang();
		Leasing leasing = cabang.getLeasing();
		boolean menuOrang = false;
		
		Long nilaiPenjaminan =  ppk.getNilaiPenjaminan();
		Long nilaiHutangPokok = ppk.getNilaiHutangPokok();
		
		StringBuilder payload = new StringBuilder()
				.append("konte=").append("4e08af77e9fb1cb0b29d617d999f4ed1").append("&");
		
		String namaNasabah = "";
		if (ppk.getNsbNamaDebitur().equals(ppk.getNsbNama())) {
			namaNasabah = "";
		} else {
			namaNasabah= ppk.getNsbNamaDebitur(); // menggunakan nama debitur
		}

		if(ppk.getNsbJenis().intValue() == NasabahJenis.BADAN.value()) {
//			Validasi Badan Hukum
			String nomorSK = null;
			String jenisBadan = referensiService.valueOf("JENIS_BADAN", badan.getJenis());
			List<String> listBakum = loadBakum(sessionid, badan.getNama(), jenisBadan);
			
			for (String string : listBakum) {
				log.debug("Bakum : "+ string);
				String[] bakum =  string.split("\\|");
				if (bakum.length > 2 && bakum[2].equals(badan.getNpwp())) {
					nomorSK = bakum[1];
				} else {
					log.debug("Badan ada di AHU tapi NPWP Unmatch, akan diregistrasi di input menu Perorangan : {}", ppk.getNomor());
					menuOrang = true;
				}
			}
			
//			tipe_pemberi: KORPORASI(1), PERORANGAN(2)
//			tipe_pemberi_asing: KORPORASI INDONESIA(1), KORPORASI ASING(2), KORPORASI LAINNYA(3)
//			sub_tipe_pemberi: BANK(1), LEMBAGA KEUANGAN BUKAN BANK(2), LAINNYA(9)
//			tipe_badan_hukum_pemberi: PT
			
			if (!"OTHER".equals(jenisBadan) && listBakum != null && !menuOrang) {
				log.debug("Badan di daftarkan di menu badan : {}", ppk.getNomor());
				payload.append("tipe_badan_hukum_pemberi=").append(badan.getJenis()).append("&")
			    .append("tipe_pemberi=").append(ppk.getNsbJenis()).append("&") // TODO
			    .append("tipe_pemberi_asing=").append(1).append("&") 
				.append("nm_pemberi=").append(ppk.getNsbNama()).append("&") // tipe_badan_hukum + nama pemberi
				.append("nik_npwp_pemberi=").append(badan.getNpwp()).append("&")
				.append("npwp_pemberi=").append(badan.getNpwp()).append("&")
				.append("sk_pemberi=").append(nomorSK).append("&") // autofield
				.append("no_hp_pemberi=").append(penerimaKuasa.getNoKontak()).append("&")// TODO
				.append("almt_pemberi=").append(badan.getAlamatPemegangBpkb().toUpperCase()).append("&")
				.append("kodepos_pemberi=").append(badan.getKodeposPemegangBpkb()).append("&")
				.append("provinsi_pemberi=").append(referensiService.valueOf("PROVINSI", badan.getProvinsiPemegangBpkb())).append("&")
				.append("provinsi_pemberi_dummy=").append(badan.getProvinsiPemegangBpkb()).append("&")
				.append("kab_kota_pemberi=").append(referensiService.valueOf("KOTA", badan.getKotaPemegangBpkb())).append("&")
				.append("kab_kota_pemberi_dummy=").append(badan.getKotaPemegangBpkb()).append("&")
				.append("kecamatan_pemberi=").append(referensiService.valueOf("KECAMATAN", badan.getKecamatanPemegangBpkb())).append("&")
				.append("label_kecamatan_pemberi=").append(badan.getKecamatanPemegangBpkb()).append("&")
				.append("kelurahan_pemberi=").append(referensiService.valueOf("KELURAHAN", badan.getKelurahanPemegangBpkb())).append("&")
				.append("label_kelurahan_pemberi=").append(badan.getKelurahanPemegangBpkb()).append("&")
				.append("rt_pemberi=").append(badan.getRtPemegangBpkb()).append("&")
				.append("rw_pemberi=").append(badan.getRwPemegangBpkb()).append("&")
				.append("nm_debitur=").append(namaNasabah).append("&");
				
			} else {
//				Badan yang tidak ada list AHU di daftarkan Perorangan
				log.debug("Badan di daftarkan di menu perorangan : {}", ppk.getNomor());
				payload.append("tipe_badan_hukum_pemberi=").append(badan.getJenis()).append("&")
			    .append("tipe_pemberi=").append(ppk.getNsbJenis()).append("&") // TODO
			    .append("tipe_pemberi_asing=").append(1).append("&") 
				.append("nm_pemberi=").append(ppk.getNsbNama()).append("&") // tipe_badan_hukum + nama pemberi
				.append("nik_npwp_pemberi=").append(badan.getNpwp()).append("&")
				.append("npwp_pemberi=").append(badan.getNpwp()).append("&")
				.append("sk_pemberi=").append(nomorSK).append("&") // autofield
				.append("no_hp_pemberi=").append(penerimaKuasa.getNoKontak()).append("&")// TODO
				.append("almt_pemberi=").append(badan.getAlamatPemegangBpkb().toUpperCase()).append("&")
				.append("kodepos_pemberi=").append(badan.getKodeposPemegangBpkb()).append("&")
				.append("provinsi_pemberi=").append(referensiService.valueOf("PROVINSI", badan.getProvinsiPemegangBpkb())).append("&")
				.append("provinsi_pemberi_dummy=").append(badan.getProvinsiPemegangBpkb()).append("&")
				.append("kab_kota_pemberi=").append(referensiService.valueOf("KOTA", badan.getKotaPemegangBpkb())).append("&")
				.append("kab_kota_pemberi_dummy=").append(badan.getKotaPemegangBpkb()).append("&")
				.append("kecamatan_pemberi=").append(referensiService.valueOf("KECAMATAN", badan.getKecamatanPemegangBpkb())).append("&")
				.append("label_kecamatan_pemberi=").append(badan.getKecamatanPemegangBpkb()).append("&")
				.append("kelurahan_pemberi=").append(referensiService.valueOf("KELURAHAN", badan.getKelurahanPemegangBpkb())).append("&")
				.append("label_kelurahan_pemberi=").append(badan.getKelurahanPemegangBpkb()).append("&")
				.append("rt_pemberi=").append(badan.getRtPemegangBpkb()).append("&")
				.append("rw_pemberi=").append(badan.getRwPemegangBpkb()).append("&")
				.append("nm_debitur=").append(namaNasabah).append("&");
			}
			
		} else {
//			if (referensiService.valueOf("KECAMATAN", orang.getKecamatan()) == null)
//				log.info("Referensi Kecamatan Invalid {} {}", ppk.getNomor(), orang.getKecamatan());
//			if (referensiService.valueOf("KELURAHAN", orang.getKelurahan()) == null) 
//				log.info("Referensi kelurahan Invalid {} {}", ppk.getNomor(), orang.getKelurahan());
			
//	        ppk.setNsbNama(data.getNamaPemegangBpkb()) --> pemegang bpkb
//	        .nsbNamaDebitur(data.getNamaDebitur()) --> nama debitur
			
			payload.append("sub_tipe_pemberi=").append(referensiService.valueOf("JENIS_KELAMIN", orang.getJenisKelamin().toUpperCase())).append("&")
		        .append("tipe_pemberi=").append(ppk.getNsbJenis()).append("&")
			    .append("tipe_pemberi_asing=").append(1).append("&") 
				.append("jenis_penggunaan=").append(2).append("&")
				.append("nm_pemberi=").append(ppk.getNsbNama()).append("&")
				.append("nik_npwp_pemberi=").append(orang.getIdNumberPemegangBpkb()!= null ? orang.getIdNumberPemegangBpkb(): orang.getNoId()).append("&")
				.append("npwp_pemberi=").append("0000000000000000").append("&") // TODO
				.append("no_hp_pemberi=").append(penerimaKuasa.getNoKontak()).append("&")
				.append("almt_pemberi=").append(orang.getAlamatPemegangBpkb() != null ? orang.getAlamatPemegangBpkb() : orang.getAlamat()).append("&")
				.append("kodepos_pemberi=").append(orang.getKodeposPemegangBpkb() != null ? orang.getKodeposPemegangBpkb() : orang.getKodePos()).append("&")
				.append("provinsi_pemberi=").append(referensiService.valueOf("PROVINSI", orang.getProvinsiPemegangBpkb() != null ? orang.getProvinsiPemegangBpkb() : orang.getProvinsi())).append("&")
				.append("provinsi_pemberi_dummy=").append(orang.getProvinsiPemegangBpkb() != null ? orang.getProvinsiPemegangBpkb() : orang.getProvinsi()).append("&")
				.append("kab_kota_pemberi=").append(referensiService.valueOf("KOTA", orang.getKotaPemegangBpkb() != null ? orang.getKotaPemegangBpkb() : orang.getKota())).append("&")
				.append("kab_kota_pemberi_dummy=").append(orang.getKotaPemegangBpkb() != null ? orang.getKotaPemegangBpkb() : orang.getKota()).append("&")
				.append("kecamatan_pemberi=").append(referensiService.valueOf("KECAMATAN",orang.getKecamatanPemegangBpkb() !=null ? orang.getKecamatanPemegangBpkb() :  orang.getKecamatan())).append("&")
				.append("label_kecamatan_pemberi=").append(orang.getKecamatanPemegangBpkb() !=null ? orang.getKecamatanPemegangBpkb() :  orang.getKecamatan()).append("&")
				.append("kelurahan_pemberi=").append(referensiService.valueOf("KELURAHAN", orang.getKelurahanPemegangBpkb() != null ? orang.getKelurahanPemegangBpkb() : orang.getKelurahan())).append("&")
				.append("label_kelurahan_pemberi=").append(orang.getKelurahanPemegangBpkb() != null ? orang.getKelurahanPemegangBpkb() : orang.getKelurahan()).append("&")
				.append("rt_pemberi=").append(orang.getRtPemegangBpkb() != null ? orang.getRtPemegangBpkb() : orang.getRt()).append("&")
				.append("rw_pemberi=").append(orang.getRwPemegangBpkb() != null ? orang.getRwPemegangBpkb() : orang.getRw()).append("&")
				.append("nm_debitur=").append(namaNasabah).append("&"); //nama_nasabah
		}
		
		payload.append("penerima_fidusia[0][tipe]=").append(1).append("&")
			.append("penerima_fidusia[0][tipe_asing]=").append(1).append("&")
			.append("penerima_fidusia[0][tipe_badan_hukum]=").append(leasing.getBadanHukum()).append("&") // add tabel leasing
			.append("penerima_fidusia[0][sub_tipe]=").append(2).append("&")
			.append("penerima_fidusia[0][nm]=").append(leasing.getBadanHukum() + " " + leasing.getNama()).append("&")
			.append("penerima_fidusia[0][nik_npwp]=").append(leasing.getNpwp()).append("&")
			.append("penerima_fidusia[0][npwp]=").append("").append("&") //opsional
			.append("penerima_fidusia[0][paspor]=").append("").append("&") //opsional
			.append("penerima_fidusia[0][sk]=").append(leasing.getSk()).append("&") // add table leasing
			.append("penerima_fidusia[0][negara_asal]=").append("").append("&")
			.append("penerima_fidusia[0][no_hp]=").append(leasing.getNoKontak()).append("&")
			.append("penerima_fidusia[0][email]=").append(leasing.getEmail()).append("&") // add table leasing
			.append("penerima_fidusia[0][cabang]=").append("").append("&") //opsional
			.append("penerima_fidusia[0][almt]=").append(leasing.getAlamat().toUpperCase()).append("&")
			.append("penerima_fidusia[0][kodepos]=").append(leasing.getKodePos()).append("&")
			.append("penerima_fidusia[0][provinsi]=").append(referensiService.valueOf("PROVINSI", leasing.getProvinsi())).append("&")
			.append("penerima_fidusia[0][label_provinsi]=").append(leasing.getProvinsi()).append("&")
			.append("penerima_fidusia[0][kab_kota]=").append(referensiService.valueOf("KOTA", leasing.getKota())).append("&")
			.append("penerima_fidusia[0][label_kab_kota]=").append(leasing.getKota()).append("&")
			.append("penerima_fidusia[0][kecamatan]=").append(referensiService.valueOf("KECAMATAN", leasing.getKecamatan())).append("&")
			.append("penerima_fidusia[0][label_kecamatan]=").append(leasing.getKecamatan()).append("&")
			.append("penerima_fidusia[0][kelurahan]=").append(referensiService.valueOf("KELURAHAN", leasing.getKecamatan())).append("&")
			.append("penerima_fidusia[0][label_kelurahan]=").append(leasing.getKelurahan()).append("&")
//			.append("penerima_fidusia[0][rt]=").append(leasing.getRt()).append("&")
//			.append("penerima_fidusia[0][rw]=").append(leasing.getRw()).append("&")
			.append("no_akta=").append(akta.getNomor()).append("&")
			.append("tgl_akta=").append(akta.getTanggal().format(dateFormat)).append("&")
			.append("nm_notaris=").append(notaris.getNamaAhu()).append("&")
			.append("kd_notaris=").append(notaris.getWilayahKerja()).append("&")
			.append("id_notaris=").append(notaris.getId()).append("&")
			.append("id_kedudukan=").append(notarisWilKerja).append("&")
			.append("id_wilayah=").append(notarisWilKerja).append("&")
			.append("flag_hutang=").append(1).append("&")
			// nilai hutang pokok akumulasi dari PK1 dan PK 2
			.append("penjaminan=").append("" + nilaiHutangPokok + ".00").append("&");
		
		String result = "";
		
		if (ppk.getNama().contains("&")) {
			result = ppk.getNama().replace("&", "and");
		} else {
			result = ppk.getNama();
		}
		
		payload.append("pokok_dasar=").append(result).append("&") 
				.append("no_perjanjian=").append(ppk.getNomor()).append("&")
				.append("tgl_perjanjian=").append(ppk.getTanggal().format(dateFormat)).append("&");
		
		LocalDate tglCicilanAwal =  ppk.getTglCicilAwal();
		LocalDate tglCicilanAkhir = ppk.getTglCicilAkhir();
		
		payload.append("tgl_jangka_waktu=").append(tglCicilanAwal.format(dateFormat)).append("&")
				.append("tgl_jangka_waktu_akhir=").append(tglCicilanAkhir.format(dateFormat)).append("&");
		

		for(int i=0; i<kendaraans.size(); i++) {
			Kendaraan kendaraan = kendaraans.get(i);
			payload.append("category[" + i + "][select][-1]=").append(1).append("&");
					
			int jenisKendRoda = referensiService.intValue("JENIS_KENDARAAN_RODA", kendaraan.getRoda().toString());
			payload.append("category[" + i + "][select][1]=").append(jenisKendRoda).append("&");
			if(jenisKendRoda == 4) {
				payload.append("category[" + i + "][input][13]=").append(kendaraan.getMerk()).append("&")
					.append("category[" + i + "][input][14]=").append(kendaraan.getTipe()).append("&")
					.append("category[" + i + "][input][22]=").append(kendaraan.getNoRangka()).append("&")
					.append("category[" + i + "][input][23]=").append(kendaraan.getNoMesin()).append("&");
			} else if(jenisKendRoda == 5) {
				payload.append("category[" + i + "][input][16]=").append(kendaraan.getMerk()).append("&")
					.append("category[" + i + "][input][17]=").append(kendaraan.getTipe()).append("&")
					.append("category[" + i + "][input][24]=").append(kendaraan.getNoRangka()).append("&")
					.append("category[" + i + "][input][25]=").append(kendaraan.getNoMesin()).append("&");
			} else if(jenisKendRoda == 45) {
				payload.append("category[" + i + "][input][46]=").append(kendaraan.getMerk()).append("&")
					.append("category[" + i + "][input][47]=").append(kendaraan.getTipe()).append("&")
					.append("category[" + i + "][input][48]=").append(kendaraan.getNoRangka()).append("&")
					.append("category[" + i + "][input][49]=").append(kendaraan.getNoMesin()).append("&");
			} else if(jenisKendRoda == 50) {
				payload.append("category[" + i + "][input][51]=").append(kendaraan.getMerk()).append("&")
					.append("category[" + i + "][input][52]=").append(kendaraan.getTipe()).append("&")
					.append("category[" + i + "][input][53]=").append(kendaraan.getNoRangka()).append("&")
					.append("category[" + i + "][input][54]=").append(kendaraan.getNoMesin()).append("&");
			} else if(jenisKendRoda == 55) {
				payload.append("category[" + i + "][input][56]=").append(kendaraan.getMerk()).append("&")
					.append("category[" + i + "][input][57]=").append(kendaraan.getTipe()).append("&")
					.append("category[" + i + "][input][58]=").append(kendaraan.getNoRangka()).append("&")
					.append("category[" + i + "][input][59]=").append(kendaraan.getNoMesin()).append("&");
			} else if(jenisKendRoda == 60) {
				payload.append("category[" + i + "][input][61]=").append(kendaraan.getMerk()).append("&")
					.append("category[" + i + "][input][62]=").append(kendaraan.getTipe()).append("&")
					.append("category[" + i + "][input][63]=").append(kendaraan.getNoRangka()).append("&")
					.append("category[" + i + "][input][64]=").append(kendaraan.getNoMesin()).append("&");
			} else if(jenisKendRoda == 65) {
				payload.append("category[" + i + "][input][66]=").append(kendaraan.getMerk()).append("&")
					.append("category[" + i + "][input][67]=").append(kendaraan.getTipe()).append("&")
					.append("category[" + i + "][input][68]=").append(kendaraan.getNoRangka()).append("&")
					.append("category[" + i + "][input][69]=").append(kendaraan.getNoMesin()).append("&");
			}
			
			payload.append("category[" + i + "][input][~a]=").append("Berdasarkan perjanjian pembiayaan Nomor" + " " + (ppk.getNomor()) + " tanggal " + ppk.getTanggal().format(dateFormat)).append("&");
			
			payload.append("category[" + i + "][input][~b]=").append("" + kendaraan.getNilaiObjekJaminan() + ".00").append("&");
			payload.append("kategori" + i + "=").append("IDR_RUPIAH").append("&");
		}
		
		int penjaminanObyek;
		long nilaiPokok = nilaiPenjaminan;
		
		if(nilaiPokok <= 5e7)
			penjaminanObyek = 1;
		else if(nilaiPokok <= 1e8)
			penjaminanObyek = 2;
		else if(nilaiPokok <= 25e7)
			penjaminanObyek = 3;
		else if(nilaiPokok <= 5e8)
			penjaminanObyek = 4;
		else if(nilaiPokok <= 1e9)
			penjaminanObyek = 5;
		else if(nilaiPokok <= 1e11)
			penjaminanObyek = 6;
		else if(nilaiPokok <= 5e11)
			penjaminanObyek = 7;
		else if(nilaiPokok <= 1e12)
			penjaminanObyek = 8;
		else
			penjaminanObyek = 9;

		payload.append("jaminan_fidusia[]=").append("" + nilaiPenjaminan + ".00").append("&")
			.append("jaminan_fidusia[]=").append("&")
			.append("penjaminan_obyek=").append(penjaminanObyek).append("&")
			.append("agree=").append(1).append("&")
			.append("tempat=").append("undefined").append("&")
			.append("time=").append(Instant.now().getEpochSecond());
//		log.debug(payload.toString());
		
		return payload.toString();
	}
	
	public List<String> loadBakum (String sessionId, String name, String source) {
		OutputStreamWriter writer = null;
		BufferedReader reader = null;
		String postData = null;
	    List<String> response = new ArrayList<>();
		
		StringBuilder payload = new StringBuilder()
				.append("name=").append(name).append("&")
				.append("source=").append(source).append("&");
		
		try {
			postData = payload.toString();
		} catch (Exception e) {
			log.error("Error on loadBakum", e);
		}
		
		try {
			SSLContext ssl_ctx = SSLContext.getInstance("TLS");
			TrustManager[] trust_mgr = get_trust_mgr();
			ssl_ctx.init(null, trust_mgr, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());

			URL url = new URL(loadBakumUrl);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setConnectTimeout(connectTimeout);
			conn.setReadTimeout(readTimeout);
		    
			conn.setHostnameVerifier(new HostnameVerifier() {
				public boolean verify(String host, SSLSession sess) {
					if (host.equals("localhost"))
						return true;
					else
						return false;
				}
			});

			conn.setRequestProperty("Accept", "*/*");
			conn.setRequestProperty("Accept-Encoding", "gzip, deflate, br");
			conn.setRequestProperty("Accept-Language", "en-US,en;q=0.9");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			conn.setRequestProperty("Cookie", "PHPSESSID=" + sessionId);
			conn.setRequestProperty("Host", host);
			conn.setRequestProperty("Origin", origin);
			conn.setRequestProperty("Referer", refererPendaftaran);
			conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
			
	        conn.setDoInput(true);
			conn.setDoOutput(true);
		    writer = new OutputStreamWriter(conn.getOutputStream());

		    writer.write(postData);
		    writer.flush();
		    
		    int responseCode = conn.getResponseCode();
		    
		    if(responseCode == HttpURLConnection.HTTP_OK) {
			    reader = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
		        String line;
		        StringBuilder sb = new StringBuilder();
		        while ((line = reader.readLine()) != null) { 
		        	sb.append(line);
		        }
		        reader.close();
		        
		        JSONArray arr = new JSONArray(sb.toString());
		        for (int i=0; i<arr.length(); i++){
			        JSONObject jsonProductObject = arr.getJSONObject(i);
			        String nama = jsonProductObject.getString("nama");
			        String nomor_sk = jsonProductObject.getString("nomor_sk");
			        String npwp = jsonProductObject.getString("npwp");
			        log.debug(nama +"|"+ nomor_sk+"|"+npwp);
			        if ((npwp != null && !"".equals(npwp)) && 
			        		(nomor_sk!=null && !"".equals(nomor_sk)) && 
			        		(nama!=null && !"".equals(nama)))
			        		response.add(nama +"|"+ nomor_sk+"|"+npwp );
			      }
		    } else {
				log.debug("loadBakum FAILED {} {}-{} {}ms");
				log.debug(postData);
			}
		} catch (Exception e) {
        	log.error("Error on loadBakum di AHU ", e);
			log.debug(postData);
		} finally {
			if(writer != null)
				try {
					writer.close();
				} catch (Exception e) { }

			if(reader != null)
				try {
					reader.close();
				} catch (IOException e) { }
		}
		
		return response;
	}

	private TrustManager[] get_trust_mgr() {
		TrustManager[] certs = new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String t) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String t) {
			}
		} };
		return certs;
	}

	private String[] readPdf(File file) {
		PDDocument document = null;
		try {
			document = Loader.loadPDF(file);
			PDFTextStripper reader = new PDFTextStripper();
			reader.setStartPage(1);
			reader.setEndPage(1);
			String payload = reader.getText(document);
			return payload.split("\\r?\\n");
		} catch (Exception e) {
			log.error("FAILED readPdf -> " + ExceptionUtils.getMessage(e));
		} finally {
			try {
				document.close();
			} catch (Exception ex) {}
		}

		return null;
	}
}
