package com.fidusia.app.domain;


import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A TransaksiAkta.
 */
@Entity
@Table(name = "transaksi_akta")
public class TransaksiAkta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "tgl_order", nullable = false)
    private LocalDate tglOrder;

    @NotNull
    @Size(max = 6)
    @Column(name = "bln_order", length = 6, nullable = false)
    private String blnOrder;

    @NotNull
    @Max(value = 999999)
    @Column(name = "jml_order", nullable = false)
    private Integer jmlOrder;

    @Column(name = "tgl_akta")
    private LocalDate tglAkta;

    @Size(max = 6)
    @Column(name = "bln_akta", length = 6)
    private String blnAkta;

    @Column(name = "jml_akta")
    private Integer jmlAkta;

    @NotNull
    @Max(value = 9)
    @Column(name = "status", nullable = false)
    private Integer status;

    @Max(value = 999999)
    @Column(name = "last_nomor")
    private Integer lastNomor;

    @Column(name = "last_pukul_akhir")
    private ZonedDateTime lastPukulAkhir;

    @ManyToOne
    private Notaris notaris;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getTglOrder() {
        return tglOrder;
    }

    public TransaksiAkta tglOrder(LocalDate tglOrder) {
        this.tglOrder = tglOrder;
        return this;
    }

    public void setTglOrder(LocalDate tglOrder) {
        this.tglOrder = tglOrder;
    }

    public String getBlnOrder() {
        return blnOrder;
    }

    public TransaksiAkta blnOrder(String blnOrder) {
        this.blnOrder = blnOrder;
        return this;
    }

    public void setBlnOrder(String blnOrder) {
        this.blnOrder = blnOrder;
    }

    public Integer getJmlOrder() {
        return jmlOrder;
    }

    public TransaksiAkta jmlOrder(Integer jmlOrder) {
        this.jmlOrder = jmlOrder;
        return this;
    }

    public void setJmlOrder(Integer jmlOrder) {
        this.jmlOrder = jmlOrder;
    }

    public LocalDate getTglAkta() {
        return tglAkta;
    }

    public TransaksiAkta tglAkta(LocalDate tglAkta) {
        this.tglAkta = tglAkta;
        return this;
    }

    public void setTglAkta(LocalDate tglAkta) {
        this.tglAkta = tglAkta;
    }

    public String getBlnAkta() {
        return blnAkta;
    }

    public TransaksiAkta blnAkta(String blnAkta) {
        this.blnAkta = blnAkta;
        return this;
    }

    public void setBlnAkta(String blnAkta) {
        this.blnAkta = blnAkta;
    }

    public Integer getJmlAkta() {
        return jmlAkta;
    }

    public TransaksiAkta jmlAkta(Integer jmlAkta) {
        this.jmlAkta = jmlAkta;
        return this;
    }

    public void setJmlAkta(Integer jmlAkta) {
        this.jmlAkta = jmlAkta;
    }

    public Integer getStatus() {
        return status;
    }

    public TransaksiAkta status(Integer status) {
        this.status = status;
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getLastNomor() {
        return lastNomor;
    }

    public TransaksiAkta lastNomor(Integer lastNomor) {
        this.lastNomor = lastNomor;
        return this;
    }

    public void setLastNomor(Integer lastNomor) {
        this.lastNomor = lastNomor;
    }

    public ZonedDateTime getLastPukulAkhir() {
        return lastPukulAkhir;
    }

    public TransaksiAkta lastPukulAkhir(ZonedDateTime lastPukulAkhir) {
        this.lastPukulAkhir = lastPukulAkhir;
        return this;
    }

    public void setLastPukulAkhir(ZonedDateTime lastPukulAkhir) {
        this.lastPukulAkhir = lastPukulAkhir;
    }

    public Notaris getNotaris() {
        return notaris;
    }

    public TransaksiAkta notaris(Notaris notaris) {
        this.notaris = notaris;
        return this;
    }

    public void setNotaris(Notaris notaris) {
        this.notaris = notaris;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TransaksiAkta transaksiAkta = (TransaksiAkta) o;
        if (transaksiAkta.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, transaksiAkta.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "TransaksiAkta{" +
            "id=" + id +
            ", tglOrder='" + tglOrder + "'" +
            ", blnOrder='" + blnOrder + "'" +
            ", jmlOrder='" + jmlOrder + "'" +
            ", tglAkta='" + tglAkta + "'" +
            ", blnAkta='" + blnAkta + "'" +
            ", jmlAkta='" + jmlAkta + "'" +
            ", status='" + status + "'" +
            ", lastNomor='" + lastNomor + "'" +
            ", lastPukulAkhir='" + lastPukulAkhir + "'" +
            '}';
    }
}
