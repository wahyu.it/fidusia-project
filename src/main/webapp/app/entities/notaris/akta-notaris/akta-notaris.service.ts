import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { IAktaDTO } from 'app/shared/model/aktaDTO.model';

type EntityArrayResponseType = HttpResponse<IAktaDTO[]>;

@Injectable({ providedIn: 'root' })
export class AktaNotarisService {
  	  public resourceUrl= SERVER_API_URL + 'api/aktas/search';
	
	  constructor(protected http: HttpClient) {}

	  search(req?: any): Observable<EntityArrayResponseType> {
	    const options = createRequestOption(req);
	    return this.http
	      .get<IAktaDTO[]>(this.resourceUrl, { params: options, observe: 'response' })
	      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
	  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
	    if (res.body) {
	      res.body.forEach((r: IAktaDTO) => {
	        r.tglOrder = r.tglOrder ? moment(r.tglOrder) : undefined;
	        r.tglAkta = r.tglAkta ? moment(r.tglAkta) : undefined;
	        r.pukulAkta = r.pukulAkta ? moment(r.pukulAkta) : undefined;
	      });
	    }
    return res;
  }
	
}