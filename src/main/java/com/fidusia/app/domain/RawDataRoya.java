package com.fidusia.app.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import com.fidusia.app.domain.util.RawDataRoyaStatus;
import com.fidusia.app.util.DateUtils;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;

/**
 * A Raw Data Roya.
 */
@Entity
@Table(name = "raw_data_roya")
public class RawDataRoya implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "tgl_order", nullable = false)
    private LocalDate tglOrder;

    @NotNull
    @Size(max = 50)
    @Column(name = "file_name", length = 50, nullable = false)
    private String fileNama;

    @NotNull
    @Column(name = "line_number", nullable = false)
    private Integer lineNumber;

    @Size(max = 50)
    @Column(name = "unique_key", length = 50)
    private String uniqueKey;

    @Size(max = 100)
    @Column(name = "nama_area", length = 100)
    private String namaArea;

    @Column(name = "tgl_lunas")
    private LocalDate tglLunas;

    @Size(max = 50)
    @Column(name = "nomor_roya", length = 50)
    private String nomorRoya;
    
    @Column(name = "tgl_roya")
    private LocalDate tglRoya;

    @NotNull
    @Size(max = 20)
    @Column(name = "status", length = 20, nullable = false)
    private String status;
    
    @Size(max = 100)
    @Column(name = "message", length = 100)
    private String message;

    @Column(name = "result_sent")
    private Boolean resultSent;

    @Column(name = "last_update_on")
    private ZonedDateTime lastUpdateOn;

    @Size(max = 50)
    @Column(name = "last_update_by", length = 50)
    private String lastUpdateBy;

    @Column(name = "desentralisasi")
    private Boolean desentralisasi;

    @Size(max = 50)
    @Column(name = "no_sertifikat_ori", length = 50)
    private String noSertifikatOri;

    @Column(name = "tgl_sertifikat_ori")
    private ZonedDateTime tglSertifikatOri;

    @Size(max = 100)
    @Column(name = "nama_notaris_ori", length = 100)
    private String namaNotarisOri;

    @Size(max = 100)
    @Column(name = "wil_notaris_ori", length = 100)
    private String wilNotarisOri;

    @Column(name = "duplicate")
    private Boolean duplicate;

    @Column(name = "registration_id", length = 100)
    private String registrationId;

    @Column(name = "roya_tipe", length = 100)
    private Integer royaTipe;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getTglOrder() {
		return tglOrder;
	}

	public void setTglOrder(LocalDate tglOrder) {
		this.tglOrder = tglOrder;
	}

	public String getFileNama() {
		return fileNama;
	}

	public void setFileNama(String fileNama) {
		this.fileNama = fileNama;
	}

	public Integer getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(Integer lineNumber) {
		this.lineNumber = lineNumber;
	}

	public String getUniqueKey() {
		return uniqueKey;
	}

	public void setUniqueKey(String uniqueKey) {
		this.uniqueKey = uniqueKey;
	}

	public String getNamaArea() {
		return namaArea;
	}

	public void setNamaArea(String namaArea) {
		this.namaArea = namaArea;
	}

	public LocalDate getTglLunas() {
		return tglLunas;
	}

	public void setTglLunas(LocalDate tglLunas) {
		this.tglLunas = tglLunas;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		if(status != null && status.length() > 100)
			status = status.substring(0, 97) + "...";
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ZonedDateTime getLastUpdateOn() {
		return lastUpdateOn;
	}

	public void setLastUpdateOn(ZonedDateTime lastUpdateOn) {
		this.lastUpdateOn = lastUpdateOn;
	}

	public String getLastUpdateBy() {
		return lastUpdateBy;
	}

	public void setLastUpdateBy(String lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}

	public String getNomorRoya() {
		return nomorRoya;
	}

	public void setNomorRoya(String nomorRoya) {
		this.nomorRoya = nomorRoya;
	}

	public LocalDate getTglRoya() {
		return tglRoya;
	}

	public void setTglRoya(LocalDate tglRoya) {
		this.tglRoya = tglRoya;
	}
	
	public Boolean getResultSent() {
		return resultSent;
	}

	public void setResultSent(Boolean resultSent) {
		this.resultSent = resultSent;
	}

	public Boolean getDesentralisasi() {
		return desentralisasi;
	}

	public void setDesentralisasi(Boolean desentralisasi) {
		this.desentralisasi = desentralisasi;
	}

	public String getNoSertifikatOri() {
		return noSertifikatOri;
	}

	public void setNoSertifikatOri(String noSertifikatOri) {
		this.noSertifikatOri = noSertifikatOri;
	}

	public ZonedDateTime getTglSertifikatOri() {
		return tglSertifikatOri;
	}

	public void setTglSertifikatOri(ZonedDateTime tglSertifikatOri) {
		this.tglSertifikatOri = tglSertifikatOri;
	}

	public String getNamaNotarisOri() {
		return namaNotarisOri;
	}

	public void setNamaNotarisOri(String namaNotarisOri) {
		this.namaNotarisOri = namaNotarisOri;
	}

	public String getWilNotarisOri() {
		return wilNotarisOri;
	}

	public void setWilNotarisOri(String wilNotarisOri) {
		this.wilNotarisOri = wilNotarisOri;
	}

	public Boolean getDuplicate() {
		return duplicate;
	}

	public void setDuplicate(Boolean duplicate) {
		this.duplicate = duplicate;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public Integer getRoyaTipe() {
		return royaTipe;
	}

	public void setRoyaTipe(Integer royaTipe) {
		this.royaTipe = royaTipe;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RawDataRoya other = (RawDataRoya) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

//	@Override
//	public String toString() {
//		return "RawDataRoya [id=" + id + ", tglOrder=" + tglOrder + ", fileNama=" + fileNama + ", lineNumber=" + lineNumber + ", uniqueKey=" + uniqueKey + ", namaArea=" + namaArea + ", tglLunas=" + tglLunas + ", nomorRoya=" + nomorRoya + ", tglRoya=" + tglRoya + ", status=" + status + ", message=" + message + ", resultSent="
//				+ resultSent + ", lastUpdateOn=" + lastUpdateOn + ", lastUpdateBy=" + lastUpdateBy + "]";
//	}
	
	@Override
	public String toString() {
		return "RawDataRoya [id=" + id + ", tglOrder=" + tglOrder + ", fileNama=" + fileNama + ", lineNumber=" + lineNumber + ", uniqueKey=" + uniqueKey + ", namaArea="
				+ namaArea + ", tglLunas=" + tglLunas + ", nomorRoya=" + nomorRoya + ", tglRoya=" + tglRoya + ", status=" + status + ", message=" + message + ", resultSent="
				+ resultSent + ", lastUpdateOn=" + lastUpdateOn + ", lastUpdateBy=" + lastUpdateBy + ", desentralisasi=" + desentralisasi + ", noSertifikatOri="
				+ noSertifikatOri + ", tglSertifikatOri=" + tglSertifikatOri + ", namaNotarisOri=" + namaNotarisOri + ", wilNotarisOri=" + wilNotarisOri + ", duplicate="
				+ duplicate + "]";
	}
	
	@Transient
	private String resultStatus;

	@Transient
	private String line;
	
	@Transient
	private String tglRoyaResp;

	public String getResultStatus() {
		return RawDataRoyaStatus.toOutputResult(status);
	}

	public void setResultStatus(String resultStatus) {
		this.resultStatus = resultStatus;
	}

	public String getLine() {
		return line;
	}

	public void setLine(String line) {
		this.line = line;
	}

	public String getTglRoyaResp() {
		return tglRoya != null ? DateUtils.formatDate(tglRoya, DateUtils.FORMATTER_DDMMYYYY_SLASH) : null;
	}

	public void setTglRoyaResp(String tglRoyaResp) {
		this.tglRoyaResp = tglRoyaResp;
	} 
}
