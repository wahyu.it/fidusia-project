package com.fidusia.app.domain;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Invoice.
 *
 * Type:
 * 1 = LEASING
 * 2 = NOTARIS
 *
 * Status:
 * 0 = No Order, Dummy Invoice
 * 1 = New
 * 2 = Partial Paid
 * 3 = All Paid
 * 4 = Completed
 */
@Entity
@Table(name = "invoice")
public class Invoice implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "type", nullable = false)
    private Integer type;

    @NotNull
    @Column(name = "tanggal", nullable = false)
    private LocalDate tanggal;

    @NotNull
    @Column(name = "tgl_order", nullable = false)
    private LocalDate tglOrder;

    @NotNull
    @Column(name = "jml_order", nullable = false)
    private Long jmlOrder;

    @Column(name = "jml_order_cancel")
    private Long jmlOrderCancel;

    @Size(max = 20)
    @Column(name = "no_proforma_pnbp", length = 20)
    private String noProformaPnbp;

    @Size(max = 20)
    @Column(name = "no_proforma_jasa", length = 20)
    private String noProformaJasa;

    @Size(max = 20)
    @Column(name = "no_billpym_pnbp", length = 20)
    private String noBillpymPnbp;

    @Size(max = 20)
    @Column(name = "no_billpym_jasa", length = 20)
    private String noBillpymJasa;

    @Column(name = "amount_pnbp")
    private Long amountPnbp;

    @Column(name = "tax_pnbp")
    private Long taxPnbp;

    @Column(name = "total_amount_pnbp")
    private Long totalAmountPnbp;

    @Column(name = "amount_jasa")
    private Long amountJasa;

    @Column(name = "vat_jasa")
    private Long vatJasa;

    @Column(name = "tax_jasa")
    private Long taxJasa;

    @Column(name = "total_amount_jasa")
    private Long totalAmountJasa;

    @Size(max = 50)
    @Column(name = "file_proforma_pnbp", length = 50)
    private String fileProformaPnbp;

    @Size(max = 50)
    @Column(name = "file_proforma_jasa", length = 50)
    private String fileProformaJasa;

    @Size(max = 50)
    @Column(name = "file_billpym_pnbp", length = 50)
    private String fileBillpymPnbp;

    @Size(max = 50)
    @Column(name = "file_billpym_jasa", length = 50)
    private String fileBillpymJasa;

    @Size(max = 2000)
    @Column(name = "file_bulk", length = 2000)
    private String fileBulk;

    @Size(max = 50)
    @Column(name = "file_tax", length = 50)
    private String fileTax;

    @Size(max = 50)
    @Column(name = "file_txt", length = 50)
    private String fileTxt;

    @Size(max = 20)
    @Column(name = "payment_trx_no", length = 20)
    private String paymentTrxNo;

    @Column(name = "payment_trx_date")
    private LocalDate paymentTrxDate;

    @Size(max = 20)
    @Column(name = "tax_trx_no", length = 20)
    private String taxTrxNo;

    @Column(name = "tax_trx_date")
    private LocalDate taxTrxDate;

    @NotNull
    @Max(value = 9)
    @Column(name = "status", nullable = false)
    private Integer status;

    @Size(max = 50)
    @Column(name = "update_by", length = 50)
    private String updateBy;

    @Column(name = "update_on")
    private ZonedDateTime updateOn;

    @Column(name = "amount_jasa_terima")
    private Long amountJasaTerima;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getTanggal() {
        return tanggal;
    }

    public void setTanggal(LocalDate tanggal) {
        this.tanggal = tanggal;
    }

    public LocalDate getTglOrder() {
        return tglOrder;
    }

    public void setTglOrder(LocalDate tglOrder) {
        this.tglOrder = tglOrder;
    }

    public String getNoProformaPnbp() {
        return noProformaPnbp;
    }

    public void setNoProformaPnbp(String noProformaPnbp) {
        this.noProformaPnbp = noProformaPnbp;
    }

    public String getNoProformaJasa() {
        return noProformaJasa;
    }

    public void setNoProformaJasa(String noProformaJasa) {
        this.noProformaJasa = noProformaJasa;
    }

    public String getNoBillpymPnbp() {
        return noBillpymPnbp;
    }

    public void setNoBillpymPnbp(String noBillpymPnbp) {
        this.noBillpymPnbp = noBillpymPnbp;
    }

    public String getNoBillpymJasa() {
        return noBillpymJasa;
    }

    public void setNoBillpymJasa(String noBillpymJasa) {
        this.noBillpymJasa = noBillpymJasa;
    }

    public Long getAmountPnbp() {
        return amountPnbp;
    }

    public void setAmountPnbp(Long amountPnbp) {
        this.amountPnbp = amountPnbp;
    }

    public Long getTaxPnbp() {
        return taxPnbp;
    }

    public void setTaxPnbp(Long taxPnbp) {
        this.taxPnbp = taxPnbp;
    }

    public Long getJmlOrder() {
        return jmlOrder;
    }

    public void setJmlOrder(Long jmlOrder) {
        this.jmlOrder = jmlOrder;
    }

    public Long getJmlOrderCancel() {
        return jmlOrderCancel;
    }

    public void setJmlOrderCancel(Long jmlOrderCancel) {
        this.jmlOrderCancel = jmlOrderCancel;
    }

    public Long getTotalAmountPnbp() {
        return totalAmountPnbp;
    }

    public void setTotalAmountPnbp(Long totalAmountPnbp) {
        this.totalAmountPnbp = totalAmountPnbp;
    }

    public Long getAmountJasa() {
        return amountJasa;
    }

    public void setAmountJasa(Long amountJasa) {
        this.amountJasa = amountJasa;
    }

    public Long getVatJasa() {
        return vatJasa;
    }

    public void setVatJasa(Long vatJasa) {
        this.vatJasa = vatJasa;
    }

    public Long getTaxJasa() {
        return taxJasa;
    }

    public void setTaxJasa(Long taxJasa) {
        this.taxJasa = taxJasa;
    }

    public Long getTotalAmountJasa() {
        return totalAmountJasa;
    }

    public void setTotalAmountJasa(Long totalAmountJasa) {
        this.totalAmountJasa = totalAmountJasa;
    }

    public String getFileProformaPnbp() {
        return fileProformaPnbp;
    }

    public void setFileProformaPnbp(String fileProformaPnbp) {
        this.fileProformaPnbp = fileProformaPnbp;
    }

    public String getFileProformaJasa() {
        return fileProformaJasa;
    }

    public void setFileProformaJasa(String fileProformaJasa) {
        this.fileProformaJasa = fileProformaJasa;
    }

    public String getFileBillpymPnbp() {
        return fileBillpymPnbp;
    }

    public void setFileBillpymPnbp(String fileBillpymPnbp) {
        this.fileBillpymPnbp = fileBillpymPnbp;
    }

    public String getFileBillpymJasa() {
        return fileBillpymJasa;
    }

    public void setFileBillpymJasa(String fileBillpymJasa) {
        this.fileBillpymJasa = fileBillpymJasa;
    }

    public String getFileBulk() {
        return fileBulk;
    }

    public void setFileBulk(String fileBulk) {
        this.fileBulk = fileBulk;
    }

    public String getFileTax() {
        return fileTax;
    }

    public void setFileTax(String fileTax) {
        this.fileTax = fileTax;
    }

    public String getFileTxt() {
        return fileTxt;
    }

    public void setFileTxt(String fileTxt) {
        this.fileTxt = fileTxt;
    }

    public String getPaymentTrxNo() {
        return paymentTrxNo;
    }

    public void setPaymentTrxNo(String paymentTrxNo) {
        this.paymentTrxNo = paymentTrxNo;
    }

    public LocalDate getPaymentTrxDate() {
        return paymentTrxDate;
    }

    public void setPaymentTrxDate(LocalDate paymentTrxDate) {
        this.paymentTrxDate = paymentTrxDate;
    }

    public String getTaxTrxNo() {
        return taxTrxNo;
    }

    public void setTaxTrxNo(String taxTrxNo) {
        this.taxTrxNo = taxTrxNo;
    }

    public LocalDate getTaxTrxDate() {
        return taxTrxDate;
    }

    public void setTaxTrxDate(LocalDate taxTrxDate) {
        this.taxTrxDate = taxTrxDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Invoice status(Integer status) {
        this.status = status;
        return this;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Invoice updateBy(String updateBy) {
        this.updateBy = updateBy;
        return this;
    }

    public ZonedDateTime getUpdateOn() {
        return updateOn;
    }

    public void setUpdateOn(ZonedDateTime updateOn) {
        this.updateOn = updateOn;
    }

    public Invoice updateOn(ZonedDateTime updateOn) {
        this.updateOn = updateOn;
        return this;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getAmountJasaTerima() {
		return amountJasaTerima;
	}

	public void setAmountJasaTerima(Long amountJasaTerima) {
		this.amountJasaTerima = amountJasaTerima;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Invoice invoice = (Invoice) o;
        if (invoice.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, invoice.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Invoice{" +
            "id=" + id +
            ", type=" + type +
            ", tanggal=" + tanggal +
            ", tglOrder=" + tglOrder +
            ", jmlOrder=" + jmlOrder +
            ", jmlOrderCancel=" + jmlOrderCancel +
            ", noProformaPnbp='" + noProformaPnbp + '\'' +
            ", noProformaJasa='" + noProformaJasa + '\'' +
            ", noBillpymPnbp='" + noBillpymPnbp + '\'' +
            ", noBillpymJasa='" + noBillpymJasa + '\'' +
            ", amountPnbp=" + amountPnbp +
            ", taxPnbp=" + taxPnbp +
            ", totalAmountPnbp=" + totalAmountPnbp +
            ", amountJasa=" + amountJasa +
            ", vatJasa=" + vatJasa +
            ", taxJasa=" + taxJasa +
            ", totalAmountJasa=" + totalAmountJasa +
            ", fileProformaPnbp='" + fileProformaPnbp + '\'' +
            ", fileProformaJasa='" + fileProformaJasa + '\'' +
            ", fileBillpymPnbp='" + fileBillpymPnbp + '\'' +
            ", fileBillpymJasa='" + fileBillpymJasa + '\'' +
            ", fileBulk='" + fileBulk + '\'' +
            ", fileTax='" + fileTax + '\'' +
            ", fileTxt='" + fileTxt + '\'' +
            ", paymentTrxNo='" + paymentTrxNo + '\'' +
            ", paymentTrxDate=" + paymentTrxDate +
            ", taxTrxNo='" + taxTrxNo + '\'' +
            ", taxTrxDate=" + taxTrxDate +
            ", status=" + status +
            ", updateBy='" + updateBy + '\'' +
            ", updateOn=" + updateOn +
            ", amountJasaTerima=" + amountJasaTerima +
            '}';
    }
}
