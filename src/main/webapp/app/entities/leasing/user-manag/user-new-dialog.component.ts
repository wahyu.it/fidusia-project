import { Component, OnInit  } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from 'app/core/user/user.service';
import { User } from 'app/core/user/user.model';
import { FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { IKeyCabangDTO } from  'app/shared/model/keycabangDTO.model';
import { IKeyNotarisDTO } from  'app/shared/model/keyNotarisDTO.model';


@Component({
  selector: 'jhi-user-new-dialog',
  templateUrl: './user-new-dialog.component.html',
  styleUrls: ['usermanage-dialog.scss']
})
export class UserNewDialogComponent implements OnInit  {
  user?: User;
  isSaving = false;
  cabangs: IKeyCabangDTO[] = [];
  notarises: IKeyNotarisDTO[] = [];
  authorities: string[] = [];
  roleCabang = false;
  pEmail = false;
  pLogin = false;
  roleNotaris = false;
  
  createForm = this.fb.group({
    firstname: ['', [Validators.required]],
    lastname: ['', [Validators.required]],
    login: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(50), Validators.pattern('^[_.@A-Za-z0-9-]*$')]],
    email: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(254), Validators.email]],
    authorities: [],
    cabangs: [],
    notaris:[],
    pass: ['', [Validators.maxLength(50), Validators.pattern(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*#?&^_-]).{8,}/)]],
  });

  constructor(
	  public activeModal: NgbActiveModal, 
      protected userService: UserService,
	  private fb: FormBuilder
    ) {
	    this.userService.queryCabang().subscribe(cabangs => {
	      	this.cabangs = cabangs;
	    });
	    
	    this.userService.authorities().subscribe(authorities => {
	      this.authorities = authorities;
	    });
	    
	    this.userService.queryNotaris().subscribe(notarises => {
	      this.notarises = notarises;
	    });
	}

  ngOnInit(): void {
	  
  }
  
  onChange(e: any): void  {
	  if ('ROLE_OFFICER' === e) {
	  		this.roleCabang = true;
	  } else if (('ROLE_PARTNER' === e) || ('ROLE_PARTNER_STAFF' === e)) {
		    this.roleNotaris = true;
	  		this.roleCabang = false;
	  } else {
	  		this.roleCabang = false;
	  		this.roleNotaris = false;
	  }
  }
  
  simpan(): void {
	  this.swalSimpan();
  }
  
  email(): void {
	    const email = this.createForm.get(['email'])!.value;
	    this.userService.checkEmail(email).subscribe(result => {
	 	this.pEmail = result
	    });
  }
  
  login(): void {
	    const login = this.createForm.get(['email'])!.value;
	    this.userService.checkUsername(login).subscribe(result => {
			this.pLogin = result;
	    });
  }
  
  swalSimpan(): void {
    const email = this.createForm.get(['email'])!.value;
    const login = this.createForm.get(['login'])!.value;
    this.userService.checkUsername(login).subscribe(result => {
 		if (result) {
			 Swal.fire({ 
		      icon: 'warning',
		      text: 'Periksa kembali username anda, Username sudah digunakan !!!',
			  confirmButtonText: "Yes!",
		    });
		 } else {
		    this.userService.checkEmail(email).subscribe(rest => {
				if (rest){
					 Swal.fire({ 
				      icon: 'warning',
				      text: 'Periksa kembali email anda, Email sudah digunakan !!!',
			  		  confirmButtonText: "Yes!",
				    });
				} else {
					 Swal.fire({
					      icon: 'warning',
					      text: 'Pastikan email anda sudah benar, email aktivasi akan dikirim ke email terdaftar!',
						  showCancelButton: true,
						  confirmButtonText: "Yes, save it!",
						  cancelButtonText: "No, cancel!",
						  reverseButtons: false
						}).then((result2) => {
						//   Read more about isConfirmed, isDenied below 
						  if (result2.isConfirmed) {
								const firstname = this.createForm.get(['firstname'])!.value;
								const lastname = this.createForm.get(['lastname'])!.value;
								const username = this.createForm.get(['login'])!.value;
								const password = this.createForm.get(['pass'])!.value;
								const mail = this.createForm.get(['email'])!.value;
								const role = this.createForm.get(['authorities'])!.value;
								const cabang = this.createForm.get(['cabangs'])!.value != null ? this.createForm.get(['cabangs'])!.value: '';
								const not = this.createForm.get(['notaris'])!.value != null ? this.createForm.get(['notaris'])!.value: '';
								this.userService.createUser(firstname, lastname, username, password, mail, role, cabang, not).subscribe( a => {
									console.log('a : ' + a);
								});
						    Swal.fire("Saved!", "", "success");
						    this.clear();
						  } else if (result2.isDismissed) {
						    Swal.fire("Changes are not saved", "", "info");
						  }
						});
				}
		    });
		 }
    });
  }
  
  create() : void {
	const firstname = this.createForm.get(['firstname'])!.value;
	const lastname = this.createForm.get(['lastname'])!.value;
	const username = this.createForm.get(['login'])!.value;
	const password = this.createForm.get(['pass'])!.value;
	const email = this.createForm.get(['email'])!.value;
	const role = this.createForm.get(['authorities'])!.value;
	const cabang = this.createForm.get(['cabang'])!.value;
	const not = this.createForm.get(['notaris'])!.value != null ? this.createForm.get(['notaris'])!.value: '';
	this.userService.createUser(firstname, lastname, username, password, email, role, cabang, not).subscribe(rest => {
		console.log('rest: ' + rest);
	});
  }

  clear(): void {
    this.activeModal.dismiss();
  }
}
