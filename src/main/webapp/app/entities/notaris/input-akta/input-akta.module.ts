import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasimonitoringSharedModule } from 'app/shared/shared.module';
import { InputAktaComponent } from './input-akta.component';
import { iputaktaState } from './input-akta.route';
import { CalendarHeaderComponent } from './calendar-header.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

@NgModule({
  imports: [SisteminformasimonitoringSharedModule,
  			RouterModule.forChild(iputaktaState),
		    CalendarModule.forRoot({
		      provide: DateAdapter,
		      useFactory: adapterFactory
		    })],
  declarations: [InputAktaComponent,CalendarHeaderComponent]
})
export class InputAktaModule {}
