package com.fidusia.app.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fidusia.app.domain.Cabang;
import com.fidusia.app.domain.EntityUser;
import com.fidusia.app.domain.Notaris;
import com.fidusia.app.domain.util.RecordStatus;
import com.fidusia.app.repository.EntityUserRepository;
import com.fidusia.app.security.SecurityUtils;


@Service
public class CabangService {

    private final Logger log = LoggerFactory.getLogger(CabangService.class);

    private final EntityUserRepository entityUserRepository;
    
    public CabangService(EntityUserRepository entityUserRepository) {
		this.entityUserRepository = entityUserRepository;
	}

    public Cabang findByUserLogin() {
        Cabang cabang = null;

        EntityUser entityUser = entityUserRepository.findOneByRecordStatusAndUserLogin(
            RecordStatus.ACTIVE.value(),
            SecurityUtils.getCurrentUserLogin().get()
        );

        if(entityUser != null) {
            cabang = entityUser.getCabang();
        }

        return cabang;
    }

    public Notaris findNotarisByUserLogin() {
    	Notaris notaris = null;

        EntityUser entityUser = entityUserRepository.findOneByRecordStatusAndUserLogin(
            RecordStatus.ACTIVE.value(),
            SecurityUtils.getCurrentUserLogin().get()
        );

        if(entityUser != null) {
            notaris = entityUser.getNotaris();
        }

        return notaris;
    }
}
