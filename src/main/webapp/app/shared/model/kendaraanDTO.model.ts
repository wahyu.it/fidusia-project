export interface IKendaraankDTO {
  id?: number;
  merk?: string;
  tipe?: string;
  roda?: number;
  tahunPembuatan?: string;
  noRangka?: string;
  noMesin?: string;
}

export class KendaraankDTO implements IKendaraankDTO {
  constructor(
    public id?: number,
    public merk?: string,
    public tipe?: string,
    public roda?: number,
    public tahunPembuatan?: string,
    public noRangka?: string,
    public noMesin?: string,
  ) {}
}
