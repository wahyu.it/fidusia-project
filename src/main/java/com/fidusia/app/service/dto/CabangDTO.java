package com.fidusia.app.service.dto;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;


/**
 * A DTO for the Cabang entity.
 */
public class CabangDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 5)
    private String kode;

    @NotNull
    @Size(max = 50)
    private String nama;

    @NotNull
    @Size(max = 200)
    private String alamat;

    @NotNull
    @Size(max = 50)
    private String provinsi;

    @NotNull
    @Size(max = 50)
    private String kota;

    @NotNull
    @Size(max = 200)
    private String pengadilanNegeri;

    @NotNull
    @Size(max = 20)
    private String noKontak;

    @Max(value = 9)
    private Integer recordStatus;

    @Size(max = 50)
    private String updateBy;

    private ZonedDateTime updateOn;

    private Long leasingId;

    @NotNull
    private LocalDate startTanggal;

    @Size(max = 5)
    private String kodeCabangSyariah;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }
    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }
    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }
    public String getPengadilanNegeri() {
        return pengadilanNegeri;
    }

    public void setPengadilanNegeri(String pengadilanNegeri) {
        this.pengadilanNegeri = pengadilanNegeri;
    }
    public String getNoKontak() {
        return noKontak;
    }

    public void setNoKontak(String noKontak) {
        this.noKontak = noKontak;
    }
    public Integer getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(Integer recordStatus) {
        this.recordStatus = recordStatus;
    }
    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }
    public ZonedDateTime getUpdateOn() {
        return updateOn;
    }

    public void setUpdateOn(ZonedDateTime updateOn) {
        this.updateOn = updateOn;
    }

    public Long getLeasingId() {
        return leasingId;
    }

    public void setLeasingId(Long leasingId) {
        this.leasingId = leasingId;
    }

    public LocalDate getStartTanggal() {
        return startTanggal;
    }

    public void setStartTanggal(LocalDate startTanggal) {
        this.startTanggal = startTanggal;
    }

    public String getKodeCabangSyariah() {
        return kodeCabangSyariah;
    }

    public void setKodeCabangSyariah(String kodeCabangSyariah) {
        this.kodeCabangSyariah = kodeCabangSyariah;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CabangDTO cabangDTO = (CabangDTO) o;

        if ( ! Objects.equals(id, cabangDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "CabangDTO{" +
            "id=" + id +
            ", kode='" + kode + "'" +
            ", nama='" + nama + "'" +
            ", alamat='" + alamat + "'" +
            ", provinsi='" + provinsi + "'" +
            ", kota='" + kota + "'" +
            ", pengadilanNegeri='" + pengadilanNegeri + "'" +
            ", noKontak='" + noKontak + "'" +
            ", recordStatus='" + recordStatus + "'" +
            ", updateBy='" + updateBy + "'" +
            ", updateOn='" + updateOn + "'" +
            '}';
    }
}
