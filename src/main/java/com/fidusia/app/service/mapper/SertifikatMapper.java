package com.fidusia.app.service.mapper;
//
//import com.fidusia.app.domain.*;
//import com.fidusia.app.service.dto.SertifikatDTO;
//
import org.mapstruct.*;
//import java.util.List;
//
///**
// * Mapper for the entity Sertifikat and its DTO SertifikatDTO.
// */
@Mapper(componentModel = "spring", uses = {})
public interface SertifikatMapper {
//
//    @Mapping(source = "akta.id", target = "aktaId")
//    @Mapping(source = "akta.notaris.id", target = "aktaNotarisId")
//    @Mapping(source = "akta.notaris.namaShort", target = "aktaNotarisNama")
//    @Mapping(source = "akta.notaris.wilayahKerja", target = "aktaNotarisWilayahKerja")
//    @Mapping(source = "akta.tglOrder", target = "aktaTglOrder")
//    @Mapping(source = "akta.kode", target = "aktaKode")
//    @Mapping(source = "akta.nomor", target = "aktaNomor")
//    @Mapping(source = "akta.tanggal", target = "aktaTanggal")
//    @Mapping(source = "akta.penerimaKuasa.nama", target = "aktaPenerimaKuasaNama")
//    @Mapping(source = "akta.ppk.nilaiPenjaminan", target = "aktaPpkNilaiPenjaminan")
//    @Mapping(source = "akta.ppk.nomor", target = "ppkNomor")
//    @Mapping(source = "akta.ppk.tanggal", target = "ppkTanggal")
//    @Mapping(source = "akta.ppk.nsbNama", target = "ppkNsbNama")
//    @Mapping(source = "akta.ppk.nsbNamaDebitur", target = "ppkNsbNamaDebitur")
//    @Mapping(source = "akta.ppk.nsbJenis", target = "ppkNsbJenis")
//    @Mapping(source = "akta.skSubtitusi", target = "aktaSkSubtitusi")
//    @Mapping(source = "cabang.nama", target = "cabangName")
//    @Mapping(source = "cabang.id", target = "cabangId")
//    @Mapping(source = "invoice.id", target = "invoiceId")
//    @Mapping(source = "invoice.tanggal", target = "invoiceTanggal")
////    @Mapping(source = "perbaikan", target = "perbaikan")
////    @Mapping(source = "tglPerbaikan", target = "tglPerbaikan")
////    @Mapping(source = "statusPerbaikan", target = "statusPerbaikan")
////    @Mapping(source = "noNewSertifikat", target = "noNewSertifikat")
////    @Mapping(source = "lampiranObject", target = "lampiranObject")
//    SertifikatDTO sertifikatToSertifikatDTO(Sertifikat sertifikat);
//
//    List<SertifikatDTO> sertifikatsToSertifikatDTOs(List<Sertifikat> sertifikats);
//
//    @Mapping(source = "aktaId", target = "akta")
//    @Mapping(source = "cabangId", target = "cabang")
//    @Mapping(source = "invoiceId", target = "invoice")
//    Sertifikat sertifikatDTOToSertifikat(SertifikatDTO sertifikatDTO);
//
//    List<Sertifikat> sertifikatDTOsToSertifikats(List<SertifikatDTO> sertifikatDTOs);
//
//    default Akta aktaFromId(Long id) {
//        if (id == null) {
//            return null;
//        }
//        Akta akta = new Akta();
//        akta.setId(id);
//        return akta;
//    }
//
//    default Cabang cabangFromId(Long id) {
//        if (id == null) {
//            return null;
//        }
//        Cabang cabang = new Cabang();
//        cabang.setId(id);
//        return cabang;
//    }
//
//    default Invoice invoiceFromId(Long id) {
//        if (id == null) {
//            return null;
//        }
//        Invoice invoice = new Invoice();
//        invoice.setId(id);
//        return invoice;
//    }
}
