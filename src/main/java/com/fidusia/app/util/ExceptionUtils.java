package com.fidusia.app.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionUtils {
	
	private final static int SHORT_TRACE_LIMIT = 1000;
	
	public static String getFullStackTrace(Throwable e) {
		if(e == null) return null;
		
		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		return sw.toString();
	}
	
	public static String getStackTrace(Throwable e) {
		String str = getFullStackTrace(e);
		return str.substring(0, Math.min(SHORT_TRACE_LIMIT, str.length()));
	}
	
	public static String getMessage(Throwable e) {
		if(e.getMessage()!=null) return e.getMessage();
		if(e.getCause()!=null) return e.getCause().toString();
		
		return "Throw " + e.getClass().getSimpleName();
	}
}
