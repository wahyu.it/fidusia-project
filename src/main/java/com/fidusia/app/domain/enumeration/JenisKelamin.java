package com.fidusia.app.domain.enumeration;

/**
 * The JenisKelamin enumeration.
 */
public enum JenisKelamin {
    LAKILAKI, PEREMPUAN
}
