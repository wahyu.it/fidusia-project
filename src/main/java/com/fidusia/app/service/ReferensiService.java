package com.fidusia.app.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fidusia.app.domain.Cabang;
import com.fidusia.app.domain.Leasing;
import com.fidusia.app.domain.Referensi;
import com.fidusia.app.domain.util.RecordStatus;
import com.fidusia.app.repository.CabangRepository;
import com.fidusia.app.repository.LeasingRepository;
import com.fidusia.app.repository.ReferensiRepository;

//import com.pitoesi.fidusia.service.dto.JobParameter;

@Service
public class ReferensiService {

    private final Logger log = LoggerFactory.getLogger(ReferensiService.class);
    
    private final static String applicationProp = "APPLICATION_PROPERTIES";
    
    private Map<String, String> map = new HashMap<String, String>();
	
	private Leasing leasing;
    
	private final ReferensiRepository referensiRepository;
	
	private final LeasingRepository leasingRepository;
	
	private final CabangRepository cabangRepository;
	
//	private final JobInstanceService jobInstanceService;
	
	public ReferensiService (
			ReferensiRepository referensiRepository,
			LeasingRepository leasingRepository,
			CabangRepository cabangRepository
//			JobInstanceService jobInstanceService
			) {
		this.referensiRepository = referensiRepository;
		this.leasingRepository = leasingRepository;
		this.cabangRepository = cabangRepository;
//		this.jobInstanceService = jobInstanceService;
	}
	
	public void reload() {
		boolean force = true;
//
//		log.debug("Request to reload force:{}", force);
//
//		log.debug("Size before reload:{}", map.size());
		
		List<Referensi> referensiList = (List<Referensi>) referensiRepository.findAll();

		leasing = leasingRepository.findOneByRecordStatus(RecordStatus.ACTIVE.value());

		List<Cabang> cabangs = cabangRepository.findByRecordStatus(RecordStatus.ACTIVE.value());
		for (Cabang cabang : cabangs) {
			Referensi r = new Referensi()
					.category("CABANG")
					.name(cabang.getNama())
					.value(cabang.getId().toString());
			referensiList.add(r);

			Referensi r2 = new Referensi()
					.category("KODE_CABANG")
					.name(cabang.getKode())
					.value(cabang.getId().toString());
			referensiList.add(r2);
		}
		
		if(force)  map = new HashMap<String, String>();
    	
    	for (Referensi ref : referensiList) {
    		map.put(concat(ref.getCategory(),ref.getName()), ref.getValue());
		}

//		String result = "Size after reload: " + map.size();
//		log.debug(result);

//        jobInstanceService.updateStatus(parameters.get("jobid"), "SUCCESS", parameters.toString(), result);
	}
    
    public String applicationProp(String name) {
    	return map.get(concat(applicationProp, name));
    }
    
    public String applicationProp1(String name) {
    	return referensiRepository.findByCategoryAndName(applicationProp, name).get(0).getValue();
    }
    
    public String valueOf(String group, String name) {
    	return map.get(concat(group, name));
    }
    
    public int intValue(String group, String name) {
    	return Integer.parseInt(map.get(concat(group, name)));
    }
    
    public long longValue(String group, String name) {
    	return Long.parseLong(map.get(concat(group, name)));
    }
    
    public boolean booleanValue(String group, String name) {
    	return Boolean.valueOf(map.get(concat(group, name)));
    }
    
    public static String concat(String... strings) {
    	StringBuilder sb = new StringBuilder();
    	for (String string : strings) {
			sb.append(".").append(string);
		}
    	
    	return sb.deleteCharAt(0).toString();
    }

	public Leasing getLeasing() {
		return leasing;
	}
	
//    public String applicationProp(String name, String usedFor) {
//		return getValue(applicationProp, name, usedFor);
//    }
    
//    public String getValue(String category, String name, String usedFor) {
//		List<Referensi> refs = referensiRepository.findByCategoryAndNameAndUsedFor(category, name, usedFor);
//    	return refs != null && refs.size() > 0 ? refs.get(0).getValue()  : null;
//    }
}
