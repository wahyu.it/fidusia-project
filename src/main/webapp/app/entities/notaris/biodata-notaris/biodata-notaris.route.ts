import { Routes } from '@angular/router';
import { BiodataNotarisComponent } from './biodata-notaris.component';
import { JhiResolvePagingParams } from 'ng-jhipster';

export const biodataNotarisState: Routes = [{
  path: '',
  component: BiodataNotarisComponent,
  resolve: {
	  pagingParams: JhiResolvePagingParams
  }
  }
];
