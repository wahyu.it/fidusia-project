import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { IBerkasDTO } from 'app/shared/model/berkasDTO.model';
import { ICabangDTO } from 'app/shared/model/cabangDTO.model';

type EntityArrayResponseType = HttpResponse<IBerkasDTO[]>;

@Injectable({ providedIn: 'root' })
export class UploadWarkahService {
  	  public resourceUrlBerkas = SERVER_API_URL + 'api/berkas/search';
  	  public resourceUrlCabang = SERVER_API_URL + 'api/cabangs/getByLogin';
  	  public resourceUrlPreviewBerkas = SERVER_API_URL + 'api/file/previewberkasfile';
  	  public resourceUrlPreviewBerkas2 = SERVER_API_URL + 'api/file/previewberkas';
	
	  constructor(protected http: HttpClient) {}

	  search(req?: any): Observable<EntityArrayResponseType> {
	    const options = createRequestOption(req);
	    return this.http
	      .get<IBerkasDTO[]>(this.resourceUrlBerkas, { params: options, observe: 'response' })
	      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
	  }

	  cabangLogin(): Observable<ICabangDTO> {
	    return this.http.get<ICabangDTO>(this.resourceUrlCabang);
	  }
	  
	 viewBerkas(ppkNomor: string): Observable<Blob> {
	    console.log(ppkNomor);
	    return this.http.get<Blob>(this.resourceUrlPreviewBerkas + (ppkNomor ? `?fileName=${ppkNomor}` : ''), {
	      observe: 'body',
	      responseType: 'blob' as 'json'
	    });
	  }
	  
	 viewBerkas2(ppkNomor: string): Observable<Blob> {
	    return this.http.get<Blob>(this.resourceUrlPreviewBerkas2 + (ppkNomor ? `?fileName=${ppkNomor}` : ''), {
	      observe: 'body',
	      responseType: 'blob' as 'json'
	    });
	  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
	    if (res.body) {
	      res.body.forEach((r: IBerkasDTO) => {
	        r.tanggalPpk = r.tanggalPpk ? moment(r.tanggalPpk) : undefined;
	        r.uploadOn = r.uploadOn ? moment(r.uploadOn) : undefined;
	      });
	    }
    return res;
  }
	
}