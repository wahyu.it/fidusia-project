import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { TransaksiComponentsPage, TransaksiDeleteDialog, TransaksiUpdatePage } from './transaksi.page-object';

const expect = chai.expect;

describe('Transaksi e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let transaksiComponentsPage: TransaksiComponentsPage;
  let transaksiUpdatePage: TransaksiUpdatePage;
  let transaksiDeleteDialog: TransaksiDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Transaksis', async () => {
    await navBarPage.goToEntity('transaksi');
    transaksiComponentsPage = new TransaksiComponentsPage();
    await browser.wait(ec.visibilityOf(transaksiComponentsPage.title), 5000);
    expect(await transaksiComponentsPage.getTitle()).to.eq('sisteminformasiakademikApp.transaksi.home.title');
    await browser.wait(ec.or(ec.visibilityOf(transaksiComponentsPage.entities), ec.visibilityOf(transaksiComponentsPage.noResult)), 1000);
  });

  it('should load create Transaksi page', async () => {
    await transaksiComponentsPage.clickOnCreateButton();
    transaksiUpdatePage = new TransaksiUpdatePage();
    expect(await transaksiUpdatePage.getPageTitle()).to.eq('sisteminformasiakademikApp.transaksi.home.createOrEditLabel');
    await transaksiUpdatePage.cancel();
  });

  it('should create and save Transaksis', async () => {
    const nbButtonsBeforeCreate = await transaksiComponentsPage.countDeleteButtons();

    await transaksiComponentsPage.clickOnCreateButton();

    await promise.all([
      transaksiUpdatePage.setNomorInput('nomor'),
      transaksiUpdatePage.setTanggalInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      transaksiUpdatePage.metodeSelectLastOption(),
      transaksiUpdatePage.bankSelectLastOption(),
      transaksiUpdatePage.setNomorRekeningInput('nomorRekening'),
      transaksiUpdatePage.setVirtualAccountInput('virtualAccount'),
      transaksiUpdatePage.statusSelectLastOption(),
      transaksiUpdatePage.setUpdateByInput('updateBy'),
      transaksiUpdatePage.setUpdateOnInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      transaksiUpdatePage.tagihanSelectLastOption()
    ]);

    expect(await transaksiUpdatePage.getNomorInput()).to.eq('nomor', 'Expected Nomor value to be equals to nomor');
    expect(await transaksiUpdatePage.getTanggalInput()).to.contain('2001-01-01T02:30', 'Expected tanggal value to be equals to 2000-12-31');
    expect(await transaksiUpdatePage.getNomorRekeningInput()).to.eq(
      'nomorRekening',
      'Expected NomorRekening value to be equals to nomorRekening'
    );
    expect(await transaksiUpdatePage.getVirtualAccountInput()).to.eq(
      'virtualAccount',
      'Expected VirtualAccount value to be equals to virtualAccount'
    );
    expect(await transaksiUpdatePage.getUpdateByInput()).to.eq('updateBy', 'Expected UpdateBy value to be equals to updateBy');
    expect(await transaksiUpdatePage.getUpdateOnInput()).to.contain(
      '2001-01-01T02:30',
      'Expected updateOn value to be equals to 2000-12-31'
    );

    await transaksiUpdatePage.save();
    expect(await transaksiUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await transaksiComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Transaksi', async () => {
    const nbButtonsBeforeDelete = await transaksiComponentsPage.countDeleteButtons();
    await transaksiComponentsPage.clickOnLastDeleteButton();

    transaksiDeleteDialog = new TransaksiDeleteDialog();
    expect(await transaksiDeleteDialog.getDialogTitle()).to.eq('sisteminformasiakademikApp.transaksi.delete.question');
    await transaksiDeleteDialog.clickOnConfirmButton();

    expect(await transaksiComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
