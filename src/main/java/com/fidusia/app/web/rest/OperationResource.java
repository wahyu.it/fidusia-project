package com.fidusia.app.web.rest;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fidusia.app.domain.Akta;
import com.fidusia.app.domain.Berkas;
import com.fidusia.app.domain.Kabupaten;
import com.fidusia.app.domain.Notaris;
import com.fidusia.app.domain.OrderData;
import com.fidusia.app.domain.Ppk;
import com.fidusia.app.domain.Sertifikat;
import com.fidusia.app.domain.util.BerkasStatus;
import com.fidusia.app.domain.util.NasabahJenis;
import com.fidusia.app.domain.util.SertifikatStatus;
import com.fidusia.app.repository.AktaRepository;
import com.fidusia.app.repository.BerkasRepository;
import com.fidusia.app.repository.KabupatenRepository;
import com.fidusia.app.repository.NotarisRepository;
import com.fidusia.app.repository.OrderDataRepository;
import com.fidusia.app.repository.PpkRepository;
import com.fidusia.app.repository.RawDataRepository;
import com.fidusia.app.repository.SertifikatRepository;
import com.fidusia.app.service.AhuPendaftaranService;
import com.fidusia.app.service.FixingBadanOrangService;
import com.fidusia.app.service.GetDokumentService;
import com.fidusia.app.service.MinutaService;
import com.fidusia.app.service.OrderNotarisService;
import com.fidusia.app.service.RawDataService;
import com.fidusia.app.service.ResultService;
import com.fidusia.app.service.SalinanService;
import com.fidusia.app.service.SlackInhookService;
import com.fidusia.app.util.LogUtils;
import com.fidusia.app.web.rest.util.CommonResponse;

@RestController
@RequestMapping("/api/operation")
@Transactional
public class OperationResource {

   private final Logger log = LoggerFactory.getLogger(OperationResource.class);
    
   private final RawDataService getRawDataService;
   
   private final KabupatenRepository kabupatenRepository;

   private final AhuPendaftaranService ahuPendaftaranService;
   
   private final RawDataRepository rawDataRepository;
   
   private final MinutaService minutaService;
   
   private final ResultService resultService;
   
   private final AktaRepository aktaRepository;
   
   private final SertifikatRepository sertifikatRepository;
   
   private final OrderDataRepository  orderDataRepository;
   
   private final PpkRepository ppkRepository;
   
   private final OrderNotarisService orderNotarisService;
   
   private final NotarisRepository notarisRepository;
   
   private final GetDokumentService getDokumentService;
   
   private final BerkasRepository berkasRepository;
   
   private final SalinanService salinanService;
   
   private final FixingBadanOrangService fixingBadanOrangService;
   
   private final SlackInhookService slackInhookService;

   private final RawDataService rawDataService;
   
   public OperationResource(
		   FixingBadanOrangService fixingBadanOrangService,
		   BerkasRepository berkasRepository,
		   ResultService resultService,
		   MinutaService minutaService,
		   RawDataService getRawDataService,
		   KabupatenRepository kabupatenRepository,
		   AhuPendaftaranService ahuPendaftaranService,
		   OrderDataRepository orderDataRepository,
		   RawDataRepository rawDataRepository,
		   AktaRepository aktaRepository,
		   SertifikatRepository sertifikatRepository,
		   PpkRepository ppkRepository,
		   OrderNotarisService orderNotarisService,
		   NotarisRepository notarisRepository,
		   GetDokumentService getDokumentService,
		   SalinanService salinanService,
		   SlackInhookService slackInhookService,
		   RawDataService rawDataService
		   ) {
	   this.minutaService = minutaService;
	   this.getRawDataService = getRawDataService;
	   this.kabupatenRepository = kabupatenRepository;
	   this.ahuPendaftaranService = ahuPendaftaranService;
	   this.rawDataRepository = rawDataRepository;
	   this.aktaRepository = aktaRepository;
	   this.resultService = resultService;
	   this.sertifikatRepository = sertifikatRepository;
	   this.orderDataRepository = orderDataRepository;
	   this.ppkRepository = ppkRepository;
	   this.orderNotarisService = orderNotarisService;
	   this.notarisRepository = notarisRepository;
	   this.getDokumentService = getDokumentService;
	   this.berkasRepository = berkasRepository;
	   this.salinanService = salinanService;
	   this.fixingBadanOrangService = fixingBadanOrangService;
	   this.slackInhookService = slackInhookService;
	   this.rawDataService = rawDataService;
   }
    
    @PostMapping("/token")
    public String getToken() {
    	log.debug("REST request to get token mtf");
    	
    	String token = getRawDataService.requestToken();
    	log.debug("token: {}", token);
    	return token;
    }
    
    @PostMapping("/rawdata")
    public ResponseEntity<String> getRawData(
			@RequestParam(value = "orderdate", required = true) String orderdate) {
    	log.debug("REST request to get token mtf");
    	
    	String service = "/api/operation/rawdata";
        String _orderdate = (orderdate!=null && orderdate.length()>0) ? "orderdate=" + orderdate + "&" : "";
        String parameter = _orderdate;
		ZonedDateTime startTime = ZonedDateTime.now();
        CommonResponse response = new CommonResponse(service, parameter, startTime);
    	
    	getRawDataService.getRawData(orderdate != null ?LocalDate.parse(orderdate): LocalDate.now(), "P");
    	getRawDataService.getRawData(orderdate != null ?LocalDate.parse(orderdate): LocalDate.now(), "C");

		List<OrderData> totalInput = orderDataRepository.findByTanggal(LocalDate.parse(orderdate));
		int orang = 0;
		int badan = 0;
		for (OrderData orderData : totalInput) {
			if (NasabahJenis.PERORANGAN.value() == orderData.getNasabahJenis().intValue()) {
				orang++;
			} else {
				badan++;
			}
		}
    	
    	StringBuilder sb = new StringBuilder();
		sb.append("\n* GET RAW DATA").append(" *");
		sb.append("\nsuccess      ").append(totalInput.size() > 0 ? totalInput.size() : 0);
		sb.append("\norang    	").append(orang);
		sb.append("\nbadan    	").append(badan);

		LogUtils.debug(response = response.complete("COMPLETED", sb.toString()), log, slackInhookService);
        
        return ResponseEntity.status(HttpStatus.OK).body(response.toString());
    }
    
    @PostMapping("/kabupaten")
    public String updateProvinsi(
			@RequestParam(value = "nama", required = true) String nama) {
    	log.debug("REST request to updateProvinsi");
    	
    	String prov = null;
    	List<Kabupaten> token = kabupatenRepository.findByNamaLike(nama);
    	for (Kabupaten kabupaten : token) {
    		prov = kabupaten.getProvinsi().getName();
		}
    	return prov;
    }
    
    @PostMapping("/getkodevoucher")
    public String getkodevoucher(
			@RequestParam(value = "sessionId", required = false) String sessionId,
			@RequestParam(value = "registrasiId", required = false) String registrasiId) throws Exception  {
    	log.debug("REST request to updateProvinsi");
    	
    	String prov = ahuPendaftaranService.getVoucherMandiri(sessionId, registrasiId);

    	return prov;
    }
    
    @PostMapping("/getkodevoucherfile")
    public String getkodevoucherfile(
			@RequestParam(value = "sessionId", required = false) String sessionId,
			@RequestParam(value = "registrationid", required = false) String registrationid,
			@RequestParam(value = "ppkNomor", required = false) String ppkNomor) {
    	log.debug("REST request to updateProvinsi");
    	
    	String prov = ahuPendaftaranService.getVoucherFile(sessionId, registrationid, ppkNomor );

    	return prov;
    }
    
    @PostMapping("/order/create")
    public String createOrder(
			@RequestParam(value = "notarisid", required = true) String notarisid,
			@RequestParam(value = "tglAkta", required = true) String tglAkta,
			@RequestParam(value = "nomorAkta", required = true) String nomorAkta,
			@RequestParam(value = "pukulAwal", required = true) String pukulAwal,
			@RequestParam(value = "jmlOrder", required = true) String jmlOrder) {
    	log.debug("REST request to create order notarisid:{} tglAkta:{} nomorAkta:{} pukulAwal:{} jmlOrder:{}");
    	
    	Notaris notaris = notarisRepository.findById(Long.parseLong(notarisid)).get();
    	
    	orderNotarisService.createOrder(Integer.parseInt(jmlOrder), notaris, LocalDate.now(), LocalDate.parse(tglAkta), nomorAkta, pukulAwal);
    	
    	return null;
    }
    
    @PostMapping("/order/create2")
    public ResponseEntity<String> createOrder2(
			@RequestParam(value = "notarisid", required = true) String notarisid,
			@RequestParam(value = "pukulAwal", required = false) String pukulAwal) {
    	log.debug("REST request to create order notarisid:{} tglAkta:{} nomorAkta:{} pukulAwal:{} jmlOrder:{}");
    	
    	String service = "/api/operation/order/create2";
        String _notarisid = (notarisid!=null && notarisid.length()>0) ? "notarisid=" + notarisid + "&" : "";
        String parameter = _notarisid;
		ZonedDateTime startTime = ZonedDateTime.now();
        CommonResponse response = new CommonResponse(service, parameter, startTime);
    	
    	Notaris notaris = notarisRepository.findById(Long.parseLong(notarisid)).get();
    	
    	orderNotarisService.createOrder(notaris, LocalDate.now());

		LogUtils.debug(response = response.complete("COMPLETED", ""), log, slackInhookService);
        
        return ResponseEntity.status(HttpStatus.OK).body(response.toString());
    }
    
    @PostMapping("/send/data")
    public ResponseEntity<String> sendData(
			@RequestParam(value = "tglorder", required = false) String tglorder,
			@RequestParam(value = "ppkNomor", required = false) String ppkNomor) {
    	log.debug("REST request to create minuta tglorder:{} ppknomor:{}", tglorder, ppkNomor);

        String service = "/api/operation/send/data";
        String _tglOrder = (tglorder!=null && tglorder.length()>0) ? "tglorder= " + tglorder + "&" : "";
        String _ppkNomor = (ppkNomor!=null && ppkNomor.length()>0) ? "ppkNomor= " + ppkNomor : "";
        String parameter = _tglOrder + _ppkNomor;

		ZonedDateTime startTime = ZonedDateTime.now();
        CommonResponse response = new CommonResponse(service, parameter, startTime);
    	
    	try {

        	StringBuilder sb = new StringBuilder();
        	String token = rawDataService.requestToken();
    		if (tglorder != null) {
    			LocalDate to = LocalDate.parse(tglorder);
    			List<Sertifikat> sertifikatList = sertifikatRepository.findByTglOrderAndStatusAndReportSubmitted(to, SertifikatStatus.DONE.value(), false);
    			
    			for (Sertifikat sertifikat : sertifikatList) {
        			resultService.sendResult(sertifikat, token);
				}
    			
        		Long success = sertifikatRepository.countByTglOrderAndReportSubmitted(to,true);
        		Long failed = sertifikatRepository.countByTglOrderAndReportSubmitted(to,false);
        		
        		sb.append("\n* SEND RESULT DATA").append(" *");
        		sb.append("\nsuccess      ").append(success > 0 ? success : 0);
        		sb.append("\nfailed    	").append(failed);
    			
    		} else if (ppkNomor != null) {
    			Sertifikat sertifikat = sertifikatRepository.findByAktaPpkNomor(ppkNomor).get(0);
    			resultService.sendResult(sertifikat, token);
    		}
    		
	        LogUtils.debug(response = response.complete("COMPLETED", sb != null? sb.toString() : null), log, slackInhookService);
	        return ResponseEntity.status(HttpStatus.OK).body(response.toString());
		} catch (Exception e) {
			log.error("Error on register()", e);

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response.toString());
		}
    }
    
    @PostMapping("/minuta/create")
    public ResponseEntity<String> sendDataDinos(
			@RequestParam(value = "tglStart", required = false) String tglStart,
			@RequestParam(value = "tglEnd", required = false) String tglEnd,
			@RequestParam(value = "ppkNomor", required = false) String ppkNomor) {
    	log.debug("REST request generate minuta salinan tgl order {}-{} ppknomor:{} nsbjenis:{}", tglStart, tglEnd, ppkNomor);

        String service = "/api/operation/minuta/create";
        String _tglStart = (tglStart!=null && tglStart.length()>0) ? "tglStart= " + tglStart + "&" : "";
        String _tglEnd = (tglEnd!=null && tglEnd.length()>0) ? "tglEnd= " + tglEnd + "&" : "";
        String _ppkNomor = (ppkNomor!=null && ppkNomor.length()>0) ? "ppkNomor= " + ppkNomor : "";
        String parameter = _tglStart + _tglEnd + _ppkNomor;

		ZonedDateTime startTime = ZonedDateTime.now();
        CommonResponse response = new CommonResponse(service, parameter, startTime);
    	
    	try {
    		
    		if (tglStart != null && tglEnd != null) {
            	LocalDate start = LocalDate.parse(tglStart);
            	LocalDate end = LocalDate.parse(tglEnd);
            	
            	if (start != null) {
    				while(!start.isAfter(end)) {
    	    			List<Akta> aktaList = aktaRepository.findByTglOrder(start);
    		            
    	    			for (Akta akta : aktaList) {
    						minutaService.generateMinutaFile(akta);
    						salinanService.generateSalinanFile(akta);
    					}
    		        	
    		        	start = start.plusDays(1);
    				}
    	        } 
            } else if (ppkNomor != null) {
            	Ppk ppk = ppkRepository.findByNomor(ppkNomor);
            	Akta akta = aktaRepository.findByPpk(ppk);
    			minutaService.generateMinutaFile(akta);
				salinanService.generateSalinanFile(akta);
    		}
    		
	        LogUtils.debug(response = response.complete("COMPLETED", null), log, slackInhookService);
	        return ResponseEntity.status(HttpStatus.OK).body(response.toString());
		} catch (Exception e) {
			log.error("Error on register()", e);

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response.toString());
		}
    }
    
    @PostMapping("/documents/get")
    public ResponseEntity<String> getDokuments(
			@RequestParam(value = "ppkNomor", required = false) String ppkNomor) {
    	log.debug("REST request to dokumens get ppknomor:{}", ppkNomor);

        String service = "/api/operation/send/data";
        String _ppkNomor = (ppkNomor!=null && ppkNomor.length()>0) ? "ppkNomor= " + ppkNomor : "";
        String parameter = _ppkNomor;

		ZonedDateTime startTime = ZonedDateTime.now();
        CommonResponse response = new CommonResponse(service, parameter, startTime);
    	
    	try {

        	String token = rawDataService.requestToken();
    		if (ppkNomor != null) {
    			getDokumentService.getDokument(ppkNomor, token);
    		} else {
    			List<Berkas> berkas = berkasRepository.findByBerkasStatus(BerkasStatus.NONE.value());
    			
    			for (Berkas berkas2 : berkas) {
    				if (berkas2.getBerkasStatus() != BerkasStatus.PARTIAL.value())
    					getDokumentService.getDokument(berkas2.getPpkNomor(), token);
				}
    			
    		}
    		
	        LogUtils.debug(response = response.complete("COMPLETED", null), log, slackInhookService);
	        return ResponseEntity.status(HttpStatus.OK).body(response.toString());
		} catch (Exception e) {
			log.error("Error on getdokuments()", e);

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response.toString());
		}
    }
    
    @PostMapping("/buktilunas")
    public ResponseEntity<String> getbuktilunasfile(
			@RequestParam(value = "sessionId", required = false) String sessionId,
			@RequestParam(value = "tglStart", required = false) String tglStart,
			@RequestParam(value = "tglEnd", required = false) String tglEnd) {
    	log.debug("REST request to buktilunas");
    	
    	String service = "/api/operation/buktilunas";
        String _start = (tglStart!=null && tglStart.length()>0) ? "tglStart= " + tglStart : "";
        String _end = (tglEnd!=null && tglEnd.length()>0) ? "tglEnd= " + tglEnd : "";
        String parameter = _start + _end;

		ZonedDateTime startTime = ZonedDateTime.now();
        CommonResponse response = new CommonResponse(service, parameter, startTime);
    	
    	try {
    		
    		if (tglStart != null && tglEnd != null) {
            	LocalDate start = LocalDate.parse(tglStart);
            	LocalDate end = LocalDate.parse(tglEnd);
            	
            	if (start != null) {
    				while(!start.isAfter(end)) {
    	    			List<Sertifikat> sertifikatList = sertifikatRepository.findByTglOrder(start);
    		            
    	    			for (Sertifikat sertifikat : sertifikatList) {
    	    				Akta akta = sertifikat.getAkta();
    	    				Ppk ppk = akta.getPpk();
    	    		    	ahuPendaftaranService.getBuktiLunasFile(sessionId, sertifikat.getRegistrationId(), ppk.getNomor());
    					}
    		        	
    		        	start = start.plusDays(1);
    				}
    	        } 
            } 
    		
	        LogUtils.debug(response = response.complete("COMPLETED", null), log, slackInhookService);
	        return ResponseEntity.status(HttpStatus.OK).body(response.toString());
		} catch (Exception e) {
			log.error("Error on getdokuments()", e);

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response.toString());
		}
    }
    

    
    @PostMapping("/fixing")
    public ResponseEntity<String> fixingTable(
			@RequestParam(value = "tanggal", required = false) String tanggal) {
    	log.debug("REST request to dokumens get tanggal:{}", tanggal);

        String service = "/api/fixing";
        String _ppkNomor = (tanggal!=null && tanggal.length()>0) ? "tanggal= " + tanggal : "";
        String parameter = _ppkNomor;

		ZonedDateTime startTime = ZonedDateTime.now();
        CommonResponse response = new CommonResponse(service, parameter, startTime);
    	
    	try {
    		
    		if (tanggal != null) {
    			fixingBadanOrangService.generate(tanggal);
    		}
    		
	        LogUtils.debug(response = response.complete("COMPLETED", null), log, slackInhookService);
	        return ResponseEntity.status(HttpStatus.OK).body(response.toString());
		} catch (Exception e) {
			log.error("Error on getdokuments()", e);

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response.toString());
		}
    }
}
