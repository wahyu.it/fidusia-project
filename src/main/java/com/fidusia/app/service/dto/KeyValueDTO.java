package com.fidusia.app.service.dto;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;


/**
 * A DTO for the Cabang entity.
 */
public class KeyValueDTO implements Serializable {

    private Long id;

    private String nama;

	public KeyValueDTO(Long id, String nama) {
		super();
		this.id = id;
		this.nama = nama;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	@Override
	public String toString() {
		return "KeyValueDTO [id=" + id + ", nama=" + nama + "]";
	}
}
