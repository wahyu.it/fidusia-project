package com.fidusia.app.web.rest;

import java.io.*;
import java.net.URI;
import java.nio.file.Files;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZonedDateTime;

import javax.activation.FileTypeMap;
import javax.validation.Valid;

import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.Loader;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.fidusia.app.config.ApplicationProperties;
import com.fidusia.app.domain.Akta;
import com.fidusia.app.domain.Berkas;
import com.fidusia.app.domain.Ppk;
import com.fidusia.app.domain.RawDataRoya;
import com.fidusia.app.domain.Sertifikat;
import com.fidusia.app.repository.AktaRepository;
import com.fidusia.app.repository.BerkasRepository;
import com.fidusia.app.repository.PpkRepository;
import com.fidusia.app.repository.RawDataRoyaRepository;
import com.fidusia.app.repository.SertifikatRepository;
import com.fidusia.app.service.MailService;
import com.fidusia.app.service.dto.FileUploadDTO;
import com.fidusia.app.util.DateUtils;
import com.fidusia.app.util.ImageResizer;

import io.github.jhipster.web.util.HeaderUtil;

@RestController
@RequestMapping("api/file")
public class FileResource {

    @Value("${jhipster.clientApp.name}")
    private String applicationName;
    
	public final static float MAX_SIZE = 2200f;

	private final Logger log = LoggerFactory.getLogger(FileResource.class);

	private final ApplicationProperties applicationProperties;

	private final SertifikatRepository sertifikatRepository;

	private final RawDataRoyaRepository rawDataRoyaRepository;

	private final AktaRepository aktaRepository;

	private final PpkRepository ppkRepository;

	private final BerkasRepository berkasRepository;

	private final MailService mailService;

	public FileResource (MailService mailService, BerkasRepository berkasRepository, RawDataRoyaRepository rawDataRoyaRepository, PpkRepository ppkRepository, AktaRepository aktaRepository, ApplicationProperties applicationProperties, SertifikatRepository sertifikatRepository) {
		this.applicationProperties = applicationProperties;
		this.sertifikatRepository = sertifikatRepository;
		this.aktaRepository = aktaRepository;
		this.ppkRepository = ppkRepository;
		this.rawDataRoyaRepository = rawDataRoyaRepository;
		this.berkasRepository = berkasRepository;
		this.mailService = mailService;
	}
	
//	@RequestMapping(value = "/previewberkas", method = RequestMethod.GET, produces = { MediaType.APPLICATION_PDF_VALUE, MediaType.ALL_VALUE })
//	@ResponseBody
//	public ResponseEntity<byte[]> previewBerkas(@RequestParam("fileName") String fileName) {
//		try {
//			log.debug("fileName : " + fileName);
//	    	
//			String uploadDirectory = applicationProperties.getDomain().getBaseDirectory() + "berkas/";
//			log.debug("applicationProperties.getDomain().getBaseDirectory() : " + applicationProperties.getDomain().getBaseDirectory());
//			log.debug("uploadDirectory : " + uploadDirectory);
//			String filePath = uploadDirectory + fileName;
//			File file = new File(filePath);
//			return ResponseEntity.ok().header("Content-Disposition", "attachment; filename=" + file.getName())
//					.contentType(fileName.endsWith("pdf") ? MediaType.APPLICATION_PDF : MediaType.valueOf(FileTypeMap.getDefaultFileTypeMap().getContentType(file)))
//					.body(Files.readAllBytes(file.toPath()));
//		} catch (IOException e) {
//			log.error("FileResource.previewBerkas failed caused by " + e.getMessage(), e);
//			throw new RuntimeException("FileResource.previewBerkas failed caused by " + e.getMessage());
//		}
//	}
	
	@RequestMapping(value = "/upload/warkah/{id}", 
			method = RequestMethod.POST, 
			consumes = MediaType.ALL_VALUE)
	public ResponseEntity<FileUploadDTO> uploadWarkah(
			@PathVariable Long id,
			@Valid @RequestParam(value = "file", required = false) MultipartFile[] files) throws IOException {
  		log.debug("Upload file berkas id: {} file:{} ", id, files.length>0 ? files.length : 0);
		
		Berkas berkas = berkasRepository.findById(id).get();
    	ResponseEntity<FileUploadDTO> response = null;

    	
        InputStream is = null;
        
        for (MultipartFile file : files) {
          String oriFilename = file.getOriginalFilename();
          String format = oriFilename.substring(oriFilename.lastIndexOf(".") + 1);
          String kategori = oriFilename.substring(0, oriFilename.lastIndexOf("."));
          String name = berkas.getPpkNomor() + "-" + kategori;
    	  log.debug("Upload file berkas {} {} ppk tanggal : {}", name , format, berkas.getTanggalPpk());
          
          if (!file.isEmpty()) {
	      		log.debug("Upload file berkas id: {} kategori:{} file:{} ", id, kategori, file.getOriginalFilename());
	  
	          	String baseDirectory = applicationProperties.getDomain().getBaseDirectory()
	      				+ applicationProperties.getDirectory().getBerkas();
	  	        
	              try {
	                  	String tglUpload = LocalDate.now().toString();
		              	File outputDir = new File(baseDirectory, tglUpload);
		              	if(!outputDir.exists()) outputDir.mkdir();
		              	File outputFile = new File(outputDir, name + ".jpg");
		              	if(outputFile.exists()) FileUtils.deleteQuietly(outputFile);
	  
		              	is = file.getInputStream();
	  
	              	if("JPG".equals(format.toUpperCase()) || "JPEG".equals(format.toUpperCase())) {
	                  	saveFile(file, outputFile, berkas, kategori, tglUpload);
	                  } else if("PNG".equals(format.toUpperCase())){
	                      if(outputFile.exists()) {
	                          FileUtils.deleteQuietly(outputFile);
	                      }
	  
	                      saveFile(file, outputFile, berkas, kategori, tglUpload);
	  	                log.debug("Converting PNG to JPG file {}", outputFile.getName());
	                      outputFile = ImageResizer.convertPngToJpg(outputFile);
	                  } else if("TIF".equals(format.toUpperCase()) || "TIFF".equals(format.toUpperCase())){
	                      if(outputFile.exists()) {
	                          FileUtils.deleteQuietly(outputFile);
	                      }
	  
	                      saveFile(file, outputFile, berkas, kategori, tglUpload);
	  	                log.debug("Converting TIF to JPG file {}", outputFile.getName());
	                      outputFile = ImageResizer.convertTifToJpg(outputFile);
	                  } else if("PDF".equals(format.toUpperCase())){
	                  	if(outputFile.exists()) {
	                          FileUtils.deleteQuietly(outputFile);
	                      }
	                  	
	                      PDDocument document = Loader.loadPDF(file.getInputStream());
	                      outputFile = new File(outputDir, name + ".pdf");
	                      document.save(outputFile);
	                      document.close();
	                      
	                      saveDataBase(outputFile, berkas, kategori, tglUpload);
	                  } else {
	                  	saveFile(file, outputFile, berkas, kategori, tglUpload);
	                  }
	  
	                  FileUploadDTO fileUploadDTO = new FileUploadDTO();
	                  fileUploadDTO.setCategory(kategori);
	                  fileUploadDTO.setFilename(tglUpload + "/" + outputFile.getName());
	                  
	                  response = ResponseEntity.ok().body(fileUploadDTO);
	                  
	              } catch (Exception e) {
	                  log.error("FileResource.uploadFile failed caused by " + e.getMessage(), e);
	                  response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
	                  		.headers(HeaderUtil.createEntityCreationAlert(applicationName, false, "warkah", berkas.getId().toString()))
	                  		.body(null);
	                  
	              } finally {
	              	if (is != null) {
	              		try { is.close(); } catch(Exception e1){}
	              	}
	              }
	          } else {
	          	response = ResponseEntity.badRequest().headers(HeaderUtil.createEntityCreationAlert(applicationName, false, "warkah", berkas.getId().toString())).body(null);
	          }
			
		}
        
//    	if (!file.isEmpty()) {
//    		log.debug("Upload file berkas id: {} kategori:{} file:{} ", id, kategori, file.getOriginalFilename());
//
//        	String baseDirectory = applicationProperties.getDomain().getBaseDirectory()
//    				+ applicationProperties.getDirectory().getBerkas();
//	        
//            try {
//                String tglUpload = LocalDate.now().toString();
//            	File outputDir = new File(baseDirectory, tglUpload);
//            	if(!outputDir.exists()) outputDir.mkdir();
//            	File outputFile = new File(outputDir, name + ".jpg");
//            	if(outputFile.exists()) FileUtils.deleteQuietly(outputFile);
//
//                is = file.getInputStream();
//
//            	if("JPG".equals(format.toUpperCase()) || "JPEG".equals(format.toUpperCase())) {
//                	saveFile(file, outputFile, berkas, kategori, tglUpload);
//                } else if("PNG".equals(format.toUpperCase())){
//                    if(outputFile.exists()) {
//                        FileUtils.deleteQuietly(outputFile);
//                    }
//
//                    saveFile(file, outputFile, berkas, kategori, tglUpload);
//	                log.debug("Converting PNG to JPG file {}", outputFile.getName());
//                    outputFile = ImageResizer.convertPngToJpg(outputFile);
//                } else if("TIF".equals(format.toUpperCase()) || "TIFF".equals(format.toUpperCase())){
//                    if(outputFile.exists()) {
//                        FileUtils.deleteQuietly(outputFile);
//                    }
//
//                    saveFile(file, outputFile, berkas, kategori, tglUpload);
//	                log.debug("Converting TIF to JPG file {}", outputFile.getName());
//                    outputFile = ImageResizer.convertTifToJpg(outputFile);
//                } else if("PDF".equals(format.toUpperCase())){
//                	if(outputFile.exists()) {
//                        FileUtils.deleteQuietly(outputFile);
//                    }
//                	
//                    PDDocument document = Loader.loadPDF(file.getInputStream());
//                    outputFile = new File(outputDir, name + ".pdf");
//                    document.save(outputFile);
//                    document.close();
//                    
//                    saveDataBase(outputFile, berkas, kategori, tglUpload);
//                } else {
//                	saveFile(file, outputFile, berkas, kategori, tglUpload);
//                }
//
//                FileUploadDTO fileUploadDTO = new FileUploadDTO();
//                fileUploadDTO.setCategory(kategori);
//                fileUploadDTO.setFilename(tglUpload + "/" + outputFile.getName());
//                
//                response = ResponseEntity.ok().body(fileUploadDTO);
//                
//            } catch (Exception e) {
//                log.error("FileResource.uploadFile failed caused by " + e.getMessage(), e);
//                response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
//                		.headers(HeaderUtil.createEntityCreationAlert(applicationName, false, "warkah", berkas.getId().toString()))
//                		.body(null);
//                
//            } finally {
//            	if (is != null) {
//            		try { is.close(); } catch(Exception e1){}
//            	}
//            }
//        } else {
//        	response = ResponseEntity.badRequest().headers(HeaderUtil.createEntityCreationAlert(applicationName, false, "warkah", berkas.getId().toString())).body(null);
//        }
    	
    	return response;
	}
	

	@RequestMapping(value = "/previewberkas", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_PDF_VALUE, MediaType.ALL_VALUE })
	@ResponseBody
	public ResponseEntity<byte[]> downloadDoc (
			@RequestParam(value = "fileName", required = false) String fileName
			) throws Exception {
		log.debug("REST Download Doc by fileName:{}", fileName);
		
		try {
			String imageDir = applicationProperties.getDomain().getBaseDirectory() + "berkas/";

			String filePath = imageDir + fileName;
			File file = new File(filePath);
			
			return ResponseEntity.ok().header("Content-Disposition", "attachment; filename=" + file.getName())
					.contentType(fileName.endsWith("pdf") ? MediaType.APPLICATION_PDF : MediaType.valueOf(FileTypeMap.getDefaultFileTypeMap().getContentType(file)))
					.body(Files.readAllBytes(file.toPath()));
		} catch (Exception e) {
			log.error("FileResource.previewberkas File failed caused by " + e.getMessage(), e);
			throw e;
		}
	}

	@RequestMapping(value = "/previewberkasfile", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_PDF_VALUE, MediaType.APPLICATION_OCTET_STREAM_VALUE  })
	@ResponseBody
	public FileSystemResource previewBerkasFile(@RequestParam("fileName") String fileName) throws Exception {
    	
		try {
			String uploadDirectory = applicationProperties.getDomain().getBaseDirectory() + applicationProperties.getDirectory().getBerkas();
			String filePath = uploadDirectory + fileName;
			File file = new File(filePath);

			return new FileSystemResource(file);
		} catch (Exception e) {
			log.error("FileResource.previewBerkasFile failed caused by " + e.getMessage(), e);
			throw e;
		}
	}

	@RequestMapping(value = "/sertifikat", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_PDF_VALUE, MediaType.APPLICATION_OCTET_STREAM_VALUE  })
	@ResponseBody
	public FileSystemResource viewSertifikat(
			@RequestParam(value = "ppkNomor", required = false) String ppkNomor
			) throws Exception {
		log.debug("REST Download Sertifikat by Ppk Nomor:{}", ppkNomor);
		
		try {
			String fileName = ppkNomor + "-sertifikat.pdf";
			
			Sertifikat sertifikat = sertifikatRepository.findByAktaPpkNomor(ppkNomor).get(0);
			String sertifikatDir = applicationProperties.getDomain().getBaseDirectory() + "sertifikat/" + sertifikat.getTglOrder()+ "/";

			String filePath = sertifikatDir + fileName;
			File file = new File(filePath);
			
			if (file.exists()) return new FileSystemResource(file);
		} catch (Exception e) {
			log.error("FileResource.previewberkas File failed caused by " + e.getMessage(), e);
			throw e;
		}

		return null;
	}

	@RequestMapping(value = "/sertifikat/roya/{ppkNomor}", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_PDF_VALUE, MediaType.APPLICATION_OCTET_STREAM_VALUE  })
	@ResponseBody
	public FileSystemResource viewSertifikatRoya(
			@PathVariable(value = "ppkNomor", required = false) String ppkNomor
			) throws Exception {
		log.debug("REST Download Sertifikat by Ppk Nomor:{}", ppkNomor);
		
		try {
			String fileName = ppkNomor;
			
			RawDataRoya roya = rawDataRoyaRepository.findByUniqueKey(ppkNomor.split("-")[0]).get(0);
			String sertifikatDir = applicationProperties.getDomain().getBaseDirectory() + "roya/pdf/" + roya.getTglRoya()+ "/";

			String filePath = sertifikatDir + fileName;
			File file = new File(filePath);
			
			if (file.exists()) return new FileSystemResource(file);
		} catch (Exception e) {
			log.error("FileResource.previewberkas File failed caused by " + e.getMessage(), e);
			throw e;
		}

		return null;
	}

	@RequestMapping(value = "/minuta/{ppkNomor}", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_PDF_VALUE})
	@ResponseBody
	public FileSystemResource viewMinuta(
			@PathVariable(value = "ppkNomor", required = false) String ppkNomor
			) throws Exception {
		log.debug("REST view minuta by ppknomor:{}", ppkNomor);
		
		try {
			String fileName = ppkNomor;
			Ppk ppk = ppkRepository.findByNomor(ppkNomor.split("-")[0]);
			Akta akta = aktaRepository.findByPpk(ppk);
			String aktaDir = applicationProperties.getDomain().getBaseDirectory() + "minuta/" + akta.getNotaris().getId()+ "/" + akta.getTglOrder() + "/";

			String filePath = aktaDir + fileName;
			File file = new File(filePath);
			log.debug("view minuta by ppknomor {} path {}", ppkNomor, filePath);
			
			return new FileSystemResource(file);
		} catch (Exception e) {
			log.error("FileResource view minuta File failed caused by " + e.getMessage(), e);
			throw e;
		}
	}

	@RequestMapping(value = "/salinan/{ppkNomor}", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_PDF_VALUE, MediaType.APPLICATION_OCTET_STREAM_VALUE  })
	@ResponseBody
	public FileSystemResource viewSalinan(
			@PathVariable(value = "ppkNomor", required = false) String ppkNomor
			) throws Exception {
		log.debug("REST view salinan by ppknomor:{}", ppkNomor);
		
		try {
			String fileName = ppkNomor;

			Ppk ppk = ppkRepository.findByNomor(ppkNomor.split("-")[0]);
			Akta akta = aktaRepository.findByPpk(ppk);
			String salinanDir = applicationProperties.getDomain().getBaseDirectory() + "salinan/" + akta.getNotaris().getId()+ "/" + akta.getTglOrder() + "/";

			String filePath = salinanDir + fileName;
			File file = new File(filePath);
			log.debug("view salinan by ppknomor {} path {}", ppkNomor, filePath);
			
			if (file.exists()) return new FileSystemResource(file);
		} catch (Exception e) {
			log.error("FileResource view salinan File failed caused by " + e.getMessage(), e);
			throw e;
		}

		return null;
	}
	
	private void saveFile(MultipartFile input, File outputFile, Berkas berkas, String kategori, String tglUpload) throws Exception {
		log.debug("Save file ppk nomor:{} - kategori: {}", berkas.getPpkNomor(), kategori);
		BufferedOutputStream bos = null;
		
		try {
			bos = new BufferedOutputStream(new FileOutputStream(outputFile));
			byte[] bytes = input.getBytes();
			bos.write(bytes);
			bos.close();
			
			saveDataBase(outputFile, berkas, kategori, tglUpload);
		} catch (Exception e) {
			log.error("FileResource.saveFile failed caused by " + e.getMessage(), e);
			if (bos != null) {
        		try { bos.close(); } catch(Exception e1){}
        	}
			throw e;
		}
	}
	
	private void saveDataBase(File outputFile, Berkas berkas, String kategori, String tglUpload) throws Exception {
		log.debug("Save berkas ppk nomor:{} - kategori: {} {}", berkas.getPpkNomor(), kategori, berkas.getTanggalPpk());
		try {
            if ("identitas".equals(kategori)) {
            	berkas.setIdentitas(tglUpload + "/" + outputFile.getName());
            } else if ("kk".equals(kategori)) {
            	berkas.setKk(tglUpload + "/" + outputFile.getName());
            } else if ("ppk".equals(kategori)) {
            	berkas.setPpk(tglUpload + "/" + outputFile.getName());
            } else if ("skf".equals(kategori)) {
            	berkas.setSkNasabah(tglUpload + "/" + outputFile.getName());
            } else if ("npwp".equals(kategori)) {
            	berkas.setNpwp(tglUpload + "/" + outputFile.getName());
            } else if ("aktapend".equals(kategori)) {
            	berkas.setAktaPend(tglUpload + "/" + outputFile.getName());
            } else if ("aktaperub".equals(kategori)) {
            	berkas.setAktaPerub(tglUpload + "/" + outputFile.getName());
            } else if ("skkemen".equals(kategori)) {
            	berkas.setSkPendirian(tglUpload + "/" + outputFile.getName());
            }
            
            berkasRepository.save(berkas);
            
		} catch (Exception e) {
			log.error("FileResource.saveDataBase failed caused by " + e.getMessage(), e);
		}
	}

	@RequestMapping(value = "/sertifikat/sendmail", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_PDF_VALUE, MediaType.APPLICATION_OCTET_STREAM_VALUE  })
	@ResponseBody
	public void sendmail(
			@RequestParam(value = "to", required = false) String to,
			@RequestParam(value = "dear", required = false) String dear
			) throws Exception {
			log.debug("REST test send mail");
			
			String exp = DateUtils.formatZonedDateTime(ZonedDateTime.now().plusMinutes(10), DateUtils.FORMATTER_ID_LOCAL_DATETIME);
			
			mailService.sendAuthentificationCode(to, dear, "0987678", exp);
		try {
		} catch (Exception e) {
			log.error("FileResource.previewberkas File failed caused by " + e.getMessage(), e);
			throw e;
		}
	}
}