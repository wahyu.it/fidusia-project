package com.fidusia.app.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A RawData.
 */
@Entity
@Table(name = "raw_data")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class RawData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "unique_key")
    private String uniqueKey;

    @Column(name = "order_date")
    private LocalDate orderDate;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "nama_nasabah")
    private String namaNasabah;

    @Column(name = "line")
    private String line;

    @Column(name = "delimiter")
    private String delimeter;

    @Column(name = "line_number")
    private Long lineNumber;

    @Column(name = "valid")
    private Boolean valid;

    @Column(name = "invalid_reason")
    private String invalidReason;

    @Column(name = "status")
    private Long status;

    @Column(name = "nsb_jenis")
    private Integer nsbJenis;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public RawData id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUniqueKey() {
        return this.uniqueKey;
    }

    public RawData uniqueKey(String uniqueKey) {
        this.setUniqueKey(uniqueKey);
        return this;
    }

    public void setUniqueKey(String uniqueKey) {
        this.uniqueKey = uniqueKey;
    }

    public LocalDate getOrderDate() {
        return this.orderDate;
    }

    public RawData orderDate(LocalDate orderDate) {
        this.setOrderDate(orderDate);
        return this;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

    public String getFileName() {
        return this.fileName;
    }

    public RawData fileName(String fileName) {
        this.setFileName(fileName);
        return this;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getLine() {
        return this.line;
    }

    public RawData line(String line) {
        this.setLine(line);
        return this;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getDelimeter() {
        return this.delimeter;
    }

    public RawData delimeter(String delimeter) {
        this.setDelimeter(delimeter);
        return this;
    }

    public void setDelimeter(String delimeter) {
        this.delimeter = delimeter;
    }

    public Long getLineNumber() {
        return this.lineNumber;
    }

    public RawData lineNumber(Long lineNumber) {
        this.setLineNumber(lineNumber);
        return this;
    }

    public void setLineNumber(Long lineNumber) {
        this.lineNumber = lineNumber;
    }

    public Boolean getValid() {
        return this.valid;
    }

    public RawData valid(Boolean valid) {
        this.setValid(valid);
        return this;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    public String getInvalidReason() {
        return this.invalidReason;
    }

    public RawData invalidReason(String invalidReason) {
        this.setInvalidReason(invalidReason);
        return this;
    }

    public void setInvalidReason(String invalidReason) {
        this.invalidReason = invalidReason;
    }

    public Long getStatus() {
        return this.status;
    }

    public RawData status(Long status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    public Integer getNsbJenis() {
		return nsbJenis;
	}

	public void setNsbJenis(Integer nsbJenis) {
		this.nsbJenis = nsbJenis;
	}

	public String getNamaNasabah() {
		return namaNasabah;
	}

	public void setNamaNasabah(String namaNasabah) {
		this.namaNasabah = namaNasabah;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RawData)) {
            return false;
        }
        return getId() != null && getId().equals(((RawData) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RawData{" +
            "id=" + getId() +
            ", uniqueKey='" + getUniqueKey() + "'" +
            ", orderDate='" + getOrderDate() + "'" +
            ", fileName='" + getFileName() + "'" +
            ", line='" + getLine() + "'" +
            ", delimeter='" + getDelimeter() + "'" +
            ", lineNumber=" + getLineNumber() +
            ", valid='" + getValid() + "'" +
            ", invalidReason='" + getInvalidReason() + "'" +
            ", status=" + getStatus() + "'" +
            ", nsbJenis=" + getNsbJenis() + 
            "}";
    }
}
