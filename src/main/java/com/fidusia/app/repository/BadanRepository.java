package com.fidusia.app.repository;

import com.fidusia.app.domain.Badan;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Badan entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BadanRepository extends JpaRepository<Badan, Long> {}
