import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { IAktaDTO } from 'app/shared/model/aktaDTO.model';

type EntityArrayResponseType = HttpResponse<IAktaDTO[]>;

@Injectable({ providedIn: 'root' })
export class CetakAktaService {
  	  public resourceUrl= SERVER_API_URL + 'api/aktas/search';
  	  public resourceUrlPreviewFile = SERVER_API_URL + 'api/file/';
	
	  constructor(protected http: HttpClient) {}

	  search(req?: any): Observable<EntityArrayResponseType> {
	    const options = createRequestOption(req);
	    return this.http
	      .get<IAktaDTO[]>(this.resourceUrl, { params: options, observe: 'response' })
	      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
	  }
	  
	 viewMinuta(ppkNomor: string): Observable<Blob> {
	    console.log(ppkNomor);
	    return this.http.get<Blob>(this.resourceUrlPreviewFile + "minuta" + (ppkNomor ? `?ppkNomor=${ppkNomor}` : ''), {
	      observe: 'body',
	      responseType: 'blob' as 'json' })
	  }
	  
	 /* public downloadExcel(data): void {
		  const url: string = '[api endpoint here ]';
		  this.http.post(url, data.body, { responseType: 'blob' })
		    .subscribe((response: Blob) => saveAs(response, data.fileName + '.xlsx'));
		}*/
		
	 viewSalinan(ppkNomor: string): Observable<Blob> {
	    console.log(ppkNomor);
	    return this.http.get<Blob>(this.resourceUrlPreviewFile + "salinan"  + (ppkNomor ? `?ppkNomor=${ppkNomor}` : ''), {
	      observe: 'body',
	      responseType: 'blob' as 'json'
	    });
	  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
	    if (res.body) {
	      res.body.forEach((r: IAktaDTO) => {
	        r.tglOrder = r.tglOrder ? moment(r.tglOrder) : undefined;
	        r.tglAkta = r.tglAkta ? moment(r.tglAkta) : undefined;
	        r.namaBpkb = r.namaBpkb ? r.namaBpkb.replace(/\s\s+/g, ' ') : undefined;
	        r.namaNasabah = r.namaNasabah ? r.namaNasabah.replace(/\s\s+/g, ' ') : undefined;
	        r.pukulAkta = r.pukulAkta ? moment(r.pukulAkta) : undefined;
	      });
	    }
    return res;
  }
	
}