package com.fidusia.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fidusia.app.domain.Holiday;

import java.time.LocalDate;
import java.util.List;

/**
 * Spring Data JPA repository for the Holiday entity.
 */
@SuppressWarnings("unused")
public interface HolidayRepository extends JpaRepository<Holiday,Long> {

    List<Holiday> findByTanggalBetween(LocalDate start, LocalDate end);

    List<Holiday> findByTanggal(LocalDate tanggal);

}
