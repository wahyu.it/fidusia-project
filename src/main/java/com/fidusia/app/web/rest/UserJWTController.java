package com.fidusia.app.web.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fidusia.app.domain.User;
import com.fidusia.app.repository.UserRepository;
import com.fidusia.app.security.jwt.JWTFilter;
import com.fidusia.app.security.jwt.TokenProvider;
import com.fidusia.app.service.LoginFailureService;
import com.fidusia.app.service.UserService;
import com.fidusia.app.web.rest.vm.LoginVM;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Controller to authenticate users.
 */
@RestController
@RequestMapping("/api")
public class UserJWTController {

    private final Logger log = LoggerFactory.getLogger(UserJWTController.class);

    private final TokenProvider tokenProvider;

    private final LoginFailureService loginFailureService;

    private final UserService userService; 
    
    private final UserRepository userRepository;

    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    private final PasswordEncoder passwordEncoder;

    public UserJWTController(PasswordEncoder passwordEncoder, UserRepository userRepository, UserService userService, LoginFailureService loginFailureService, TokenProvider tokenProvider, AuthenticationManagerBuilder authenticationManagerBuilder) {
        this.tokenProvider = tokenProvider;
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.loginFailureService = loginFailureService;
        this.userService = userService;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping("/authenticate")
    public ResponseEntity<JWTToken> authorize(@Valid @RequestBody LoginVM loginVM) {

        UsernamePasswordAuthenticationToken authenticationToken =
            new UsernamePasswordAuthenticationToken(loginVM.getUsername(), loginVM.getPassword());
        
        Authentication authentication = null;
		String login;
		try {
			login = loginVM.getUsername();

			authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
			
			loginFailureService.resetCounter(login);
		} catch (AuthenticationException e) {

			if ("Bad credentials".equals(e.getMessage())) {
				login = loginVM.getUsername();

				loginFailureService.addCounter(login);

				if (loginFailureService.hasReachLimit(login)) {
					// TODO ACTION
					userService.activateBlock(login);
					
					loginFailureService.resetCounter(login);
				}
			}
			throw e;
		}

//        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        boolean rememberMe = (loginVM.isRememberMe() == null) ? false : loginVM.isRememberMe();
        String jwt = tokenProvider.createToken(authentication, rememberMe);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
        return new ResponseEntity<>(new JWTToken(jwt), httpHeaders, HttpStatus.OK);
    }

    @PostMapping("/authenticate2")
    public Boolean authorize2(@Valid @RequestBody LoginVM loginVM) {
        User user = userRepository.findOneByLogin(loginVM.getUsername().toLowerCase()).get();
    	Boolean result = passwordEncoder.matches(loginVM.getPassword(),user.getPassword());
    	
    	if (result) {
    		loginFailureService.resetCounter(loginVM.getUsername());
    	} else {
    		loginFailureService.addCounter(loginVM.getUsername());

			if (loginFailureService.hasReachLimit(loginVM.getUsername())) {
				// TODO ACTION
				userService.activateBlock(loginVM.getUsername());
				
				loginFailureService.resetCounter(loginVM.getUsername());
			}
    	}
		
        return result;
    }

    /**
     * Object to return as body in JWT Authentication.
     */
    static class JWTToken {

        private String idToken;

        JWTToken(String idToken) {
            this.idToken = idToken;
        }

        @JsonProperty("id_token")
        String getIdToken() {
            return idToken;
        }

        void setIdToken(String idToken) {
            this.idToken = idToken;
        }
    }
}
