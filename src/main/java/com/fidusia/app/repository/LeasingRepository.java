package com.fidusia.app.repository;

import com.fidusia.app.domain.Leasing;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Leasing entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LeasingRepository extends JpaRepository<Leasing, Long> {

	Leasing findOneByRecordStatus(int value);}
