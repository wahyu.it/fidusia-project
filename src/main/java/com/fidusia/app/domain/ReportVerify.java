package com.fidusia.app.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fidusia.app.util.DateUtils;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * Created by ataufani on 9/24/2018.
 */
@Entity
@Table(name = "report_verify")
public class ReportVerify {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private User user;

    @NotNull
    @Column(name = "tanggal", nullable = false)
    private LocalDate tanggal;

    @NotNull
    @Column(name = "verified", nullable = true)
    private Integer verified;

    @NotNull
    @Column(name = "unverified", nullable = true)
    private Integer unverified;

    @NotNull
    @Size(max = 10)
    @Column(name = "start_time", nullable = true, length = 10)
    private String startTime;

    @NotNull
    @Size(max = 10)
    @Column(name = "end_time", nullable = true, length = 10)
    private String endTime;
    
    public ReportVerify() {
	}

	public ReportVerify(ZonedDateTime verifyOn, User user, Long verified, Long unverified) {
		this.user = user;
		this.tanggal = verifyOn.toLocalDate();
		this.verified = verified.intValue();
		this.unverified = unverified.intValue();
		this.startTime = DateUtils.formatZonedDateTime(verifyOn, DateUtils.FORMATTER_TIME);
		this.endTime = DateUtils.formatZonedDateTime(DateUtils.nowDateTime(), DateUtils.FORMATTER_TIME);
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDate getTanggal() {
        return tanggal;
    }

    public void setTanggal(LocalDate tanggal) {
        this.tanggal = tanggal;
    }

    public Integer getVerified() {
        return verified;
    }

    public void setVerified(Integer verified) {
        this.verified = verified;
    }

    public Integer getUnverified() {
        return unverified;
    }

    public void setUnverified(Integer unverified) {
        this.unverified = unverified;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ReportVerify reportVerify = (ReportVerify) o;
        if (reportVerify.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, reportVerify.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ReportVerify{" +
            "id=" + id +
            ", user=" + user +
            ", tanggal=" + tanggal +
            ", verified=" + verified +
            ", unverified=" + unverified +
            ", startTime='" + startTime + '\'' +
            ", endTime='" + endTime + '\'' +
            '}';
    }
}
