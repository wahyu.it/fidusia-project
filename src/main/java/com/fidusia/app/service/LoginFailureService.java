package com.fidusia.app.service;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class LoginFailureService {
	private Map<Integer, String> counterMap = new HashMap<Integer, String>();

	private final Logger log = LoggerFactory.getLogger(LoginFailureService.class);
	private Integer key = 0;
	private int blockCount = 0;

	public void addCounter(String login) {

		counterMap.put(key, login);
		key++;
		log.debug("KEY MAP " + key);
		log.debug("COUNTER MAP" + counterMap.size());

	}

	public boolean hasReachLimit(String login) {
		boolean limit = false;

		int count = Collections.frequency(counterMap.values(), login);
		if (count == 5) {
			limit = true;
		}
		return limit;
	}

	public void resetCounter(String login) {
		counterMap.values().removeIf(value -> value.contains(login));
		log.debug("MAP SIZE " + counterMap.size());
		if (counterMap.size() == 0) {
			key = 0;
		}
	}

}
