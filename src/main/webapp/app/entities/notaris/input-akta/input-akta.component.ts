import { Component, OnInit } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators} from '@angular/forms';
import { InputAktaService } from './input-akta.service';
import { INotarisDTO } from 'app/shared/model/notarisDTO.model';
import { ITransaksiAktaDTO } from 'app/shared/model/transaksiAktaDTO.model';
import { DatePipe } from '@angular/common';
import { DATE_FORMAT_ID } from 'app/shared/constants/input.constants';
import { CalendarEvent } from 'angular-calendar';
import Swal from 'sweetalert2';

@Component({
  selector: 'jhi-input-akta',
  templateUrl: './input-akta.component.html',
  styleUrls: ['input-akta.scss']
})

export class InputAktaComponent implements OnInit {
  notaris!: INotarisDTO;
  transaksiAkta?: ITransaksiAktaDTO |any;
  edit?: any;
  tanggalOrder?:any;
  view = 'month';
  viewDate: Date = new Date();
  events: CalendarEvent[] = [];
  clickedDate?: Date;
  
  
  settingsForm = this.fb.group({
    date: [{value: '', disabled: true}],
    tglOrder: [{value: '', disabled: true}],
    jmlOrder: [{value: '', disabled: true}],
    startNomor: ['', [Validators.pattern(/^[0-9]\d*$/)]],
    startPukul: [''],
  });

  constructor(
    protected eventManager: JhiEventManager,
    protected activatedRoute: ActivatedRoute,
    protected inputAktaService: InputAktaService,
    protected router: Router,
    private fb: FormBuilder,
    private datePipe: DatePipe,
	) {
		this.edit = true;
		this.tanggalOrder = this.datePipe.transform(new Date, "dd MMMM yyyy")!;
	}
	
	ngOnInit() : void {
		this.inputAktaService.notarisLogin().subscribe(notaris => {
		if (notaris) {
	      this.notaris = notaris;
		}
	    });
		this.inputAktaService.jmlOrder().subscribe(akta => {
		    const curentDate = new Date();
			const tanggal = new Date();
		    const startNo = 1;
	        const pklMulai = curentDate;
	        	pklMulai.setHours(8);
	        	pklMulai.setMinutes(0);
	        	pklMulai.setSeconds(0);
				tanggal.setDate(1);
			
			if (akta) {
		        this.settingsForm.patchValue({
		          tglOrder: akta.tglOrder,
		          jmlOrder: akta.jmlOrder,
		        });
		        this.transaksiAkta = akta;
		        
				this.inputAktaService.dataMandatory().subscribe(data => {
					if (data){
						const start = data.lastNomor ? data.lastNomor + 1 : 1;
				        this.settingsForm.patchValue({
				          startNomor: start,
				          startPukul: this.datePipe.transform(pklMulai, "HH:mm"),
				        });
					} else {
				        this.settingsForm.patchValue({
				          startNomor: startNo,
				          startPukul: this.datePipe.transform(pklMulai, "HH:mm"),
				        });
					}
				})
		     } else {
		        this.settingsForm.patchValue({
		          tglOrder: this.datePipe.transform(curentDate, "dd MMMM yyyy"),
		          jmlOrder: 0,
		        });
				this.inputAktaService.dataMandatory().subscribe(data => {
					if (data){
						const start = data.lastNomor ? data.lastNomor + 1 : 1;
				        this.settingsForm.patchValue({
				          startNomor: start,
				  		  startPukul: this.datePipe.transform(data.lastPukulAkhir, "HH:mm"),
				        });
					} else {
				        this.settingsForm.patchValue({
				          startNomor: startNo,
				        });
					}
				})
			 }
	    });
	}
	
	generate() : any {
		 Swal.fire({ 
	      icon: 'question',
	      text: 'Pastikan nomor awal dan pukul awal sudah sesuai !!!',
	      showCancelButton: true,
  		  confirmButtonText: "Yes!",
		  cancelButtonText: "Cancel!",
	    }).then( result => {
			if (result.isConfirmed) {
				Swal.fire({ 
			      icon: 'success',
			      text: 'Generate Berhasil, periksa hasilnya di halaman Daftar Akta !!!',
		  		  confirmButtonText: "Yes!",
			    });
			} else {
				Swal.fire("Changes are not saved", "", "info");
			}
			
		});
	}
	
	  private sDate(): string {
	    const date = new Date();
	    return this.datePipe.transform(date, DATE_FORMAT_ID)!;
	  }
	
	  private tglODr(): string {
	    const date = new Date();
	    const rDate =this.datePipe.transform(date, "yyyy-MM-dd")!;
	    return rDate;
	  }
}
