import { Component, OnInit } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';

import { BiodataNotarisService } from './biodata-notaris.service';
import { INotarisDTO } from 'app/shared/model/notarisDTO.model';

@Component({
  selector: 'jhi-biodatanotaris',
  templateUrl: './biodata-notaris.component.html',
  styleUrls: ['biodata-notaris.scss']
})
export class BiodataNotarisComponent implements OnInit {
  notaris?: INotarisDTO |any;
  edit?: any;
  settingsForm = this.fb.group({
    nama: [undefined, [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
    namaShort: [undefined, [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
    titelShortOne: [undefined],
    titelLongOne: [undefined],
    titelShortTwo: [undefined, [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
    titelLongTwo: [undefined, [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
    wilayahKerja: [undefined, [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
    kedudukan: [undefined, [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
    sk: [undefined, [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
    tglSk: [undefined, [Validators.required, Validators.minLength(1), Validators.maxLength(50)]],
  });

  constructor(
    protected eventManager: JhiEventManager,
    protected activatedRoute: ActivatedRoute,
    protected biodataNotarisService: BiodataNotarisService,
    protected router: Router,
     private fb: FormBuilder
	) {
		this.edit = true;
	}
	
	ngOnInit() : void {
		this.biodataNotarisService.notarisLogin().subscribe(notaris => {
		if (notaris) {
	        this.settingsForm.patchValue({
				nama: notaris.nama,
				namaShort: notaris.namaShort,
				titelShortOne: notaris.titelShortOne,
				titelLongOne: notaris.titelLongOne,
				titelShortTwo: notaris.titelShortTwo,
				titelLongTwo: notaris.titelLongTwo,
				wilayahKerja: notaris.wilayahKerja,
				kedudukan: notaris.kedudukan,
				sk: notaris.sk,
				tglSk: notaris.tglSk
			})
	      this.notaris = notaris;
		}
	    });
	}
	
	view() : any {
		if (this.edit) {
			this.edit = false;
		} else {
			this.edit = true;
		}
	}
}
