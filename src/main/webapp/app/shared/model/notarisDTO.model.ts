import { Moment } from 'moment';

export interface INotarisDTO {
        nama?: string;
        namaShort?: string;
        titelShortOne?: string;
        titelShortTwo?: string;
        titelLongOne?: string;
        titelLongTwo?: string;
        wilayahKerja?: string;
        kedudukan?: string;
        sk?: string;
        tglSk?: Moment;
        alamatKantor?: string;
        alamatEmail?: string;
        noKontak?: string;
        maksAkta?: string;
        id?: number;
        selisihPukul?: number;
        jenisHariKerja?: number;
        namaAhu?: string;
}

export class NotarisDTO implements INotarisDTO {
  constructor(
        public nama?: string,
        public namaShort?: string,
        public titelShortOne?: string,
        public titelShortTwo?: string,
        public titelLongOne?: string,
        public titelLongTwo?: string,
        public wilayahKerja?: string,
        public kedudukan?: string,
        public sk?: string,
        public tglSk?: Moment,
        public alamatKantor?: string,
        public alamatEmail?: string,
        public noKontak?: string,
        public maksAkta?: string,
        public id?: number,
        public selisihPukul?: number,
        public jenisHariKerja?: number,
        public namaAhu?: string
  ) {}
}
