package com.fidusia.app.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "job_instance")
public class JobInstance implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(max = 10)
    @Column(name = "type", length = 10, nullable = false)
    private String type;

    @NotNull
    @Size(max = 50)
    @Column(name = "name", length = 50, nullable = false)
    private String name;

    @Size(max = 50)
    @Column(name = "method", length = 50)
    private String method;

    @Size(max = 50)
    @Column(name = "cron", length = 50)
    private String cron;

    @Size(max = 200)
    @Column(name = "parameter", length = 200)
    private String parameter;

    @Column(name = "last_execute")
    private ZonedDateTime lastExecute;

    @Column(name = "last_success")
    private ZonedDateTime lastSuccess;

    @Size(max = 10)
    @Column(name = "last_status", length = 10)
    private String lastStatus;

    @Size(max = 30)
    @Column(name = "last_executor", length = 30)
    private String lastExecutor;

    @Size(max = 200)
    @Column(name = "req_parameter", length = 200)
    private String reqParameter;

    @Column(name = "sequence_no")
    private Integer sequenceNo;

    @Size(max = 100)
    @Column(name = "description", length = 100)
    private String description;

    @ManyToOne(fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name="parent_id")
    private JobInstance parent;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getCron() {
		return cron;
	}

	public void setCron(String cron) {
		this.cron = cron;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public ZonedDateTime getLastExecute() {
		return lastExecute;
	}

	public void setLastExecute(ZonedDateTime lastExecute) {
		this.lastExecute = lastExecute;
	}

	public ZonedDateTime getLastSuccess() {
		return lastSuccess;
	}

	public void setLastSuccess(ZonedDateTime lastSuccess) {
		this.lastSuccess = lastSuccess;
	}

	public String getLastStatus() {
		return lastStatus;
	}

	public void setLastStatus(String lastStatus) {
		this.lastStatus = lastStatus;
	}

	public String getLastExecutor() {
		return lastExecutor;
	}

	public void setLastExecutor(String lastExecutor) {
		this.lastExecutor = lastExecutor;
	}

	public String getReqParameter() {
		return reqParameter;
	}

	public void setReqParameter(String reqParameter) {
		this.reqParameter = reqParameter;
	}

	public Integer getSequenceNo() {
		return sequenceNo;
	}

	public void setSequenceNo(Integer sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public JobInstance getParent() {
		return parent;
	}

	public void setParent(JobInstance parent) {
		this.parent = parent;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JobInstance other = (JobInstance) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "JobInstance [id=" + id + ", type=" + type + ", name=" + name + ", method=" + method + ", cron=" + cron + ", parameter=" + parameter + ", lastExecute="
				+ lastExecute + ", lastSuccess=" + lastSuccess + ", lastStatus=" + lastStatus + ", lastExecutor=" + lastExecutor + ", reqParameter=" + reqParameter + 
				", sequenceNo=" + sequenceNo + ", description=" + description + ", parent=" + (parent != null ? parent.getName() : null) + "]";
	}
}
