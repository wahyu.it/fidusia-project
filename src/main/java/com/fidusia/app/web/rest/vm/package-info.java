/**
 * View Models used by Spring MVC REST controllers.
 */
package com.fidusia.app.web.rest.vm;
