package com.fidusia.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fidusia.app.domain.Provinsi;

@Repository
public interface ProvinsiRepository extends JpaRepository<Provinsi, Long>  {

}
