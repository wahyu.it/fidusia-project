import { Component, OnInit } from '@angular/core';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { HttpResponse, HttpHeaders } from '@angular/common/http';
import { JhiEventManager } from 'ng-jhipster';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { FormControl, FormGroup } from '@angular/forms';

import { CetakAktaService } from 'app/entities/notaris/cetak-akta/cetak-akta.service';
import { RegistrasiFidusiaService } from './registrasifidusia.service';
import { ISertifikatDTO } from 'app/shared/model/sertifikatDTO.model';
import { ICabangDTO } from 'app/shared/model/cabangDTO.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'jhi-registrasifidusia',
  templateUrl: './registrasifidusia.component.html',
  styleUrls: ['registrasifidusia.scss']
})
export class RegistrasiFidusiaComponent implements OnInit {
  sertifikats?: ISertifikatDTO[] |any;
  cabang?: ICabangDTO;
  date = '';
  month = '';
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  totalItems = 0;
  eventSubscriber?: Subscription;
  sPpd!: boolean;
  key!: string;
  value!: string;

  private dateFormat = 'yyyy-MM-dd';
  private mothFormat = 'yyyy-MM';
  
  editForm = new FormGroup({
    ppd: new FormControl(''),
    date: new FormControl(''),
    month: new FormControl(''),
    ppknomor: new FormControl('')
  });
  
  ppkGroup = new FormGroup({
    ppknomor: new FormControl('')
  });

  constructor(
    protected eventManager: JhiEventManager,
    protected activatedRoute: ActivatedRoute,
    protected registrasiFidusiaService: RegistrasiFidusiaService,
    protected cetakAktaService: CetakAktaService,
    private datePipe: DatePipe,
    protected router: Router,
	) {
		this.sPpd = true;	
		
	    this.registrasiFidusiaService.cabangLogin().subscribe(cabang => {
	      this.cabang = cabang;
	    });
	}

  ngOnInit(): void {
		this.month = this.sMonth();
		this.date = this.sDate();
	   this.activatedRoute.data.subscribe(data => {
	      this.page = data.pagingParams.page;
	      this.ascending = data.pagingParams.ascending;
	      this.predicate = data.pagingParams.predicate;
	      this.ngbPaginationPage = data.pagingParams.page;
	      this.loadPage();
	    });
    this.registerChangeInLogBook();
    this.loadData();
    
  }
  
  onChange(e: any): void  {
	  console.log("ppd:" + e);
	  if (e === 'tgl'){
		  this.sPpd = true;
		  this.key = 'ppdDate';
	  } else {
		 this.sPpd = false;
		 this.key = 'ppdMonth';
	  }
	  console.log("this.sPpd:" + this.sPpd);
  }
  
  viewMinuta(ppkNomor: string): void {
	 /* this.cetakAktaService.viewMinuta(ppkNomor).subscribe(res => {
 	 const a = document.createElement("a") ;
     const fileURL = URL.createObjectURL(res);
      console.log("atribute:" + a);
      window.open('api/file/minuta/'+ ppkNomor + '-minuta.pdf', '_blank');
    	});*/
      window.open('api/file/minuta/'+ ppkNomor + '-minuta.pdf', '_blank');
  }
  
  searchppk(ppkNomor: string): void {
	  let ppkNo = this.ppkGroup.get(['ppknomor'])!.value;;
	  if (this.key === 'ppkNomor') {
		  ppkNo = this.ppkGroup.get(['ppknomor'])!.value;
	  }
      this.searchByParam(ppkNomor, ppkNo);
  }

  searchPpd(key: string): void {
	  console.log(this.key + " " + key)
	  if (key === 'ppdDate') {
		  this.value = this.date;
	  } else {
		  this.value = this.month;
	  //	  console.log("this.month : " +this.month)
	  } 
	  
      this.searchByParam(this.key, this.value);
  }
  
  searchByParam(key: string, value: string): void  {
	  this.key = key;
	  this.value = value;
	  console.log("searchByParam key : " + this.key + " value: " + this.value)
      this.router.navigate(['/laporan/registrasi-fidusia'], {
        queryParams: {
          skey: this.key,
          svalue: this.value,
          page: this.page,
          sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
        }
      });
      this.loadData();
  }

  private loadData(page?: number): void {
    const pageToLoad: number = page ? page : this.page;
    this.registrasiFidusiaService
      .search({
        skey: this.key,
        svalue: this.value,
        page: this.page - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe((res: HttpResponse<ISertifikatDTO[]>) => this.onSuccess(res.body, res.headers, pageToLoad));
  }

  loadPage(page?: number): void {
    const pageToLoad: number = page ? page : this.page;

     this.registrasiFidusiaService
        .search({
          skey: this.key,
          svalue: this.value,
          page: this.page-1,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe((res: HttpResponse<ISertifikatDTO[]>) => this.onSuccess(res.body, res.headers, pageToLoad));
  }
  
  cleardate(): void {
    this.editForm.patchValue({
      date: '',
      month:''
    });
  }
  
  viewSertifikat(ppkNomor: string): void {
	 this.registrasiFidusiaService.viewSertifikat(ppkNomor).subscribe(res => {
      const fileURL = URL.createObjectURL(res);
      
      window.open(fileURL, '_blank');
    });
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  registerChangeInLogBook(): void {
    this.eventSubscriber = this.eventManager.subscribe('reportListModification', () => this.loadPage());
  }

  protected onSuccess(data: ISertifikatDTO[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/laporan/registrasi-fidusia'], {
      queryParams: {
      	skey: this.key,
      	svalue: this.value,
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.sertifikats = data;
    this.cleardate();
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }

  private sDate(): string {
    const date = new Date();
	date.setDate(1);
    return this.datePipe.transform(date, this.dateFormat)!;
  }

  private sMonth(): string {
    const date = new Date();
    const result = this.datePipe.transform(date, this.mothFormat)!;
    console.log("result :" +result)
    return result
  }
}
