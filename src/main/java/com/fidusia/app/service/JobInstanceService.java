package com.fidusia.app.service;

import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.support.CronSequenceGenerator;
import org.springframework.stereotype.Service;

import com.fidusia.app.domain.JobExecution;
import com.fidusia.app.domain.JobInstance;
import com.fidusia.app.repository.JobExecutionRepository;
import com.fidusia.app.repository.JobInstanceRepository;
import com.fidusia.app.service.dto.JobParameter;
import com.fidusia.app.util.StringUtils;


//TODO need to fix deprecated code

@Service
@EnableScheduling
public class JobInstanceService {

    private final Logger log = LoggerFactory.getLogger(JobInstanceService.class);
	
	private final static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	
	private final static String WAIT = "WAIT";
	private final static String STARTED = "STARTED";
	private final static String SUCCESS = "SUCCESS";
	private final static String ERROR = "ERROR";
	
	private final ApplicationContext appContext;

	private final JobInstanceRepository jobInstanceRepository;

	private final JobExecutionRepository jobExecutionRepository;

	public JobInstanceService (
			ApplicationContext appContext, JobInstanceRepository jobInstanceRepository,
			JobExecutionRepository jobExecutionRepository
			) {
		this.appContext = appContext;
		this.jobInstanceRepository = jobInstanceRepository;
		this.jobExecutionRepository = jobExecutionRepository;
	}
	
    @Scheduled(cron = "3 * * ? * *")
    public void perform() {
    	Date date = new Date();
    	String now = dateFormat.format(date);
    	
    	date.setTime(date.getTime() - 10000);
    	
    	List<JobInstance> jobs = jobInstanceRepository.findByCronNotNullOrLastStatus(WAIT);
    	if(jobs != null && jobs.size() > 0) {
    		for (JobInstance job : jobs) {
    			if(WAIT.equals(job.getLastStatus())) {
    				runJob(job, true);
    			} else {
    				try {
            			CronSequenceGenerator cronSequence = new CronSequenceGenerator(job.getCron());
            			String next = dateFormat.format(cronSequence.next(date));
            			
            			if(next.equals(now)) runJob(job, false);
            			else {
            				Date cron = cronSequence.next(date);
            				if((cron.getTime() - date.getTime()) < 180000) {
            					log.debug("{} schedule in {}", job.getName(), next);
            				}
            			}
    				} catch(Exception e) {
    					String message = "Error on checking job " + job.getName() + " " + job.getMethod() + ": " + e.getMessage();
    					log.error(message, e);
    					updateStatus(job, ERROR, job.getParameter(), message);
    				}
    			}
			}
    	}
    }

    @Async
    private void runJob(JobInstance job, boolean manualTrigger) {
    	log.debug("Run job: {}", job);
    	
    	updateStatus(job, STARTED, null, null);
    	
    	JobParameter jobParameters = new JobParameter("jobid=" + job.getId().toString() + ",date=" + LocalDate.now().toString());
    	jobParameters.add(replaceVariables(manualTrigger && job.getReqParameter() != null ? job.getReqParameter() : job.getParameter()));
    	
		if("service".equals(job.getType())) {
			try {
				Object t = appContext.getBean(job.getName());
			    Method[] allMethods = t.getClass().getDeclaredMethods();
			    for (Method m : allMethods) {
			        if (m.getName().equals(job.getMethod())) {
			            m.setAccessible(true);
			            m.invoke(t, jobParameters);
			            break;
			        }
			    }
			} catch(Exception e) {
				log.error("Error on running service " + job.getName() + " " + job.getMethod() + ": " + e.getMessage(), e);
				updateStatus(job, ERROR, "", e.getMessage());
			}
		} else if("method".equals(job.getType())) {
			try {
				Object t = Class.forName(job.getName()).getConstructor().newInstance();
			    Method[] allMethods = t.getClass().getDeclaredMethods();
			    for (Method m : allMethods) {
			        if (m.getName().equals(job.getMethod())) {
			            m.setAccessible(true);
			            m.invoke(t, jobParameters);
			            break;
			        }
			    }
			} catch(Exception e) {
				log.error("Error on method " + job.getName() + " " + job.getMethod() + ": " + e.getMessage(), e);
				updateStatus(job, ERROR, "", e.getMessage());
			}
		}
    }
    
    public void updateStatus(String jobid, String status, String input, String output) {
    	if(jobid != null) {
        	JobInstance job = jobInstanceRepository.findById(Long.parseLong(jobid)).get();
        	if(job != null && job.getId() != null) updateStatus(job, status, input, output);
    	}
    }
    
    public JobInstance trigger(String name, String method, String parameter, String actor) {
    	JobInstance job = jobInstanceRepository.findOneByNameAndMethod(name, method);
    	if(job != null && job.getId() != null) {
    		job.setReqParameter(parameter);
    		job.setLastExecutor(actor);
    		job.setLastStatus(WAIT);
    		return jobInstanceRepository.save(job);
    	}
    	
    	log.debug("JobInstance is not found for name:{} method:{}", name, method);
    	return null;
    }
    
    private void updateStatus(JobInstance job, String status, String input, String output) {
    	ZonedDateTime now = ZonedDateTime.now(ZoneId.systemDefault());
		
    	if(STARTED.equals(status)) {
    		job.setLastExecute(now);
    	} else if(SUCCESS.equals(status) || ERROR.equals(status)) {
    		if(SUCCESS.equals(status))  job.setLastSuccess(now);
    		
    		JobExecution exec = new JobExecution();
    		exec.setJob(job);
    		exec.setStatus(status);
    		exec.setStarttime(job.getLastExecute());
    		exec.setEndtime(now);
    		exec.setExecutor(job.getLastExecutor());
    		exec.setInput(StringUtils.abbreviate(input, 200));
    		exec.setOutput(StringUtils.abbreviate(output, 500));
    		jobExecutionRepository.save(exec);
    	}

		job.setLastStatus(status);
		jobInstanceRepository.save(job);
    }

	private final static DateFormat currentDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private final static DateFormat currentDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private final static String CURRENT_DATE = "CURRENT_DATE";
	private final static String CURRENT_DATETIME = "CURRENT_DATETIME";
	
    private String replaceVariables(String parameters) {
    	if(parameters != null && parameters.contains(CURRENT_DATE)) {
    		Date date = new Date();
        	String currentDate = currentDateFormat.format(date);
        	String currentDateTime = currentDateTimeFormat.format(date);
        	return parameters.replaceAll(CURRENT_DATETIME, currentDateTime).replaceAll(CURRENT_DATE, currentDate);
    	}
    	
    	return parameters;
    }
}
