import { Routes } from '@angular/router';
import { AktaNotarisComponent } from './akta-notaris.component';
import { JhiResolvePagingParams } from 'ng-jhipster';

export const aktaNotarisState: Routes = [{
  path: '',
  component: AktaNotarisComponent,
  resolve: {
	  pagingParams: JhiResolvePagingParams
  }
  }
];
