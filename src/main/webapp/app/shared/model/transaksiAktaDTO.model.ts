import { Moment } from 'moment';

export interface ITransaksiAktaDTO {
        id?: number;
        tglOrder?: Moment;
        blnOrder?: string;
        jmlOrder?: number;
        tglAkta?: Moment;
        blnAkta?: string;
        jmlAkta?: number;
        status?: number;
        lastNomor?: number;
        lastPukulAkhir?: Moment;
        notarisId?: number;
}

export class TransaksiAktaDTO implements ITransaksiAktaDTO {
  constructor(
        public id?: number,
        public tglOrder?: Moment,
        public blnOrder?: string,
        public jmlOrder?: number,
        public tglAkta?: Moment,
        public blnAkta?: string,
        public jmlAkta?: number,
        public status?: number,
        public lastNomor?: number,
        public lastPukulAkhir?: Moment,
        public notarisId?: number,
  ) {}
}
