package com.fidusia.app.domain;

import javax.persistence.*;
import javax.validation.constraints.Size;

import java.io.Serializable;
import java.time.ZonedDateTime;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Leasing.
 */
@Entity
@Table(name = "leasing")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Leasing implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "kode")
    private String kode;

    @Column(name = "nama")
    private String nama;

    @Column(name = "alamat")
    private String alamat;

    @Column(name = "kota")
    private String kota;

    @Column(name = "provinsi")
    private String provinsi;

    @Column(name = "kecamatan")
    private String kecamatan;

    @Column(name = "kelurahan")
    private String kelurahan;

    @Column(name = "rt")
    private String rt;

    @Column(name = "rw")
    private String rw;

    @Column(name = "npwp")
    private String npwp;

    @Column(name = "kode_pos")
    private String kodePos;

    @Column(name = "no_kontak")
    private String noKontak;

    @Column(name = "tech_no_kontak")
    private String techNoKontak;

    @Column(name = "tech_email")
    private String techEmail;

    @Column(name = "acct_no_kontak")
    private String acctNoKontak;

    @Column(name = "acct_email")
    private String acctEmail;

    @Column(name = "record_status")
    private Integer recordStatus;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "update_on")
    private ZonedDateTime updateOn;

    @Column(name = "email")
    private String email;

    @Column(name = "sk")
    private String sk;

    @Size(max = 100)
    @Column(name = "badan_hukum", length = 100)
    private String badanHukum;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public String getBadanHukum() {
		return badanHukum;
	}

	public void setBadanHukum(String badanHukum) {
		this.badanHukum = badanHukum;
	}

	public Long getId() {
        return this.id;
    }

    public Leasing id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKode() {
        return this.kode;
    }

    public Leasing kode(String kode) {
        this.setKode(kode);
        return this;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return this.nama;
    }

    public Leasing nama(String nama) {
        this.setNama(nama);
        return this;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return this.alamat;
    }

    public Leasing alamat(String alamat) {
        this.setAlamat(alamat);
        return this;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getKota() {
        return this.kota;
    }

    public Leasing kota(String kota) {
        this.setKota(kota);
        return this;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getProvinsi() {
        return this.provinsi;
    }

    public Leasing provinsi(String provinsi) {
        this.setProvinsi(provinsi);
        return this;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public String getKecamatan() {
        return this.kecamatan;
    }

    public Leasing kecamatan(String kecamatan) {
        this.setKecamatan(kecamatan);
        return this;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKelurahan() {
        return this.kelurahan;
    }

    public Leasing kelurahan(String kelurahan) {
        this.setKelurahan(kelurahan);
        return this;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getRt() {
        return this.rt;
    }

    public Leasing rt(String rt) {
        this.setRt(rt);
        return this;
    }

    public void setRt(String rt) {
        this.rt = rt;
    }

    public String getRw() {
        return this.rw;
    }

    public Leasing rw(String rw) {
        this.setRw(rw);
        return this;
    }

    public void setRw(String rw) {
        this.rw = rw;
    }

    public String getNpwp() {
        return this.npwp;
    }

    public Leasing npwp(String npwp) {
        this.setNpwp(npwp);
        return this;
    }

    public void setNpwp(String npwp) {
        this.npwp = npwp;
    }

    public String getKodePos() {
		return kodePos;
	}

	public void setKodePos(String kodePos) {
		this.kodePos = kodePos;
	}

	public String getNoKontak() {
        return this.noKontak;
    }

    public Leasing noKontak(String noKontak) {
        this.setNoKontak(noKontak);
        return this;
    }

    public void setNoKontak(String noKontak) {
        this.noKontak = noKontak;
    }

    public String getTechNoKontak() {
        return this.techNoKontak;
    }

    public Leasing techNoKontak(String techNoKontak) {
        this.setTechNoKontak(techNoKontak);
        return this;
    }

    public void setTechNoKontak(String techNoKontak) {
        this.techNoKontak = techNoKontak;
    }

    public String getTechEmail() {
        return this.techEmail;
    }

    public Leasing techEmail(String techEmail) {
        this.setTechEmail(techEmail);
        return this;
    }

    public void setTechEmail(String techEmail) {
        this.techEmail = techEmail;
    }

    public String getAcctNoKontak() {
        return this.acctNoKontak;
    }

    public Leasing acctNoKontak(String acctNoKontak) {
        this.setAcctNoKontak(acctNoKontak);
        return this;
    }

    public void setAcctNoKontak(String acctNoKontak) {
        this.acctNoKontak = acctNoKontak;
    }

    public String getAcctEmail() {
        return this.acctEmail;
    }

    public Leasing acctEmail(String acctEmail) {
        this.setAcctEmail(acctEmail);
        return this;
    }

    public void setAcctEmail(String acctEmail) {
        this.acctEmail = acctEmail;
    }

    public Integer getRecordStatus() {
        return this.recordStatus;
    }

    public Leasing recordStatus(Integer recordStatus) {
        this.setRecordStatus(recordStatus);
        return this;
    }

    public void setRecordStatus(Integer recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getUpdateBy() {
        return this.updateBy;
    }

    public Leasing updateBy(String updateBy) {
        this.setUpdateBy(updateBy);
        return this;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public ZonedDateTime getUpdateOn() {
		return updateOn;
	}

	public void setUpdateOn(ZonedDateTime updateOn) {
		this.updateOn = updateOn;
	}

	public String getEmail() {
        return this.email;
    }

    public Leasing email(String email) {
        this.setEmail(email);
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSk() {
        return this.sk;
    }

    public Leasing sk(String sk) {
        this.setSk(sk);
        return this;
    }

    public void setSk(String sk) {
        this.sk = sk;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Leasing)) {
            return false;
        }
        return getId() != null && getId().equals(((Leasing) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Leasing{" +
            "id=" + getId() +
            ", kode='" + getKode() + "'" +
            ", nama='" + getNama() + "'" +
            ", alamat='" + getAlamat() + "'" +
            ", kota='" + getKota() + "'" +
            ", provinsi='" + getProvinsi() + "'" +
            ", kecamatan='" + getKecamatan() + "'" +
            ", kelurahan='" + getKelurahan() + "'" +
            ", rt='" + getRt() + "'" +
            ", rw='" + getRw() + "'" +
            ", npwp='" + getNpwp() + "'" +
            ", kodePos='" + getKodePos() + "'" +
            ", noKontak='" + getNoKontak() + "'" +
            ", techNoKontak='" + getTechNoKontak() + "'" +
            ", techEmail='" + getTechEmail() + "'" +
            ", acctNoKontak='" + getAcctNoKontak() + "'" +
            ", acctEmail='" + getAcctEmail() + "'" +
            ", recordStatus=" + getRecordStatus() +
            ", updateBy='" + getUpdateBy() + "'" +
            ", updateOn='" + getUpdateOn() + "'" +
            ", email='" + getEmail() + "'" +
            ", sk='" + getSk() + "'" +
            "}";
    }
}
