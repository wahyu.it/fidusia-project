package com.fidusia.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A OrderKerja.
 */
@Entity
@Table(name = "order_kerja")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class OrderKerja implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "nomor")
    private String nomor;

    @Column(name = "tanggal")
    private LocalDate tanggal;

    @Column(name = "kategori")
    private Integer kategori;

    @Column(name = "tanggal_terima")
    private LocalDate tanggalTerima;

    @Column(name = "jumlah_akta")
    private Integer jumlahAkta;

    @Column(name = "jumlah_cancel")
    private Integer jumlahCancel;

    @Column(name = "jumlah_cetak_akta")
    private Integer jumlahCetakAkta;

    @Column(name = "upload_file")
    private String uploadFile;

    @Column(name = "status")
    private Integer status;

    @Column(name = "status_signed")
    private Boolean statusSigned;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "update_on")
    private ZonedDateTime updateOn;

    @Column(name = "approve_by")
    private String approveBy;

    @Column(name = "approve_on")
    private ZonedDateTime approveOn;

    @ManyToOne(fetch = FetchType.LAZY)
    private SkSubstitusi skSubtitusi;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "penerimaKuasa" }, allowSetters = true)
    private Notaris notaris;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "notaris" }, allowSetters = true)
    private PenerimaKuasa penerimaKuasa;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public OrderKerja id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomor() {
        return this.nomor;
    }

    public OrderKerja nomor(String nomor) {
        this.setNomor(nomor);
        return this;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public LocalDate getTanggal() {
        return this.tanggal;
    }

    public OrderKerja tanggal(LocalDate tanggal) {
        this.setTanggal(tanggal);
        return this;
    }

    public void setTanggal(LocalDate tanggal) {
        this.tanggal = tanggal;
    }

    public Integer getKategori() {
        return this.kategori;
    }

    public OrderKerja kategori(Integer kategori) {
        this.setKategori(kategori);
        return this;
    }

    public void setKategori(Integer kategori) {
        this.kategori = kategori;
    }

    public LocalDate getTanggalTerima() {
        return this.tanggalTerima;
    }

    public OrderKerja tanggalTerima(LocalDate tanggalTerima) {
        this.setTanggalTerima(tanggalTerima);
        return this;
    }

    public void setTanggalTerima(LocalDate tanggalTerima) {
        this.tanggalTerima = tanggalTerima;
    }

    public Integer getJumlahAkta() {
        return this.jumlahAkta;
    }

    public OrderKerja jumlahAkta(Integer jumlahAkta) {
        this.setJumlahAkta(jumlahAkta);
        return this;
    }

    public void setJumlahAkta(Integer jumlahAkta) {
        this.jumlahAkta = jumlahAkta;
    }

    public Integer getJumlahCancel() {
        return this.jumlahCancel;
    }

    public OrderKerja jumlahCancel(Integer jumlahCancel) {
        this.setJumlahCancel(jumlahCancel);
        return this;
    }

    public void setJumlahCancel(Integer jumlahCancel) {
        this.jumlahCancel = jumlahCancel;
    }

    public Integer getJumlahCetakAkta() {
        return this.jumlahCetakAkta;
    }

    public OrderKerja jumlahCetakAkta(Integer jumlahCetakAkta) {
        this.setJumlahCetakAkta(jumlahCetakAkta);
        return this;
    }

    public void setJumlahCetakAkta(Integer jumlahCetakAkta) {
        this.jumlahCetakAkta = jumlahCetakAkta;
    }

    public String getUploadFile() {
        return this.uploadFile;
    }

    public OrderKerja uploadFile(String uploadFile) {
        this.setUploadFile(uploadFile);
        return this;
    }

    public void setUploadFile(String uploadFile) {
        this.uploadFile = uploadFile;
    }

    public Integer getStatus() {
        return this.status;
    }

    public OrderKerja status(Integer status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Boolean getStatusSigned() {
        return this.statusSigned;
    }

    public OrderKerja statusSigned(Boolean statusSigned) {
        this.setStatusSigned(statusSigned);
        return this;
    }

    public void setStatusSigned(Boolean statusSigned) {
        this.statusSigned = statusSigned;
    }

    public String getUpdateBy() {
        return this.updateBy;
    }

    public OrderKerja updateBy(String updateBy) {
        this.setUpdateBy(updateBy);
        return this;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public ZonedDateTime getUpdateOn() {
        return this.updateOn;
    }

    public OrderKerja updateOn(ZonedDateTime updateOn) {
        this.setUpdateOn(updateOn);
        return this;
    }

    public void setUpdateOn(ZonedDateTime updateOn) {
        this.updateOn = updateOn;
    }

    public String getApproveBy() {
        return this.approveBy;
    }

    public OrderKerja approveBy(String approveBy) {
        this.setApproveBy(approveBy);
        return this;
    }

    public void setApproveBy(String approveBy) {
        this.approveBy = approveBy;
    }

    public ZonedDateTime getApproveOn() {
        return this.approveOn;
    }

    public OrderKerja approveOn(ZonedDateTime approveOn) {
        this.setApproveOn(approveOn);
        return this;
    }

    public void setApproveOn(ZonedDateTime approveOn) {
        this.approveOn = approveOn;
    }

    public SkSubstitusi getSkSubtitusi() {
        return this.skSubtitusi;
    }

    public void setSkSubtitusi(SkSubstitusi skSubtitusi) {
        this.skSubtitusi = skSubtitusi;
    }

    public OrderKerja skSubtitusi(SkSubstitusi skSubtitusi) {
        this.setSkSubtitusi(skSubtitusi);
        return this;
    }

    public Notaris getNotaris() {
        return this.notaris;
    }

    public void setNotaris(Notaris notaris) {
        this.notaris = notaris;
    }

    public OrderKerja notaris(Notaris notaris) {
        this.setNotaris(notaris);
        return this;
    }

    public PenerimaKuasa getPenerimaKuasa() {
        return this.penerimaKuasa;
    }

    public void setPenerimaKuasa(PenerimaKuasa penerimaKuasa) {
        this.penerimaKuasa = penerimaKuasa;
    }

    public OrderKerja penerimaKuasa(PenerimaKuasa penerimaKuasa) {
        this.setPenerimaKuasa(penerimaKuasa);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrderKerja)) {
            return false;
        }
        return getId() != null && getId().equals(((OrderKerja) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OrderKerja{" +
            "id=" + getId() +
            ", nomor='" + getNomor() + "'" +
            ", tanggal='" + getTanggal() + "'" +
            ", kategori=" + getKategori() +
            ", tanggalTerima='" + getTanggalTerima() + "'" +
            ", jumlahAkta=" + getJumlahAkta() +
            ", jumlahCancel=" + getJumlahCancel() +
            ", jumlahCetakAkta=" + getJumlahCetakAkta() +
            ", uploadFile='" + getUploadFile() + "'" +
            ", status=" + getStatus() +
            ", statusSigned='" + getStatusSigned() + "'" +
            ", updateBy='" + getUpdateBy() + "'" +
            ", updateOn='" + getUpdateOn() + "'" +
            ", approveBy='" + getApproveBy() + "'" +
            ", approveOn='" + getApproveOn() + "'" +
            "}";
    }
}
