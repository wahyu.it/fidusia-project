package com.fidusia.app.web.rest;

import com.fidusia.app.domain.SkSubstitusi;
import com.fidusia.app.repository.SkSubtitusiRepository;
import com.fidusia.app.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.fidusia.app.domain.SkSubstitusi}.
 */
@RestController
@RequestMapping("/api/sk-subtitusis")
@Transactional
public class SkSubtitusiResource {

    private final Logger log = LoggerFactory.getLogger(SkSubtitusiResource.class);

    private static final String ENTITY_NAME = "skSubtitusi";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SkSubtitusiRepository skSubtitusiRepository;

    public SkSubtitusiResource(SkSubtitusiRepository skSubtitusiRepository) {
        this.skSubtitusiRepository = skSubtitusiRepository;
    }

    /**
     * {@code POST  /sk-subtitusis} : Create a new skSubtitusi.
     *
     * @param skSubtitusi the skSubtitusi to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new skSubtitusi, or with status {@code 400 (Bad Request)} if the skSubtitusi has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<SkSubstitusi> createSkSubtitusi(@RequestBody SkSubstitusi skSubtitusi) throws URISyntaxException {
        log.debug("REST request to save SkSubtitusi : {}", skSubtitusi);
        if (skSubtitusi.getId() != null) {
            throw new BadRequestAlertException("A new skSubtitusi cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SkSubstitusi result = skSubtitusiRepository.save(skSubtitusi);
        return ResponseEntity
            .created(new URI("/api/sk-subtitusis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /sk-subtitusis/:id} : Updates an existing skSubtitusi.
     *
     * @param id the id of the skSubtitusi to save.
     * @param skSubtitusi the skSubtitusi to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated skSubtitusi,
     * or with status {@code 400 (Bad Request)} if the skSubtitusi is not valid,
     * or with status {@code 500 (Internal Server Error)} if the skSubtitusi couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<SkSubstitusi> updateSkSubtitusi(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody SkSubstitusi skSubtitusi
    ) throws URISyntaxException {
        log.debug("REST request to update SkSubtitusi : {}, {}", id, skSubtitusi);
        if (skSubtitusi.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, skSubtitusi.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!skSubtitusiRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        SkSubstitusi result = skSubtitusiRepository.save(skSubtitusi);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, skSubtitusi.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /sk-subtitusis/:id} : Partial updates given fields of an existing skSubtitusi, field will ignore if it is null
     *
     * @param id the id of the skSubtitusi to save.
     * @param skSubtitusi the skSubtitusi to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated skSubtitusi,
     * or with status {@code 400 (Bad Request)} if the skSubtitusi is not valid,
     * or with status {@code 404 (Not Found)} if the skSubtitusi is not found,
     * or with status {@code 500 (Internal Server Error)} if the skSubtitusi couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<SkSubstitusi> partialUpdateSkSubtitusi(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody SkSubstitusi skSubtitusi
    ) throws URISyntaxException {
        log.debug("REST request to partial update SkSubtitusi partially : {}, {}", id, skSubtitusi);
        if (skSubtitusi.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, skSubtitusi.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!skSubtitusiRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<SkSubstitusi> result = skSubtitusiRepository
            .findById(skSubtitusi.getId())
            .map(existingSkSubtitusi -> {
                if (skSubtitusi.getNomor() != null) {
                    existingSkSubtitusi.setNomor(skSubtitusi.getNomor());
                }
                if (skSubtitusi.getTanggal() != null) {
                    existingSkSubtitusi.setTanggal(skSubtitusi.getTanggal());
                }
                if (skSubtitusi.getRecordStatus() != null) {
                    existingSkSubtitusi.setRecordStatus(skSubtitusi.getRecordStatus());
                }
                if (skSubtitusi.getUpdateBy() != null) {
                    existingSkSubtitusi.setUpdateBy(skSubtitusi.getUpdateBy());
                }
                if (skSubtitusi.getUpdateOn() != null) {
                    existingSkSubtitusi.setUpdateOn(skSubtitusi.getUpdateOn());
                }
                if (skSubtitusi.getFileNameSk() != null) {
                    existingSkSubtitusi.setFileNameSk(skSubtitusi.getFileNameSk());
                }
                if (skSubtitusi.getFileNameLamp() != null) {
                    existingSkSubtitusi.setFileNameLamp(skSubtitusi.getFileNameLamp());
                }
                if (skSubtitusi.getStatusSign() != null) {
                    existingSkSubtitusi.setStatusSign(skSubtitusi.getStatusSign());
                }

                return existingSkSubtitusi;
            })
            .map(skSubtitusiRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, skSubtitusi.getId().toString())
        );
    }

    /**
     * {@code GET  /sk-subtitusis} : get all the skSubtitusis.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of skSubtitusis in body.
     */
    @GetMapping("")
    public ResponseEntity<List<SkSubstitusi>> getAllSkSubtitusis(Pageable pageable) {
        log.debug("REST request to get a page of SkSubtitusis");
        Page<SkSubstitusi> page = skSubtitusiRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /sk-subtitusis/:id} : get the "id" skSubtitusi.
     *
     * @param id the id of the skSubtitusi to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the skSubtitusi, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<SkSubstitusi> getSkSubtitusi(@PathVariable("id") Long id) {
        log.debug("REST request to get SkSubtitusi : {}", id);
        Optional<SkSubstitusi> skSubtitusi = skSubtitusiRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(skSubtitusi);
    }

    /**
     * {@code DELETE  /sk-subtitusis/:id} : delete the "id" skSubtitusi.
     *
     * @param id the id of the skSubtitusi to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteSkSubtitusi(@PathVariable("id") Long id) {
        log.debug("REST request to delete SkSubtitusi : {}", id);
        skSubtitusiRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
