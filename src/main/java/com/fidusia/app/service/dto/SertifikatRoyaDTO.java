package com.fidusia.app.service.dto;

import java.time.LocalDate;
import java.time.ZonedDateTime;


public class SertifikatRoyaDTO {

    private Long id;

    private String noSertifikatOri;

    private ZonedDateTime tglSertifikatOri;

    private String uniqueKey;

    private LocalDate tglOrder;

    private LocalDate tglLunas;

    private String namaNotarisOri;
    
    private String wilNotarisOri;
    
    private Integer royaType;
    
    private String noRoya;
    
    private LocalDate tglRoya;

    private String status;

    private String message;

    private String registrationId;
    
    private Boolean desentralisasi;
    
	public SertifikatRoyaDTO(Long id, String noSertifikatOri, ZonedDateTime tglSertifikatOri, String uniqueKey,
			LocalDate tglOrder, LocalDate tglLunas, String namaNotarisOri, String wilNotarisOri, Integer royaType,
			String noRoya, LocalDate tglRoya, String status, String message, String registrationId, boolean desentralisasi) {
		super();
		this.id = id;
		this.noSertifikatOri = noSertifikatOri;
		this.tglSertifikatOri = tglSertifikatOri;
		this.uniqueKey = uniqueKey;
		this.tglOrder = tglOrder;
		this.tglLunas = tglLunas;
		this.namaNotarisOri = namaNotarisOri;
		this.wilNotarisOri = wilNotarisOri;
		this.royaType = royaType;
		this.noRoya = noRoya;
		this.tglRoya = tglRoya;
		this.status = status;
		this.message = message;
		this.registrationId = registrationId;
		this.desentralisasi = desentralisasi;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNoSertifikatOri() {
		return noSertifikatOri;
	}

	public void setNoSertifikatOri(String noSertifikatOri) {
		this.noSertifikatOri = noSertifikatOri;
	}

	public ZonedDateTime getTglSertifikatOri() {
		return tglSertifikatOri;
	}

	public void setTglSertifikatOri(ZonedDateTime tglSertifikatOri) {
		this.tglSertifikatOri = tglSertifikatOri;
	}

	public String getUniqueKey() {
		return uniqueKey;
	}

	public void setUniqueKey(String uniqueKey) {
		this.uniqueKey = uniqueKey;
	}

	public LocalDate getTglOrder() {
		return tglOrder;
	}

	public void setTglOrder(LocalDate tglOrder) {
		this.tglOrder = tglOrder;
	}

	public LocalDate getTglLunas() {
		return tglLunas;
	}

	public void setTglLunas(LocalDate tglLunas) {
		this.tglLunas = tglLunas;
	}

	public String getNamaNotarisOri() {
		return namaNotarisOri;
	}

	public void setNamaNotarisOri(String namaNotarisOri) {
		this.namaNotarisOri = namaNotarisOri;
	}

	public String getWilNotarisOri() {
		return wilNotarisOri;
	}

	public void setWilNotarisOri(String wilNotarisOri) {
		this.wilNotarisOri = wilNotarisOri;
	}

	public Integer getRoyaType() {
		return royaType;
	}

	public void setRoyaType(Integer royaType) {
		this.royaType = royaType;
	}

	public String getNoRoya() {
		return noRoya;
	}

	public void setNoRoya(String noRoya) {
		this.noRoya = noRoya;
	}

	public LocalDate getTglRoya() {
		return tglRoya;
	}

	public void setTglRoya(LocalDate tglRoya) {
		this.tglRoya = tglRoya;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public Boolean isDesentralisasi() {
		return desentralisasi;
	}

	public void setDesentralisasi(Boolean desentralisasi) {
		this.desentralisasi = desentralisasi;
	}

	@Override
	public String toString() {
		return "SertifikatRoyaDTO [id=" + id + ", noSertifikatOri=" + noSertifikatOri + ", tglSertifikatOri="
				+ tglSertifikatOri + ", uniqueKey=" + uniqueKey + ", tglOrder=" + tglOrder + ", tglLunas="
				+ tglLunas + ", namaNotarisOri=" + namaNotarisOri + ", wilNotarisOri=" + wilNotarisOri + ", royaType="
				+ royaType + ", noRoya=" + noRoya + ", tglRoya=" + tglRoya + ", status=" + status + ", message="
				+ message + ", registrationId=" + registrationId + ", desentralisasi=" + desentralisasi + "]";
	}
}
