package com.fidusia.app.domain.util;


public enum BerkasStatus {

    REJECTED(-2), CANCELLED(-1), NONE(0), INCOMPLETE(1), PARTIAL(2), COMPLETE(3), DONE(4);

    private int value;

    BerkasStatus(int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }
}
