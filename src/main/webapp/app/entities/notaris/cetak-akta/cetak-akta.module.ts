import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasimonitoringSharedModule } from 'app/shared/shared.module';
import { CetakAktaComponent } from './cetak-akta.component';
import { cetakAktaState } from './cetak-akta.route';

@NgModule({
  imports: [SisteminformasimonitoringSharedModule, RouterModule.forChild(cetakAktaState)],
  declarations: [CetakAktaComponent]
})
export class CetakAktaModule {}
