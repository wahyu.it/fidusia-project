import { Routes } from '@angular/router';
import { RegistrasiRoyaComponent } from './registrasiroya.component';
import { JhiResolvePagingParams } from 'ng-jhipster';

export const registrasiRoyaState: Routes = [{
  path: '',
  component: RegistrasiRoyaComponent,
  resolve: {
	  pagingParams: JhiResolvePagingParams
  }
  }
];
