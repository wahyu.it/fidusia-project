package com.fidusia.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A PenerimaKuasa.
 */
@Entity
@Table(name = "penerima_kuasa")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class PenerimaKuasa implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "titel")
    private String titel;

    @Column(name = "nama")
    private String nama;

    @Column(name = "komparisi")
    private String komparisi;

    @Column(name = "tts")
    private String tts;

    @Column(name = "sk")
    private String sk;

    @Column(name = "tanggal_sk")
    private LocalDate tanggalSk;

    @Column(name = "no_kontak")
    private String noKontak;

    @Column(name = "record_status")
    private Integer recordStatus;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "update_on")
    private ZonedDateTime updateOn;

    @JsonIgnoreProperties(value = { "penerimaKuasa" }, allowSetters = true)
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "penerimaKuasa")
    private Notaris notaris;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public PenerimaKuasa id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitel() {
        return this.titel;
    }

    public PenerimaKuasa titel(String titel) {
        this.setTitel(titel);
        return this;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getNama() {
        return this.nama;
    }

    public PenerimaKuasa nama(String nama) {
        this.setNama(nama);
        return this;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKomparisi() {
        return this.komparisi;
    }

    public PenerimaKuasa komparisi(String komparisi) {
        this.setKomparisi(komparisi);
        return this;
    }

    public void setKomparisi(String komparisi) {
        this.komparisi = komparisi;
    }

    public String getTts() {
        return this.tts;
    }

    public PenerimaKuasa tts(String tts) {
        this.setTts(tts);
        return this;
    }

    public void setTts(String tts) {
        this.tts = tts;
    }

    public String getSk() {
        return this.sk;
    }

    public PenerimaKuasa sk(String sk) {
        this.setSk(sk);
        return this;
    }

    public void setSk(String sk) {
        this.sk = sk;
    }

    public LocalDate getTanggalSk() {
        return this.tanggalSk;
    }

    public PenerimaKuasa tanggalSk(LocalDate tanggalSk) {
        this.setTanggalSk(tanggalSk);
        return this;
    }

    public void setTanggalSk(LocalDate tanggalSk) {
        this.tanggalSk = tanggalSk;
    }

    public String getNoKontak() {
        return this.noKontak;
    }

    public PenerimaKuasa noKontak(String noKontak) {
        this.setNoKontak(noKontak);
        return this;
    }

    public void setNoKontak(String noKontak) {
        this.noKontak = noKontak;
    }

    public Integer getRecordStatus() {
        return this.recordStatus;
    }

    public PenerimaKuasa recordStatus(Integer recordStatus) {
        this.setRecordStatus(recordStatus);
        return this;
    }

    public void setRecordStatus(Integer recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getUpdateBy() {
        return this.updateBy;
    }

    public PenerimaKuasa updateBy(String updateBy) {
        this.setUpdateBy(updateBy);
        return this;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public ZonedDateTime getUpdateOn() {
        return this.updateOn;
    }

    public PenerimaKuasa updateOn(ZonedDateTime updateOn) {
        this.setUpdateOn(updateOn);
        return this;
    }

    public void setUpdateOn(ZonedDateTime updateOn) {
        this.updateOn = updateOn;
    }

    public Notaris getNotaris() {
        return this.notaris;
    }

    public void setNotaris(Notaris notaris) {
        if (this.notaris != null) {
            this.notaris.setPenerimaKuasa(null);
        }
        if (notaris != null) {
            notaris.setPenerimaKuasa(this);
        }
        this.notaris = notaris;
    }

    public PenerimaKuasa notaris(Notaris notaris) {
        this.setNotaris(notaris);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PenerimaKuasa)) {
            return false;
        }
        return getId() != null && getId().equals(((PenerimaKuasa) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PenerimaKuasa{" +
            "id=" + getId() +
            ", titel='" + getTitel() + "'" +
            ", nama='" + getNama() + "'" +
            ", komparisi='" + getKomparisi() + "'" +
            ", tts='" + getTts() + "'" +
            ", sk='" + getSk() + "'" +
            ", tanggalSk='" + getTanggalSk() + "'" +
            ", noKontak='" + getNoKontak() + "'" +
            ", recordStatus=" + getRecordStatus() +
            ", updateBy='" + getUpdateBy() + "'" +
            ", updateOn='" + getUpdateOn() + "'" +
            "}";
    }
}
