package com.fidusia.app.web.rest;

import com.fidusia.app.domain.Leasing;
import com.fidusia.app.repository.LeasingRepository;
import com.fidusia.app.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.fidusia.app.domain.Leasing}.
 */
@RestController
@RequestMapping("/api/leasings")
@Transactional
public class LeasingResource {

    private final Logger log = LoggerFactory.getLogger(LeasingResource.class);

    private static final String ENTITY_NAME = "leasing";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LeasingRepository leasingRepository;

    public LeasingResource(LeasingRepository leasingRepository) {
        this.leasingRepository = leasingRepository;
    }

    /**
     * {@code POST  /leasings} : Create a new leasing.
     *
     * @param leasing the leasing to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new leasing, or with status {@code 400 (Bad Request)} if the leasing has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<Leasing> createLeasing(@RequestBody Leasing leasing) throws URISyntaxException {
        log.debug("REST request to save Leasing : {}", leasing);
        if (leasing.getId() != null) {
            throw new BadRequestAlertException("A new leasing cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Leasing result = leasingRepository.save(leasing);
        return ResponseEntity
            .created(new URI("/api/leasings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /leasings/:id} : Updates an existing leasing.
     *
     * @param id the id of the leasing to save.
     * @param leasing the leasing to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated leasing,
     * or with status {@code 400 (Bad Request)} if the leasing is not valid,
     * or with status {@code 500 (Internal Server Error)} if the leasing couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<Leasing> updateLeasing(@PathVariable(value = "id", required = false) final Long id, @RequestBody Leasing leasing)
        throws URISyntaxException {
        log.debug("REST request to update Leasing : {}, {}", id, leasing);
        if (leasing.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, leasing.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!leasingRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Leasing result = leasingRepository.save(leasing);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, leasing.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /leasings/:id} : Partial updates given fields of an existing leasing, field will ignore if it is null
     *
     * @param id the id of the leasing to save.
     * @param leasing the leasing to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated leasing,
     * or with status {@code 400 (Bad Request)} if the leasing is not valid,
     * or with status {@code 404 (Not Found)} if the leasing is not found,
     * or with status {@code 500 (Internal Server Error)} if the leasing couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Leasing> partialUpdateLeasing(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Leasing leasing
    ) throws URISyntaxException {
        log.debug("REST request to partial update Leasing partially : {}, {}", id, leasing);
        if (leasing.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, leasing.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!leasingRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Leasing> result = leasingRepository
            .findById(leasing.getId())
            .map(existingLeasing -> {
                if (leasing.getKode() != null) {
                    existingLeasing.setKode(leasing.getKode());
                }
                if (leasing.getNama() != null) {
                    existingLeasing.setNama(leasing.getNama());
                }
                if (leasing.getAlamat() != null) {
                    existingLeasing.setAlamat(leasing.getAlamat());
                }
                if (leasing.getKota() != null) {
                    existingLeasing.setKota(leasing.getKota());
                }
                if (leasing.getProvinsi() != null) {
                    existingLeasing.setProvinsi(leasing.getProvinsi());
                }
                if (leasing.getKecamatan() != null) {
                    existingLeasing.setKecamatan(leasing.getKecamatan());
                }
                if (leasing.getKelurahan() != null) {
                    existingLeasing.setKelurahan(leasing.getKelurahan());
                }
                if (leasing.getRt() != null) {
                    existingLeasing.setRt(leasing.getRt());
                }
                if (leasing.getRw() != null) {
                    existingLeasing.setRw(leasing.getRw());
                }
                if (leasing.getNpwp() != null) {
                    existingLeasing.setNpwp(leasing.getNpwp());
                }
                if (leasing.getKodePos() != null) {
                    existingLeasing.setKodePos(leasing.getKodePos());
                }
                if (leasing.getNoKontak() != null) {
                    existingLeasing.setNoKontak(leasing.getNoKontak());
                }
                if (leasing.getTechNoKontak() != null) {
                    existingLeasing.setTechNoKontak(leasing.getTechNoKontak());
                }
                if (leasing.getTechEmail() != null) {
                    existingLeasing.setTechEmail(leasing.getTechEmail());
                }
                if (leasing.getAcctNoKontak() != null) {
                    existingLeasing.setAcctNoKontak(leasing.getAcctNoKontak());
                }
                if (leasing.getAcctEmail() != null) {
                    existingLeasing.setAcctEmail(leasing.getAcctEmail());
                }
                if (leasing.getRecordStatus() != null) {
                    existingLeasing.setRecordStatus(leasing.getRecordStatus());
                }
                if (leasing.getUpdateBy() != null) {
                    existingLeasing.setUpdateBy(leasing.getUpdateBy());
                }
                if (leasing.getUpdateOn() != null) {
                    existingLeasing.setUpdateOn(leasing.getUpdateOn());
                }
                if (leasing.getEmail() != null) {
                    existingLeasing.setEmail(leasing.getEmail());
                }
                if (leasing.getSk() != null) {
                    existingLeasing.setSk(leasing.getSk());
                }

                return existingLeasing;
            })
            .map(leasingRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, leasing.getId().toString())
        );
    }

    /**
     * {@code GET  /leasings} : get all the leasings.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of leasings in body.
     */
    @GetMapping("")
    public ResponseEntity<List<Leasing>> getAllLeasings(Pageable pageable) {
        log.debug("REST request to get a page of Leasings");
        Page<Leasing> page = leasingRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /leasings/:id} : get the "id" leasing.
     *
     * @param id the id of the leasing to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the leasing, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<Leasing> getLeasing(@PathVariable("id") Long id) {
        log.debug("REST request to get Leasing : {}", id);
        Optional<Leasing> leasing = leasingRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(leasing);
    }

    /**
     * {@code DELETE  /leasings/:id} : delete the "id" leasing.
     *
     * @param id the id of the leasing to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteLeasing(@PathVariable("id") Long id) {
        log.debug("REST request to delete Leasing : {}", id);
        leasingRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
