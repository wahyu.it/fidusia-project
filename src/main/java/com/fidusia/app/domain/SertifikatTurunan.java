package com.fidusia.app.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import com.fidusia.app.domain.util.SertifikatStatus;
import com.fidusia.app.util.DateUtils;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;

/*
 * status: 0=new, 1=in-progress, 3=paid, 4=completed, -1=cancel, -2=rejected (not found in ahu)
 * kategori : 2 = perbaikan, 3 = perubahan, 4 = penghapusan
 */
@Entity
@Table(name = "sertifikat_turunan")
public class SertifikatTurunan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Max(value = 5)
    @Column(name = "kategori", nullable = false)
    private Integer kategori;

    @Size(max = 20)
    @Column(name = "registration_id", length = 20)
    private String registrationId;

    @Size(max = 20)
    @Column(name = "kode_voucher", length = 20)
    private String kodeVoucher;

    @Column(name = "tgl_voucher")
    private ZonedDateTime tglVoucher;

    @Column(name = "nilai_pnbp")
    private Long nilaiPnbp;

    @Column(name = "tgl_bayar_pnbp")
    private ZonedDateTime tglBayarPnbp;

    @Column(name = "no_referensi_bni")
    private String noReferensiBni;
    
    @Size(max = 50)
    @Column(name = "no_sertifikat", length = 50)
    private String noSertifikat;

    @Column(name = "tgl_sertifikat")
    private ZonedDateTime tglSertifikat;

    @Column(name = "nilai_jasa")
    private Long nilaiJasa;

    @ManyToOne
    @JoinColumn(name="invoice_id")
    private Invoice invoice;  

    @Column(name = "tgl_cancel")
    private LocalDate tglCancel;

    @ManyToOne
    @JoinColumn(name="invoice_cancel_id")
    private Invoice invoiceCancel;
    
    @NotNull
    @Max(value = 9)
    @Column(name = "status", nullable = false)
    private Integer status;

    @Column(name = "update_on")
    private ZonedDateTime updateOn;

    @Size(max = 50)
    @Column(name = "update_by", length = 50)
    private String updateBy;

    @Column(name = "sync_flag")
    private Boolean syncFlag;

    @ManyToOne
    @JoinColumn(name="akta_id")
    private Akta akta;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="sertifikat_ori_id")
    private Sertifikat sertifikatOri;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="sertifikat_turunan_id")
    private SertifikatTurunan sertifikatTurunan;

    @Size(max = 50)
    @Column(name = "pernyataan", length = 50)
    private String pernyataan;

    @Column(name = "invoice_submitted")
    private Boolean invoiceSubmitted;

    @Transient
    private String tglSertifikatTr;

    @Transient
    private String biayaPnbpTr;

    @Transient
    private String biayaJasaTr;

    @Transient
    private String ppkNomorTr;

    @Transient
    private String aktaNotarisNamaTr;

    @Transient
    private String aktaNotarisWilayahTr;

    @Transient
    private String aktaTanggalTr;

    @Transient
    private String aktaNomorTr;

    @Transient
    private Long nilaiPenjaminanTr;
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getKategori() {
		return kategori;
	}

	public void setKategori(Integer kategori) {
		this.kategori = kategori;
	}

	public String getRegistrationId() {
		return registrationId;
	}

	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	public String getKodeVoucher() {
		return kodeVoucher;
	}

	public void setKodeVoucher(String kodeVoucher) {
		this.kodeVoucher = kodeVoucher;
	}

	public ZonedDateTime getTglVoucher() {
		return tglVoucher;
	}

	public void setTglVoucher(ZonedDateTime tglVoucher) {
		this.tglVoucher = tglVoucher;
	}

	public Long getNilaiPnbp() {
		return nilaiPnbp;
	}

	public void setNilaiPnbp(Long nilaiPnbp) {
		this.nilaiPnbp = nilaiPnbp;
	}

	public ZonedDateTime getTglBayarPnbp() {
		return tglBayarPnbp;
	}

	public void setTglBayarPnbp(ZonedDateTime tglBayarPnbp) {
		this.tglBayarPnbp = tglBayarPnbp;
	}

	public String getNoReferensiBni() {
		return noReferensiBni;
	}

	public void setNoReferensiBni(String noReferensiBni) {
		this.noReferensiBni = noReferensiBni;
	}

	public String getNoSertifikat() {
		return noSertifikat;
	}

	public void setNoSertifikat(String noSertifikat) {
		this.noSertifikat = noSertifikat;
	}

	public ZonedDateTime getTglSertifikat() {
		return tglSertifikat;
	}

	public void setTglSertifikat(ZonedDateTime tglSertifikat) {
		this.tglSertifikat = tglSertifikat;
	}

	public Long getNilaiJasa() {
		return nilaiJasa;
	}

	public void setNilaiJasa(Long nilaiJasa) {
		this.nilaiJasa = nilaiJasa;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public LocalDate getTglCancel() {
		return tglCancel;
	}

	public void setTglCancel(LocalDate tglCancel) {
		this.tglCancel = tglCancel;
	}

	public Invoice getInvoiceCancel() {
		return invoiceCancel;
	}

	public void setInvoiceCancel(Invoice invoiceCancel) {
		this.invoiceCancel = invoiceCancel;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public ZonedDateTime getUpdateOn() {
		return updateOn;
	}

	public void setUpdateOn(ZonedDateTime updateOn) {
		this.updateOn = updateOn;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Boolean getSyncFlag() {
		return syncFlag;
	}

	public void setSyncFlag(Boolean syncFlag) {
		this.syncFlag = syncFlag;
	}

	public Akta getAkta() {
		return akta;
	}

	public void setAkta(Akta akta) {
		this.akta = akta;
	}

	public Sertifikat getSertifikatOri() {
		return sertifikatOri;
	}

	public void setSertifikatOri(Sertifikat sertifikatOri) {
		this.sertifikatOri = sertifikatOri;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public Boolean getInvoiceSubmitted() {
		return invoiceSubmitted;
	}

	public void setInvoiceSubmitted(Boolean invoiceSubmitted) {
		this.invoiceSubmitted = invoiceSubmitted;
	}

    public String getTglSertifikatTr() {
        return (tglSertifikat != null ? DateUtils.formatZonedDateTime(tglSertifikat,DateUtils.FORMATTER_ISO_LOCAL_DATE_V2) : null);
    }

	public void setTglSertifikatTr(String tglSertifikatTr) {
		this.tglSertifikatTr = tglSertifikatTr;
	}

    public String getBiayaPnbpTr() {
    	Long biayaPnbp = (status != null && SertifikatStatus.CANCELLED.value() == status.intValue()) ? 0L : this.nilaiPnbp;
        return ((biayaPnbp != null && biayaPnbp > 0) ? biayaPnbp.toString() : "") + ".00";
    }

	public void setBiayaPnbpTr(String biayaPnbpTr) {
		this.biayaPnbpTr = biayaPnbpTr;
	}

    public String getBiayaJasaTr() {
    	Long biayaJasa = (status != null && SertifikatStatus.CANCELLED.value() == status.intValue()) ? (-1)*this.nilaiJasa : this.nilaiJasa;
        return ((biayaJasa != null && biayaJasa != 0) ? biayaJasa.toString() : "") + ".00";
    }

	public void setBiayaJasaTr(String biayaJasaTr) {
		this.biayaJasaTr = biayaJasaTr;
	}

    public String getPpkNomorTr() {
        if(akta != null) {
            Ppk ppk = akta.getPpk();
            if(ppk != null)
                return ppk.getNomor();
        }
        return ppkNomorTr;
    }

	public void setPpkNomorTr(String ppkNomorTr) {
		this.ppkNomorTr = ppkNomorTr;
	}

    public String getAktaNotarisNamaTr() {
        if(akta != null) {
            Notaris notaris = akta.getNotaris();
            if(notaris != null)
                return (notaris.getTitelShortOne() != null && notaris.getTitelShortOne().length() > 0 ? notaris.getTitelShortOne() + " " : "")
                        + notaris.getNama()
                        + (notaris.getTitelShortTwo() != null && notaris.getTitelShortTwo().length() > 0 ? ", " + notaris.getTitelShortTwo() : "");
        }

        return aktaNotarisNamaTr;
    }

	public void setAktaNotarisNamaTr(String aktaNotarisNamaTr) {
		this.aktaNotarisNamaTr = aktaNotarisNamaTr;
	}

    public String getAktaNotarisWilayahTr() {
        if(akta != null) {
            Notaris notaris = akta.getNotaris();
            if(notaris != null)
                return notaris.getWilayahKerja();
        }
        return aktaNotarisWilayahTr;
    }

	public void setAktaNotarisWilayahTr(String aktaNotarisWilayahTr) {
		this.aktaNotarisWilayahTr = aktaNotarisWilayahTr;
	}

    public String getAktaTanggalTr() {
        if(akta != null) {
            return DateUtils.formatDate(akta.getTanggal().toString(), DateUtils.FORMATTER_ISO_LOCAL_DATE_V2);
        }
        return aktaTanggalTr;
    }

	public void setAktaTanggalTr(String aktaTanggalTr) {
		this.aktaTanggalTr = aktaTanggalTr;
	}

    public String getAktaNomorTr() {
        if(akta != null) {
            return akta.getNomor();
        }
        return aktaNomorTr;
    }

	public void setAktaNomorTr(String aktaNomorTr) {
		this.aktaNomorTr = aktaNomorTr;
	}

	public Long getNilaiPenjaminanTr() {
        if(akta != null && akta.getPpk() != null) {
            return akta.getPpk().getNilaiPenjaminan();
        }
		return nilaiPenjaminanTr;
	}

	public void setNilaiPenjaminanTr(Long nilaiPenjaminanTr) {
		this.nilaiPenjaminanTr = nilaiPenjaminanTr;
	}

	public SertifikatTurunan getSertifikatTurunan() {
		return sertifikatTurunan;
	}

	public void setSertifikatTurunan(SertifikatTurunan sertifikatTurunan) {
		this.sertifikatTurunan = sertifikatTurunan;
	}

	public String getPernyataan() {
		return pernyataan;
	}

	public void setPernyataan(String pernyataan) {
		this.pernyataan = pernyataan;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SertifikatTurunan other = (SertifikatTurunan) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SertifikatTurunan [id=" + id + ", kategori=" + kategori + ", registrationId=" + registrationId
				+ ", kodeVoucher=" + kodeVoucher + ", tglVoucher=" + tglVoucher + ", nilaiPnbp=" + nilaiPnbp
				+ ", tglBayarPnbp=" + tglBayarPnbp + ", noReferensiBni=" + noReferensiBni + ", noSertifikat="
				+ noSertifikat + ", tglSertifikat=" + tglSertifikat + ", nilaiJasa=" + nilaiJasa + ", invoice="
				+ invoice + ", tglCancel=" + tglCancel + ", invoiceCancel=" + invoiceCancel + ", status=" + status
				+ ", updateOn=" + updateOn + ", updateBy=" + updateBy + ", syncFlag=" + syncFlag + ", akta=" + akta
				+ ", sertifikatOri=" + sertifikatOri + ", sertifikatTurunan=" + sertifikatTurunan
				+ ", invoiceSubmitted=" + invoiceSubmitted + ", tglSertifikatTr=" + tglSertifikatTr + ", biayaPnbpTr="
				+ biayaPnbpTr + ", biayaJasaTr=" + biayaJasaTr + ", ppkNomorTr=" + ppkNomorTr + ", aktaNotarisNamaTr="
				+ aktaNotarisNamaTr + ", aktaNotarisWilayahTr=" + aktaNotarisWilayahTr + ", aktaTanggalTr="
				+ aktaTanggalTr + ", aktaNomorTr=" + aktaNomorTr + ", nilaiPenjaminanTr=" + nilaiPenjaminanTr + ", pernyataan=" + pernyataan + "]";
	}

//	@Override
//	public String toString() {
//		return "SertifikatTurunan [id=" + id + ", kategori=" + kategori + ", registrationId=" + registrationId + ", kodeVoucher=" + kodeVoucher + ", tglVoucher=" + tglVoucher
//				+ ", nilaiPnbp=" + nilaiPnbp + ", tglBayarPnbp=" + tglBayarPnbp + ", noReferensiBni=" + noReferensiBni + ", noSertifikat=" + noSertifikat + ", tglSertifikat="
//				+ tglSertifikat + ", nilaiJasa=" + nilaiJasa + ", invoice=" + invoice + ", tglCancel=" + tglCancel + ", invoiceCancel=" + invoiceCancel + ", status=" + status
//				+ ", updateOn=" + updateOn + ", updateBy=" + updateBy + ", syncFlag=" + syncFlag + ", akta=" + akta + ", sertifikatOri=" + sertifikatOri
//				+ ", invoiceSubmitted=" + invoiceSubmitted + ", tglSertifikatTr=" + tglSertifikatTr + ", biayaPnbpTr=" + biayaPnbpTr + ", biayaJasaTr=" + biayaJasaTr
//				+ ", ppkNomorTr=" + ppkNomorTr + ", aktaNotarisNamaTr=" + aktaNotarisNamaTr + ", aktaNotarisWilayahTr=" + aktaNotarisWilayahTr + ", aktaTanggalTr="
//				+ aktaTanggalTr + ", aktaNomorTr=" + aktaNomorTr + ", nilaiPenjaminanTr=" + nilaiPenjaminanTr + "]";
//	}
}
