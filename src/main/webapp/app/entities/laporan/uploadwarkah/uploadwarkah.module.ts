import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasimonitoringSharedModule } from 'app/shared/shared.module';
import { UploadWarkahComponent } from '../uploadwarkah/uploadwarkah.component';
import { uploadWarkahState } from './uploadwarkah.route';
import { WarkahDialogComponent } from './warkah-dialog.component';

@NgModule({
  imports: [SisteminformasimonitoringSharedModule, RouterModule.forChild(uploadWarkahState)],
  declarations: [UploadWarkahComponent, WarkahDialogComponent],
  entryComponents: [WarkahDialogComponent]
})
export class UploadWarkahModule {}
