package com.fidusia.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Sertifikat.
 */
@Entity
@Table(name = "sertifikat")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Sertifikat implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "kode_voucher")
    private String kodeVoucher;

    @Column(name = "tgl_voucher")
    private ZonedDateTime tglVoucher;

    @Column(name = "no_sertifikat")
    private String noSertifikat;

    @Column(name = "tgl_sertifikat")
    private ZonedDateTime tglSertifikat;

    @Column(name = "biaya_pnbp")
    private Long biayaPnbp;

    @Column(name = "no_referensi_bni")
    private String noReferensiBni;

    @Column(name = "status")
    private Integer status;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "update_on")
    private ZonedDateTime updateOn;

    @Column(name = "registration_id")
    private String registrationId;

    @Column(name = "tgl_order")
    private LocalDate tglOrder;

    @Column(name = "tgl_cancel")
    private LocalDate tglCancel;

    @Column(name = "biaya_jasa")
    private Long biayaJasa;

    @Column(name = "invoice_submitted")
    private Boolean invoiceSubmitted;

    @Column(name = "report_submitted")
    private Boolean reportSubmitted;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(
        value = { "ppk", "notaris", "penerimaKuasa", "saksiOne", "saksiTwo", "cabang", "skSubtitusi" },
        allowSetters = true
    )
    private Akta akta;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "leasing" }, allowSetters = true)
    private Cabang cabang;

    @ManyToOne(fetch = FetchType.LAZY)
    private Invoice invoice;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Sertifikat id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKodeVoucher() {
        return this.kodeVoucher;
    }

    public Sertifikat kodeVoucher(String kodeVoucher) {
        this.setKodeVoucher(kodeVoucher);
        return this;
    }

    public void setKodeVoucher(String kodeVoucher) {
        this.kodeVoucher = kodeVoucher;
    }

    public ZonedDateTime getTglVoucher() {
        return this.tglVoucher;
    }

    public Sertifikat tglVoucher(ZonedDateTime tglVoucher) {
        this.setTglVoucher(tglVoucher);
        return this;
    }

    public void setTglVoucher(ZonedDateTime tglVoucher) {
        this.tglVoucher = tglVoucher;
    }

    public String getNoSertifikat() {
        return this.noSertifikat;
    }

    public Sertifikat noSertifikat(String noSertifikat) {
        this.setNoSertifikat(noSertifikat);
        return this;
    }

    public void setNoSertifikat(String noSertifikat) {
        this.noSertifikat = noSertifikat;
    }

    public ZonedDateTime getTglSertifikat() {
        return this.tglSertifikat;
    }

    public Sertifikat tglSertifikat(ZonedDateTime tglSertifikat) {
        this.setTglSertifikat(tglSertifikat);
        return this;
    }

    public void setTglSertifikat(ZonedDateTime tglSertifikat) {
        this.tglSertifikat = tglSertifikat;
    }

    public Long getBiayaPnbp() {
        return this.biayaPnbp;
    }

    public Sertifikat biayaPnbp(Long biayaPnbp) {
        this.setBiayaPnbp(biayaPnbp);
        return this;
    }

    public void setBiayaPnbp(Long biayaPnbp) {
        this.biayaPnbp = biayaPnbp;
    }

    public String getNoReferensiBni() {
        return this.noReferensiBni;
    }

    public Sertifikat noReferensiBni(String noReferensiBni) {
        this.setNoReferensiBni(noReferensiBni);
        return this;
    }

    public void setNoReferensiBni(String noReferensiBni) {
        this.noReferensiBni = noReferensiBni;
    }

    public Integer getStatus() {
        return this.status;
    }

    public Sertifikat status(Integer status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getUpdateBy() {
        return this.updateBy;
    }

    public Sertifikat updateBy(String updateBy) {
        this.setUpdateBy(updateBy);
        return this;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public ZonedDateTime getUpdateOn() {
        return this.updateOn;
    }

    public Sertifikat updateOn(ZonedDateTime updateOn) {
        this.setUpdateOn(updateOn);
        return this;
    }

    public void setUpdateOn(ZonedDateTime updateOn) {
        this.updateOn = updateOn;
    }

    public String getRegistrationId() {
        return this.registrationId;
    }

    public Sertifikat registrationId(String registrationId) {
        this.setRegistrationId(registrationId);
        return this;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    public LocalDate getTglOrder() {
        return this.tglOrder;
    }

    public Sertifikat tglOrder(LocalDate tglOrder) {
        this.setTglOrder(tglOrder);
        return this;
    }

    public void setTglOrder(LocalDate tglOrder) {
        this.tglOrder = tglOrder;
    }

    public LocalDate getTglCancel() {
        return this.tglCancel;
    }

    public Sertifikat tglCancel(LocalDate tglCancel) {
        this.setTglCancel(tglCancel);
        return this;
    }

    public void setTglCancel(LocalDate tglCancel) {
        this.tglCancel = tglCancel;
    }

    public Long getBiayaJasa() {
        return this.biayaJasa;
    }

    public Sertifikat biayaJasa(Long biayaJasa) {
        this.setBiayaJasa(biayaJasa);
        return this;
    }

    public void setBiayaJasa(Long biayaJasa) {
        this.biayaJasa = biayaJasa;
    }

    public Boolean getInvoiceSubmitted() {
        return this.invoiceSubmitted;
    }

    public Sertifikat invoiceSubmitted(Boolean invoiceSubmitted) {
        this.setInvoiceSubmitted(invoiceSubmitted);
        return this;
    }

    public void setInvoiceSubmitted(Boolean invoiceSubmitted) {
        this.invoiceSubmitted = invoiceSubmitted;
    }

    public Boolean getReportSubmitted() {
        return this.reportSubmitted;
    }

    public Sertifikat reportSubmitted(Boolean reportSubmitted) {
        this.setReportSubmitted(reportSubmitted);
        return this;
    }

    public void setReportSubmitted(Boolean reportSubmitted) {
        this.reportSubmitted = reportSubmitted;
    }

    public Akta getAkta() {
        return this.akta;
    }

    public void setAkta(Akta akta) {
        this.akta = akta;
    }

    public Sertifikat akta(Akta akta) {
        this.setAkta(akta);
        return this;
    }

    public Cabang getCabang() {
        return this.cabang;
    }

    public void setCabang(Cabang cabang) {
        this.cabang = cabang;
    }

    public Sertifikat cabang(Cabang cabang) {
        this.setCabang(cabang);
        return this;
    }

    public Invoice getInvoice() {
        return this.invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public Sertifikat invoice(Invoice invoice) {
        this.setInvoice(invoice);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Sertifikat)) {
            return false;
        }
        return getId() != null && getId().equals(((Sertifikat) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Sertifikat{" +
            "id=" + getId() +
            ", kodeVoucher='" + getKodeVoucher() + "'" +
            ", tglVoucher='" + getTglVoucher() + "'" +
            ", noSertifikat='" + getNoSertifikat() + "'" +
            ", tglSertifikat='" + getTglSertifikat() + "'" +
            ", biayaPnbp=" + getBiayaPnbp() +
            ", noReferensiBni='" + getNoReferensiBni() + "'" +
            ", status=" + getStatus() +
            ", updateBy='" + getUpdateBy() + "'" +
            ", updateOn='" + getUpdateOn() + "'" +
            ", registrationId='" + getRegistrationId() + "'" +
            ", tglOrder='" + getTglOrder() + "'" +
            ", tglCancel='" + getTglCancel() + "'" +
            ", biayaJasa=" + getBiayaJasa() +
            ", invoiceSubmitted='" + getInvoiceSubmitted() + "'" +
            ", reportSubmitted='" + getReportSubmitted() + "'" +
            "}";
    }
}
