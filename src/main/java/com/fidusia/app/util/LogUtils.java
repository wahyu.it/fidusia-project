package com.fidusia.app.util;

import org.slf4j.Logger;

import com.fidusia.app.service.SlackInhookService;

public class LogUtils {
	
	private final static String NO_CONTENT = "(No content to log)";

	private final static String LOG_PREFIX = "Debug:\n";
	
//	public static void debug(Object o, Logger log, GChatService gChat) {
//		debug(o, log, null, null);
//	}
//	
//	public static void debugReport(Object o, Logger log, GChatService gChat) {
//		if(o == null) {
//			log.debug(NO_CONTENT);
//		} else {
//			log.debug(LOG_PREFIX + o.toString());
//
//			if (gChat != null) {
//				gChat.sendReportAsync(o.toString());
//			}
//		}
//	}
//	
	public static void debug(Object o, Logger log, SlackInhookService slack) {
		debug(o, log);
		if (slack != null) {
			slack.sendMessageAsync(o.toString());
		}
	}
	
	public static void debug(Object o, Logger log) {
		if(o == null) {
			log.debug(NO_CONTENT);
		} else {
			log.debug(LOG_PREFIX + o.toString());
		}
	}
}
