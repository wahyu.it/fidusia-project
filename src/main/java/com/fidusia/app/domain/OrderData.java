package com.fidusia.app.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;

/**
 * A OrderData.
 */
@Entity
@Table(name = "order_data")
public class OrderData implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "ppk_nomor")
    private String ppkNomor;

    @Column(name = "ppk_tanggal")
    private LocalDate ppkTanggal;

    @Column(name = "nasabah_nama")
    private String nasabahNama;

    @Column(name = "nasabah_jenis")
    private Integer nasabahJenis;

    @Column(name = "tanggal")
    private LocalDate tanggal;

    @Column(name = "status")
    private Integer status;

    @Column(name = "type")
    private Integer type;

    @NotNull
    @Column(name = "akta_no")
    private String aktaNo;
    
    @NotNull
    @Column(name = "akta_date")
    private ZonedDateTime aktaDate;

    @Column(name = "fidusia_online_id")
    private String fidusiaOnlineId;

    @Column(name = "fidusia_type")
    private String fidusiaType;

    @ManyToOne
    private RawData rawData;

    @ManyToOne
    private SkSubstitusi skSubstitusi;

    @ManyToOne
    private Berkas berkas;

    @ManyToOne
    private Cabang cabang;

    @ManyToOne
    private Notaris notaris;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public OrderData id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPpkNomor() {
        return this.ppkNomor;
    }

    public OrderData ppkNomor(String ppkNomor) {
        this.setPpkNomor(ppkNomor);
        return this;
    }

    public void setPpkNomor(String ppkNomor) {
        this.ppkNomor = ppkNomor;
    }

    public LocalDate getPpkTanggal() {
        return this.ppkTanggal;
    }

    public OrderData ppkTanggal(LocalDate ppkTanggal) {
        this.setPpkTanggal(ppkTanggal);
        return this;
    }

    public void setPpkTanggal(LocalDate ppkTanggal) {
        this.ppkTanggal = ppkTanggal;
    }

    public String getNasabahNama() {
        return this.nasabahNama;
    }

    public OrderData nasabahNama(String nasabahNama) {
        this.setNasabahNama(nasabahNama);
        return this;
    }

    public void setNasabahNama(String nasabahNama) {
        this.nasabahNama = nasabahNama;
    }

    public Integer getNasabahJenis() {
        return this.nasabahJenis;
    }

    public OrderData nasabahJenis(Integer nasabahJenis) {
        this.setNasabahJenis(nasabahJenis);
        return this;
    }

    public void setNasabahJenis(Integer nasabahJenis) {
        this.nasabahJenis = nasabahJenis;
    }

    public LocalDate getTanggal() {
        return this.tanggal;
    }

    public OrderData tanggal(LocalDate tanggal) {
        this.setTanggal(tanggal);
        return this;
    }

    public void setTanggal(LocalDate tanggal) {
        this.tanggal = tanggal;
    }

    public Integer getStatus() {
        return this.status;
    }

    public OrderData status(Integer status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getType() {
        return this.type;
    }

    public OrderData type(Integer type) {
        this.setType(type);
        return this;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public RawData getRawData() {
        return this.rawData;
    }

    public void setRawData(RawData rawData) {
        this.rawData = rawData;
    }

    public OrderData rawData(RawData rawData) {
        this.setRawData(rawData);
        return this;
    }

    public SkSubstitusi getSkSubstitusi() {
		return skSubstitusi;
	}

	public void setSkSubstitusi(SkSubstitusi skSubstitusi) {
		this.skSubstitusi = skSubstitusi;
	}

	public OrderData skSubstitusi(SkSubstitusi skSubstitusi) {
        this.setSkSubstitusi(skSubstitusi);
        return this;
    }

    public Berkas getBerkas() {
        return this.berkas;
    }

    public void setBerkas(Berkas berkas) {
        this.berkas = berkas;
    }

    public OrderData berkas(Berkas berkas) {
        this.setBerkas(berkas);
        return this;
    }

    public Cabang getCabang() {
        return this.cabang;
    }

    public void setCabang(Cabang cabang) {
        this.cabang = cabang;
    }

    public OrderData cabang(Cabang cabang) {
        this.setCabang(cabang);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    public String getAktaNo() {
		return aktaNo;
	}

	public void setAktaNo(String aktaNo) {
		this.aktaNo = aktaNo;
	}

	public ZonedDateTime getAktaDate() {
		return aktaDate;
	}

	public void setAktaDate(ZonedDateTime aktaDate) {
		this.aktaDate = aktaDate;
	}

	public Notaris getNotaris() {
		return notaris;
	}

	public void setNotaris(Notaris notaris) {
		this.notaris = notaris;
	}

	public String getFidusiaOnlineId() {
		return fidusiaOnlineId;
	}

	public void setFidusiaOnlineId(String fidusiaOnlineId) {
		this.fidusiaOnlineId = fidusiaOnlineId;
	}

	public String getFidusiaType() {
		return fidusiaType;
	}

	public void setFidusiaType(String fidusiaType) {
		this.fidusiaType = fidusiaType;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrderData)) {
            return false;
        }
        return getId() != null && getId().equals(((OrderData) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

	@Override
	public String toString() {
		return "OrderData [id=" + id + ", ppkNomor=" + ppkNomor + ", ppkTanggal=" + ppkTanggal + ", nasabahNama="
				+ nasabahNama + ", nasabahJenis=" + nasabahJenis + ", tanggal=" + tanggal + ", status=" + status
				+ ", type=" + type + ", aktaNo=" + aktaNo + ", aktaDate=" + aktaDate + ", fidusiaOnlineId="
				+ fidusiaOnlineId + ", fidusiaType=" + fidusiaType + ", rawData=" + rawData + ", skSubstitusi="
				+ skSubstitusi + ", berkas=" + berkas + ", cabang=" + cabang + ", notaris=" + notaris + "]";
	}
}
