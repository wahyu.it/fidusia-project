package com.fidusia.app.web.rest;

import com.fidusia.app.domain.Berkas;
import com.fidusia.app.domain.Cabang;
import com.fidusia.app.domain.Remark;
import com.fidusia.app.domain.util.BerkasStatus;
import com.fidusia.app.domain.util.NasabahJenis;
import com.fidusia.app.repository.BerkasRepository;
import com.fidusia.app.repository.CabangRepository;
import com.fidusia.app.repository.RemarkRepository;
import com.fidusia.app.security.AuthoritiesConstants;
import com.fidusia.app.security.SecurityUtils;
import com.fidusia.app.service.CabangService;
import com.fidusia.app.service.dto.BerkasDTO;
import com.fidusia.app.service.mapper.BerkasMapper;
import com.fidusia.app.util.DateUtils;
import com.fidusia.app.web.rest.errors.BadRequestAlertException;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing {@link com.fidusia.app.domain.Berkas}.
 */
@RestController
@RequestMapping("/api/berkas")
@Transactional
public class BerkasResource {

    private final Logger log = LoggerFactory.getLogger(BerkasResource.class);

    private static final String ENTITY_NAME = "berkas";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BerkasRepository berkasRepository;
    
    private final CabangRepository cabangRepository;
    
    private final BerkasMapper berkasMapper;
    
    private final CabangService cabangService;
    
    private final RemarkRepository remarkRepository;

    public BerkasResource(
    		BerkasRepository berkasRepository,
    		BerkasMapper berkasMapper, 
    		CabangService cabangService,
    		CabangRepository cabangRepository,
    		RemarkRepository remarkRepository) {
        this.berkasRepository = berkasRepository;
        this.berkasMapper = berkasMapper;
        this.cabangService = cabangService;
        this.cabangRepository = cabangRepository;
        this.remarkRepository = remarkRepository;
    }

    /**
     * {@code POST  /berkas} : Create a new berkas.
     *
     * @param berkas the berkas to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new berkas, or with status {@code 400 (Bad Request)} if the berkas has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<Berkas> createBerkas(@RequestBody Berkas berkas) throws URISyntaxException {
        log.debug("REST request to save Berkas : {}", berkas);
        if (berkas.getId() != null) {
            throw new BadRequestAlertException("A new berkas cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Berkas result = berkasRepository.save(berkas);
        return ResponseEntity
            .created(new URI("/api/berkas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /berkas/:id} : Updates an existing berkas.
     *
     * @param id the id of the berkas to save.
     * @param berkas the berkas to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated berkas,
     * or with status {@code 400 (Bad Request)} if the berkas is not valid,
     * or with status {@code 500 (Internal Server Error)} if the berkas couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<Berkas> updateBerkas(@PathVariable(value = "id", required = false) final Long id, @RequestBody Berkas berkas)
        throws URISyntaxException {
        log.debug("REST request to update Berkas : {}, {}", id, berkas);
        if (berkas.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, berkas.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!berkasRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Berkas result = berkasRepository.save(berkas);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, berkas.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /berkas} : get all the berkas.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of berkas in body.
     */
    @GetMapping("")
    public ResponseEntity<List<Berkas>> getAllBerkas(Pageable pageable) {
        log.debug("REST request to get a page of Berkas");
        Page<Berkas> page = berkasRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /berkas/:id} : get the "id" berkas.
     *
     * @param id the id of the berkas to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the berkas, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<Berkas> getBerkas(@PathVariable("id") Long id) {
        log.debug("REST request to get Berkas : {}", id);
        Optional<Berkas> berkas = berkasRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(berkas);
    }

    /**
     * {@code DELETE  /berkas/:id} : delete the "id" berkas.
     *
     * @param id the id of the berkas to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteBerkas(@PathVariable("id") Long id) {
        log.debug("REST request to delete Berkas : {}", id);
        berkasRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
    
    @GetMapping("/search")
//    @Secured({AuthoritiesConstants.OPERATOR,AuthoritiesConstants.OFFICER,AuthoritiesConstants.MANAGER,AuthoritiesConstants.HEADQUARTER})
    public ResponseEntity<List<BerkasDTO>> search(
        @RequestParam(value = "skey", required = false) String skey,
        @RequestParam(value = "svalue", required = false) String svalue,
        @RequestParam(value = "sstatus", required = false) String sstatus,
        @ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to search berkas a page for {} by {} : {} status {}", skey, svalue, sstatus);

        boolean isRoleCabang = SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.OFFICER) || SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.MANAGER);
        
        Cabang cabang = null;
        if(isRoleCabang) {
            cabang = cabangService.findByUserLogin();
        }
        Integer[] statusArr = new Integer[]{-1,0,1,2,3};
        List<Integer> statusList = Arrays.asList(statusArr);

        Page<Berkas> page = null;

        if("ppkNomor".equals(skey) && svalue != null) {
//            if(isRoleCabang) {
//                page = berkasRepository.findByPpkNomorAndCabang(svalue, cabang,pageable);
//            } else {
                page = berkasRepository.findByPpkNomor(svalue, pageable);
//                page = berkasRepository.findByPpkNomorAndRefNomorPkIsNull(svalue, pageable);//Change Query temporari Ppk Perubahan in Menu Berkas upload
//            }
        } else if(("uploadDate".equals(skey) || "uploadMonth".equals(skey)) && svalue != null) {
            LocalDate uploadDate = "uploadMonth".equals(skey) ? LocalDate.parse(svalue + "-01") : LocalDate.parse(svalue);
            ZonedDateTime start = uploadDate.atStartOfDay(ZoneId.systemDefault());
            ZonedDateTime end = "uploadMonth".equals(skey) ? start.plusMonths(1) : start.plusDays(1);

            if(sstatus!=null && !"".equals(sstatus) && cabang==null)
                page = berkasRepository.findByUploadOnBetweenAndBerkasStatus(start, end, Integer.parseInt(sstatus), pageable);
            else if(sstatus!=null && !"".equals(sstatus) && cabang!=null)
                page = berkasRepository.findByCabangAndUploadOnBetweenAndBerkasStatus(cabang, start, end, Integer.parseInt(sstatus), pageable);
            else if((sstatus==null || "".equals(sstatus)) && cabang==null)
                page = berkasRepository.findByUploadOnBetweenAndBerkasStatusLessThan(start, end, BerkasStatus.COMPLETE.value(), pageable);
            else
                page = berkasRepository.findByCabangAndUploadOnBetweenAndBerkasStatusLessThan(cabang, start, end, BerkasStatus.COMPLETE.value(), pageable);

        } else if(("ppdDate".equals(skey) || "ppdMonth".equals(skey)) && svalue != null) {
            LocalDate start = "ppdMonth".equals(skey) ? LocalDate.parse(svalue + "-01") : LocalDate.parse(svalue);
            LocalDate end = "ppdMonth".equals(skey) ? start.plusMonths(1) : start.plusDays(1);

            if(sstatus!=null && !"".equals(sstatus) && cabang==null)
                page = berkasRepository.findByTanggalPpkBetweenAndBerkasStatus(start, end, Integer.parseInt(sstatus), pageable);
            else if(sstatus!=null && !"".equals(sstatus) && cabang!=null)
                page = berkasRepository.findByCabangAndTanggalPpkBetweenAndBerkasStatus(cabang, start, end, Integer.parseInt(sstatus), pageable);
            else if((sstatus==null || "".equals(sstatus)) && cabang==null)
                page = berkasRepository.findByTanggalPpkBetweenAndBerkasStatusLessThan(start, end, BerkasStatus.COMPLETE.value(), pageable);
            else
                page = berkasRepository.findByCabangAndTanggalPpkBetweenAndBerkasStatusLessThan(cabang, start, end, BerkasStatus.COMPLETE.value(), pageable);
        } else {
            LocalDate start = LocalDate.now().withDayOfMonth(1);
            LocalDate end = LocalDate.now().plusMonths(1).withDayOfMonth(1);

            if(sstatus!=null && !"".equals(sstatus) && cabang==null) {
                page = berkasRepository.findByTanggalPpkBetweenAndBerkasStatus(start, end, Integer.parseInt(sstatus), pageable);
            } else if(sstatus!=null && !"".equals(sstatus) && cabang!=null) {
                page = berkasRepository.findByCabangAndTanggalPpkBetweenAndBerkasStatus(cabang, start, end, Integer.parseInt(sstatus), pageable);
            } else if((sstatus==null || "".equals(sstatus)) && cabang==null) {
            	log.debug("default query...");
                page = berkasRepository.findByTanggalPpkBetweenAndBerkasStatusLessThan(start, end, BerkasStatus.COMPLETE.value(), pageable);
            } else {
                page = berkasRepository.findByCabangAndTanggalPpkBetweenAndBerkasStatusLessThan(cabang, start, end, BerkasStatus.COMPLETE.value(), pageable);
            }
        }

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(),page);
        return new ResponseEntity<>(berkasMapper.berkasToBerkasDTOs(page.getContent()), headers, HttpStatus.OK);
    }
    
    @GetMapping("/searchByCase")
//    @Secured({AuthoritiesConstants.ADMIN, AuthoritiesConstants.OPERATOR,AuthoritiesConstants.OFFICER,AuthoritiesConstants.MANAGER,AuthoritiesConstants.HEADQUARTER})
    public ResponseEntity<List<BerkasDTO>> searchByCase(
        @RequestParam(value = "skey", required = false) String skey,
        @RequestParam(value = "svalue", required = false) String svalue,
        @RequestParam(value = "scabang", required = false) String scabang,
        @RequestParam(value = "scase", required = false) String scase,
        @RequestParam(value = "spart", required = false) String spart,
        @RequestParam(value = "snsbjenis", required = false) String snsbjenis,
        @ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to searchByCase {} for cabang {} by {} : {} {}", scase, scabang, skey, svalue,spart, snsbjenis);

        Cabang cabang = null;
        if(scabang != null) {
            cabang = cabangRepository.findOneByNama(scabang);
        }

        ZonedDateTime currentSod = DateUtils.nowDate().atStartOfDay(ZoneId.systemDefault());
        
        Integer statusNsbJns = 2;
         if ("badan".equals(snsbjenis))
        	 statusNsbJns = 1;

        Integer[] statusArr = null;
        switch (scase) {
            case "verify" :
                statusArr = new Integer[]{BerkasStatus.PARTIAL.value()};
                break;
            case "verifyfisik" :
                statusArr = new Integer[]{BerkasStatus.PARTIAL.value()};
                break;
            case "badan" :
                statusArr = new Integer[]{NasabahJenis.BADAN.value()};
                break;
            default :
                statusArr = new Integer[]{-1,0,1,2,3};
                break;
        }

        List<Integer> statusList = Arrays.asList(statusArr);

        Page<Berkas> page = null;

        Page<BerkasDTO> page2 = null;
        
        Page<BerkasDTO> page3 = null;

        if("ppkNomor".equals(skey) && svalue != null) {
            if(spart!=null && !"".equals(spart) && "verify".equals(scase)) {
                page = berkasRepository.findByPpkNomorIn(svalue, statusList, pageable);//TODO
            } else if("badan".equals(scase)) {
                page2 = berkasRepository.findByPpkNomorAndAndNsbJenis(svalue, NasabahJenis.BADAN.value(), pageable);
            } else if("".equals(scase)){
            	page = berkasRepository.findByPpkNomor(svalue, pageable);
			} 
				else { page = berkasRepository.findByPpkNomor(svalue, pageable); }
				 
        } else if("namaNasabah".equals(skey) && svalue != null) {
            if("verify".equals(scase)) {
                page = berkasRepository.findByNamaNasabah(svalue, pageable);
            } else if("badan".equals(scase)) {
                page = berkasRepository.findByNamaNasabah(svalue, pageable);
            } else if("".equals(scase)) {
                page = berkasRepository.findByNamaNasabah(svalue, pageable);
            } else {
            	page = berkasRepository.findByNamaNasabah(svalue, pageable);
            }
        
        }else if("uploadDate".equals(skey) && svalue != null) {
            LocalDate uploadDate = LocalDate.parse(svalue);
            ZonedDateTime sod = uploadDate.atStartOfDay(ZoneId.systemDefault());
            ZonedDateTime eod = sod.plusDays(1);

            if(spart!=null && !"".equals(spart) && cabang==null)
            	page = berkasRepository.findByUploadOnBetweenAndPartitionIdAndBerkasStatusInAndNsbJenis(sod, eod, Integer.parseInt(spart), statusList, statusNsbJns, pageable);
            else if(spart!=null && !"".equals(spart) && cabang!=null)
                page = berkasRepository.findByCabangAndUploadOnBetweenAndPartitionIdAndBerkasStatusIn(cabang, sod, eod, Integer.parseInt(spart), statusList, pageable);
            else if((spart==null || "".equals(spart)) && cabang==null)
            	page = berkasRepository.findByUploadOnBetweenAndBerkasStatusInAndNsbJenis(sod, eod, statusList, statusNsbJns, pageable);
            else
                page = berkasRepository.findByCabangAndUploadOnBetweenAndBerkasStatusIn(cabang, sod, eod, statusList, pageable);
        } else if("uploadMonth".equals(skey) && svalue != null && cabang == null) {
            LocalDate uploadDate = LocalDate.parse(svalue + "-01");
            ZonedDateTime som = uploadDate.atStartOfDay(ZoneId.systemDefault());
            ZonedDateTime eom = (("verify".equals(scase)) && currentSod.compareTo(som.plusMonths(1))<0) ? currentSod : som.plusMonths(1);

            if(spart!=null && !"".equals(spart) && cabang==null)
            	page = berkasRepository.findByUploadOnBetweenAndPartitionIdAndBerkasStatusInAndNsbJenis(som, eom, Integer.parseInt(spart), statusList, statusNsbJns, pageable);
            else if(spart!=null && !"".equals(spart) && cabang!=null)
                page = berkasRepository.findByCabangAndUploadOnBetweenAndPartitionIdAndBerkasStatusIn(cabang, som, eom, Integer.parseInt(spart), statusList, pageable);
            else if((spart==null || "".equals(spart)) && cabang==null)
            	page = berkasRepository.findByUploadOnBetweenAndBerkasStatusInAndNsbJenis(som, eom, statusList, statusNsbJns, pageable);
            else
                page = berkasRepository.findByCabangAndUploadOnBetweenAndBerkasStatusIn(cabang, som, eom, statusList, pageable);
        } else if("updateDate".equals(skey) && svalue != null) {
            LocalDate updateDate = LocalDate.parse(svalue);
            ZonedDateTime sod = updateDate.atStartOfDay(ZoneId.systemDefault());
            ZonedDateTime eod = sod.plusDays(1);

            if(spart!=null && !"".equals(spart) && cabang==null){
            	if ("verify".equals(scase))
        			page = berkasRepository.findByUpdateOnBetweenAndPartitionIdAndBerkasStatusInAndNsbJenis(sod, eod, Integer.parseInt(spart), statusList, statusNsbJns, pageable);
            	else if ("badan".equals(scase))
            		page3 = berkasRepository.findByUpdateOnBetweenAndNsbJenisAndPartitionIdAndBerkasStatus(sod, eod, NasabahJenis.BADAN.value(), Integer.parseInt(spart), BerkasStatus.PARTIAL.value(), pageable);
            	}
            else if(spart!=null && !"".equals(spart) && cabang!=null){
            	if ("verify".equals(scase))
            		page = berkasRepository.findByCabangAndUpdateOnBetweenAndPartitionIdAndBerkasStatusIn(cabang, sod, eod, Integer.parseInt(spart), statusList, pageable);
            	else if ("badan".equals(scase))
                    page3 = berkasRepository.findByCabangAndUpdateOnBetweenAndNsbJenisAndPartitionIdAndBerkasStatus(cabang, sod, eod, NasabahJenis.BADAN.value(), Integer.parseInt(spart), BerkasStatus.PARTIAL.value(), pageable);	
            	}
            else if((spart==null || "".equals(spart)) && cabang==null){
            	if ("verify".equals(scase))
            		page = berkasRepository.findByUpdateOnBetweenAndBerkasStatusInAndNsbJenis(sod, eod, statusList, statusNsbJns, pageable);
            	else if ("badan".equals(scase))
                    page3 = berkasRepository.findByUpdateOnBetweenAndBerkasStatusAndNsbJenis(sod, eod, BerkasStatus.PARTIAL.value(), NasabahJenis.BADAN.value(), pageable);
            	}
            else
                page = berkasRepository.findByCabangAndUpdateOnBetweenAndBerkasStatusIn(cabang, sod, eod, statusList, pageable);
        } else if("updateMonth".equals(skey) && svalue != null && cabang == null) {
            LocalDate updateDate = LocalDate.parse(svalue + "-01");
            ZonedDateTime som = updateDate.atStartOfDay(ZoneId.systemDefault());
            ZonedDateTime eom = (("verify".equals(scase) || "badan".equals(scase)) && currentSod.compareTo(som.plusMonths(1))<0) ? currentSod : som.plusMonths(1);

            if(spart!=null && !"".equals(spart) && cabang==null) {
            	if ("verify".equals(scase) || "verifyfisik".equals(scase))
        			page = berkasRepository.findByUpdateOnBetweenAndPartitionIdAndBerkasStatusInAndNsbJenis(som, eom, Integer.parseInt(spart), statusList, statusNsbJns, pageable);
            	else if ("badan".equals(scase))
            		page3 = berkasRepository.findByUpdateOnBetweenAndNsbJenisAndPartitionIdAndBerkasStatus(som, eom, NasabahJenis.BADAN.value(), Integer.parseInt(spart), BerkasStatus.PARTIAL.value(), pageable);
            	}
            else if(spart!=null && !"".equals(spart) && cabang!=null) {
            	if ("verify".equals(scase))
            		page = berkasRepository.findByCabangAndUpdateOnBetweenAndPartitionIdAndBerkasStatusIn(cabang, som, eom, Integer.parseInt(spart), statusList, pageable);
            	else if ("badan".equals(scase))
                    page3 = berkasRepository.findByCabangAndUpdateOnBetweenAndNsbJenisAndPartitionIdAndBerkasStatus(cabang, som, eom, NasabahJenis.BADAN.value(), Integer.parseInt(spart), BerkasStatus.PARTIAL.value(), pageable);	
            	}
            else if((spart==null || "".equals(spart)) && cabang==null) {
            	if ("verify".equals(scase))
        			page = berkasRepository.findByUpdateOnBetweenAndBerkasStatusInAndNsbJenis(som, eom, statusList, statusNsbJns, pageable);
            	else if ("badan".equals(scase))
                    page3 = berkasRepository.findByUpdateOnBetweenAndBerkasStatusAndNsbJenis(som, eom, BerkasStatus.PARTIAL.value(), NasabahJenis.BADAN.value(), pageable);
            	}
            else
                page = berkasRepository.findByUpdateOnBetweenAndBerkasStatusIn(som, eom, statusList, pageable);
        } else {
            LocalDate uploadDate = LocalDate.now().withDayOfMonth(1);
            ZonedDateTime som = uploadDate.atStartOfDay(ZoneId.systemDefault());
            ZonedDateTime eom = (("verify".equals(scase) || "badan".equals(scase)) && currentSod.compareTo(som.plusMonths(1)) < 0) ? currentSod : som.plusMonths(1);

            if (spart != null && !"".equals(spart) && cabang == null) {
                if ("verify".equals(scase))
                	page = berkasRepository.findByUpdateOnBetweenAndPartitionIdAndBerkasStatusInAndNsbJenis(som, eom, Integer.parseInt(spart), statusList, statusNsbJns, pageable);
                else if ("badan".equals(scase))
                	page3 = berkasRepository.findByUpdateOnBetweenAndNsbJenisAndPartitionIdAndBerkasStatus(som, eom, NasabahJenis.BADAN.value(), Integer.parseInt(spart), BerkasStatus.PARTIAL.value(), pageable);
                else
                    page = berkasRepository.findByUploadOnBetweenAndPartitionIdAndBerkasStatusIn(som, eom, Integer.parseInt(spart), statusList, pageable);
            } else if(spart!=null && !"".equals(spart) && cabang!=null) {
                if ("verify".equals(scase))
                	page = berkasRepository.findByCabangAndUpdateOnBetweenAndPartitionIdAndBerkasStatusInAndNsbJenis(cabang, som, eom, Integer.parseInt(spart), statusList, statusNsbJns, pageable);
                else if ("badan".equals(scase))
                    page3 = berkasRepository.findByCabangAndUpdateOnBetweenAndNsbJenisAndPartitionIdAndBerkasStatus(cabang, som, eom, NasabahJenis.BADAN.value(), Integer.parseInt(spart), BerkasStatus.PARTIAL.value(), pageable);
                else
                    page = berkasRepository.findByCabangAndUploadOnBetweenAndPartitionIdAndBerkasStatusIn(cabang, som, eom, Integer.parseInt(spart), statusList, pageable);
            } else if((spart==null || "".equals(spart)) && cabang==null) {
                if ("verify".equals(scase))
                	page = berkasRepository.findByUpdateOnBetweenAndBerkasStatusInAndNsbJenis(som, eom, statusList, statusNsbJns, pageable);
                else if ("badan".equals(scase))
                    page3 = berkasRepository.findByUpdateOnBetweenAndBerkasStatusAndNsbJenis(som, eom, BerkasStatus.PARTIAL.value(), NasabahJenis.BADAN.value(), pageable);
                else
                    page = berkasRepository.findByUploadOnBetweenAndBerkasStatusIn(som, eom, statusList, pageable);
            } else {
                if ("verify".equals(scase))
                	page = berkasRepository.findByCabangAndUpdateOnBetweenAndBerkasStatusInAndNsbJenis(cabang, som, eom, statusList, statusNsbJns, pageable);
                else if ("badan".equals(scase))
                    page3 = berkasRepository.findByCabangAndUpdateOnBetweenAndBerkasStatusAndNsbJenis(cabang, som, eom, BerkasStatus.PARTIAL.value(), NasabahJenis.BADAN.value(), pageable);
                else
                    page = berkasRepository.findByCabangAndUploadOnBetweenAndBerkasStatusIn(cabang, som, eom, statusList, pageable);
            }
        }
        
        if(page3 != null) {
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page3);
            return new ResponseEntity<>(page3.getContent(), headers, HttpStatus.OK);
        } else if(page2 != null) {
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page2);
            return new ResponseEntity<>(page2.getContent(), headers, HttpStatus.OK);
        } else {
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
            return new ResponseEntity<>(berkasMapper.berkasToBerkasDTOs(page.getContent()), headers, HttpStatus.OK);
        }
    }    

    @PutMapping("/verify/{id}")
//    @Secured({AuthoritiesConstants.ADMIN, AuthoritiesConstants.OPERATOR,AuthoritiesConstants.OFFICER})
    public ResponseEntity<Berkas> updateBerkas(@PathVariable("id") Long id)
        throws URISyntaxException {
        log.debug("REST request to verifikasi berkas id {} ", id);
        
        if (id == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        
        Optional<Berkas> berkas = berkasRepository.findById(id);

        if (berkas == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        berkas.get().setBerkasStatus(BerkasStatus.COMPLETE.value());
        berkas.get().setVerifyOn(ZonedDateTime.now());
        berkas.get().setVerifyBy(SecurityUtils.getCurrentUserLogin().get().toString());
        
        berkasRepository.save(berkas.get());

        return ResponseUtil.wrapOrNotFound(berkas,
                HeaderUtil.createAlert(applicationName, "userManagement.updated", berkas.get().getId().toString()));
   }  
    
    @GetMapping("/reject/{id}")
//    @Secured({AuthoritiesConstants.ADMIN, AuthoritiesConstants.OPERATOR,AuthoritiesConstants.OFFICER})
    public ResponseEntity<Berkas> updateBerkas(
    		@PathVariable("id") Long id,
            @RequestParam(value = "remarkid", required = false) String remarkid) throws URISyntaxException  {
        log.debug("REST request to reject berkas id {}, remarks id {} ", id, remarkid);
        
        if (id == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        
        Optional<Berkas> berkas = berkasRepository.findById(id);

        if (berkas == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        berkas.get().setBerkasStatus(BerkasStatus.INCOMPLETE.value());
        if (remarkid != null) {
            Remark remark = remarkRepository.findById(Long.parseLong(remarkid)).get();
            berkas.get().setRemark(remark);
        } else {
        	berkas.get().setRemark(null);
        }
        berkas.get().setVerifyOn(ZonedDateTime.now());
        berkas.get().setVerifyBy(SecurityUtils.getCurrentUserLogin().get().toString());
        
        berkasRepository.save(berkas.get());

        return ResponseUtil.wrapOrNotFound(
                berkas,
                HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, berkas.get().getId().toString())
            );
   }
}
