package com.fidusia.app.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "notaris_desentralisasi")
public class NotarisDesentralisasi implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Size(max = 100)
    @Column(name = "nama", length = 100, nullable = false, unique = true)
    private String nama;

    @Max(value = 9999)
    @Column(name = "id_ahu")
    private Long idAhu;

    @Size(max = 100)
    @Column(name = "nama_ahu", length = 100)
    private String namaAhu;

    @Size(max = 100)
    @Column(name = "kedudukan", length = 100)
    private String kedudukan;

    @Max(value = 9999)
    @Column(name = "id_wilayah")
    private Integer idWilayah;
    
    public NotarisDesentralisasi() {}

	public NotarisDesentralisasi(Long id, String nama, Long idAhu, String namaAhu, String kedudukan, Integer idWilayah) {
		this.id = id;
		this.nama = nama;
		this.idAhu = idAhu;
		this.namaAhu = namaAhu;
		this.kedudukan = kedudukan;
		this.idWilayah = idWilayah;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public Long getIdAhu() {
		return idAhu;
	}

	public void setIdAhu(Long idAhu) {
		this.idAhu = idAhu;
	}

	public String getNamaAhu() {
		return namaAhu;
	}

	public void setNamaAhu(String namaAhu) {
		this.namaAhu = namaAhu;
	}

	public String getKedudukan() {
		return kedudukan;
	}

	public void setKedudukan(String kedudukan) {
		this.kedudukan = kedudukan;
	}

	public Integer getIdWilayah() {
		return idWilayah;
	}

	public void setIdWilayah(Integer idWilayah) {
		this.idWilayah = idWilayah;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NotarisDesentralisasi other = (NotarisDesentralisasi) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NotarisDesentralisasi [id=" + id + ", nama=" + nama + ", idAhu=" + idAhu + ", namaAhu=" + namaAhu + ", kedudukan=" + kedudukan + ", idWilayah=" + idWilayah + "]";
	}
}
