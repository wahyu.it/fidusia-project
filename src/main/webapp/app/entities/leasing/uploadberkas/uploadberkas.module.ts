import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasimonitoringSharedModule } from 'app/shared/shared.module';
import { UploadBerkasComponent } from './uploadberkas.component';
import { uploadBerkasState } from './uploadberkas.route';
import { BerkasDialogComponent } from './berkas-dialog.component';
import { UploadDialogComponent } from './upload-dialog.component';

@NgModule({
  imports: [SisteminformasimonitoringSharedModule, RouterModule.forChild(uploadBerkasState)],
  declarations: [UploadBerkasComponent, BerkasDialogComponent, UploadDialogComponent],
  entryComponents: [BerkasDialogComponent, UploadDialogComponent]
})
export class UploadBerkasModule {}
