package com.fidusia.app.service.mapper;

import org.mapstruct.*;

import com.fidusia.app.domain.Cabang;
import com.fidusia.app.domain.Leasing;
import com.fidusia.app.service.dto.CabangDTO;

import java.util.List;

//@Mapper(componentModel = "spring", uses = {UserMapper.class, })
@Mapper(componentModel = "spring", uses = {})
public interface CabangMapper {

    @Mapping(source = "leasing.id", target = "leasingId")
    CabangDTO cabangToCabangDTO(Cabang cabang);

    List<CabangDTO> cabangsToCabangDTOs(List<Cabang> cabangs);

    @Mapping(source = "leasingId", target = "leasing")
    Cabang cabangDTOToCabang(CabangDTO cabangDTO);

    List<Cabang> cabangDTOsToCabangs(List<CabangDTO> cabangDTOs);

    default Leasing leasingFromId(Long id) {
        if (id == null) {
            return null;
        }
        Leasing leasing = new Leasing();
        leasing.setId(id);
        return leasing;
    }
}
