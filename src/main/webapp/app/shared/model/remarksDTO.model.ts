export interface IRemarksDTO {
        id?: number;
        category?: string;
        label?: string;
        name?: string;
        description?: string;
}

export class RemarksDTO implements IRemarksDTO {
  constructor(
       	public id?: number,
		public category?: string,
		public label?: string,
		public name?: string,
		public description?: string,
  ) {}
}
