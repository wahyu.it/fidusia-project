export const DATE_FORMAT = 'YYYY-MM-DD';
export const DATE_FORMAT_ID = 'MM/dd/yyyy';
export const DATE_TIME_FORMAT = 'YYYY-MM-DDTHH:mm';
export const DATE_TIME = 'DD MMM YYYY HH:mm';
