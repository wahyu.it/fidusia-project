import { Component, OnInit } from '@angular/core';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { HttpResponse, HttpHeaders } from '@angular/common/http';
import { JhiEventManager } from 'ng-jhipster';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer } from '@angular/platform-browser';

import { VerifyKendaraanService } from './verifykendaraan.service';
import { IKendaraankDTO, KendaraankDTO } from 'app/shared/model/kendaraanDTO.model';
import { VerifyKendaraanDialogComponent } from './verifykendaraan-dialog.component';

@Component({
  selector: 'jhi-verifykendaraan',
  templateUrl: './verifykendaraan.component.html',
  styleUrls: ['verifykendaraan.scss']
})
export class VerifikasiKendaraanComponent implements OnInit {
  kendaraans?: IKendaraankDTO[] |any;
  date = '';
  month = '';
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  totalItems = 0;
  eventSubscriber?: Subscription;
  sPpd!: boolean;
  key!: string;
  value!: string;
  ext!: string;
  image: any;

  private dateFormat = 'yyyy-MM-dd';
  private mothFormat = 'yyyy-MM';
  
  editForm = new FormGroup({
    ppd: new FormControl(''),
    date: new FormControl(''),
    month: new FormControl(''),
    ppknomor: new FormControl('')
  });
  
  ppkGroup = new FormGroup({
    ppknomor: new FormControl('')
  });

  constructor(
    protected eventManager: JhiEventManager,
    protected activatedRoute: ActivatedRoute,
    protected verifyKendaraanService: VerifyKendaraanService,
    private datePipe: DatePipe,
    protected router: Router,
    private modalService: NgbModal,
    private sanitizer: DomSanitizer,
	) {
		this.sPpd = true;
	}

  ngOnInit(): void {
		this.month = this.sMonth();
		this.date = this.sDate();
	   this.activatedRoute.data.subscribe(data => {
	      this.page = data.pagingParams.page;
	      this.ascending = data.pagingParams.ascending;
	      this.predicate = data.pagingParams.predicate;
	      this.ngbPaginationPage = data.pagingParams.page;
	    /*  this.loadPage();*/
	    });
    this.registerChangeInLogBook();
    this.loadData();
  }
  
  onChange(e: any): void  {
	  console.log("ppd:" + e);
	  if (e === 'tgl'){
		  this.sPpd = true;
		  this.key = 'ppdDate';
	  } else {
		 this.sPpd = false;
		 this.key = 'ppdMonth';
	  }
	  console.log("this.sPpd:" + this.sPpd);
  }
  
  searchppk(ppkNomor: string): void {
	  let ppkNo = this.ppkGroup.get(['ppknomor'])!.value;;
	  if (this.key === 'ppkNomor') {
		  ppkNo = this.ppkGroup.get(['ppknomor'])!.value;
	  }
      this.searchByParam(ppkNomor, ppkNo);
  }

  searchPpd(key: string): void {
	  console.log(this.key + " " + key)
	  if (key === 'ppdDate') {
		  this.value = this.date;
	  } else {
		  this.value = this.month;
	  	  console.log("this.month : " +this.month)
	  } 
	  
      this.searchByParam(this.key, this.value);
  }
  
  searchByParam(key: string, value: string): void  {
	  this.key = key;
	  this.value = value;
	  console.log("searchByParam key : " + this.key + " value: " + this.value)
      this.router.navigate(['/operation/verifykendaraan'], {
        queryParams: {
          skey: this.key,
          svalue: this.value,
          page: this.page,
          sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
        }
      });
      this.loadData();
  }
  
  viewBerkas(filename: string): void {
       this.ext =  filename.split(".")[1];
       if ("PDF" === this.ext || "pdf" === this.ext) {
		 this.verifyKendaraanService.viewBerkas(filename).subscribe(res => {
	      const fileURL = URL.createObjectURL(res);
	      		const w = window.open(fileURL);
	      		
	      		if (w) {
		      		w.addEventListener('load', function (): void {
		    		w.document.title = filename; 
		    		console.log("test: "+w.document.title);
		    		console.log("w : "+w.document.documentURI);
					})
				}
	    	});
	   } else {
		   const modalRef = this.modalService.open(VerifyKendaraanDialogComponent, { size: 'xl', backdrop: 'static' });
		   modalRef.componentInstance.filename = filename;
	   }
  }
  
  opendetail(kendaraan : KendaraankDTO): any {
	  const modalRef = this.modalService.open(VerifyKendaraanDialogComponent, { size: 'xl', backdrop: 'static' });
		   modalRef.componentInstance.kendaraan = kendaraan;
  }

  transition(): void {
      this.router.navigate(['/operation/verifykendaraan'], {
        queryParams: {
          page: this.page,
          sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
        }
      });
      this.loadData();
  }

  loadData(page?: number): void {
    const pageToLoad: number = page ? page : this.page;
    this.verifyKendaraanService
      .search({
        skey: this.key,
        svalue: this.value,
        page: this.page - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe((res: HttpResponse<IKendaraankDTO[]>) => this.onSuccess(res.body, res.headers, pageToLoad));
  }

/*  loadPage(page?: number): void {
    const pageToLoad: number = page ? page : this.page;

     this.uploadBerkasService
        .search({
          skey: this.key,
          svalue: this.value,
          page: this.page-1,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe((res: HttpResponse<IBerkasDTO[]>) => this.onSuccess(res.body, res.headers, pageToLoad));
  }*/
  
  cleardate(): void {
    this.editForm.patchValue({
      date: '',
      month:''
    });
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  registerChangeInLogBook(): void {
    this.eventSubscriber = this.eventManager.subscribe('reportListModification', () => this.loadData());
  }

  protected onSuccess(data: IKendaraankDTO[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/operation/verifykendaraan'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.kendaraans = data;
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }

  private sDate(): string {
    const date = new Date();
	date.setDate(1);
    return this.datePipe.transform(date, this.dateFormat)!;
  }

  private sMonth(): string {
    const date = new Date();
    const result = this.datePipe.transform(date, this.mothFormat)!;
    console.log("result :" +result)
    return result
  }
}
