package com.fidusia.app.repository;

import com.fidusia.app.domain.Kendaraan;
import com.fidusia.app.domain.Ppk;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Kendaraan entity.
 */
@SuppressWarnings("unused")
@Repository
public interface KendaraanRepository extends JpaRepository<Kendaraan, Long> {

	List<Kendaraan> findByPpk(Ppk ppk);
	
	Page<Kendaraan> findByValid(Boolean valid, Pageable pageable);
}