package com.fidusia.app.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A SkSubtitusi.
 */
@Entity
@Table(name = "sk_substitusi")
public class SkSubstitusi implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "nomor")
    private String nomor;

    @Column(name = "tanggal")
    private LocalDate tanggal;

    @Column(name = "record_status")
    private Integer recordStatus;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "update_on")
    private ZonedDateTime updateOn;

    @Column(name = "file_name_sk")
    private String fileNameSk;

    @Column(name = "file_name_lamp")
    private String fileNameLamp;

    @Column(name = "status_sign")
    private Integer statusSign;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public SkSubstitusi id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomor() {
        return this.nomor;
    }

    public SkSubstitusi nomor(String nomor) {
        this.setNomor(nomor);
        return this;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public LocalDate getTanggal() {
        return this.tanggal;
    }

    public SkSubstitusi tanggal(LocalDate tanggal) {
        this.setTanggal(tanggal);
        return this;
    }

    public void setTanggal(LocalDate tanggal) {
        this.tanggal = tanggal;
    }

    public Integer getRecordStatus() {
        return this.recordStatus;
    }

    public SkSubstitusi recordStatus(Integer recordStatus) {
        this.setRecordStatus(recordStatus);
        return this;
    }

    public void setRecordStatus(Integer recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getUpdateBy() {
        return this.updateBy;
    }

    public SkSubstitusi updateBy(String updateBy) {
        this.setUpdateBy(updateBy);
        return this;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public ZonedDateTime getUpdateOn() {
        return this.updateOn;
    }

    public SkSubstitusi updateOn(ZonedDateTime updateOn) {
        this.setUpdateOn(updateOn);
        return this;
    }

    public void setUpdateOn(ZonedDateTime updateOn) {
        this.updateOn = updateOn;
    }

    public String getFileNameSk() {
        return this.fileNameSk;
    }

    public SkSubstitusi fileNameSk(String fileNameSk) {
        this.setFileNameSk(fileNameSk);
        return this;
    }

    public void setFileNameSk(String fileNameSk) {
        this.fileNameSk = fileNameSk;
    }

    public String getFileNameLamp() {
        return this.fileNameLamp;
    }

    public SkSubstitusi fileNameLamp(String fileNameLamp) {
        this.setFileNameLamp(fileNameLamp);
        return this;
    }

    public void setFileNameLamp(String fileNameLamp) {
        this.fileNameLamp = fileNameLamp;
    }

    public Integer getStatusSign() {
        return this.statusSign;
    }

    public SkSubstitusi statusSign(Integer statusSign) {
        this.setStatusSign(statusSign);
        return this;
    }

    public void setStatusSign(Integer statusSign) {
        this.statusSign = statusSign;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SkSubstitusi)) {
            return false;
        }
        return getId() != null && getId().equals(((SkSubstitusi) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SkSubtitusi{" +
            "id=" + getId() +
            ", nomor='" + getNomor() + "'" +
            ", tanggal='" + getTanggal() + "'" +
            ", recordStatus=" + getRecordStatus() +
            ", updateBy='" + getUpdateBy() + "'" +
            ", updateOn='" + getUpdateOn() + "'" +
            ", fileNameSk='" + getFileNameSk() + "'" +
            ", fileNameLamp='" + getFileNameLamp() + "'" +
            ", statusSign=" + getStatusSign() +
            "}";
    }
}
