package com.fidusia.app.web.rest;

import com.fidusia.app.domain.Ppk;
import com.fidusia.app.repository.PpkRepository;
import com.fidusia.app.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.fidusia.app.domain.Ppk}.
 */
@RestController
@RequestMapping("/api/ppks")
@Transactional
public class PpkResource {

    private final Logger log = LoggerFactory.getLogger(PpkResource.class);

    private static final String ENTITY_NAME = "ppk";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PpkRepository ppkRepository;

    public PpkResource(PpkRepository ppkRepository) {
        this.ppkRepository = ppkRepository;
    }

    /**
     * {@code POST  /ppks} : Create a new ppk.
     *
     * @param ppk the ppk to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new ppk, or with status {@code 400 (Bad Request)} if the ppk has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<Ppk> createPpk(@RequestBody Ppk ppk) throws URISyntaxException {
        log.debug("REST request to save Ppk : {}", ppk);
        if (ppk.getId() != null) {
            throw new BadRequestAlertException("A new ppk cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Ppk result = ppkRepository.save(ppk);
        return ResponseEntity
            .created(new URI("/api/ppks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /ppks/:id} : Updates an existing ppk.
     *
     * @param id the id of the ppk to save.
     * @param ppk the ppk to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated ppk,
     * or with status {@code 400 (Bad Request)} if the ppk is not valid,
     * or with status {@code 500 (Internal Server Error)} if the ppk couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<Ppk> updatePpk(@PathVariable(value = "id", required = false) final Long id, @RequestBody Ppk ppk)
        throws URISyntaxException {
        log.debug("REST request to update Ppk : {}, {}", id, ppk);
        if (ppk.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, ppk.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!ppkRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Ppk result = ppkRepository.save(ppk);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, ppk.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /ppks/:id} : Partial updates given fields of an existing ppk, field will ignore if it is null
     *
     * @param id the id of the ppk to save.
     * @param ppk the ppk to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated ppk,
     * or with status {@code 400 (Bad Request)} if the ppk is not valid,
     * or with status {@code 404 (Not Found)} if the ppk is not found,
     * or with status {@code 500 (Internal Server Error)} if the ppk couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Ppk> partialUpdatePpk(@PathVariable(value = "id", required = false) final Long id, @RequestBody Ppk ppk)
        throws URISyntaxException {
        log.debug("REST request to partial update Ppk partially : {}, {}", id, ppk);
        if (ppk.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, ppk.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!ppkRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Ppk> result = ppkRepository
            .findById(ppk.getId())
            .map(existingPpk -> {
                if (ppk.getNomor() != null) {
                    existingPpk.setNomor(ppk.getNomor());
                }
                if (ppk.getNama() != null) {
                    existingPpk.setNama(ppk.getNama());
                }
                if (ppk.getJenis() != null) {
                    existingPpk.setJenis(ppk.getJenis());
                }
                if (ppk.getTanggal() != null) {
                    existingPpk.setTanggal(ppk.getTanggal());
                }
                if (ppk.getTglSk() != null) {
                    existingPpk.setTglSk(ppk.getTglSk());
                }
                if (ppk.getNilaiHutangPokok() != null) {
                    existingPpk.setNilaiHutangPokok(ppk.getNilaiHutangPokok());
                }
                if (ppk.getNilaiPenjaminan() != null) {
                    existingPpk.setNilaiPenjaminan(ppk.getNilaiPenjaminan());
                }
                if (ppk.getPeriodeTenor() != null) {
                    existingPpk.setPeriodeTenor(ppk.getPeriodeTenor());
                }
                if (ppk.getTglCicilAwal() != null) {
                    existingPpk.setTglCicilAwal(ppk.getTglCicilAwal());
                }
                if (ppk.getTglCicilAkhir() != null) {
                    existingPpk.setTglCicilAkhir(ppk.getTglCicilAkhir());
                }
                if (ppk.getNsbNamaDebitur() != null) {
                    existingPpk.setNsbNamaDebitur(ppk.getNsbNamaDebitur());
                }
                if (ppk.getNsbNama() != null) {
                    existingPpk.setNsbNama(ppk.getNsbNama());
                }
                if (ppk.getNsbJenis() != null) {
                    existingPpk.setNsbJenis(ppk.getNsbJenis());
                }

                return existingPpk;
            })
            .map(ppkRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, ppk.getId().toString())
        );
    }

    /**
     * {@code GET  /ppks} : get all the ppks.
     *
     * @param pageable the pagination information.
     * @param filter the filter of the request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of ppks in body.
     */
    @GetMapping("")
    public ResponseEntity<List<Ppk>> getAllPpks(Pageable pageable,
        @RequestParam(name = "filter", required = false) String filter
    ) {
//        if ("akta-is-null".equals(filter)) {
//            log.debug("REST request to get all Ppks where akta is null");
//            return new ResponseEntity<>(
//                StreamSupport.stream(ppkRepository.findAll().spliterator(), false).filter(ppk -> ppk.getAkta() == null).toList(),
//                HttpStatus.OK
//            );
//        }
        log.debug("REST request to get a page of Ppks");
        Page<Ppk> page = ppkRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /ppks/:id} : get the "id" ppk.
     *
     * @param id the id of the ppk to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the ppk, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<Ppk> getPpk(@PathVariable("id") Long id) {
        log.debug("REST request to get Ppk : {}", id);
        Optional<Ppk> ppk = ppkRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(ppk);
    }

    /**
     * {@code DELETE  /ppks/:id} : delete the "id" ppk.
     *
     * @param id the id of the ppk to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePpk(@PathVariable("id") Long id) {
        log.debug("REST request to delete Ppk : {}", id);
        ppkRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
