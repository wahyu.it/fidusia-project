package com.fidusia.app.repository;

import com.fidusia.app.domain.Akta;
import com.fidusia.app.domain.Notaris;
import com.fidusia.app.domain.Ppk;
import com.fidusia.app.domain.util.AktaStatus;
import com.fidusia.app.service.dto.AktaDTO;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Akta entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AktaRepository extends JpaRepository<Akta, Long> {

	Akta findOneByPpkNomor(String ppknomor);

	Page<Akta> findByStatus(Integer status, PageRequest of);

	Long countByStatus(Integer status);

	List<Akta> findByTglOrderAndStatus(LocalDate tglOrder, Integer status);

	List<Akta> findByStatusAndTglOrder(Integer status, LocalDate tglOrder);

	List<Akta> findByTglOrder(LocalDate to);

	Long countByTglOrderBetweenAndNotaris(LocalDate tglOrderFrom, LocalDate tglOrderTo, Notaris notaris);
    
	static final String AKTA_DTO  = "select new com.fidusia.app.service.dto.AktaDTO(a.id, a.tglOrder, a.nomor, a.tanggal, a.pukulAwal, "
			+ " n.id, n.nama, n.wilayahKerja, pk.nama, s1.nama, s2.nama, p.nsbNamaDebitur, p.nsbNama, p.orang.id, p.badan.id, p.nomor) from Akta a"
			+ " INNER JOIN Ppk p on p.id=a.ppk.id"
			+ " INNER JOIN PenerimaKuasa pk on pk.id=a.penerimaKuasa.id"
			+ " INNER JOIN Saksi s1 on s1.id=a.saksiOne.id"
			+ " INNER JOIN Saksi s2 on s2.id=a.saksiTwo.id"
			+ " INNER JOIN Notaris n on n.id=a.notaris.id";
	
	@Query(AKTA_DTO + " WHERE a.tglOrder=:tanggal")
    Page<AktaDTO> findByTglOrder(
    		@Param("tanggal") LocalDate tanggal,
    		Pageable pageable);

	@Query(AKTA_DTO + " WHERE a.tglOrder between :start and :end")
    Page<AktaDTO> findByTglOrderBetween(
    		@Param("start") LocalDate start,
    		@Param("end") LocalDate end,
    		Pageable pageable);
	
	@Query(AKTA_DTO + " WHERE p.nomor=:ppkNomor")
    Page<AktaDTO> findByPpkNomor(
    		@Param("ppkNomor") String ppkNomor,
    		Pageable pageable);
	
	@Query(AKTA_DTO + " WHERE p.nomor=:ppkNomor And a.notaris.id=:notarisId")
    Page<AktaDTO> findByPpkNomorAndNotarisId(
    		@Param("ppkNomor") String ppkNomor,
    		@Param("notarisId") Long notarisId,
    		Pageable pageable);
	
	@Query(AKTA_DTO + " WHERE a.tglOrder=:tanggal And a.notaris.id=:notarisId")
    Page<AktaDTO> findByTglOrderAndNotarisId(
    		@Param("tanggal") LocalDate tanggal,
    		@Param("notarisId") Long notarisId,
    		Pageable pageable);

	@Query(AKTA_DTO + " WHERE a.tglOrder between :start and :end And a.notaris.id=:notarisId")
    Page<AktaDTO> findByTglOrderBetweenAndNotarisId(
    		@Param("start") LocalDate start,
    		@Param("end") LocalDate end,
    		@Param("notarisId") Long notarisId,
    		Pageable pageable);

	Akta findByPpk(Ppk ppk);
}
