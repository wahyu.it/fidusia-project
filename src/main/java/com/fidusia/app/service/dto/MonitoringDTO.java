package com.fidusia.app.service.dto;

import java.io.Serializable;
import java.time.LocalDate;

public class MonitoringDTO  implements Serializable  {

    private LocalDate tglOrder;

    private Long jmlOdn;

    private Long jmlOndCl;

    private Long jmlOdnSc;

    private Long jmlAkta;

    private Long jmlAktaCl;

    private Long jmlAktaBlReg;
    
    private Long jmlAktaScReg;
    
    private Long jmlSerBl;
    
    private Long jmlSerSc;
    
    private Long jmlLamp;
    
    private Long jmlBktLns;

    private LocalDate tglInv;
    
    private String drive;
    
    private Long totalSpace;
    
    private Float freeSpace;

	public LocalDate getTglOrder() {
		return tglOrder;
	}

	public void setTglOrder(LocalDate tglOrder) {
		this.tglOrder = tglOrder;
	}

	public Long getJmlOdn() {
		return jmlOdn;
	}

	public void setJmlOdn(Long jmlOdn) {
		this.jmlOdn = jmlOdn;
	}

	public Long getJmlOndCl() {
		return jmlOndCl;
	}

	public void setJmlOndCl(Long jmlOndCl) {
		this.jmlOndCl = jmlOndCl;
	}

	public Long getJmlOdnSc() {
		return jmlOdnSc;
	}

	public void setJmlOdnSc(Long jmlOdnSc) {
		this.jmlOdnSc = jmlOdnSc;
	}

	public Long getJmlAkta() {
		return jmlAkta;
	}

	public void setJmlAkta(Long jmlAkta) {
		this.jmlAkta = jmlAkta;
	}

	public Long getJmlAktaCl() {
		return jmlAktaCl;
	}

	public void setJmlAktaCl(Long jmlAktaCl) {
		this.jmlAktaCl = jmlAktaCl;
	}

	public Long getJmlAktaBlReg() {
		return jmlAktaBlReg;
	}

	public void setJmlAktaBlReg(Long jmlAktaBlReg) {
		this.jmlAktaBlReg = jmlAktaBlReg;
	}

	public Long getJmlAktaScReg() {
		return jmlAktaScReg;
	}

	public void setJmlAktaScReg(Long jmlAktaScReg) {
		this.jmlAktaScReg = jmlAktaScReg;
	}

	public Long getJmlSerBl() {
		return jmlSerBl;
	}

	public void setJmlSerBl(Long jmlSerBl) {
		this.jmlSerBl = jmlSerBl;
	}

	public Long getJmlSerSc() {
		return jmlSerSc;
	}

	public void setJmlSerSc(Long jmlSerSc) {
		this.jmlSerSc = jmlSerSc;
	}

	public Long getJmlLamp() {
		return jmlLamp;
	}

	public void setJmlLamp(Long jmlLamp) {
		this.jmlLamp = jmlLamp;
	}

	public Long getJmlBktLns() {
		return jmlBktLns;
	}

	public void setJmlBktLns(Long jmlBktLns) {
		this.jmlBktLns = jmlBktLns;
	}

	public LocalDate getTglInv() {
		return tglInv;
	}

	public void setTglInv(LocalDate tglInv) {
		this.tglInv = tglInv;
	}

	public String getDrive() {
		return drive;
	}

	public void setDrive(String drive) {
		this.drive = drive;
	}

	public Long getTotalSpace() {
		return totalSpace;
	}

	public void setTotalSpace(Long totalSpace) {
		this.totalSpace = totalSpace;
	}

	public Float getFreeSpace() {
		return freeSpace;
	}

	public void setFreeSpace(Float freeSpace) {
		this.freeSpace = freeSpace;
	}

	@Override
	public String toString() {
		return "MonitoringDTO [tglOrder=" + tglOrder + ", jmlOdn=" + jmlOdn + ", jmlOndCl=" + jmlOndCl + ", jmlOdnSc="
				+ jmlOdnSc + ", jmlAkta=" + jmlAkta + ", jmlAktaCl=" + jmlAktaCl + ", jmlAktaBlReg=" + jmlAktaBlReg
				+ ", jmlAktaScReg=" + jmlAktaScReg + ", jmlSerBl=" + jmlSerBl + ", jmlSerSc=" + jmlSerSc + ", jmlLamp="
				+ jmlLamp + ", jmlBktLns=" + jmlBktLns + ", tglInv=" + tglInv + ", drive=" + drive + ", totalSpace="
				+ totalSpace + ", freeSpace=" + freeSpace + "]";
	}

}
