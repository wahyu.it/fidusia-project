package com.fidusia.app.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.pdfbox.Loader;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fidusia.app.config.ApplicationProperties;
import com.fidusia.app.domain.NotarisDesentralisasi;
import com.fidusia.app.domain.RawDataRoya;
import com.fidusia.app.domain.util.RawDataRoyaStatus;
import com.fidusia.app.repository.NotarisDesentralisasiRepository;
import com.fidusia.app.repository.NotarisRepository;
import com.fidusia.app.repository.PpkRepository;
import com.fidusia.app.repository.RawDataRoyaRepository;
import com.fidusia.app.repository.SertifikatRepository;
import com.fidusia.app.util.DateUtils;
import com.fidusia.app.util.ExceptionUtils;

@Service
public class AhuPenghapusanService {
    private final Logger log = LoggerFactory.getLogger(AhuPenghapusanService.class);
	
	private final static DateTimeFormatter dateFormat =  DateTimeFormatter.ofPattern("dd-MM-yyyy");
	
    private final static String konteRoyaUrl = "https://fidusia.ahu.go.id/roya.html";
	
    private final static String cekRoyaUrl = "https://fidusia.ahu.go.id/app/cek_ser_peng.php?roya=1";
    
    private final static String cekDataAkhirUrl = "https://fidusia.ahu.go.id/app/cekdataakhir.php";
	
    private final static String formRoyaUrl = "https://fidusia.ahu.go.id/app/form_daftar_roya.php";
	
    private final static String regRoyaUrl = "https://fidusia.ahu.go.id/app/add_act_roya.php";
	
    private final static String regRoyaUrlApi2 = "https://fidusia.ahu.go.id/app/add_act_roya_insert.php";
	
    private final static String downloadRoyaUrl = "https://fidusia.ahu.go.id/app/form_cetak_sertifikat_roya_pdf.php";

    private final static int BUFFER_SIZE = 4096;
    private final static int connectTimeout = 60000;
    private final static int readTimeout = 60000;
    private final static String host = "fidusia.ahu.go.id";
    private final static String origin = "https://fidusia.ahu.go.id";
    private final static String refererKonte = "https://fidusia.ahu.go.id/roya.html";
    private final static String refererCekRoya = "https://fidusia.ahu.go.id/roya.html";
    private final static String refererFormRoya = "https://fidusia.ahu.go.id/roya.html";
    private final static String refererDownloadRoya = "https://fidusia.ahu.go.id/roya.html";
	
	private final NotarisDesentralisasiRepository notarisDesentralisasiRepository;

	private final RawDataRoyaRepository rawDataRoyaRepository;

	private final ApplicationProperties applicationProperties;
    
	private final SertifikatRepository sertifikatRepository;
	
	private final ReferensiService referensiService;
	
	private final SlackInhookService slackService;

	private final JobInstanceService jobInstanceService;

	private final NotarisRepository notarisRepository;

	private final PpkRepository ppkRepository;
	
	public AhuPenghapusanService (
			PpkRepository ppkRepository,
			NotarisRepository notarisRepository,
			NotarisDesentralisasiRepository notarisDesentralisasiRepository,
			RawDataRoyaRepository rawDataRoyaRepository,
			SertifikatRepository sertifikatRepository,
			ReferensiService referensiService,
			SlackInhookService slackService,
			JobInstanceService jobInstanceService,
			ApplicationProperties applicationProperties
			) {
		this.notarisDesentralisasiRepository = notarisDesentralisasiRepository;
		this.rawDataRoyaRepository = rawDataRoyaRepository;
		this.sertifikatRepository = sertifikatRepository;
		this.referensiService = referensiService;
		this.slackService = slackService;
		this.jobInstanceService = jobInstanceService;
		this.notarisRepository = notarisRepository;
		this.ppkRepository = ppkRepository;
		this.applicationProperties = applicationProperties;
	}
	
	
	public String generateRoyaKonte(String sessionId) throws Exception {
		BufferedReader reader = null;
		
		try {
			long startTimestamp = System.currentTimeMillis();

			SSLContext ssl_ctx = SSLContext.getInstance("TLS");
			TrustManager[] trust_mgr = get_trust_mgr();
			ssl_ctx.init(null, trust_mgr, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());

			URL url = new URL(konteRoyaUrl);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setConnectTimeout(connectTimeout);
			conn.setReadTimeout(readTimeout);
		    
			conn.setHostnameVerifier(new HostnameVerifier() {
				public boolean verify(String host, SSLSession sess) {
					if (host.equals("localhost"))
						return true;
					else
						return false;
				}
			});

			conn.setRequestProperty("Accept", "*/*");
			conn.setRequestProperty("Cookie", "PHPSESSID=" + sessionId);
			conn.setRequestProperty("Host", host);
			conn.setRequestProperty("Origin", origin);
			conn.setRequestProperty("Referer", refererKonte);
			conn.setDoOutput(true);
		    
		    int responseCode = conn.getResponseCode();
		    String responseMessage = conn.getResponseMessage();
			long endTimestamp = System.currentTimeMillis();
		    
		    if(responseCode == HttpURLConnection.HTTP_OK) {
			    reader = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
		        String line;
		        StringBuilder sb2 = new StringBuilder();
		        while ((line = reader.readLine()) != null) { 
		        	sb2.append(line);
		        }
		        
		        reader.close();
		        
		        String html = sb2.toString();
				Document document = Jsoup.parse(html);
				Element konte = document.getElementsByAttributeValue("name", "cekkonte").first();
				if(konte != null) {
				    reader.close();

					log.debug("generate roya konte SUCCESS in {}ms", (endTimestamp-startTimestamp));
					return konte.val();
				}
		    } else {
				log.debug("generate roya konte FAILED {}-{} {}ms", responseCode, responseMessage, (endTimestamp-startTimestamp));
			}
		    
		    reader.close();
			
			return null;
		} catch (Exception e) {
        	log.error("Error on generate roya konte ", e);

			if(reader != null)
				try {
					reader.close();
				} catch (IOException e2) { }
			
			throw e;
		}
	}
	
	public boolean cekRoya(String ppkNomor, String noSertifikat, ZonedDateTime tglSertifikat, String namaNotaris, Long idNotaris, String idWilayahKerja, String konte, String sessionId) throws Exception {
		OutputStreamWriter writer = null;
		BufferedReader reader = null;
		
		StringBuilder sb = new StringBuilder();
		sb.append("no_ser=").append(URLEncoder.encode(noSertifikat, StandardCharsets.UTF_8.toString())).append("&");
		sb.append("tgl_sertifikat=").append(DateUtils.formatDate(tglSertifikat.toLocalDate(), DateUtils.FORMATTER_ISO_LOCAL_DATE_V2)).append("&");
		sb.append("nm_not=").append(URLEncoder.encode(namaNotaris, StandardCharsets.UTF_8.toString())).append("&");
		sb.append("_id_not=").append(idNotaris).append("&");
		sb.append("_id_not_old=").append(idNotaris).append("&");
		sb.append("tempat=").append(idWilayahKerja).append("&");
		sb.append("cekkonte=").append(konte).append("&");
		sb.append("_flag=").append("W");
		String postData = sb.toString();
		
		try {
			long startTimestamp = System.currentTimeMillis();

			SSLContext ssl_ctx = SSLContext.getInstance("TLS");
			TrustManager[] trust_mgr = get_trust_mgr();
			ssl_ctx.init(null, trust_mgr, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());

			URL url = new URL(cekRoyaUrl);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setConnectTimeout(connectTimeout);
			conn.setReadTimeout(readTimeout);
		    
			conn.setHostnameVerifier(new HostnameVerifier() {
				public boolean verify(String host, SSLSession sess) {
					if (host.equals("localhost"))
						return true;
					else
						return false;
				}
			});

			conn.setRequestProperty("Accept", "*/*");
			conn.setRequestProperty("Accept-Encoding", "gzip, deflate, br");
			conn.setRequestProperty("Accept-Language", "en-US,en;q=0.9");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			conn.setRequestProperty("Cookie", "PHPSESSID=" + sessionId);
			conn.setRequestProperty("Host", host);
			conn.setRequestProperty("Origin", origin);
			conn.setRequestProperty("Referer", refererCekRoya);
			conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
			
	        conn.setDoInput(true);
			conn.setDoOutput(true);
		    writer = new OutputStreamWriter(conn.getOutputStream());

		    writer.write(postData);
		    writer.flush();
		    
		    int responseCode = conn.getResponseCode();
		    String responseMessage = conn.getResponseMessage();
			long endTimestamp = System.currentTimeMillis();
		    
		    if(responseCode == HttpURLConnection.HTTP_OK) {
			    reader = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
		        String line;
		        StringBuilder sb2 = new StringBuilder();
		        while ((line = reader.readLine()) != null) { 
		        	sb2.append(line);
		        }
		        
		        reader.close();
		        
		        String responsePayload = sb2.toString();
		        
				String[] respTokens = responsePayload.split("\\^");
				if("A1".equals(respTokens[0])) {
					log.debug("cek roya 1 SUCCESS {} in {}ms", ppkNomor, (endTimestamp-startTimestamp));

				    writer.close();
				    reader.close();
					return true;
		        } else {
					log.debug("cek roya 1 FAILED {} {} {}ms", ppkNomor, responsePayload, (endTimestamp-startTimestamp));
					log.debug(postData);
				}
		    } else {
				log.debug("cek roya 1 FAILED {} {}-{} {}ms", ppkNomor, responseCode, responseMessage, (endTimestamp-startTimestamp));
				log.debug(postData);
			}
		    
		    writer.close();
		    reader.close();
			
			return false;
		} catch (Exception e) {
        	log.error("Error on cek roya " + ppkNomor, e);
			log.debug(postData);

			if(writer != null)
				try {
					writer.close();
				} catch (Exception e2) { }

			if(reader != null)
				try {
					reader.close();
				} catch (IOException e2) { }
			
			throw e;
		}
	}
	
	public Map<String, String> generateFormRoya(String ppkNomor, String noSertifikat, ZonedDateTime tglSertifikat, String namaNotaris, Long idNotaris, String idWilayahKerja, String konte, String sessionId) throws Exception {
		OutputStreamWriter writer = null;
		BufferedReader reader = null;
		
		StringBuilder sb = new StringBuilder();
		sb.append("_q%5B%5D=").append(URLEncoder.encode(noSertifikat, StandardCharsets.UTF_8.toString())).append("&");
		sb.append("_q%5B%5D=").append(URLEncoder.encode(namaNotaris, StandardCharsets.UTF_8.toString())).append("&");
		sb.append("_q%5B%5D=").append(konte).append("&");
		sb.append("_q%5B%5D=").append(DateUtils.formatDate(tglSertifikat.toLocalDate(), DateUtils.FORMATTER_ISO_LOCAL_DATE_V2)).append("&");
		sb.append("_q%5B%5D=").append(0).append("&");
		sb.append("_q%5B%5D=").append(0).append("&");
		sb.append("_q%5B%5D=").append(idWilayahKerja).append("&");
		sb.append("_q%5B%5D=").append(1).append("&");
		sb.append("_w%5B%5D=").append(1);
				
		String postData = sb.toString();
		
		try {
			long startTimestamp = System.currentTimeMillis();

			SSLContext ssl_ctx = SSLContext.getInstance("TLS");
			TrustManager[] trust_mgr = get_trust_mgr();
			ssl_ctx.init(null, trust_mgr, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());

			URL url = new URL(formRoyaUrl);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setConnectTimeout(connectTimeout);
			conn.setReadTimeout(readTimeout);
		    
			conn.setHostnameVerifier(new HostnameVerifier() {
				public boolean verify(String host, SSLSession sess) {
					if (host.equals("localhost"))
						return true;
					else
						return false;
				}
			});
			
			conn.setRequestProperty("Accept", "*/*");
			conn.setRequestProperty("Accept-Encoding", "gzip, deflate, br");
			conn.setRequestProperty("Accept-Language", "en-US,en;q=0.9");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			conn.setRequestProperty("Cookie", "PHPSESSID=" + sessionId);
			conn.setRequestProperty("Host", host);
			conn.setRequestProperty("Origin", origin);
			conn.setRequestProperty("Referer", refererFormRoya);
			conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
			
	        conn.setDoInput(true);
			conn.setDoOutput(true);
		    writer = new OutputStreamWriter(conn.getOutputStream());

		    writer.write(postData);
		    writer.flush();
		    
		    int responseCode = conn.getResponseCode();
		    String responseMessage = conn.getResponseMessage();
			long endTimestamp = System.currentTimeMillis();
		    
		    if(responseCode == HttpURLConnection.HTTP_OK) {
			    reader = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
		        String line;
		        StringBuilder sb2 = new StringBuilder();
		        while ((line = reader.readLine()) != null) { 
		        	sb2.append(line);
		        }

				String html = sb2.toString();
				Document document = Jsoup.parse(html);
				Elements hiddens = document.getElementsByAttributeValue("type", "hidden");
				if(hiddens != null && hiddens.size() > 0) {
					Map<String, String> result = new HashMap<String, String>();
					for (Element hidden : hiddens) {
						result.put(hidden.attr("name"), hidden.attr("value"));
					}

					log.debug("form roya SUCCESS {} in {}ms", ppkNomor, (endTimestamp-startTimestamp));
					if(!result.containsKey("no_transaksi_lama") || result.get("no_transaksi_lama") == null || "".equals(result.get("no_transaksi_lama"))) {
//						log.debug(html);
					}
					
				    writer.close();
				    reader.close();
					return result;
				}
		    }

			log.debug("form roya FAILED {} {}-{} {}ms", ppkNomor, responseCode, responseMessage, (endTimestamp-startTimestamp));
			log.debug(postData);
		    
		    writer.close();
//		    reader.close();
			return null;
		} catch (Exception e) {
        	log.error("Error on form roya " + ppkNomor, e);
			log.debug(postData);

			if(writer != null)
				try {
					writer.close();
				} catch (Exception e2) { }

			if(reader != null)
				try {
					reader.close();
				} catch (IOException e2) { }
			
			throw e;
		}
	}

	private String cekDataAkhir(String nomorPk, String noSertifikat, String sessionId) {
		log.debug("process roya cekDataAkhir nomorPk:{} String:{} sessionId:{}", nomorPk, noSertifikat, sessionId);
	
		String result = null;
		
		OutputStreamWriter writer = null;
		BufferedReader reader = null;
		String postData = "param=" + noSertifikat;
		
		try {
			long startTimestamp = System.currentTimeMillis();

			SSLContext ssl_ctx = SSLContext.getInstance("TLS");
			TrustManager[] trust_mgr = get_trust_mgr();
			ssl_ctx.init(null, trust_mgr, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());

			URL url = new URL(cekDataAkhirUrl);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setConnectTimeout(connectTimeout);
			conn.setReadTimeout(readTimeout);
		    
			conn.setHostnameVerifier(new HostnameVerifier() {
				public boolean verify(String host, SSLSession sess) {
					if (host.equals("localhost"))
						return true;
					else
						return false;
				}
			});

			conn.setRequestProperty("Accept", "*/*");
			conn.setRequestProperty("Accept-Encoding", "gzip, deflate, br");
			conn.setRequestProperty("Accept-Language", "en-US,en;q=0.9");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			conn.setRequestProperty("Cookie", "PHPSESSID=" + sessionId);
			conn.setRequestProperty("Host", host);
			conn.setRequestProperty("Origin", origin);
			conn.setRequestProperty("Referer", refererCekRoya);
			conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
			
	        conn.setDoInput(true);
			conn.setDoOutput(true);
		    writer = new OutputStreamWriter(conn.getOutputStream());

		    writer.write(postData);
		    writer.flush();
		    
		    String responsePayload = null;
		    
		    int responseCode = conn.getResponseCode();
		    String responseMessage = conn.getResponseMessage();
			long endTimestamp = System.currentTimeMillis();
		    
		    if(responseCode == HttpURLConnection.HTTP_OK) {
			    reader = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
		        String line;
		        StringBuilder sb = new StringBuilder();
		        while ((line = reader.readLine()) != null) { 
		        	sb.append(line);
		        }
		        reader.close();
		        
		       result = responsePayload = sb.toString();
		        
		        if(!responsePayload.trim().contains("Data kosong") )
		        	log.debug("cekDataAkhir FAILED {} {} {}ms", nomorPk, responsePayload, (endTimestamp-startTimestamp));
		    } else {
				log.debug("cekDataAkhir FAILED {} {}-{} {}ms", nomorPk, responseCode, responseMessage, (endTimestamp-startTimestamp));
				log.debug(postData);
			}
		} catch (Exception e) {
        	log.error("Error on cekDataAkhir " + nomorPk, e);
			log.debug(postData);
		} finally {
			if(writer != null)
				try {
					writer.close();
				} catch (Exception e) { }

			if(reader != null)
				try {
					reader.close();
				} catch (IOException e) { }
		}
		
		return result;
	}
	
	private String getValueOrDefault(Map<String, String> fieldMap, String key, String _default, boolean urlEncoded) throws Exception {
		String val = (fieldMap.get(key)!=null && !"".equals(fieldMap.get(key))) ? fieldMap.get(key) :  _default;
		return (urlEncoded && val!=null) ? URLEncoder.encode(val, StandardCharsets.UTF_8.toString()) : val;
	}
	
//	API 1
	public String regRoya(String ppkNomor, String noSertifikat, ZonedDateTime tglSertifikat, String registrationId, String namaDebitur, String alamatDebitur, String kedudukanDebitur, String idKedudukanDebitur, String namaNotaris, Long idNotaris, String idWilayahKerja, String konte, LocalDate tglProses, String sessionId, Integer tipeRoya) throws Exception {
		Map<String, String> fieldMap = generateFormRoya(ppkNomor, noSertifikat, tglSertifikat, namaNotaris, idNotaris, idWilayahKerja, konte, sessionId);
		if(fieldMap != null && fieldMap.size() > 0) {
			String noTransaksiOri = getValueOrDefault(fieldMap, "no_transaksi_lama", registrationId, true);
			if(noTransaksiOri == null) {
				log.warn("Roya desentralisasi has invalid form-roya data, PK:{}", ppkNomor);
				return "-1";
			}
			
			OutputStreamWriter writer = null;
			BufferedReader reader = null;
			
			String labelMusnah = "SURAT KETERANGAN PENGHAPUSAN KARENA MUSNAHNYA BENDA YANG MENJADI OBJEK JAMINAN FIDUSIA"; // tipe =5
			String labelLunas = "SURAT KETERANGAN PENGHAPUSAN KARENA PELUNASAN UTANG"; // tipe =3
			String labelAlih = "SURAT KETERANGAN PENGHAPUSAN KARENA PELEPASAN HAK ATAS JAMINAN FIDUSIA OLEH PENERIMA FIDUSIA"; // tipe=4
			
			StringBuilder sb = new StringBuilder();
			sb.append("no_sertifikat_ori=").append(getValueOrDefault(fieldMap, "no_sertifikat_ori", noSertifikat, true)).append("&");
			sb.append("no_sertifikat=").append(getValueOrDefault(fieldMap, "no_sertifikat", noSertifikat, true)).append("&");
			sb.append("nama_penerima=").append(getValueOrDefault(fieldMap, "nama_penerima", "ADIRA DINAMIKA MULTI FINANCE", true)).append("&");
			sb.append("alamat_penerima=").append(getValueOrDefault(fieldMap, "alamat_penerima", "MILLENIUM CENTENNIAL CENTER, LT. 53-61, JL. JENDERAL SUDIRMAN KAV. 25, DESA/KELURAHAN KARET KUNINGAN, KECAMATAN SETIABUDI, KABUPATEN/KOTA JAKARTA SELATAN, PROVINSI DKI JAKARTA, KODE POS 12920", true)).append("&");
			sb.append("tanggal_sertifikat=").append(getValueOrDefault(fieldMap, "tanggal_sertifikat", tglSertifikat.toLocalDate().toString(), true)).append("&");
			sb.append("no_ser_akhir=").append(getValueOrDefault(fieldMap, "no_ser_akhir", noSertifikat, true)).append("&");
			sb.append("konte=").append("").append("&");
			sb.append("tempat_obyek=").append(getValueOrDefault(fieldMap, "tempat_obyek", idKedudukanDebitur, true)).append("&");
			sb.append("tgl_pembayaran=").append(getValueOrDefault(fieldMap, "tgl_pembayaran", DateUtils.formatZonedDateTime(tglSertifikat, DateUtils.FORMATTER_ID_LOCAL_DATETIME_V2), true)).append("&");
			sb.append("nama_notaris=").append(getValueOrDefault(fieldMap, "nama_notaris", namaNotaris, true)).append("&");
			sb.append("no_transaksi_lama=").append(getValueOrDefault(fieldMap, "no_transaksi_lama", registrationId, true)).append("&");
			sb.append("nama_kedudukan=").append(getValueOrDefault(fieldMap, "nama_kedudukan", kedudukanDebitur, true)).append("&");
			sb.append("alamat_pemberi=").append(getValueOrDefault(fieldMap, "alamat_pemberi", alamatDebitur, true)).append("&");
			sb.append("no_transaksi_ori=").append(getValueOrDefault(fieldMap, "no_transaksi_ori", registrationId, true)).append("&");
			sb.append("nama_pemberi=").append(getValueOrDefault(fieldMap, "nama_pemberi", namaDebitur, true)).append("&");
			if (5 == tipeRoya) {
				sb.append("label_typeroya=").append(getValueOrDefault(fieldMap, "label_typeroya", labelMusnah, true)).append("&");
				sb.append("type_roya=").append("5").append("&");
			} else if (4 == tipeRoya) {
				sb.append("label_typeroya=").append(getValueOrDefault(fieldMap, "label_typeroya", labelAlih, true)).append("&");
				sb.append("type_roya=").append("4").append("&");
			} else {
				sb.append("label_typeroya=").append(getValueOrDefault(fieldMap, "label_typeroya", labelLunas, true)).append("&");
				sb.append("type_roya=").append("3").append("&");
			}
			
			sb.append("tgl_permohonan2=").append(DateUtils.formatDate(tglProses, DateUtils.FORMATTER_ISO_LOCAL_DATE_V2)).append("&");
			sb.append("agree2=").append("on").append("&");
			sb.append("agree=").append("1");
			
			String postData = sb.toString();
			try {
				long startTimestamp = System.currentTimeMillis();

				SSLContext ssl_ctx = SSLContext.getInstance("TLS");
				TrustManager[] trust_mgr = get_trust_mgr();
				ssl_ctx.init(null, trust_mgr, new SecureRandom());
				HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());

				URL url = new URL(regRoyaUrl);
				HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
				conn.setRequestMethod("POST");
				conn.setConnectTimeout(connectTimeout);
				conn.setReadTimeout(readTimeout);
			    
				conn.setHostnameVerifier(new HostnameVerifier() {
					public boolean verify(String host, SSLSession sess) {
						if (host.equals("localhost"))
							return true;
						else
							return false;
					}
				});

				conn.setRequestProperty("Accept", "*/*");
				conn.setRequestProperty("Accept-Encoding", "gzip, deflate, br");
				conn.setRequestProperty("Accept-Language", "en-US,en;q=0.9");
				conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
				conn.setRequestProperty("Cookie", "PHPSESSID=" + sessionId);
				conn.setRequestProperty("Host", host);
				conn.setRequestProperty("Origin", origin);
				conn.setRequestProperty("Referer", refererFormRoya);
				conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
				
		        conn.setDoInput(true);
				conn.setDoOutput(true);
			    writer = new OutputStreamWriter(conn.getOutputStream());

			    writer.write(postData);
			    writer.flush();
			    
			    int responseCode = conn.getResponseCode();
			    String responseMessage = conn.getResponseMessage();
				long endTimestamp = System.currentTimeMillis();
			    
			    if(responseCode == HttpURLConnection.HTTP_OK) {
				    reader = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
			        String line;
			        StringBuilder sb2 = new StringBuilder();
			        while ((line = reader.readLine()) != null) { 
			        	sb2.append(line);
			        }
			        
			        reader.close();
			        
			        String responsePayload = sb2.toString();
			        log.debug(responsePayload);
			        
					String[] respTokens = responsePayload.split("\\^");
					if("1".equals(respTokens[0])) {
						log.debug("reg roya 1 SUCCESS {} in {}ms", ppkNomor, (endTimestamp-startTimestamp));

					    writer.close();
					    reader.close();
						return respTokens[1];
			        } else {
						log.debug("reg roya 1 FAILED {} {} {}ms", ppkNomor, responsePayload, (endTimestamp-startTimestamp));
						log.debug(postData);
					}
			    } else {
					log.debug("reg roya 1 FAILED {} {}-{} {}ms", ppkNomor, responseCode, responseMessage, (endTimestamp-startTimestamp));
					log.debug(postData);
				}
			    
			    writer.close();
			    reader.close();
				
				return null;
			} catch (Exception e) {
	        	log.error("Error on reg roya " + ppkNomor, e);
				log.debug(postData);

				if(writer != null)
					try {
						writer.close();
					} catch (Exception e2) { }

				if(reader != null)
					try {
						reader.close();
					} catch (IOException e2) { }
				
				throw e;
			}
		}
		
		
		return null;
	}

	public String downloadRoya(String ppkNomor, LocalDate orderDate, String registrationId, String sessionId) throws Exception {
		FileOutputStream outputStream = null;
		InputStream inputStream = null;
		InputStream inputStream2 = null;

		String royaDir = applicationProperties.getDomain().getBaseDirectory() + "roya/";
		try {
			long startTimestamp = System.currentTimeMillis();

			SSLContext ssl_ctx = SSLContext.getInstance("TLS");
			TrustManager[] trust_mgr = get_trust_mgr();
			ssl_ctx.init(null, trust_mgr, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());
			
			String stringURL = downloadRoyaUrl + "?id=" + registrationId;
			URL url = new URL(stringURL);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setConnectTimeout(connectTimeout);
			conn.setReadTimeout(readTimeout);
		    
			conn.setHostnameVerifier(new HostnameVerifier() {
				public boolean verify(String host, SSLSession sess) {
					if (host.equals("localhost"))
						return true;
					else
						return false;
				}
			});

			conn.setRequestProperty("Accept", "*/*");
			conn.setRequestProperty("Content-Type", "application/pdf");
			conn.setRequestProperty("Cookie", "PHPSESSID=" + sessionId);
			conn.setRequestProperty("Host", host);
			conn.setRequestProperty("Origin", origin);
			conn.setRequestProperty("Referer", refererDownloadRoya);
			conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
		    
		    int responseCode = conn.getResponseCode();
		    String responseMessage = conn.getResponseMessage();
			
			if (responseCode == HttpURLConnection.HTTP_OK) {
	            inputStream = conn.getInputStream();

	            File savedFileDir = new File(royaDir + orderDate);
	            if(!savedFileDir.exists())
	            	savedFileDir.mkdirs();
	            
	            String saveFilePath = savedFileDir.getPath() + "/" + ppkNomor + "-roya.pdf";
	            
	            File downloadFile = new File(saveFilePath);
	            if(downloadFile.exists()) {
	            	try {
	            		downloadFile.delete();
	            	} catch(Exception ex1) {}
	            }
	            
	            outputStream = new FileOutputStream(saveFilePath);
	 
	            int bytesRead = -1;
	            byte[] buffer = new byte[BUFFER_SIZE];
	            boolean read = false;
	            while ((bytesRead = inputStream.read(buffer)) != -1) {
	            	if(!read) read = true;
	            	outputStream.write(buffer, 0, bytesRead);
	            }
	            
				if(read) {
					String[] lines = readPdf(new File(saveFilePath));
					String noSertifikat = null;
					if(lines != null) {
						if(lines.length > 5) {
							String[] noSertifikatTokens = lines[4].trim().split(" ");
							noSertifikat = noSertifikatTokens.length >= 5 ? noSertifikatTokens[2] + " " + noSertifikatTokens[3] + " " + noSertifikatTokens[4] : null;
							
							log.debug("download roya SUCCESS {} {} {} -> {}, in {} ms.", ppkNomor, orderDate, registrationId, noSertifikat, System.currentTimeMillis()-startTimestamp);

							outputStream.close();
							inputStream.close();
							
							return noSertifikat;
						}
					}

					if(noSertifikat == null)
						log.debug("download roya FAILED {} {} {} -> Content is empty or invalid, in {} ms.", ppkNomor, orderDate, registrationId, System.currentTimeMillis()-startTimestamp);
				} else {
					log.debug("download roya FAILED {} {} {} -> {}, in {} ms.", ppkNomor, orderDate, registrationId, responseCode + " " + responseMessage, System.currentTimeMillis()-startTimestamp);
				}
	        } else {
				log.debug("download roya FAILED {} {} {} -> {}, in {} ms.", ppkNomor, orderDate, registrationId, responseCode + " " + responseMessage, System.currentTimeMillis()-startTimestamp);
	        }
		    
			outputStream.close();
			inputStream.close();
			
			return null;
		} catch (Exception e) {
        	log.error("download roya ERROR " + ppkNomor + " " + orderDate + " " + ExceptionUtils.getMessage(e));
        	
			if(outputStream != null)
				try {
					outputStream.close();
				} catch (Exception e2) { }

			if(inputStream != null)
				try {
					inputStream.close();
				} catch (IOException e2) { }
			
			throw e;
		}
	}
	
	public void readPdfInDirectory(String tglRoya) {
		String royaDirectory = applicationProperties.getDomain().getBaseDirectory() + "roya/" + tglRoya;
		
        File directory = new File(royaDirectory);
		File[] files = directory.listFiles();
		
		for (File file : files) {
			String ppkNomor = file.getName().split("-")[0];
			RawDataRoya  roya = rawDataRoyaRepository.findOneByUniqueKey(ppkNomor);
			
			if (roya.getNomorRoya() == null) {
				String[] lines = readPdf(new File(file.getAbsolutePath()));
				if(lines != null) {
					if(lines.length > 5) {
						String[] noSertifikatTokens = lines[4].trim().split(" ");
						String noSertifikat = noSertifikatTokens.length >= 5 ? noSertifikatTokens[2] + " " + noSertifikatTokens[3] + " " + noSertifikatTokens[4] : null;
						log.debug("read pdf roya {} - {}", ppkNomor, noSertifikat);
						roya.setNomorRoya(noSertifikat);
						roya.setTglRoya(LocalDate.parse(tglRoya));
						roya.setStatus("SECCESS");
						roya.setMessage(null);
						roya.setLastUpdateOn(ZonedDateTime.now());
						roya.setLastUpdateBy("readPdfInDirectory.update");
						
						rawDataRoyaRepository.save(roya);
					}
				}
				
			}
			
		}
		
	}

	public Map<String, String> processRoyaDesentralisasi(List<RawDataRoya> list, LocalDate tglProses, String sessionId, boolean force) throws Exception {
		log.debug("process roya desentralisasi list:{} tglProses:{} sessionId:{} force:{}", list!=null ? list.size() : null, tglProses, sessionId, force);
		
		Map<String, String> result = new HashMap<String, String>();
		int successCount = 0;
		int failedCount = 0;
		int errorCount = 0;
		
		if(list != null && list.size() > 0) {
			for (RawDataRoya roya : list) {
				String ppkNomor = roya.getUniqueKey();
				String status = RawDataRoyaStatus.NOTFOUND;
				String message = null;
				
				try {
					String noSertifikat = roya.getNoSertifikatOri();
					ZonedDateTime tglSertifikat = roya.getTglSertifikatOri();
					
					String namaNotaris = roya.getNamaNotarisOri();
					NotarisDesentralisasi notaris = notarisDesentralisasiRepository.findOneByNama(namaNotaris);
					
					if(notaris != null && notaris.getNamaAhu() != null && notaris.getIdWilayah() != null) {
						String konte = generateRoyaKonte(sessionId);
						log.debug("konte desentralisasi:{}", konte);
						if(konte != null) {
							boolean cekRoya = force ? true : cekRoya(ppkNomor, noSertifikat, tglSertifikat, namaNotaris, notaris.getIdAhu(), notaris.getIdWilayah().toString(), konte, sessionId);
							
							if (cekRoya) {
								String cekDataAkhir = cekDataAkhir(ppkNomor, noSertifikat, sessionId);
								
								if(cekDataAkhir.trim().contains("Data kosong")) {
									String regRoya = regRoya(ppkNomor, noSertifikat, tglSertifikat, null, null, null, null, null,
											notaris.getNamaAhu(), notaris.getIdAhu(), notaris.getIdWilayah().toString(), konte, roya.getTglLunas(), sessionId, roya.getRoyaTipe());
									if(regRoya != null && !"-1".equals(regRoya)) {
										String download = downloadRoya(ppkNomor, tglProses, regRoya, sessionId);
										roya.setRegistrationId(regRoya);
										if(download != null) {
											status = RawDataRoyaStatus.SUCCESS;
											roya.setNomorRoya(download);
											roya.setTglRoya(tglProses);
											roya.setRegistrationId(regRoya);
											roya.setResultSent(false);
										} else {
											status = RawDataRoyaStatus.FAILED;
											message = "Gagal mendaftarkan roya di AHU";
										}
									} else  if (regRoya != null && "-1".equals(regRoya)){
										status = RawDataRoyaStatus.FAILED;
										message = "Data transaksi lama tidak ditemukan di AHU";
									} else {
										status = RawDataRoyaStatus.FAILED;
										message = "Gagal mendaftarkan roya di AHU";
									}
								} else if (cekDataAkhir.trim().contains("hapus")) {
									status = RawDataRoyaStatus.NOTFOUND;
									message = "Sudah dihapus di AHU";
								} else if( "".equals(cekDataAkhir) || cekDataAkhir.trim().equals("null") ) {
									status = RawDataRoyaStatus.NOTFOUND;
									message = "Respon AHU null";
								} else {
									status = RawDataRoyaStatus.FAILED; //status sebelumnya = RawDataRoyaStatus.NOTFOUND;
									message = "Data tidak ditemukan";
								}
							} else {
								status = RawDataRoyaStatus.FAILED; //status sebelumnya = RawDataRoyaStatus.NOTFOUND;
								message = "Data sudah pernah dicoret/dihapus AHU";
							}
						} else {
							status = RawDataRoyaStatus.FAILED;
	  						message = "Gagal generate token AHU";
						}
					} else {
						status = RawDataRoyaStatus.NOTFOUND;
						message = "Data notaris tidak ditemukan";
					}

					roya.setStatus(status);
					roya.setMessage(message);
					roya.setLastUpdateOn(DateUtils.nowDateTime());
					roya.setLastUpdateBy("OnlineService.processRoya");
					rawDataRoyaRepository.save(roya);
				} catch (Exception e) {
					log.error("Error on OnlineService.processRoya for ppk " + ppkNomor + " caused by " + e.getMessage(), e);
					roya.setStatus(RawDataRoyaStatus.ERROR);
					roya.setLastUpdateOn(DateUtils.nowDateTime());
					roya.setLastUpdateBy("OnlineService.processRoya");
					rawDataRoyaRepository.save(roya);
				}
			}
		}
		
		result.put("successCount", "" + successCount);
		result.put("failedCount", "" + failedCount);
		result.put("errorCount", "" + errorCount);
		return result;
	}

	private String[] readPdf(File file) {
		PDDocument document = null;
		try {
			document = Loader.loadPDF(file);
			PDFTextStripper reader = new PDFTextStripper();
			reader.setStartPage(1);
			reader.setEndPage(1);
			String payload = reader.getText(document);
			return payload.split("\\r?\\n");
		} catch (Exception e) {
			log.error("FAILED readPdf -> " + ExceptionUtils.getMessage(e));
		} finally {
			try {
				document.close();
			} catch (Exception ex) {}
		}

		return null;
	}

	private TrustManager[] get_trust_mgr() {
		TrustManager[] certs = new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String t) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String t) {
			}
		} };
		return certs;
	}
	
	

	
//	public Map<String, String> processRoya(List<RawDataRoya> list, LocalDate tglProses, String sessionId, boolean force) throws Exception {
//		log.debug("process roya list:{} tglProses:{} sessionId:{} force:{}", list!=null ? list.size() : null, tglProses, sessionId, force);
//		
//		Map<String, String> result = new HashMap<String, String>();
//		int successCount = 0;
//		int failedCount = 0;
//		int errorCount = 0;
//		Date maxHourDt = DateUtils.parse(LocalDate.now() + " " + maxHour, "yyyy-MM-dd HH:mm");
//		
//		if(list != null && list.size() > 0) {
//			for (RawDataRoya roya : list) {
//				if(maxHourDt.before(new Date())) break;
//				
//				String ppkNomor = roya.getUniqueKey();
//				String status = RawDataRoyaStatus.NOTFOUND;
//				String message = null;
//				try {
//					List<Sertifikat> sertifikats = sertifikatRepository.findByAktaPpkNomor(ppkNomor);
//					
//					if(sertifikats != null && sertifikats.size() > 0) {
//						Sertifikat sertifikat = null;
//						for (Sertifikat sert : sertifikats) {
//							if(sertifikat == null || sertifikat.getNoSertifikat() == null || sertifikat.getTglSertifikat() == null) sertifikat = sert;
//							else if(sert.getTglSertifikat() != null && sertifikat.getTglSertifikat() != null && sert.getTglSertifikat().isAfter(sertifikat.getTglSertifikat())) sertifikat = sert;
//						}
//						
//						if(sertifikat != null && sertifikat.getNoSertifikat() != null) {
//							String noSertifikat = sertifikat.getNoSertifikat();
//							Notaris notaris = sertifikat.getAkta().getNotaris();
//							Long idNotaris = notaris.getId();
//							String namaNotaris = notaris.getNamaAhu();
//							String idWilayahKerja = referensiService.valueOf("WILAYAH_KERJA", notaris.getWilayahKerja());
//							SertifikatTurunan serT = sertifikatTurunanRepository.findOneByKategoriAndSertifikatOri(4, sertifikat);
//							
//							if(serT != null && serT.getRegistrationId() != null)  {
//								if(serT.getNoSertifikat() == null) {
//								  	try {
//										String download = downloadRoya(ppkNomor, tglProses, serT.getRegistrationId(), sessionId);
//										if(download != null) {
//											serT.setNoSertifikat(download);
//											serT.setTglSertifikat(tglProses.atStartOfDay(ZoneId.systemDefault()));
//											serT.setUpdateBy("AhuPenghapusanService");
//											serT.setUpdateOn(DateUtils.nowDateTime());
//											serT.setSyncFlag(false);
//											sertifikatTurunanRepository.save(serT);
//											
//											status = RawDataRoyaStatus.SUCCESS;
//											roya.setNomorRoya(download);
//											roya.setTglRoya(tglProses);
//											roya.setResultSent(false);
//										} else {
//											status = RawDataRoyaStatus.FAILED;
//									  		message = "Gagal download nomor roya dari AHU";
//										}
//								  	} catch (Exception e) {
//								  		log.error("Error on downloadRoya: " + e.getMessage(), e);
//								  		status = RawDataRoyaStatus.FAILED;
//								  		message = "Gagal download nomor roya dari AHU";
//								  	}
//								}  else {
//									status = RawDataRoyaStatus.SUCCESS;
//									roya.setNomorRoya(serT.getNoSertifikat());
//									roya.setTglRoya(serT.getTglSertifikat().toLocalDate());
//									roya.setResultSent(false);
//								}
//							} else if(sertifikat.getStatusRoya() != null && sertifikat.getRoyaRegId() != null)  {
//								serT = new SertifikatTurunan();
//								serT.setKategori(4);
//								serT.setRegistrationId(sertifikat.getRoyaRegId());
//								serT.setNilaiPnbp(0L);
//								serT.setNilaiJasa(0L);
//								serT.setStatus(4);
//								serT.setUpdateBy("AhuPenghapusanService");
//								serT.setUpdateOn(DateUtils.nowDateTime());
//								serT.setSyncFlag(false);
//								serT.setSertifikatOri(sertifikat);
//								serT.setInvoiceSubmitted(true);
//								
//								if(sertifikat.getNomorRoya() == null) {
//								  	try {
//										String download = downloadRoya(ppkNomor, tglProses, sertifikat.getRoyaRegId(), sessionId);
//										if(download != null) {
//											serT.setNoSertifikat(download);
//											serT.setTglSertifikat(tglProses.atStartOfDay(ZoneId.systemDefault()));
//											
//											status = RawDataRoyaStatus.SUCCESS;
//											roya.setNomorRoya(download);
//											roya.setTglRoya(tglProses);
//											roya.setResultSent(false);
//										} else {
//											status = RawDataRoyaStatus.FAILED;
//									  		message = "Gagal download nomor roya dari AHU";
//										}
//								  	} catch (Exception e) {
//								  		log.error("Error on downloadRoya: " + e.getMessage(), e);
//								  		status = RawDataRoyaStatus.FAILED;
//								  		message = "Gagal download nomor roya dari AHU";
//								  	}
//								}  else {
//									serT.setNoSertifikat(sertifikat.getNomorRoya());
//									serT.setTglSertifikat(sertifikat.getTglRoya().atStartOfDay(ZoneId.systemDefault()));
//									
//									status = RawDataRoyaStatus.SUCCESS;
//									roya.setNomorRoya(sertifikat.getNomorRoya());
//									roya.setTglRoya(sertifikat.getTglRoya());
//									roya.setResultSent(false);
//								}
//								
//								sertifikatTurunanRepository.save(serT);
//							} else {
//								String konte = generateRoyaKonte(sessionId);
//								if(konte != null) {
//									boolean cekRoya = force ? true : cekRoya(ppkNomor, noSertifikat, sertifikat.getTglSertifikat(), namaNotaris, idNotaris, idWilayahKerja, konte, sessionId);
//									boolean cekRoya2 = false;
//									if (!cekRoya)
//										cekRoya2 = force ? true : cekRoya2(ppkNomor, noSertifikat, sertifikat.getTglSertifikat(), namaNotaris, idNotaris, idWilayahKerja, konte, sessionId);
//									
//									if (cekRoya) {
//										String cekDataAkhir = cekDataAkhir(ppkNomor, noSertifikat, sessionId);
//										if (cekDataAkhir.trim().contains("Data kosong")) {
//											Ppk ppk = sertifikat.getAkta().getPpk();
//											Badan badan = ppk.getBadan();
//											Orang orang = ppk.getOrang();
//											
//											String regRoya = regRoya(ppkNomor, noSertifikat, sertifikat.getTglSertifikat(), sertifikat.getRegistrationId(), ppk.getNsbNama(),
//													ppk.getNsbJenis()==1 ? badan.getAlamat() + (("0".equals(badan.getRt()) || "000".equals(badan.getRt())) ? "" : " RT " + badan.getRt()) +
//															(("0".equals(badan.getRw()) || "000".equals(badan.getRw())) ? "" : " RW " + badan.getRw()) +
//															", DESA/KELURAHAN " + badan.getKelurahan() + ", KECAMATAN " + badan.getKecamatan() + ", " + badan.getKota() + ", KODE POS " + badan.getKodePos() 
//														: orang.getAlamat() + (("0".equals(orang.getRt()) || "000".equals(orang.getRt())) ? "" : " RT " + orang.getRt()) +
//															(("0".equals(orang.getRw()) || "000".equals(orang.getRw())) ? "" : " RW " + orang.getRw()) +
//															", DESA/KELURAHAN " + orang.getKelurahan() + ", KECAMATAN " + orang.getKecamatan() + ", " + orang.getKota() + ", KODE POS " + orang.getKodePos(),
//													ppk.getNsbJenis()==1 ? (badan.getProvinsi().startsWith("PROVINSI") ? badan.getProvinsi().substring(9) : badan.getProvinsi())
//														: (orang.getProvinsi().startsWith("PROVINSI") ? orang.getProvinsi().substring(9) : orang.getProvinsi()),
//													ppk.getNsbJenis()==1 ? referensiService.valueOf("PROVINSI", badan.getProvinsi()) : referensiService.valueOf("PROVINSI", orang.getProvinsi()), 
//													namaNotaris, idNotaris, idWilayahKerja, konte, roya.getTglLunas(), sessionId);
//											if(regRoya != null && !"-1".equals(regRoya)) {
//												serT = new SertifikatTurunan();
//												serT.setKategori(4);
//												serT.setRegistrationId(regRoya);
//												serT.setNilaiPnbp(0L);
//												serT.setNilaiJasa(0L);
//												serT.setStatus(4);
//												serT.setUpdateBy("AhuPenghapusanService");
//												serT.setUpdateOn(DateUtils.nowDateTime());
//												serT.setSyncFlag(false);
//												serT.setSertifikatOri(sertifikat);
//												sertifikatTurunanRepository.save(serT);
//
//												String download = downloadRoya(ppkNomor, tglProses, regRoya, sessionId);
//												if(download != null) {
//													serT.setNoSertifikat(download);
//													serT.setTglSertifikat(tglProses.atStartOfDay(ZoneId.systemDefault()));
//
//													status = RawDataRoyaStatus.SUCCESS;
//													roya.setNomorRoya(download);
//													roya.setTglRoya(tglProses);
//													roya.setResultSent(false);
//												} else {
//													status = RawDataRoyaStatus.FAILED;
//													message = "Gagal mendaftarkan roya di AHU";
//												}
//												sertifikatTurunanRepository.save(serT);
//											} else  if (regRoya != null && "-1".equals(regRoya)){
//												status = RawDataRoyaStatus.FAILED;
//												message = "Data transaksi lama tidak ditemukan di AHU";
//											} else {
//												status = RawDataRoyaStatus.FAILED;
//												message = "Gagal mendaftarkan roya di AHU";
//											}
//										} else if (cekDataAkhir.trim().contains("hapus")) {
//											status = RawDataRoyaStatus.NOTFOUND;
//											message = "Sudah dihapus di AHU";
//										} else if( "".equals(cekDataAkhir) || cekDataAkhir.trim().equals("null") ) {
//											status = RawDataRoyaStatus.NOTFOUND;
//											message = "Respon AHU null";
//										} else {
//											status = RawDataRoyaStatus.FAILED; // status sebelumnya = RawDataRoyaStatus.NOTFOUND;
//											message = "Data tidak ditemukan";
//										}
//									} else if (cekRoya2) {
//										Ppk ppk = sertifikat.getAkta().getPpk();
//										Badan badan = ppk.getBadan();
//										Orang orang = ppk.getOrang();
//											
//											String regRoya2 = regRoya2(ppkNomor, noSertifikat, sertifikat.getTglSertifikat(), sertifikat.getRegistrationId(), ppk.getNsbNama(),
//													ppk.getNsbJenis()==1 ? badan.getAlamat() + (("0".equals(badan.getRt()) || "000".equals(badan.getRt())) ? "" : " RT " + badan.getRt()) +
//															(("0".equals(badan.getRw()) || "000".equals(badan.getRw())) ? "" : " RW " + badan.getRw()) +
//															", DESA/KELURAHAN " + badan.getKelurahan() + ", KECAMATAN " + badan.getKecamatan() + ", " + badan.getKota() + ", KODE POS " + badan.getKodePos() 
//														: orang.getAlamat() + (("0".equals(orang.getRt()) || "000".equals(orang.getRt())) ? "" : " RT " + orang.getRt()) +
//															(("0".equals(orang.getRw()) || "000".equals(orang.getRw())) ? "" : " RW " + orang.getRw()) +
//															", DESA/KELURAHAN " + orang.getKelurahan() + ", KECAMATAN " + orang.getKecamatan() + ", " + orang.getKota() + ", KODE POS " + orang.getKodePos(),
//													ppk.getNsbJenis()==1 ? (badan.getProvinsi().startsWith("PROVINSI") ? badan.getProvinsi().substring(9) : badan.getProvinsi())
//														: (orang.getProvinsi().startsWith("PROVINSI") ? orang.getProvinsi().substring(9) : orang.getProvinsi()),
//													ppk.getNsbJenis()==1 ? referensiService.valueOf("PROVINSI", badan.getProvinsi()) : referensiService.valueOf("PROVINSI", orang.getProvinsi()), 
//													namaNotaris, idNotaris, idWilayahKerja, konte, roya.getTglLunas(), sessionId);
//											if(regRoya2 != null && !"-1".equals(regRoya2)) {
//												serT = new SertifikatTurunan();
//												serT.setKategori(4);
//												serT.setRegistrationId(regRoya2);
//												serT.setNilaiPnbp(0L);
//												serT.setNilaiJasa(0L);
//												serT.setStatus(4);
//												serT.setUpdateBy("AhuPenghapusanService");
//												serT.setUpdateOn(DateUtils.nowDateTime());
//												serT.setSyncFlag(false);
//												serT.setSertifikatOri(sertifikat);
//	
//												String download = downloadRoya(ppkNomor, tglProses, regRoya2, sessionId);
//												if(download != null) {
//													serT.setNoSertifikat(download);
//													serT.setTglSertifikat(tglProses.atStartOfDay(ZoneId.systemDefault()));
//	
//													status = RawDataRoyaStatus.SUCCESS;
//													roya.setNomorRoya(download);
//													roya.setTglRoya(tglProses);
//													roya.setResultSent(false);
//												} else {
//													status = RawDataRoyaStatus.FAILED;
//													message = "Gagal mendaftarkan roya di AHU";
//												}
//												sertifikatTurunanRepository.save(serT);
//											} else  if (regRoya2 != null && "-1".equals(regRoya2)){
//												status = RawDataRoyaStatus.FAILED;
//												message = "Data transaksi lama tidak ditemukan di AHU";
//											} else {
//												status = RawDataRoyaStatus.FAILED;
//												message = "Gagal mendaftarkan roya di AHU";
//											}
////										} else if (cekDataAkhir.trim().contains("hapus")) {
////											status = RawDataRoyaStatus.NOTFOUND;
////											message = "Sudah dihapus di AHU";
////										} else if( "".equals(cekDataAkhir) || cekDataAkhir.trim().equals("null") ) {
////											status = RawDataRoyaStatus.NOTFOUND;
////											message = "Respon AHU null";
////										} else {
////											status = RawDataRoyaStatus.FAILED; // status sebelumnya = RawDataRoyaStatus.NOTFOUND;
////											message = "Data tidak ditemukan";
////										}
//									
//								    } else {
//										status = RawDataRoyaStatus.FAILED; // status sebelumnya = RawDataRoyaStatus.NOTFOUND;
//										message = "Data sudah pernah dicoret/dihapus AHU";
//									}
//								} else {
//									status = RawDataRoyaStatus.FAILED;
//			  						message = "Gagal generate token AHU";
//								}
//							}
//						}
//					}
//
//					roya.setStatus(status);
//					roya.setMessage(message);
//					roya.setLastUpdateOn(DateUtils.nowDateTime());
//					roya.setLastUpdateBy("OnlineService.processRoya");
//					rawDataRoyaRepository.save(roya);
//				} catch (Exception e) {
//					log.error("Error on OnlineService.processRoya for ppk " + ppkNomor + " caused by " + e.getMessage(), e);
//					roya.setStatus(RawDataRoyaStatus.ERROR);
//					roya.setLastUpdateOn(DateUtils.nowDateTime());
//					roya.setLastUpdateBy("OnlineService.processRoya");
//					rawDataRoyaRepository.save(roya);
//				}
//			}
//		}
//		
//		result.put("successCount", "" + successCount);
//		result.put("failedCount", "" + failedCount);
//		result.put("errorCount", "" + errorCount);
//		return result;
//	}

////API2
//public String regRoya2(String ppkNomor, String noSertifikat, ZonedDateTime tglSertifikat, String registrationId, String namaDebitur, String alamatDebitur, String kedudukanDebitur, String idKedudukanDebitur, String namaNotaris, Long idNotaris, String idWilayahKerja, String konte, LocalDate tglProses, String sessionId) throws Exception {
//	Map<String, String> fieldMap = generateFormRoya(ppkNomor, noSertifikat, tglSertifikat, namaNotaris, idNotaris, idWilayahKerja, konte, sessionId);
//	
//	Sertifikat sertifikat = sertifikatRepository.findOneByAktaPpkNomor(ppkNomor);
//	Akta akta = sertifikat.getAkta();
//	Ppk ppk = akta.getPpk();
//	Badan badan = ppk.getBadan();
//	Orang orang = ppk.getOrang();
//	Notaris notaris = akta.getNotaris();
//	PenerimaKuasa penerimaKuasa = akta.getPenerimaKuasa();
//	String notarisWilKerja = referensiService.valueOf("WILAYAH_KERJA", notaris.getWilayahKerja());
//	Cabang cabang = akta.getCabang();
//	Leasing leasing = cabang.getLeasing();
//	
//	if(fieldMap != null && fieldMap.size() > 0) {
//		String noTransaksiOri = getValueOrDefault(fieldMap, "no_transaksi_lama", registrationId, true);
//		if(noTransaksiOri == null) {
//			log.warn("Roya desentralisasi has invalid form-roya data, PK:{}", ppkNomor);
//			return "-1";
//		}
//		
//		OutputStreamWriter writer = null;
//		BufferedReader reader = null;
//		
//		StringBuilder sb = new StringBuilder();
//		sb.append("konte=").append("").append("&");
//		sb.append("no_ser=").append(getValueOrDefault(fieldMap, "no_ser", noSertifikat, true)).append("&");
//		sb.append("no_sertifikat=").append(getValueOrDefault(fieldMap, "no_sertifikat", noSertifikat, true)).append("&");
//		sb.append("flag_data=").append("").append("&");
//		sb.append("nama_notaris=").append(getValueOrDefault(fieldMap, "nama_notaris", namaNotaris, true)).append("&");
//		sb.append("tanggal_sertifikat=").append(getValueOrDefault(fieldMap, "tanggal_sertifikat", tglSertifikat.toLocalDate().toString(), true)).append("&");
//		sb.append("tempat_notaris=").append(idWilayahKerja).append("&");
//		sb.append("tgl_pembayaran=").append(getValueOrDefault(fieldMap, "tgl_pembayaran", DateUtils.formatZonedDateTime(tglSertifikat, DateUtils.FORMATTER_ID_LOCAL_DATETIME_V2), true)).append("&");
//		sb.append("label_typeroya=").append(getValueOrDefault(fieldMap, "label_typeroya", "Penghapusan karena pelunasan", true)).append("&");
//		sb.append("type_roya=").append("1").append("&");
//		sb.append("_flag=").append("2").append("&");
//		sb.append("nama_penerima=").append(getValueOrDefault(fieldMap, "nama_penerima", "ADIRA DINAMIKA MULTI FINANCE", true)).append("&");
//		sb.append("sebutan=").append("rupiah").append("&");
//		sb.append("inisial_kurs=").append("Rp.").append("&");
//		sb.append("tipe_pemberi=").append(ppk.getNsbJenis()).append("&");
//		
//		if(ppk.getNsbJenis().intValue() == NasabahJenis.BADAN.value()) {
//			sb.append("nama_pemberi=").append(getValueOrDefault(fieldMap, "nama_pemberi", badan.getNama(), true)).append("&");
//			sb.append("nik_npwp_pemberi=").append(getValueOrDefault(fieldMap, "nik_npwp_pemberi", badan.getNpwp(), true)).append("&");
//			sb.append("no_hp_pemberi=").append(getValueOrDefault(fieldMap, "no_hp_pemberi", penerimaKuasa.getNoKontak(), true)).append("&");
//			sb.append("alamat_pemberi=").append(getValueOrDefault(fieldMap, "alamat_pemberi", badan.getAlamat(), true)).append("&");
//			sb.append("kodepos_pemberi=").append(getValueOrDefault(fieldMap, "kodepos_pemberi", badan.getKodePos(), true)).append("&");
//			sb.append("provinsi_pemberi=").append(getValueOrDefault(fieldMap, "provinsi_pemberi", referensiService.valueOf("PROVINSI", badan.getProvinsi()), true)).append("&");
//			sb.append("kab_kota_pemberi=").append(getValueOrDefault(fieldMap, "kab_kota_pemberi", referensiService.valueOf("KOTA", badan.getKota()), true)).append("&");
//			sb.append("kecamatan_pemberi=").append(getValueOrDefault(fieldMap, "kecamatan_pemberi", badan.getKecamatan(), true)).append("&");
//			sb.append("kelurahan_pemberi=").append(getValueOrDefault(fieldMap, "kelurahan_pemberi", badan.getKelurahan(), true)).append("&");
//			sb.append("rt_pemberi=").append(getValueOrDefault(fieldMap, "rt_pemberi", badan.getRt(), true)).append("&");
//			sb.append("rw_pemberi=").append(getValueOrDefault(fieldMap, "rw_pemberi", badan.getRw(), true)).append("&");
//		} else {
//			sb.append("nama_pemberi=").append(getValueOrDefault(fieldMap, "nama_pemberi", orang.getNama(), true)).append("&");
//			sb.append("nik_npwp_pemberi=").append(getValueOrDefault(fieldMap, "nik_npwp_pemberi", orang.getNoId(), true)).append("&");
//			sb.append("no_hp_pemberi=").append(getValueOrDefault(fieldMap, "no_hp_pemberi", penerimaKuasa.getNoKontak(), true)).append("&");
//			sb.append("alamat_pemberi=").append(getValueOrDefault(fieldMap, "alamat_pemberi", orang.getAlamat(), true)).append("&");
//			sb.append("kodepos_pemberi=").append(getValueOrDefault(fieldMap, "kodepos_pemberi", orang.getKodePos(), true)).append("&");
//			sb.append("provinsi_pemberi=").append(getValueOrDefault(fieldMap, "provinsi_pemberi", referensiService.valueOf("PROVINSI", orang.getProvinsi()), true)).append("&");
//			sb.append("kab_kota_pemberi=").append(getValueOrDefault(fieldMap, "kab_kota_pemberi", referensiService.valueOf("KOTA", orang.getKota()), true)).append("&");
//			sb.append("kecamatan_pemberi=").append(getValueOrDefault(fieldMap, "kecamatan_pemberi", orang.getKecamatan(), true)).append("&");
//			sb.append("kelurahan_pemberi=").append(getValueOrDefault(fieldMap, "kelurahan_pemberi", orang.getKelurahan(), true)).append("&");
//			sb.append("rt_pemberi=").append(getValueOrDefault(fieldMap, "rt_pemberi", orang.getRt(), true)).append("&");
//			sb.append("rw_pemberi=").append(getValueOrDefault(fieldMap, "rw_pemberi", orang.getRw(), true)).append("&");
//		}
//		
//		sb.append("tipe_penerima=").append("1").append("&");
//		sb.append("nm_penerima=").append(leasing.getNama()).append("&");
//		sb.append("nik_npwp_penerima=").append(getValueOrDefault(fieldMap, "nik_npwp_penerima", leasing.getNpwp(), true)).append("&");
//		sb.append("no_hp_penerima=").append(getValueOrDefault(fieldMap, "no_hp_penerima", leasing.getNoKontak(), true)).append("&");
//		sb.append("alamat_penerima=").append(getValueOrDefault(fieldMap, "alamat_penerima", leasing.getAlamat(), true)).append("&");
//		sb.append("kodepos_penerima=").append(getValueOrDefault(fieldMap, "kodepos_penerima", leasing.getKodePos(), true)).append("&");
//		sb.append("provinsi_penerima=").append(getValueOrDefault(fieldMap, "provinsi_penerima", referensiService.valueOf("PROVINSI", leasing.getProvinsi()), true)).append("&");
//		sb.append("kab_kota_penerima=").append(getValueOrDefault(fieldMap, "kab_kota_penerima", referensiService.valueOf("KOTA", leasing.getKota()), true)).append("&");
//		sb.append("kecamatan_penerima=").append(getValueOrDefault(fieldMap, "kecamatan_penerima", leasing.getKecamatan(), true)).append("&");
//		sb.append("kelurahan_penerima=").append(getValueOrDefault(fieldMap, "kelurahan_penerima", leasing.getKelurahan(), true)).append("&");
//		sb.append("rt_penerima=").append(getValueOrDefault(fieldMap, "rt_penerima", leasing.getRw(), true)).append("&");
//		sb.append("rw_penerima=").append(getValueOrDefault(fieldMap, "rw_penerima", leasing.getRt(), true)).append("&");
//		sb.append("flag_hutang=").append("1").append("&");
//		sb.append("penjaminan=").append("" + ppk.getNilaiHutangPokok() + ".00").append("&");
//		sb.append("jaminan_fidusia[]=").append("" + ppk.getNilaiPenjaminan() + ".00").append("&");
//		
//		sb.append("no_akta=").append(akta.getNomor()).append("&");
//		sb.append("tgl_akta=").append(akta.getTanggal().format(dateFormat)).append("&");
//		sb.append("nm_notaris=").append(notaris.getNamaAhu()).append("&");
//		sb.append("kd_notaris=").append(notaris.getWilayahKerja()).append("&");
//		sb.append("id_notaris=").append(notaris.getId()).append("&");
//		sb.append("id_kedudukan=").append(notarisWilKerja).append("&");
//		sb.append("id_wilayah=").append(notarisWilKerja).append("&");
//		sb.append("flag_hutang=").append(1).append("&");
//		
////		sb.append("rp_lain2=").append("").append("&");
////		sb.append("nilai_kurs_2=").append("").append("&");
////		sb.append("sebutan_2=").append("").append("&");
////		sb.append("inisial_kurs_2=").append("").append("&");
//		sb.append("tgl_permohonan2=").append(DateUtils.formatDate(tglProses, DateUtils.FORMATTER_ISO_LOCAL_DATE_V2)).append("&");
//		sb.append("agree2=").append("on").append("&");
//		sb.append("agree=").append("1");
//		
//		String postData = sb.toString();
//		try {
//			long startTimestamp = System.currentTimeMillis();
//
//			SSLContext ssl_ctx = SSLContext.getInstance("TLS");
//			TrustManager[] trust_mgr = get_trust_mgr();
//			ssl_ctx.init(null, trust_mgr, new SecureRandom());
//			HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());
//
//			URL url = new URL(regRoyaUrlApi2);
//			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
//			conn.setRequestMethod("POST");
//			conn.setConnectTimeout(connectTimeout);
//			conn.setReadTimeout(readTimeout);
//		    
//			conn.setHostnameVerifier(new HostnameVerifier() {
//				public boolean verify(String host, SSLSession sess) {
//					if (host.equals("localhost"))
//						return true;
//					else
//						return false;
//				}
//			});
//
//			conn.setRequestProperty("Accept", "*/*");
//			conn.setRequestProperty("Accept-Encoding", "gzip, deflate, br");
//			conn.setRequestProperty("Accept-Language", "en-US,en;q=0.9");
//			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
//			conn.setRequestProperty("Cookie", "PHPSESSID=" + sessionId);
//			conn.setRequestProperty("Host", host);
//			conn.setRequestProperty("Origin", origin);
//			conn.setRequestProperty("Referer", refererFormRoya);
//			conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
//			
//	        conn.setDoInput(true);
//			conn.setDoOutput(true);
//		    writer = new OutputStreamWriter(conn.getOutputStream());
//
//		    writer.write(postData);
//		    writer.flush();
//		    
//		    int responseCode = conn.getResponseCode();
//		    String responseMessage = conn.getResponseMessage();
//			long endTimestamp = System.currentTimeMillis();
//		    
//		    if(responseCode == HttpURLConnection.HTTP_OK) {
//			    reader = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
//		        String line;
//		        StringBuilder sb2 = new StringBuilder();
//		        while ((line = reader.readLine()) != null) { 
//		        	sb2.append(line);
//		        }
//		        
//		        reader.close();
//		        
//		        String responsePayload = sb2.toString();
//		        log.debug(responsePayload);
//		        
//				String[] respTokens = responsePayload.split("\\^");
//				if("1".equals(respTokens[0])) {
//					log.debug("reg roya 2 SUCCESS {} in {}ms", ppkNomor, (endTimestamp-startTimestamp));
//
//				    writer.close();
//				    reader.close();
//					return respTokens[1];
//		        } else {
//					log.debug("reg roya 2 FAILED {} {} {}ms", ppkNomor, responsePayload, (endTimestamp-startTimestamp));
//					log.debug(postData);
//				}
//		    } else {
//				log.debug("reg roya 2 FAILED {} {}-{} {}ms", ppkNomor, responseCode, responseMessage, (endTimestamp-startTimestamp));
//				log.debug(postData);
//			}
//		    
//		    writer.close();
//		    reader.close();
//			
//			return null;
//		} catch (Exception e) {
//        	log.error("Error on reg roya " + ppkNomor, e);
//			log.debug(postData);
//
//			if(writer != null)
//				try {
//					writer.close();
//				} catch (Exception e2) { }
//
//			if(reader != null)
//				try {
//					reader.close();
//				} catch (IOException e2) { }
//			
//			throw e;
//		}
//	}
//	
//	
//	return null;
//}
//
//public String readRoyaPdf( String sessionId, String registrationId) throws Exception {
//	FileOutputStream outputStream = null;
//	InputStream inputStream = null;
//	try {
//		long startTimestamp = System.currentTimeMillis();
//
//		SSLContext ssl_ctx = SSLContext.getInstance("TLS");
//		TrustManager[] trust_mgr = get_trust_mgr();
//		ssl_ctx.init(null, trust_mgr, new SecureRandom());
//		HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());
//		
//		String stringURL = downloadRoyaUrl + "?id=" + registrationId;
//		URL url = new URL(stringURL);
//		HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
//		conn.setRequestMethod("GET");
//		conn.setConnectTimeout(connectTimeout);
//		conn.setReadTimeout(readTimeout);
//	    
//		conn.setHostnameVerifier(new HostnameVerifier() {
//			public boolean verify(String host, SSLSession sess) {
//				if (host.equals("localhost"))
//					return true;
//				else
//					return false;
//			}
//		});
//
//		conn.setRequestProperty("Accept", "*/*");
//		conn.setRequestProperty("Content-Type", "application/pdf");
//		conn.setRequestProperty("Cookie", "PHPSESSID=" + sessionId);
//		conn.setRequestProperty("Host", host);
//		conn.setRequestProperty("Origin", origin);
//		conn.setRequestProperty("Referer", refererDownloadRoya);
//		conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
//	    
//	    int responseCode = conn.getResponseCode();
//	    String responseMessage = conn.getResponseMessage();
//		
//		if (responseCode == HttpURLConnection.HTTP_OK) {
//            inputStream = conn.getInputStream();
//
//            File savedFileDir = new File(royaDirectory + "cek");
//            if(!savedFileDir.exists())
//            	savedFileDir.mkdirs();
//            
//            String saveFilePath = savedFileDir.getPath() + "/" + registrationId + "-roya.pdf";
//            File downloadFile = new File(saveFilePath);
//            if(downloadFile.exists()) {
//            	try {
//            		downloadFile.delete();
//            	} catch(Exception ex1) {}
//            }
//            
//            outputStream = new FileOutputStream(saveFilePath);
// 
//            int bytesRead = -1;
//            byte[] buffer = new byte[BUFFER_SIZE];
//            boolean read = false;
//            while ((bytesRead = inputStream.read(buffer)) != -1) {
//            	if(!read) read = true;
//            	outputStream.write(buffer, 0, bytesRead);
//            }
//            
//			if(read) {
//				String[] lines = readPdf(new File(saveFilePath));
//				String noSertifikat = null;
//				if(lines != null) {
//					if(lines.length > 0) {
//						String noSertifikatTokens = lines[16];
//						
//						log.debug("result = {} ,{} " , noSertifikatTokens , registrationId);
//
//						outputStream.close();
//						inputStream.close();
//						
//						return noSertifikat;
//					}
//				}
//
//				if(noSertifikat == null)
//					log.debug("download roya FAILED {} {} {} -> Content is empty or invalid, in {} ms.", registrationId, System.currentTimeMillis()-startTimestamp);
//			} else {
//				log.debug("download roya FAILED {} {} {} -> {}, in {} ms.",  registrationId, responseCode + " " + responseMessage, System.currentTimeMillis()-startTimestamp);
//			}
//
//			
//	        if(downloadFile.exists()) {
//	        	try {
//	        		downloadFile.delete();
//	        	} catch(Exception ex1) {}
//	        }
//        } else {
//			log.debug("download roya FAILED {} {} {} -> {}, in {} ms.", registrationId, responseCode + " " + responseMessage, System.currentTimeMillis()-startTimestamp);
//        }
//	    
//		outputStream.close();
//		inputStream.close();
//		
//		return null;
//	} catch (Exception e) {
//    	log.error("download roya ERROR " + " " + ExceptionUtils.getMessage(e));
//    	
//		if(outputStream != null)
//			try {
//				outputStream.close();
//			} catch (Exception e2) { }
//
//		if(inputStream != null)
//			try {
//				inputStream.close();
//			} catch (IOException e2) { }
//		
//		throw e;
//	}
//}

	
//	public boolean cekRoya2(String ppkNomor, String noSertifikat, ZonedDateTime tglSertifikat, String namaNotaris, Long idNotaris, String idWilayahKerja, String konte, String sessionId) throws Exception {
//		OutputStreamWriter writer = null;
//		BufferedReader reader = null;
//		
//		StringBuilder sb = new StringBuilder();
//		sb.append("no_ser_2=").append(URLEncoder.encode(noSertifikat, StandardCharsets.UTF_8.toString())).append("&");
//		sb.append("tgl_sertifikat_2=").append(DateUtils.formatDate(tglSertifikat.toLocalDate(), DateUtils.FORMATTER_ISO_LOCAL_DATE_V2)).append("&");
//		sb.append("nm_not_2=").append(URLEncoder.encode(namaNotaris, StandardCharsets.UTF_8.toString())).append("&");
////		sb.append("_id_not=").append(idNotaris).append("&");
////		sb.append("_id_not_old=").append(idNotaris).append("&");
//		sb.append("tempat2=").append(idWilayahKerja).append("&");
////		sb.append("cekkonte=").append(konte).append("&");
//		sb.append("_flag=").append("Q");
//		String postData = sb.toString();
//		
//		try {
//			long startTimestamp = System.currentTimeMillis();
//
//			SSLContext ssl_ctx = SSLContext.getInstance("TLS");
//			TrustManager[] trust_mgr = get_trust_mgr();
//			ssl_ctx.init(null, trust_mgr, new SecureRandom());
//			HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());
//
//			URL url = new URL(cekRoyaUrl);
//			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
//			conn.setRequestMethod("POST");
//			conn.setConnectTimeout(connectTimeout);
//			conn.setReadTimeout(readTimeout);
//		    
//			conn.setHostnameVerifier(new HostnameVerifier() {
//				public boolean verify(String host, SSLSession sess) {
//					if (host.equals("localhost"))
//						return true;
//					else
//						return false;
//				}
//			});
//
//			conn.setRequestProperty("Accept", "*/*");
//			conn.setRequestProperty("Accept-Encoding", "gzip, deflate, br");
//			conn.setRequestProperty("Accept-Language", "en-US,en;q=0.9");
//			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
//			conn.setRequestProperty("Cookie", "PHPSESSID=" + sessionId);
//			conn.setRequestProperty("Host", host);
//			conn.setRequestProperty("Origin", origin);
//			conn.setRequestProperty("Referer", refererCekRoya);
//			conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
//			
//	        conn.setDoInput(true);
//			conn.setDoOutput(true);
//		    writer = new OutputStreamWriter(conn.getOutputStream());
//
//		    writer.write(postData);
//		    writer.flush();
//		    
//		    int responseCode = conn.getResponseCode();
//		    String responseMessage = conn.getResponseMessage();
//			long endTimestamp = System.currentTimeMillis();
//		    
//		    if(responseCode == HttpURLConnection.HTTP_OK) {
//			    reader = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
//		        String line;
//		        StringBuilder sb2 = new StringBuilder();
//		        while ((line = reader.readLine()) != null) { 
//		        	sb2.append(line);
//		        }
//		        
//		        reader.close();
//		        
//		        String responsePayload = sb2.toString();
//		        
//				String[] respTokens = responsePayload.split("\\^");
//				if("A1".equals(respTokens[0])) {
//					log.debug("cek roya 2 SUCCESS {} in {}ms", ppkNomor, (endTimestamp-startTimestamp));
//
//				    writer.close();
//				    reader.close();
//					return true;
//		        } else {
//					log.debug("cek roya 2 FAILED {} {} {}ms", ppkNomor, responsePayload, (endTimestamp-startTimestamp));
//					log.debug(postData);
//				}
//		    } else {
//				log.debug("cek roya 2 FAILED {} {}-{} {}ms", ppkNomor, responseCode, responseMessage, (endTimestamp-startTimestamp));
//				log.debug(postData);
//			}
//		    
//		    writer.close();
//		    reader.close();
//			
//			return false;
//		} catch (Exception e) {
//        	log.error("Error on cek roya " + ppkNomor, e);
//			log.debug(postData);
//
//			if(writer != null)
//				try {
//					writer.close();
//				} catch (Exception e2) { }
//
//			if(reader != null)
//				try {
//					reader.close();
//				} catch (IOException e2) { }
//			
//			throw e;
//		}
//	}
}
