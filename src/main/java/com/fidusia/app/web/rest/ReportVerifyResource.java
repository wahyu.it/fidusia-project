package com.fidusia.app.web.rest;

import io.github.jhipster.web.util.HeaderUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import com.fidusia.app.domain.ReportVerify;
import com.fidusia.app.repository.ReportVerifyRepository;
import com.fidusia.app.repository.UserRepository;
import com.fidusia.app.service.dto.ReportVerifyDTO;
import com.fidusia.app.service.mapper.ReportVerifyMapper;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class ReportVerifyResource {

    private final Logger log = LoggerFactory.getLogger(ReportVerifyResource.class);

    private final ReportVerifyRepository reportVerifyRepository;

    private final ReportVerifyMapper reportVerifyMapper;

    private final UserRepository userRepository;
    
    public ReportVerifyResource(ReportVerifyRepository reportVerifyRepository, ReportVerifyMapper reportVerifyMapper, UserRepository userRepository) {
    	this.reportVerifyRepository = reportVerifyRepository;
    	this.reportVerifyMapper = reportVerifyMapper;
    	this.userRepository = userRepository;
    }


    /**
     * POST  /reportverify : Create a new reportVerify.
     *
     * @param reportVerifyDTO the reportVerifyDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new reportVerifyDTO, or with status 400 (Bad Request) if the reportVerify has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/reportverify")
    public ResponseEntity<ReportVerifyDTO> createReportVerify(@Valid @RequestBody ReportVerifyDTO reportVerifyDTO) throws URISyntaxException {
        log.debug("REST request to save ReportVerify : {}", reportVerifyDTO);
        
        if (reportVerifyDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("reportVerify", false, "idexists", "A new reportVerify cannot already have an ID", null)).body(null);
        }
        
        ReportVerify reportVerify = reportVerifyMapper.reportVerifyDTOToReportVerify(reportVerifyDTO);
        reportVerify = reportVerifyRepository.save(reportVerify);
        ReportVerifyDTO result = reportVerifyMapper.reportVerifyToReportVerifyDTO(reportVerify);
        return ResponseEntity.created(new URI("/api/reportverify/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("reportVerify", false, result.getId().toString(), null))
            .body(result);
    }

    /**
     * PUT  /reportverify : Updates an existing reportVerify.
     *
     * @param reportVerifyDTO the reportVerifyDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated reportVerifyDTO,
     * or with status 400 (Bad Request) if the reportVerifyDTO is not valid,
     * or with status 500 (Internal Server Error) if the reportVerifyDTO couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/reportverify")
    public ResponseEntity<ReportVerifyDTO> updateReportVerify(@Valid @RequestBody ReportVerifyDTO reportVerifyDTO) throws URISyntaxException {
        log.debug("REST request to update ReportVerify : {}", reportVerifyDTO);
    	
        if (reportVerifyDTO.getId() == null) {
            return createReportVerify(reportVerifyDTO);
        }
        ReportVerify reportVerify = reportVerifyMapper.reportVerifyDTOToReportVerify(reportVerifyDTO);
        reportVerify = reportVerifyRepository.save(reportVerify);
        ReportVerifyDTO result = reportVerifyMapper.reportVerifyToReportVerifyDTO(reportVerify);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("reportVerify", false, reportVerifyDTO.getId().toString(), null))
            .body(result);
    }

    /**
     * GET  /reportverify : get all the reportverify.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of reportverify in body
     */
    @GetMapping("/reportverify")
    public List<ReportVerifyDTO> getAllReportVerifys() {
        log.debug("REST request to get all ReportVerifys");
    	
        List<ReportVerify> reportVerifies = reportVerifyRepository.findAll();
        return reportVerifyMapper.reportVerifiesToReportVerifyDTOs(reportVerifies);
    }

    /**
     * GET  /reportverify/:id : get the "id" reportVerify.
     *
     * @param id the id of the reportVerifyDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the reportVerifyDTO, or with status 404 (Not Found)
     */
    @GetMapping("/reportverify/{id}")
    public ResponseEntity<ReportVerifyDTO> getReportVerify(@PathVariable Long id) {
        log.debug("REST request to get ReportVerify : {}", id);
    	
        ReportVerify reportVerify = reportVerifyRepository.findById(id).get();
        ReportVerifyDTO reportVerifyDTO = reportVerifyMapper.reportVerifyToReportVerifyDTO(reportVerify);
        return Optional.ofNullable(reportVerifyDTO)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /reportverify/:id : delete the "id" reportVerify.
     *
     * @param id the id of the reportVerifyDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/reportverify/{id}")
    public ResponseEntity<Void> deleteReportVerify(@PathVariable Long id) {
        log.debug("REST request to delete ReportVerify : {}", id);
    	
        reportVerifyRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("reportVerify", false, id.toString(), null)).build();
    }
}
