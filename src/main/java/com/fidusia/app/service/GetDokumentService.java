package com.fidusia.app.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.fidusia.app.config.ApplicationProperties;
import com.fidusia.app.domain.Akta;
import com.fidusia.app.domain.Berkas;
import com.fidusia.app.domain.util.BerkasStatus;
import com.fidusia.app.domain.util.NasabahJenis;
import com.fidusia.app.domain.util.ResponseStatus;
import com.fidusia.app.repository.ApplicationConfigRepository;
import com.fidusia.app.repository.BerkasRepository;
import com.fidusia.app.service.dto.JobParameter;
import com.fidusia.app.service.dto.ResponseBerkasDTO;
import com.fidusia.app.service.dto.ResponseGetDataDTO;
import com.fidusia.app.util.ExceptionUtils;
import com.fidusia.app.util.LogUtils;
import com.fidusia.app.web.rest.util.CommonResponse;

@Service
public class GetDokumentService {

    private final static Logger log = LoggerFactory.getLogger(GetDokumentService.class);

    private final ApplicationConfigRepository applicationConfigRepository;
    
    private final ApplicationProperties  applicationProperties;

    private final BerkasRepository berkasRepository;

    private final RawDataService rawDataService;

    private final Base64Service base64Service;
	
    private final JobInstanceService jobInstanceService;
    
    private final SlackInhookService slackInhookService;

    private final static int connectTimeout = 60000 ;
    
    private final static int readTimeout = 60000 ;
	
	public GetDokumentService(SlackInhookService slackInhookService, JobInstanceService jobInstanceService, Base64Service base64Service, ApplicationProperties  applicationProperties, BerkasRepository berkasRepository, ApplicationConfigRepository applicationConfigRepository, RawDataService rawDataService) {
		this.applicationConfigRepository = applicationConfigRepository;
		this.rawDataService = rawDataService;
		this.berkasRepository = berkasRepository;
		this.applicationProperties = applicationProperties;
		this.base64Service = base64Service;
		this.jobInstanceService = jobInstanceService;
		this.slackInhookService = slackInhookService;
	}
	
    public void getDokumenAsync(JobParameter parameters) throws Exception {
		String tglorder = parameters.get("date");
		String ppknomor = parameters.get("ppknomor");
		LocalDate date = tglorder != null ? LocalDate.parse(tglorder) : LocalDate.now();
		
        String service = "Get Dokument";
        String parameter = "date:" + date;
        CommonResponse response = new CommonResponse(service, parameter);
        
        try {
        	StringBuilder sb = new StringBuilder();

        	String token = rawDataService.requestToken();
        	
        	if (ppknomor != null) {
    			getDokument(ppknomor, token);
    		} else if (date != null) {
    			List<Berkas> berkas = berkasRepository.findByBerkasStatus(BerkasStatus.NONE.value());
    			
    			for (Berkas berkas2 : berkas) {
    				if (berkas2.getBerkasStatus() != BerkasStatus.PARTIAL.value())
    					getDokument(berkas2.getPpkNomor(), token);
				}
            	
        		sb.append("\n* Get Dokumen Job").append(" *");
//        		sb.append("\ntotal        ").append(aktaList.size() > 0 ? aktaList.size() : 0);
//        		sb.append("\nsuccess      ").append(count > 0 ? count : 0);
//        		sb.append("\npending      ").append(aktaList.size()-count);
    		}

            LogUtils.debug(response = response.complete(ResponseStatus.COMPLETED.toString(), sb.toString()), log, slackInhookService);
            jobInstanceService.updateStatus(parameters.get("jobid"), "SUCCESS", parameters.toString(), "");
		} catch (Exception e) {
            e.printStackTrace();
            log.error(ExceptionUtils.getMessage(e));
            
            LogUtils.debug(response = response.complete(ResponseStatus.FAILED.toString(), ExceptionUtils.getMessage(e), ExceptionUtils.getStackTrace(e)), log, slackInhookService);
            jobInstanceService.updateStatus(parameters.get("jobid"), "ERROR", parameters.toString(), ExceptionUtils.getStackTrace(e));
            return;
		}
    }
	
	public String getDokument(String ppknomor, String token) {
    	log.debug("Start service Request dokumen ppknomor {}", ppknomor);
    	OutputStreamWriter writer = null;
    	ResponseGetDataDTO response = null;
		BufferedReader reader = null;
    	
		try {
			
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("agreementNo", ppknomor);
            
            String postData = jsonObject.toString();
			
    		String url_mtf = applicationConfigRepository.findByCategoryAndName("APPLICATION_PROPERTIES", "URL_MTF").getValue();

    		SSLContext ssl_ctx = SSLContext.getInstance("TLS");
			TrustManager[] trust_mgr = get_trust_mgr();
			ssl_ctx.init(null, trust_mgr, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());
			
//			String token = rawDataService.requestToken();
			
			URL url = new URL(url_mtf + "api/agreements/document");
//			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection(); TODO
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty("Accept", "*/*");
			conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Authorization","Bearer " + token);
			conn.setConnectTimeout(connectTimeout);
			conn.setReadTimeout(readTimeout);
	        conn.setDoInput(true);
			conn.setDoOutput(true);
			
			writer = new OutputStreamWriter(conn.getOutputStream());
            writer.write(postData);
            writer.flush();
			
			int responseCode = conn.getResponseCode();
			
			if(responseCode == HttpStatus.OK.value()) {

				reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				
				String line;
				StringBuffer sb = new StringBuffer();
				while ((line = reader.readLine()) != null) {
					sb.append(line);
				}

				JSONObject result = new JSONObject(sb.toString());
				
                if ("success".equals(result.getString("status"))) {
//    					log.debug(result.getJSONArray("docs").toString());

    					List<ResponseBerkasDTO> dataArray = parseResponseBerkas(result.getJSONArray("docs"), ppknomor);
    					
    					insertBerkas(dataArray, ppknomor);
				} else {
					
				}
			}
			
		} catch (Exception e) {
            log.error("Failed service Get data token Job caused by {}", e.getMessage(), e);
		} finally {
			if(writer != null)
				try {
					writer.close();
				} catch (Exception e) { }
	
			if(reader != null)
				try {
					reader.close();
				} catch (IOException e) { }
		}
    	
    	return null;
	}
	
	public Berkas insertBerkas(List<ResponseBerkasDTO> arrayBerkas, String ppkNomor) {
        String baseDirectory = applicationProperties.getDomain().getBaseDirectory() + applicationProperties.getDirectory().getBerkas();
        String directory = baseDirectory;
        File basedir = new File(directory + LocalDate.now());
        if(!basedir.exists())
            basedir.mkdir();
		
		Berkas berkas = berkasRepository.findByPpkNomor(ppkNomor);
		
		for (ResponseBerkasDTO responseBerkasDTO : arrayBerkas) {
			
			if ("DOCKONTRAK".equals(responseBerkasDTO.getTypeDoc())) {
				try {
					String filePath = base64Service.decodedFile(responseBerkasDTO.getFileContent(), directory, LocalDate.now() + "/"+ ppkNomor + "-ppk." + responseBerkasDTO.getFileExt());
					berkas.setDocKontrak(filePath);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if ("KTPBPKB".equals(responseBerkasDTO.getTypeDoc())) {
				try {
					String filePath = base64Service.decodedFile(responseBerkasDTO.getFileContent(), directory, LocalDate.now() + "/"+ ppkNomor + "-ktpb." + responseBerkasDTO.getFileExt());
					berkas.setKtpBpkb(filePath);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if ("KTPP".equals(responseBerkasDTO.getTypeDoc())) {
				try {
					String filePath = base64Service.decodedFile(responseBerkasDTO.getFileContent(), directory, LocalDate.now() + "/"+ ppkNomor + "-ktpp." + responseBerkasDTO.getFileExt());
					berkas.setKtpp(filePath);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if ("SKPJF".equals(responseBerkasDTO.getTypeDoc())) {
				try {
					String filePath = base64Service.decodedFile(responseBerkasDTO.getFileContent(), directory, LocalDate.now() + "/"+ ppkNomor + "-skn." + responseBerkasDTO.getFileExt());
					berkas.setSkpjf(filePath);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if ("KTPDP".equals(responseBerkasDTO.getTypeDoc())) {
				try {
					String filePath = base64Service.decodedFile(responseBerkasDTO.getFileContent(), directory, LocalDate.now() + "/"+ ppkNomor + "-ktpp." + responseBerkasDTO.getFileExt());
					berkas.setKtpdp(filePath);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if ("NPWP".equals(responseBerkasDTO.getTypeDoc())) {
				try {
					String filePath = base64Service.decodedFile(responseBerkasDTO.getFileContent(), directory, LocalDate.now() + "/"+ ppkNomor + "-npwp." + responseBerkasDTO.getFileExt());
					berkas.setNpwp(filePath);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if ("KTPDIRUT".equals(responseBerkasDTO.getTypeDoc())) {
				try {
					String filePath = base64Service.decodedFile(responseBerkasDTO.getFileContent(), directory, LocalDate.now() + "/"+ ppkNomor + "-ktpdirut." + responseBerkasDTO.getFileExt());
					berkas.setKtpDirut(filePath);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if ("TDP".equals(responseBerkasDTO.getTypeDoc())) {
				try {
					String filePath = base64Service.decodedFile(responseBerkasDTO.getFileContent(), directory, LocalDate.now() + "/"+ ppkNomor + "-tdp." + responseBerkasDTO.getFileExt());
					berkas.setTdp(filePath);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if ("SITU".equals(responseBerkasDTO.getTypeDoc())) {
				try {
					String filePath = base64Service.decodedFile(responseBerkasDTO.getFileContent(), directory, LocalDate.now() + "/"+ ppkNomor + "-situ." + responseBerkasDTO.getFileExt());
					berkas.setSitu(filePath);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if ("AKTAPEND".equals(responseBerkasDTO.getTypeDoc())) {
				try {
					String filePath = base64Service.decodedFile(responseBerkasDTO.getFileContent(), directory, LocalDate.now() + "/"+ ppkNomor + "-aktapend." + responseBerkasDTO.getFileExt());
					berkas.setAktaPend(filePath);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if ("AKTAPER".equals(responseBerkasDTO.getTypeDoc())) {
				try {
					String filePath = base64Service.decodedFile(responseBerkasDTO.getFileContent(), directory, LocalDate.now() + "/"+ ppkNomor + "-aktaper." + responseBerkasDTO.getFileExt());
					berkas.setAktaPerub(filePath);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if ("SIUP".equals(responseBerkasDTO.getTypeDoc())) {
				try {
					String filePath = base64Service.decodedFile(responseBerkasDTO.getFileContent(), directory, LocalDate.now() + "/"+ ppkNomor + "-siup." + responseBerkasDTO.getFileExt());
					berkas.setSiup(filePath);
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if ("NIB".equals(responseBerkasDTO.getTypeDoc())) {
				try {
					String filePath = base64Service.decodedFile(responseBerkasDTO.getFileContent(), directory, LocalDate.now() + "/"+ ppkNomor + "-nib." + responseBerkasDTO.getFileExt());
					berkas.setNib(filePath);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		if (berkas.getNsbJenis() == NasabahJenis.PERORANGAN.value()) {
			if (berkas.getKtpBpkb() != null && (berkas.getKtpp() != null || berkas.getKtpdp() !=null) && berkas.getDocKontrak() != null && berkas.getSkpjf() != null) {
				berkas.setBerkasStatus(BerkasStatus.PARTIAL.value());
			}
		} else {
			if (berkas.getKtpBpkb() != null
					&& berkas.getNpwp() != null 
					&& berkas.getKtpDirut() != null
					&& berkas.getAktaPend() != null 
					&& berkas.getSitu() != null 
					&& berkas.getDocKontrak() != null 
					&& berkas.getSkpjf() != null
					&& berkas.getSitu() != null 
					&& berkas.getTdp() != null
					&& berkas.getAktaPerub() != null
					&& berkas.getSiup() != null
					&& berkas.getNib() != null) {
				berkas.setBerkasStatus(BerkasStatus.PARTIAL.value());
			}
		}
		berkas.setUpdateOn(ZonedDateTime.now());
		berkas.setUpdateBy("BACKEND");
		berkasRepository.save(berkas);
		
		return null;
	}
	

    private static List<ResponseBerkasDTO> parseResponseBerkas(JSONArray jsonArray, String ppknomor) throws Exception  {
    	List<ResponseBerkasDTO> arrayResponse = new ArrayList<ResponseBerkasDTO>();
		
			if(jsonArray != null && jsonArray.length() > 0) {
			 try {
				 for (int i=0; i<jsonArray.length(); i++) {
					 ResponseBerkasDTO berkas = new ResponseBerkasDTO();
					 JSONArray jsonArray1 = jsonArray.getJSONArray(i);

					 for (int j=0; j<jsonArray1.length(); j++) {
						 	if (j == 0) {
								 JSONObject ob = jsonArray1.getJSONObject(j);
								 berkas.setPpkNomor(ppknomor);
								 berkas.setTypeDoc(ob.getString("documentType"));
						 	} else {
								 JSONArray jsonArray2 = jsonArray1.getJSONArray(j);
								 for (int k=0; k<jsonArray2.length(); k++) {
									 JSONObject ob1 = jsonArray2.getJSONObject(k);
									 berkas.setFileContent(ob1.getString("fileContent"));  
									 berkas.setFileExt(ob1.getString("fileExtionsion"));
									 	
									 arrayResponse.add(berkas);
								 }
						 	}
					 }
				 }
			} catch (Exception e) {
				throw e;
			}
		}
		return arrayResponse;
    }

	private TrustManager[] get_trust_mgr() {
		TrustManager[] certs = new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String t) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String t) {
			}
		} };
		return certs;
	}

}
