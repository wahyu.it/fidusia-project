package com.fidusia.app.service.dto;

public class ResponseGetTokenDTO {
	
    private String status;
    private String message;
    private String token;
    
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
	@Override
	public String toString() {
		return "ResponseDTO [status=" + status + ", message=" + message + ", token=" + token + "]";
	}
}
