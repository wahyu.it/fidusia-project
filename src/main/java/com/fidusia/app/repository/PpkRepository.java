package com.fidusia.app.repository;

import com.fidusia.app.domain.Ppk;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Ppk entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PpkRepository extends JpaRepository<Ppk, Long> {

	Ppk findByNomor(String ppkNomor);}
