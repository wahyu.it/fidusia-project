import { Component, OnInit, ElementRef  } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { UploadBerkasService } from './uploadberkas.service';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'jhi-berkas-dialog',
  templateUrl: './berkas-dialog.component.html',
  styleUrls: ['uploadberkas.scss']
})
export class BerkasDialogComponent implements OnInit  {
	url !: any;
	filename !: string;
	image !: any;
	ext !: any;
	blob !: any;

  constructor(
	  public activeModal: NgbActiveModal, 
	  private eventManager: JhiEventManager,
      protected uploadBerkasService: UploadBerkasService,
      private sanitizer: DomSanitizer,
	  private playerRef: ElementRef
    ) {}

  clear(): void {
    this.activeModal.dismiss();
  }

  ngOnInit(): void {
	 this.uploadBerkasService.viewBerkas2(this.filename).subscribe(res => {
	  this.ext = this.filename.split(".")[1];
	  	 try{
            this.blob = new Blob([res], { type: "image/"+ this.ext});
            const imageUrl = window.URL.createObjectURL(this.blob);
            
   		   this.image = this.sanitizer.bypassSecurityTrustResourceUrl(imageUrl);
   		   
         } catch (ex) {
             console.log(ex);
         }
	 });
	  
  }
}
