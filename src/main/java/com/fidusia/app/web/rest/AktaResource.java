package com.fidusia.app.web.rest;

import com.fidusia.app.domain.Akta;
import com.fidusia.app.domain.Notaris;
import com.fidusia.app.repository.AktaRepository;
import com.fidusia.app.security.AuthoritiesConstants;
import com.fidusia.app.security.SecurityUtils;
import com.fidusia.app.service.CabangService;
import com.fidusia.app.service.dto.AktaDTO;
import com.fidusia.app.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing {@link com.fidusia.app.domain.Akta}.
 */
@RestController
@RequestMapping("/api/aktas")
@Transactional
public class AktaResource {

    private final Logger log = LoggerFactory.getLogger(AktaResource.class);

    private static final String ENTITY_NAME = "akta";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AktaRepository aktaRepository;

    private final CabangService cabangService;

    public AktaResource(AktaRepository aktaRepository, CabangService cabangService) {
        this.aktaRepository = aktaRepository;
        this.cabangService = cabangService;    
    }

    /**
     * {@code POST  /aktas} : Create a new akta.
     *
     * @param akta the akta to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new akta, or with status {@code 400 (Bad Request)} if the akta has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<Akta> createAkta(@RequestBody Akta akta) throws URISyntaxException {
        log.debug("REST request to save Akta : {}", akta);
        if (akta.getId() != null) {
            throw new BadRequestAlertException("A new akta cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Akta result = aktaRepository.save(akta);
        return ResponseEntity
            .created(new URI("/api/aktas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /aktas/:id} : Updates an existing akta.
     *
     * @param id the id of the akta to save.
     * @param akta the akta to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated akta,
     * or with status {@code 400 (Bad Request)} if the akta is not valid,
     * or with status {@code 500 (Internal Server Error)} if the akta couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<Akta> updateAkta(@PathVariable(value = "id", required = false) final Long id, @RequestBody Akta akta)
        throws URISyntaxException {
        log.debug("REST request to update Akta : {}, {}", id, akta);
        if (akta.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, akta.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!aktaRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Akta result = aktaRepository.save(akta);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, akta.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /aktas/:id} : Partial updates given fields of an existing akta, field will ignore if it is null
     *
     * @param id the id of the akta to save.
     * @param akta the akta to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated akta,
     * or with status {@code 400 (Bad Request)} if the akta is not valid,
     * or with status {@code 404 (Not Found)} if the akta is not found,
     * or with status {@code 500 (Internal Server Error)} if the akta couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Akta> partialUpdateAkta(@PathVariable(value = "id", required = false) final Long id, @RequestBody Akta akta)
        throws URISyntaxException {
        log.debug("REST request to partial update Akta partially : {}, {}", id, akta);
        if (akta.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, akta.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!aktaRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Akta> result = aktaRepository
            .findById(akta.getId())
            .map(existingAkta -> {
                if (akta.getTglOrder() != null) {
                    existingAkta.setTglOrder(akta.getTglOrder());
                }
                if (akta.getOrderType() != null) {
                    existingAkta.setOrderType(akta.getOrderType());
                }
                if (akta.getKode() != null) {
                    existingAkta.setKode(akta.getKode());
                }
                if (akta.getNomor() != null) {
                    existingAkta.setNomor(akta.getNomor());
                }
                if (akta.getTanggal() != null) {
                    existingAkta.setTanggal(akta.getTanggal());
                }
                if (akta.getPukulAwal() != null) {
                    existingAkta.setPukulAwal(akta.getPukulAwal());
                }
                if (akta.getPukulAkhir() != null) {
                    existingAkta.setPukulAkhir(akta.getPukulAkhir());
                }
                if (akta.getStatus() != null) {
                    existingAkta.setStatus(akta.getStatus());
                }
                if (akta.getUpdateBy() != null) {
                    existingAkta.setUpdateBy(akta.getUpdateBy());
                }
                if (akta.getUpdateOn() != null) {
                    existingAkta.setUpdateOn(akta.getUpdateOn());
                }

                return existingAkta;
            })
            .map(aktaRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, akta.getId().toString())
        );
    }

    /**
     * {@code GET  /aktas} : get all the aktas.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of aktas in body.
     */
    @GetMapping("")
    public ResponseEntity<List<Akta>> getAllAktas(Pageable pageable) {
        log.debug("REST request to get a page of Aktas");
        Page<Akta> page = aktaRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /aktas/:id} : get the "id" akta.
     *
     * @param id the id of the akta to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the akta, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<Akta> getAkta(@PathVariable("id") Long id) {
        log.debug("REST request to get Akta : {}", id);
        Optional<Akta> akta = aktaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(akta);
    }

    /**
     * {@code DELETE  /aktas/:id} : delete the "id" akta.
     *
     * @param id the id of the akta to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteAkta(@PathVariable("id") Long id) {
        log.debug("REST request to delete Akta : {}", id);
        aktaRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }

    @GetMapping("/search")
    public ResponseEntity<List<AktaDTO>> search(
    		@RequestParam(value = "skey", required = false) String skey,
    		@RequestParam(value = "svalue", required = false) String svalue,
    		@ApiParam Pageable pageable) throws URISyntaxException {
        log.debug("REST request to search akta page by {} : {} {}", skey, svalue);
      
        Page<AktaDTO> page = null;
        
        boolean isPartner = SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.PARTNER);
        
        Notaris notaris = null;
        if(isPartner) {
        	notaris = cabangService.findNotarisByUserLogin();
        }

        if("ppkNomor".equals(skey) && svalue != null) {
        	 if (isPartner) {
   	          	page = aktaRepository.findByPpkNomorAndNotarisId(svalue, notaris.getId(), pageable);
        	 } else {
   	          	page = aktaRepository.findByPpkNomor(svalue, pageable);
        	 }
        } else if("date".equals(skey) && svalue != null) {
            LocalDate ppkTanggalDt = LocalDate.parse(svalue);
       	 	if (isPartner) {
                page = aktaRepository.findByTglOrder(ppkTanggalDt, pageable);
       	 	} else {
                page = aktaRepository.findByTglOrderAndNotarisId(ppkTanggalDt, notaris.getId(), pageable);
       	 	}
        } else if("month".equals(skey) && svalue != null) {
            LocalDate startDate = LocalDate.parse(svalue + "-01");
            LocalDate endDate = startDate.withDayOfMonth(startDate.lengthOfMonth());
       	 	if (isPartner) {
                page = aktaRepository.findByTglOrderBetweenAndNotarisId(startDate, endDate, notaris.getId(), pageable);
       	 	} else {
                page = aktaRepository.findByTglOrderBetween(startDate, endDate, pageable);
       	 	}
        } else {
            LocalDate startDate = LocalDate.now().withDayOfMonth(1);
            LocalDate endDate = startDate.withDayOfMonth(startDate.lengthOfMonth());
       	 	if (isPartner) {
                page = aktaRepository.findByTglOrderBetweenAndNotarisId(startDate, endDate, notaris.getId(), pageable);
       	 	} else {
       	 		page = aktaRepository.findByTglOrderBetween(startDate, endDate, pageable);
       	 	}
        }
        
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(),page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
