package com.fidusia.app.repository;

import com.fidusia.app.domain.Cabang;
import com.fidusia.app.service.dto.KeyValueDTO;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Cabang entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CabangRepository extends JpaRepository<Cabang, Long> {

	Cabang findOneByNama(String scabang);

	Cabang findByKode(String kodeCabang);

	List<Cabang> findByRecordStatus(int value);
	
	static final String CABANG_DTO  = "select new com.fidusia.app.service.dto.KeyValueDTO(c.id, c.nama) from Cabang c";

	@Query(CABANG_DTO + " WHERE c.recordStatus=:recordStatus")
	List<KeyValueDTO> findByRecordStatus2(@Param("recordStatus") int recordStatus);
	
}
