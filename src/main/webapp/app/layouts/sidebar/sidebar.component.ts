import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import PerfectScrollbar from 'perfect-scrollbar';
import 'bootstrap';

import { LoginService, AccountService, MenuService } from 'app/core';
import { Account } from 'app/core/user/account.model';

@Component({
  selector: 'jhi-sidebar',
  templateUrl: 'sidebar.component.html'
})
export class SidebarComponent implements OnInit {
  public menuItems!: any[];
  account: Account | null = null;
  sidebarVisible: boolean | undefined;
  toggleButton: any;
  mobileMenuVisible: any = 0;

  constructor(
    private loginService: LoginService,
    private accountService: AccountService,
    private router: Router,
    private menuService: MenuService
  ) {}

  isMobileMenu(): any {
    if (window.innerWidth > 991) {
      return false;
    }
    return true;
  }

  ngOnInit(): void {
    this.menuItems = this.menuService.getMenuItems();
    this.accountService.getAuthenticationState().subscribe(account => (this.account = account));
  }

  updatePS(): void {
    if (window.matchMedia(`(min-width: 960px)`).matches) {
      const elemSidebar = document.querySelector('.sidebar .sidebar-wrapper') as HTMLElement;
      const ps = new PerfectScrollbar(elemSidebar, { wheelSpeed: 1, suppressScrollX: true });
      console.log(ps);
    }
  }

  isMac(): boolean {
    let bool = false; //

    if (navigator.platform.toUpperCase().indexOf('MAC') ? true : false || navigator.platform.toUpperCase().indexOf('IPAD') ? true : false) {
      bool = true;
    }
    return bool;
  }

  logout(): void {
    this.loginService.logout();
    this.router.navigate(['']);
  }

  getImageUrl(): any {
    return this.accountService.getImageUrl();
  }

  getAccountName(): any {
    return this.accountService.getName();
  }
}
