package com.fidusia.app.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.fidusia.app.domain.ReportVerify;
import com.fidusia.app.domain.User;
import com.fidusia.app.service.dto.ReportVerifyDTO;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;


public interface ReportVerifyRepository extends JpaRepository<ReportVerify,Long>, JpaSpecificationExecutor<ReportVerify> {

    Page<ReportVerify> findByTanggalBetween(LocalDate tanggal1, LocalDate tanggal2, Pageable pageable);
    
    Page<ReportVerify> findByTanggal(LocalDate tanggal1, Pageable pageable);
    
    Page<ReportVerify> findByTanggalAndUser(LocalDate tanggal1, User user, Pageable pageable);

    Page<ReportVerify> findByTanggalBetweenAndUser(LocalDate tanggal1, LocalDate tanggal2, User user, Pageable pageable);

}
