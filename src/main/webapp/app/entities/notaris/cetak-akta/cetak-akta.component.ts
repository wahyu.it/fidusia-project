import { Component, OnInit } from '@angular/core';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { HttpResponse, HttpHeaders } from '@angular/common/http';
import { JhiEventManager } from 'ng-jhipster';
import { Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
import { FormControl, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';

import { CetakAktaService } from './cetak-akta.service';
import { IAktaDTO } from 'app/shared/model/aktaDTO.model';

@Component({
  selector: 'jhi-cetakakta',
  templateUrl: './cetak-akta.component.html',
  styleUrls: ['cetak-akta.scss']
})
export class CetakAktaComponent implements OnInit {
  aktas?: IAktaDTO[] |any;
  date = '';
  month = '';
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  totalItems = 0;
  eventSubscriber?: Subscription;
  sPpd!: boolean;
  key!: string;
  value!: string;

  private dateFormat = 'yyyy-MM-dd';
  private mothFormat = 'yyyy-MM';
  
  editForm = new FormGroup({
    ppd: new FormControl(''),
    date: new FormControl(''),
    month: new FormControl(''),
    ppknomor: new FormControl('')
  });
  
  ppkGroup = new FormGroup({
    ppknomor: new FormControl('')
  });

  constructor(
    protected eventManager: JhiEventManager,
    protected activatedRoute: ActivatedRoute,
    protected cetakAktaService: CetakAktaService,
    private datePipe: DatePipe,
    protected router: Router,
	) {
		this.sPpd = true;	
	}

  ngOnInit(): void {
		this.month = this.sMonth();
		this.date = this.sDate();
	   this.activatedRoute.data.subscribe(data => {
	      this.page = data.pagingParams.page;
	      this.ascending = data.pagingParams.ascending;
	      this.predicate = data.pagingParams.predicate;
	      this.ngbPaginationPage = data.pagingParams.page;
	     /* this.loadPage();*/
	    });
    this.registerChangeInLogBook();
    this.loadData();
  }
  
  onChange(e: any): void  {
	  console.log("ppd:" + e);
	  if (e === 'tgl'){
		  this.sPpd = true;
		  this.key = 'date';
	  } else {
		 this.sPpd = false;
		 this.key = 'month';
	  }
	  console.log("this.sPpd:" + this.sPpd);
  }
  
  searchppk(ppkNomor: string): void {
	  let ppkNo = this.ppkGroup.get(['ppknomor'])!.value;;
	  if (this.key === 'ppkNomor') {
		  ppkNo = this.ppkGroup.get(['ppknomor'])!.value;
	  }
      this.searchByParam(ppkNomor, ppkNo);
  }

  searchPpd(key: string): void {
	  console.log(this.key + " " + key)
	  if (key === 'date') {
		  this.value = this.date;
	  } else {
		  this.value = this.month;
	  	  console.log("this.month : " +this.month)
	  } 
	  
      this.searchByParam(this.key, this.value);
  }
  
  viewMinuta(ppkNomor: string): void {
	/* this.cetakAktaService.viewMinuta(ppkNomor).subscribe(res => {
      const fileURL = URL.createObjectURL(res);
      
      window.open(fileURL, '_blank');
    });*/
      window.open('api/file/minuta/'+ ppkNomor + '-minuta.pdf', '_blank');
  }
  
  viewSalinan(ppkNomor: string): void {
	/* this.cetakAktaService.viewSalinan(ppkNomor).subscribe(res => {
      const fileURL = URL.createObjectURL(res);
      
      window.open(fileURL, '_blank');
    });*/
      window.open('api/file/salinan/'+ ppkNomor + '-salinan.pdf', '_blank');
  }
  
  searchByParam(key: string, value: string): void  {
	  this.key = key;
	  this.value = value;
	  console.log("searchByParam key : " + this.key + " value: " + this.value)
      this.router.navigate(['/notaris/cetak-akta'], {
        queryParams: {
          skey: this.key,
          svalue: this.value,
          page: this.page,
          sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
        }
      });
      this.loadData();
  }

  transition(): void {
      this.router.navigate(['/notaris/cetak-akta'], {
        queryParams: {
          page: this.page,
          sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
        }
      });
      this.loadData();
  }

  loadData(page?: number): void {
    const pageToLoad: number = page ? page : this.page;
    this.cetakAktaService
      .search({
        skey: this.key,
        svalue: this.value,
        page: this.page - 1,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe((res: HttpResponse<IAktaDTO[]>) => this.onSuccess(res.body, res.headers, pageToLoad));
  }
  
  cleardate(): void {
    this.editForm.patchValue({
      date: '',
      month:''
    });
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  registerChangeInLogBook(): void {
    this.eventSubscriber = this.eventManager.subscribe('reportListModification', () => this.loadData());
  }

  protected onSuccess(data: IAktaDTO[] | null, headers: HttpHeaders, page: number): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/notaris/cetak-akta'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.aktas = data;
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }

  private sDate(): string {
    const date = new Date();
	date.setDate(1);
    return this.datePipe.transform(date, this.dateFormat)!;
  }

  private sMonth(): string {
    const date = new Date();
    const result = this.datePipe.transform(date, this.mothFormat)!;
    console.log("result :" +result)
    return result
  }
}
