import { Routes } from '@angular/router';
import { UploadWarkahComponent } from './uploadwarkah.component';
import { JhiResolvePagingParams } from 'ng-jhipster';

export const uploadWarkahState: Routes = [{
  path: '',
  component: UploadWarkahComponent,
  resolve: {
	  pagingParams: JhiResolvePagingParams
  }
  }
];
