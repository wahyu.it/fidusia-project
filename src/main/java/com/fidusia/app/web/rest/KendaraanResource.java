package com.fidusia.app.web.rest;

import com.fidusia.app.domain.Kendaraan;
import com.fidusia.app.repository.KendaraanRepository;
import com.fidusia.app.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
/**
 * REST controller for managing {@link com.fidusia.app.domain.Kendaraan}.
 */
@RestController
@RequestMapping("/api/kendaraans")
@Transactional
public class KendaraanResource {

    private final Logger log = LoggerFactory.getLogger(KendaraanResource.class);

    private static final String ENTITY_NAME = "kendaraan";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final KendaraanRepository kendaraanRepository;

    public KendaraanResource(KendaraanRepository kendaraanRepository) {
        this.kendaraanRepository = kendaraanRepository;
    }

    /**
     * {@code POST  /kendaraans} : Create a new kendaraan.
     *
     * @param kendaraan the kendaraan to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new kendaraan, or with status {@code 400 (Bad Request)} if the kendaraan has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<Kendaraan> createKendaraan(@RequestBody Kendaraan kendaraan) throws URISyntaxException {
        log.debug("REST request to save Kendaraan : {}", kendaraan);
        if (kendaraan.getId() != null) {
            throw new BadRequestAlertException("A new kendaraan cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Kendaraan result = kendaraanRepository.save(kendaraan);
        return ResponseEntity
            .created(new URI("/api/kendaraans/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /kendaraans/:id} : Updates an existing kendaraan.
     *
     * @param id the id of the kendaraan to save.
     * @param kendaraan the kendaraan to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated kendaraan,
     * or with status {@code 400 (Bad Request)} if the kendaraan is not valid,
     * or with status {@code 500 (Internal Server Error)} if the kendaraan couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<Kendaraan> updateKendaraan(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Kendaraan kendaraan
    ) throws URISyntaxException {
        log.debug("REST request to update Kendaraan : {}, {}", id, kendaraan);
        if (kendaraan.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, kendaraan.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!kendaraanRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Kendaraan result = kendaraanRepository.save(kendaraan);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, kendaraan.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /kendaraans/:id} : Partial updates given fields of an existing kendaraan, field will ignore if it is null
     *
     * @param id the id of the kendaraan to save.
     * @param kendaraan the kendaraan to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated kendaraan,
     * or with status {@code 400 (Bad Request)} if the kendaraan is not valid,
     * or with status {@code 404 (Not Found)} if the kendaraan is not found,
     * or with status {@code 500 (Internal Server Error)} if the kendaraan couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Kendaraan> partialUpdateKendaraan(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Kendaraan kendaraan
    ) throws URISyntaxException {
        log.debug("REST request to partial update Kendaraan partially : {}, {}", id, kendaraan);
        if (kendaraan.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, kendaraan.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!kendaraanRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Kendaraan> result = kendaraanRepository
            .findById(kendaraan.getId())
            .map(existingKendaraan -> {
                if (kendaraan.getKondisi() != null) {
                    existingKendaraan.setKondisi(kendaraan.getKondisi());
                }
                if (kendaraan.getRoda() != null) {
                    existingKendaraan.setRoda(kendaraan.getRoda());
                }
                if (kendaraan.getMerk() != null) {
                    existingKendaraan.setMerk(kendaraan.getMerk());
                }
                if (kendaraan.getTipe() != null) {
                    existingKendaraan.setTipe(kendaraan.getTipe());
                }
                if (kendaraan.getWarna() != null) {
                    existingKendaraan.setWarna(kendaraan.getWarna());
                }
                if (kendaraan.getTahunPembuatan() != null) {
                    existingKendaraan.setTahunPembuatan(kendaraan.getTahunPembuatan());
                }
                if (kendaraan.getNoRangka() != null) {
                    existingKendaraan.setNoRangka(kendaraan.getNoRangka());
                }
                if (kendaraan.getNoMesin() != null) {
                    existingKendaraan.setNoMesin(kendaraan.getNoMesin());
                }
                if (kendaraan.getNoBpkb() != null) {
                    existingKendaraan.setNoBpkb(kendaraan.getNoBpkb());
                }
                if (kendaraan.getNilaiObjekJaminan() != null) {
                    existingKendaraan.setNilaiObjekJaminan(kendaraan.getNilaiObjekJaminan());
                }

                return existingKendaraan;
            })
            .map(kendaraanRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, kendaraan.getId().toString())
        );
    }

    /**
     * {@code GET  /kendaraans} : get all the kendaraans.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of kendaraans in body.
     */
    @GetMapping("")
    public ResponseEntity<List<Kendaraan>> getAllKendaraans(Pageable pageable) {
        log.debug("REST request to get a page of Kendaraans");
        Page<Kendaraan> page = kendaraanRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /kendaraans/:id} : get the "id" kendaraan.
     *
     * @param id the id of the kendaraan to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the kendaraan, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<Kendaraan> getKendaraan(@PathVariable("id") Long id) {
        log.debug("REST request to get Kendaraan : {}", id);
        Optional<Kendaraan> kendaraan = kendaraanRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(kendaraan);
    }

    /**
     * {@code DELETE  /kendaraans/:id} : delete the "id" kendaraan.
     *
     * @param id the id of the kendaraan to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteKendaraan(@PathVariable("id") Long id) {
        log.debug("REST request to delete Kendaraan : {}", id);
        kendaraanRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
    
    @GetMapping("/search")
    public ResponseEntity<List<Kendaraan>> searchKendaraan(Pageable pageable) {
        log.debug("REST request to get a page of Kendaraans valid");
        Page<Kendaraan> page = kendaraanRepository.findByValid(false, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
}
