import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasimonitoringSharedModule } from 'app/shared/shared.module';
import { RegistrasiFidusiaComponent } from './registrasifidusia.component';
import { registrasiFidusiaState } from './registrasifidusia.route';

@NgModule({
  imports: [SisteminformasimonitoringSharedModule, RouterModule.forChild(registrasiFidusiaState)],
  declarations: [RegistrasiFidusiaComponent]
})
export class RegistrasiFidusiaModule {}
