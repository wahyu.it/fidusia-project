package com.fidusia.app.domain.enumeration;

/**
 * The Jenjang enumeration.
 */
public enum Jenjang {
    MA, MTS, MI, TK
}
