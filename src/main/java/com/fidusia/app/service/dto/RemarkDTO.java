package com.fidusia.app.service.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;


/**
 * A DTO for the Remark entity.
 */
public class RemarkDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 50)
    private String category;

    private String label;

    @Size(max = 50)
    private String name;

    @Size(max = 200)
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RemarkDTO referensiDTO = (RemarkDTO) o;

        if ( ! Objects.equals(id, referensiDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ReferensiDTO{" +
            "id=" + id +
            ", category='" + category + "'" +
            ", label='" + label + "'" +
            ", name='" + name + "'" +
            ", description='" + description + "'" +
            '}';
    }
}
