package com.fidusia.app.web.rest;

import com.fidusia.app.domain.OrderData;
import com.fidusia.app.repository.OrderDataRepository;
import com.fidusia.app.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.fidusia.app.domain.OrderData}.
 */
@RestController
@RequestMapping("/api/order-data")
@Transactional
public class OrderDataResource {

    private final Logger log = LoggerFactory.getLogger(OrderDataResource.class);

    private static final String ENTITY_NAME = "orderData";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OrderDataRepository orderDataRepository;

    public OrderDataResource(OrderDataRepository orderDataRepository) {
        this.orderDataRepository = orderDataRepository;
    }

    /**
     * {@code POST  /order-data} : Create a new orderData.
     *
     * @param orderData the orderData to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new orderData, or with status {@code 400 (Bad Request)} if the orderData has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<OrderData> createOrderData(@RequestBody OrderData orderData) throws URISyntaxException {
        log.debug("REST request to save OrderData : {}", orderData);
        if (orderData.getId() != null) {
            throw new BadRequestAlertException("A new orderData cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrderData result = orderDataRepository.save(orderData);
        return ResponseEntity
            .created(new URI("/api/order-data/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /order-data/:id} : Updates an existing orderData.
     *
     * @param id the id of the orderData to save.
     * @param orderData the orderData to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated orderData,
     * or with status {@code 400 (Bad Request)} if the orderData is not valid,
     * or with status {@code 500 (Internal Server Error)} if the orderData couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<OrderData> updateOrderData(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody OrderData orderData
    ) throws URISyntaxException {
        log.debug("REST request to update OrderData : {}, {}", id, orderData);
        if (orderData.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, orderData.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!orderDataRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        OrderData result = orderDataRepository.save(orderData);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, orderData.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /order-data/:id} : Partial updates given fields of an existing orderData, field will ignore if it is null
     *
     * @param id the id of the orderData to save.
     * @param orderData the orderData to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated orderData,
     * or with status {@code 400 (Bad Request)} if the orderData is not valid,
     * or with status {@code 404 (Not Found)} if the orderData is not found,
     * or with status {@code 500 (Internal Server Error)} if the orderData couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<OrderData> partialUpdateOrderData(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody OrderData orderData
    ) throws URISyntaxException {
        log.debug("REST request to partial update OrderData partially : {}, {}", id, orderData);
        if (orderData.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, orderData.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!orderDataRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<OrderData> result = orderDataRepository
            .findById(orderData.getId())
            .map(existingOrderData -> {
                if (orderData.getPpkNomor() != null) {
                    existingOrderData.setPpkNomor(orderData.getPpkNomor());
                }
                if (orderData.getPpkTanggal() != null) {
                    existingOrderData.setPpkTanggal(orderData.getPpkTanggal());
                }
                if (orderData.getNasabahNama() != null) {
                    existingOrderData.setNasabahNama(orderData.getNasabahNama());
                }
                if (orderData.getNasabahJenis() != null) {
                    existingOrderData.setNasabahJenis(orderData.getNasabahJenis());
                }
                if (orderData.getTanggal() != null) {
                    existingOrderData.setTanggal(orderData.getTanggal());
                }
                if (orderData.getStatus() != null) {
                    existingOrderData.setStatus(orderData.getStatus());
                }
                if (orderData.getType() != null) {
                    existingOrderData.setType(orderData.getType());
                }

                return existingOrderData;
            })
            .map(orderDataRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, orderData.getId().toString())
        );
    }

    /**
     * {@code GET  /order-data} : get all the orderData.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of orderData in body.
     */
    @GetMapping("")
    public ResponseEntity<List<OrderData>> getAllOrderData(Pageable pageable) {
        log.debug("REST request to get a page of OrderData");
        Page<OrderData> page = orderDataRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /order-data/:id} : get the "id" orderData.
     *
     * @param id the id of the orderData to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the orderData, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<OrderData> getOrderData(@PathVariable("id") Long id) {
        log.debug("REST request to get OrderData : {}", id);
        Optional<OrderData> orderData = orderDataRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(orderData);
    }

    /**
     * {@code DELETE  /order-data/:id} : delete the "id" orderData.
     *
     * @param id the id of the orderData to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteOrderData(@PathVariable("id") Long id) {
        log.debug("REST request to delete OrderData : {}", id);
        orderDataRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
