package com.fidusia.app.domain.util;

public enum SaksiStatus {
	INACTIVE(0), ACTIVE(1);
	
	private int value;
	
	SaksiStatus (int value) {
		this.value = value;
	}

    public int value() {
        return value;
    }
}
