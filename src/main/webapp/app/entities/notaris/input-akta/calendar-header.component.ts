import { Component, EventEmitter } from '@angular/core';

@Component({
  selector: 'jhi-calendar-header',
  templateUrl: './calendar-header.component.html'
})

export class CalendarHeaderComponent {
  view?: string;
  viewDate?: Date;
  locale?: string = 'en';
  viewChange: EventEmitter<string> = new EventEmitter();
  viewDateChange: EventEmitter<Date> = new EventEmitter();
}