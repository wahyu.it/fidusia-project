package com.fidusia.app.util;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

public class ParagraphBorder extends PdfPageEventHelper{
	
	public boolean active = false;
    public void setActive(boolean active) {
        this.active = active;
    }

    public float indentationLeft;
    public void setIndentationLeft(float indentationLeft) {
        this.indentationLeft = indentationLeft;
    }

    public boolean startDocument = false;
    public ParagraphBorder setStartDocument(boolean startDocument) {
        this.startDocument = startDocument;
        return this;
    }

    public boolean endDocument = false;
    public ParagraphBorder setEndDocument(boolean endDocument) {
        this.endDocument = endDocument;
        return this;
    }

    public float offset = 5;
    public float startPosition;

    @Override
    public void onStartPage(PdfWriter writer, Document document) {
        startPosition = document.top();
        if (active) {
            PdfContentByte cb = writer.getDirectContentUnder();
            cb.setLineWidth(0.6);
            cb.moveTo(document.left() - offset + indentationLeft, startPosition - offset + 2);
            cb.lineTo(document.left() - offset + indentationLeft + 30, startPosition + 2);
            cb.moveTo(document.left() - offset + indentationLeft - 10, startPosition - offset - 2);
            cb.lineTo(document.left() - offset + indentationLeft + 40, startPosition + 2);
        }
    }

    @Override
    public void onParagraph(PdfWriter writer, Document document, float paragraphPosition) {
        this.startPosition = paragraphPosition;
    }

    @Override
    public void onEndPage(PdfWriter writer, Document document) {
        if (active) {
            PdfContentByte cb = writer.getDirectContentUnder();
            cb.setLineWidth(0.6);
            cb.moveTo(document.left() - offset + indentationLeft - 10, document.bottom() + offset + 6);
            cb.lineTo(document.left() - offset + indentationLeft + 40, document.bottom() + 2);
            cb.moveTo(document.left() - offset + indentationLeft, document.bottom() + offset + 2);
            cb.lineTo(document.left() - offset + indentationLeft + 30, document.bottom() + 2);
            cb.stroke();
        }
    }

    @Override
    public void onParagraphEnd(PdfWriter writer, Document document, float paragraphPosition) {
        if (active) {
            if(startDocument) {
                float startPos = paragraphPosition - offset + (startPosition - paragraphPosition);

                PdfContentByte cb2 = writer.getDirectContentUnder();
                cb2.setLineWidth(0.6);
                cb2.moveTo(document.left() - offset + indentationLeft, startPos + 2);
                cb2.lineTo(document.left() - offset + indentationLeft + 30, startPos + offset + 2);
                cb2.moveTo(document.left() - offset + indentationLeft - 10, startPos - 2);
                cb2.lineTo(document.left() - offset + indentationLeft + 40, startPos + offset + 2);
                cb2.stroke();

                startDocument = false;
            }

            if(endDocument) {
                float startPos = paragraphPosition + offset - (startPosition - paragraphPosition);

                PdfContentByte cb2 = writer.getDirectContentUnder();
                cb2.setLineWidth(0.6);
                cb2.moveTo(document.left() - offset + indentationLeft - 10, startPos + offset + 4);
                cb2.lineTo(document.left() - offset + indentationLeft + 40, startPos);
                cb2.moveTo(document.left() - offset + indentationLeft, startPos + offset);
                cb2.lineTo(document.left() - offset + indentationLeft + 30, startPos);
                cb2.stroke();

                endDocument = false;
            }

            PdfContentByte cb = writer.getDirectContentUnder();
            cb.setLineWidth(0.6);
            cb.moveTo(document.left() - offset + indentationLeft, paragraphPosition - offset + (startPosition - paragraphPosition));
            cb.lineTo(document.left() - offset + indentationLeft, paragraphPosition - offset);
            cb.stroke();
        }
    }

}
