export const enum Jenjang {
  MA = 'MA',

  MTS = 'MTS',

  MI = 'MI',

  TK = 'TK'
}
