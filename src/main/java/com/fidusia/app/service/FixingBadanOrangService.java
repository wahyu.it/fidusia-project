package com.fidusia.app.service;

import java.time.LocalDate;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fidusia.app.domain.Badan;
import com.fidusia.app.domain.Kabupaten;
import com.fidusia.app.domain.Orang;
import com.fidusia.app.domain.OrderData;
import com.fidusia.app.domain.Ppk;
import com.fidusia.app.domain.RawData;
import com.fidusia.app.repository.BadanRepository;
import com.fidusia.app.repository.KabupatenRepository;
import com.fidusia.app.repository.OrangRepository;
import com.fidusia.app.repository.OrderDataRepository;
import com.fidusia.app.repository.PpkRepository;
import com.fidusia.app.repository.RawDataRepository;

@Service
public class FixingBadanOrangService {

    private final Logger log = LoggerFactory.getLogger(FixingBadanOrangService.class);
    
    private final RawDataRepository rawDataRepository;
    
    private final PpkRepository ppkRepository;
    
    private final OrderDataRepository orderDataRepository;
    
    private final OrangRepository orangRepository;
    
    private final BadanRepository badanRepository;
    
    private final KabupatenRepository kabupatenRepository;

    private static final Pattern PATTERN_RTRW = Pattern.compile("^([0-9]{1,5})\\/([0-9]{1,5})$");
    
    public FixingBadanOrangService(KabupatenRepository kabupatenRepository, BadanRepository badanRepository, OrangRepository orangRepository, PpkRepository ppkRepository, RawDataRepository rawDataRepository, OrderDataRepository orderDataRepository) {
    	this.rawDataRepository = rawDataRepository;
    	this.orderDataRepository = orderDataRepository;
    	this.ppkRepository = ppkRepository;
    	this.orangRepository = orangRepository;
    	this.badanRepository = badanRepository;
    	this.kabupatenRepository = kabupatenRepository;
    }
    
    public void generate(String tgl) {
    	LocalDate tanggal = LocalDate.parse(tgl);
    	List<OrderData> listOrderData = orderDataRepository.findByTanggal(tanggal);
    	
    	for (OrderData orderData : listOrderData) {
    		
			RawData rwData = rawDataRepository.findByUniqueKey(orderData.getPpkNomor()).get(0);
			String[] lines = rwData.getLine().split("\\|");
			Ppk ppk = ppkRepository.findByNomor(rwData.getUniqueKey());
			
			if (rwData.getNsbJenis() == 2) {
	            String kota = null;
	            String provinsi= null;
	            
	            List<Kabupaten> kabupatens = kabupatenRepository.findByNamaLike(lines[26]);
	            
	            if (kabupatens.size() > 0) {
	                Kabupaten kabupaten = kabupatens.get(0);
	            	kota = kabupaten.getName();
	            	provinsi = kabupaten.getProvinsi().getName();
	            } else {
	            	kota = lines[26];
	            	//TODO notif
	            }
	            
	            String orangRtRw = lines[46];
	            Matcher matcherOrangRtRw = PATTERN_RTRW.matcher(orangRtRw);
				
				Orang orang = ppk.getOrang();
					orang.setIdNumberPemegangBpkb(lines[12]);
					orang.setAlamatPemegangBpkb(lines[2]);
					orang.setKecamatanPemegangBpkb(lines[17]);
					orang.setNamaPemegangBpkb(lines[32]);
					orang.setKelurahanPemegangBpkb(lines[19]);
					orang.setKodeposPemegangBpkb(lines[24]);
					orang.setKotaPemegangBpkb(kota.toUpperCase());
					orang.setProvinsiPemegangBpkb(provinsi.toUpperCase());
//					orang.setJenisKelaminPemegangBpkb(tgl);
					orang.setRtPemegangBpkb(matcherOrangRtRw.matches() ? matcherOrangRtRw.group(1) : "0");
					orang.setRwPemegangBpkb(matcherOrangRtRw.matches() ? matcherOrangRtRw.group(2) : "0");
					
				orangRepository.save(orang);
			} else {
	            String kota = null;
	            String provinsi= null;
	            
	            List<Kabupaten> kabupatens = kabupatenRepository.findByNamaLike(lines[34]);
	            
	            if (kabupatens.size() > 0) {
	                Kabupaten kabupaten = kabupatens.get(0);
	            	kota = kabupaten.getName();
	            	provinsi = kabupaten.getProvinsi().getName();
	            } else {
	            	kota = lines[26];
	            	//TODO notif
	            }
	            
	            String badanRtRw = lines[62];
	            Matcher matcherOrangRtRw = PATTERN_RTRW.matcher(badanRtRw);
	            
				Badan badan = ppk.getBadan();
					badan.setIdNumberPemegangBpkb(lines[19]);
					badan.setAlamatPemegangBpkb(lines[10]);
					badan.setAlamatPenjamin(lines[11]);
					badan.setNamaPemegangBpkb(lines[44]);
					badan.setKecamatanPemegangBpkb(lines[24]);
					badan.setKelurahanPemegangBpkb(lines[27]);
					badan.setKodeposPemegangBpkb(lines[31]);
					badan.setKotaPemegangBpkb(kota.toUpperCase());
					badan.setProvinsiPemegangBpkb(provinsi.toUpperCase());
					badan.setRtPemegangBpkb(matcherOrangRtRw.matches() ? matcherOrangRtRw.group(1) : "0");
					badan.setRwPemegangBpkb(matcherOrangRtRw.matches() ? matcherOrangRtRw.group(2) : "0");
				
				badanRepository.save(badan);
			}
			
		}
    	
    }

}
