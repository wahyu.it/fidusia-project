package com.fidusia.app.domain.enumeration;

/**
 * The MetodeTransaksi enumeration.
 */
public enum MetodeTransaksi {
    TUNAI, TRANSFER
}
