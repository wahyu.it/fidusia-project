package com.fidusia.app.repository;

import com.fidusia.app.domain.Akta;
import com.fidusia.app.domain.Cabang;
import com.fidusia.app.domain.Leasing;
import com.fidusia.app.domain.Sertifikat;
import com.fidusia.app.service.dto.SertifikatDTO;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Sertifikat entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SertifikatRepository extends JpaRepository<Sertifikat, Long> {

    Sertifikat findByAkta(Akta akta);
    
    List<Sertifikat> findByAktaPpkNomor(String ppkNomor);

	Page<Sertifikat> findByTglVoucherBetween(ZonedDateTime tglVoucherFrom, ZonedDateTime tglVoucherTo, Pageable pageable);

	Page<Sertifikat> findByTglOrder(LocalDate tglOrder, Pageable pageable);

	Page<Sertifikat> findByTglOrderBetween(LocalDate tglOrderFrom, LocalDate tglOrderTo, Pageable pageable);

    Page<Sertifikat> findByAktaCabangNama(String namaCabang, Pageable pageable);
    
	static final String SERTIFIKAT_DTO  = "select new com.fidusia.app.service.dto.SertifikatDTO(s.noSertifikat, s.tglSertifikat, p.nomor,"
			+ " p.tanggal, p.nsbNamaDebitur, p.nsbJenis, n.nama, n.wilayahKerja, a.nomor, a.tanggal, a.tanggal, s.status) from Sertifikat s"
			+ " INNER JOIN Akta a on a.id=s.akta.id"
			+ " INNER JOIN Ppk p on p.id=a.ppk.id"
			+ " INNER JOIN Notaris n on n.id=a.notaris.id";
    
	static final String AKTA_SERTIFIKAT_DTO  = "select new com.fidusia.app.service.dto.SertifikatDTO(s.noSertifikat, s.tglSertifikat, p.nomor,"
			+ " p.tanggal, p.nsbNamaDebitur, p.nsbJenis, n.nama, n.wilayahKerja, a.nomor, a.tanggal, a.tglOrder, a.status, s.kodeVoucher, s.tglVoucher) from Akta a"
			+ " LEFT JOIN Sertifikat s on a.id=s.akta.id"
			+ " INNER JOIN Ppk p on p.id=a.ppk.id"
			+ " INNER JOIN Notaris n on n.id=a.notaris.id";
    
	static final String SERTIFIKAT_DTO2 = "select new com.fidusia.app.service.dto.SertifikatDTO(s.noSertifikat, s.tglSertifikat, p.nomor,"
			+ " p.tanggal, p.nsbNama, p.nsbJenis, n.nama, n.wilayahKerja, a.nomor, a.tanggal, a.tanggal, s.status, p.nsbNamaDebitur) from Sertifikat s"
			+ " INNER JOIN Akta a on a.id=s.akta.id"
			+ " INNER JOIN Ppk p on p.id=a.ppk.id"
			+ " INNER JOIN Notaris n on n.id=a.notaris.id";
//			+ " INNER JOIN Invoice i on i.id=s.invoice.id";

	@Query(SERTIFIKAT_DTO + " WHERE p.nomor=:ppkNomor AND a.cabang.id=:cabangid")
    Page<SertifikatDTO> findByAktaPpkNomorAndCabang(
    		@Param("ppkNomor") String ppkNomor,
    		@Param("cabangid") Long cabangid,
    		Pageable pageable);

	@Query(SERTIFIKAT_DTO + " WHERE p.nomor=:ppkNomor")
    Page<SertifikatDTO> findByAktaPpkNomor(
    		@Param("ppkNomor") String ppkNomor,
    		Pageable pageable);

	@Query(SERTIFIKAT_DTO + " WHERE p.tanggal=:tanggal")
    Page<SertifikatDTO> findByAktaPpkTanggal(
    		@Param("tanggal") LocalDate tanggal,
    		Pageable pageable);

	@Query(SERTIFIKAT_DTO + " WHERE s.tglOrder=:tglOrder")
    Page<SertifikatDTO> findByAktaTglOrder(
    		@Param("tglOrder") LocalDate tanggal,
    		Pageable pageable);

	@Query(SERTIFIKAT_DTO + " WHERE a.cabang.id=:cabangid AND p.tanggal=:tanggal")
    Page<SertifikatDTO> findByCbngIdAndPpkTanggal(
    		@Param("cabangid") Long cabangid,
    		@Param("tanggal") LocalDate tanggal,
    		Pageable pageable);
// i.tanggal
	@Query(SERTIFIKAT_DTO + " WHERE a.tanggal=:tanggal")
    Page<SertifikatDTO> findByInvoiceTanggal(
    		@Param("tanggal") LocalDate tanggal,
    		Pageable pageable);
	// i.tanggal
	@Query(SERTIFIKAT_DTO + " WHERE a.tanggal=:tanggal AND a.cabang.id=:cabangid")
    Page<SertifikatDTO> findByCabangAndInvoiceTanggal(
    		@Param("cabangid") Long cabangid,
    		@Param("tanggal") LocalDate tanggal,
    		Pageable pageable);

	@Query(SERTIFIKAT_DTO + " WHERE p.tanggal between :tglPpdFrom and :tglPpdTo")
    Page<SertifikatDTO> findByAktaPpkTanggalBetween(
    		@Param("tglPpdFrom") LocalDate tglPpdFrom,
    		@Param("tglPpdTo") LocalDate tglPpdTo,
    		Pageable pageable);

	@Query(SERTIFIKAT_DTO + " WHERE s.tglOrder between :tglPpdFrom and :tglPpdTo")
    Page<SertifikatDTO> findByAktaTglOrderBetween(
    		@Param("tglPpdFrom") LocalDate tglPpdFrom,
    		@Param("tglPpdTo") LocalDate tglPpdTo,
    		Pageable pageable);

	@Query(SERTIFIKAT_DTO + " WHERE a.cabang.id=:cabangid AND p.tanggal between :tglPpdFrom and :tglPpdTo")
    Page<SertifikatDTO> findByCabangAndAktaPpkTanggalBetween(
    		@Param("cabangid") Long cabangid,
    		@Param("tglPpdFrom") LocalDate tglPpdFrom,
    		@Param("tglPpdTo") LocalDate tglPpdTo,
    		Pageable pageable);

	@Query(SERTIFIKAT_DTO2 + " WHERE s.tglOrder between :tglOrderTo and :tglOrderEnd")
    Page<SertifikatDTO> findByTglOrderBetween2(
    		@Param("tglOrderTo") LocalDate tglPpdFrom,
    		@Param("tglOrderEnd") LocalDate tglPpdTo,
    		Pageable pageable);

	@Query(SERTIFIKAT_DTO2 + " WHERE s.tglOrder=:tanggal")
    Page<SertifikatDTO> findByTglOrder2(
    		@Param("tanggal") LocalDate tanggal,
    		Pageable pageable);

	@Query(AKTA_SERTIFIKAT_DTO + " WHERE a.tglOrder between :tglPpdFrom and :tglPpdTo")
    Page<SertifikatDTO> findByAktaSertifikatTglOrderBetween(
    		@Param("tglPpdFrom") LocalDate tglPpdFrom,
    		@Param("tglPpdTo") LocalDate tglPpdTo,
    		Pageable pageable);

	@Query(AKTA_SERTIFIKAT_DTO + " WHERE a.cabang.id=:cabangid AND tglOrder between :tglOrderFrom and :tglOrderTo")
    Page<SertifikatDTO> findByAktaCabangAndAktaTglOrderBetween(
    		@Param("cabangid") Long cabangid,
    		@Param("tglOrderFrom") LocalDate tglPpdFrom,
    		@Param("tglOrderTo") LocalDate tglPpdTo,
    		Pageable pageable);

	@Query(AKTA_SERTIFIKAT_DTO + " WHERE a.tglOrder=:tglOrder")
    Page<SertifikatDTO> findByTglOrderAkta(
    		@Param("tglOrder") LocalDate tanggal,
    		Pageable pageable);

	@Query(AKTA_SERTIFIKAT_DTO + " WHERE a.cabang.id=:cabangid AND a.tglOrder=:tanggal")
    Page<SertifikatDTO> findByCabangIdAndPpkTanggal(
    		@Param("cabangid") Long cabangid,
    		@Param("tanggal") LocalDate tanggal,
    		Pageable pageable);

	@Query("SELECT COUNT(a) FROM Sertifikat a WHERE a.updateOn>=:updateOn AND a.status = :status")
	Long findTotalRegistered(@Param("updateOn") ZonedDateTime updateOn, @Param("status") Integer status);

	List<Sertifikat> findByTglOrder(LocalDate orderDt);

	long countByTglOrderAndStatus(LocalDate orderDt, int value);

	long countByTglOrderBetween(LocalDate tglOrderFrom, LocalDate tglOrderTo);

	List<Sertifikat> findByTglOrderAndStatus(LocalDate to, int value);

	List<Sertifikat> findByTglOrderAndStatusAndReportSubmitted(LocalDate to, int value, boolean b);

	long countByTglOrderAndReportSubmitted(LocalDate to, boolean b);
}
