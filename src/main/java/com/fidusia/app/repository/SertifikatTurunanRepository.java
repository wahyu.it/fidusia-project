package com.fidusia.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fidusia.app.domain.SertifikatTurunan;

public interface SertifikatTurunanRepository extends JpaRepository<SertifikatTurunan,Long> {
}
