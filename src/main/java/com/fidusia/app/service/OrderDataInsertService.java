package com.fidusia.app.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fidusia.app.domain.Berkas;
import com.fidusia.app.domain.Cabang;
import com.fidusia.app.domain.Kabupaten;
import com.fidusia.app.domain.Kendaraan;
import com.fidusia.app.domain.Orang;
import com.fidusia.app.domain.OrderData;
import com.fidusia.app.domain.Ppk;
import com.fidusia.app.domain.RawData;
import com.fidusia.app.repository.BerkasRepository;
import com.fidusia.app.repository.BadanRepository;
import com.fidusia.app.repository.CabangRepository;
import com.fidusia.app.repository.KabupatenRepository;
import com.fidusia.app.repository.KendaraanRepository;
import com.fidusia.app.repository.OrangRepository;
import com.fidusia.app.repository.OrderDataRepository;
import com.fidusia.app.repository.PpkRepository;
import com.fidusia.app.repository.ProvinsiRepository;
import com.fidusia.app.service.dto.ResponseArrayGetDataDTO;
import com.fidusia.app.util.DateUtils;
import com.fidusia.app.domain.Badan;
import com.fidusia.app.domain.util.BerkasStatus;
import com.fidusia.app.domain.util.OrderType;
import com.fidusia.app.domain.util.State;

@Service
public class OrderDataInsertService {

    private final Logger log = LoggerFactory.getLogger(OrderDataInsertService.class);
    
    private final OrderDataRepository orderDataRepository;
    
    private final BerkasRepository berkasRepository;
    
    private final ReferensiService referensiService;
    
    private final PpkRepository ppkRepository;
    
    private final KendaraanRepository kendaraanRepository;
    
    private final CabangRepository cabangRepository;
    
    private final OrangRepository orangRepository;
    
    private final KabupatenRepository kabupatenRepository;
    
    private final ProvinsiRepository provinsiRepository;
    
    private final BadanRepository badanRepository;
    
    private static final Pattern PATTERN_RTRW = Pattern.compile("^([0-9]{1,5})\\/([0-9]{1,5})$");
    
    OrderDataInsertService(
    		BadanRepository badanRepository,
    		KabupatenRepository kabupatenRepository,
    		ProvinsiRepository provinsiRepository,
    		CabangRepository cabangRepository,
    		OrangRepository orangRepository,
    		KendaraanRepository kendaraanRepository,
    		PpkRepository ppkRepository,
    		OrderDataRepository orderDataRepository,
    		BerkasRepository berkasRepository,
    		ReferensiService referensiService) {
    	this.orderDataRepository = orderDataRepository;
    	this.berkasRepository = berkasRepository;
    	this.referensiService = referensiService;
    	this.ppkRepository = ppkRepository;
    	this.kendaraanRepository = kendaraanRepository;
    	this.orangRepository = orangRepository;
    	this.cabangRepository = cabangRepository;
    	this.kabupatenRepository = kabupatenRepository;
    	this.provinsiRepository = provinsiRepository;
    	this.badanRepository = badanRepository;
    }
    
    public String process(RawData rawData, ResponseArrayGetDataDTO data) {
    	log.debug("Run service Proses Order Data Insert : {}", rawData.getUniqueKey());
    	
    	try {
            RawData rdata = new RawData();
            rdata.setId(rawData.getId());
            String ppkNomor = rawData.getUniqueKey();
            LocalDate ppkTanggal = LocalDate.parse(data.getTanggalKontrak());
            String nsbNama = data.getNamaDebitur();

            OrderData orderData = orderDataRepository.findOneByPpkNomor(rawData.getUniqueKey());
            Cabang cabang = cabangRepository.findByKode(data.getKodeCabang());
            
            if(orderData != null) {
                log.debug("OrderData already exists for ppk {}", orderData.getPpkNomor());
                return null;
            } else {
            	if ("".equals(data.getAktaNo()) || "".equals(data.getAktaDate()) ) {
                	return null;
                }
            	
                orderData = new OrderData();
                orderData.setRawData(rdata);
                orderData.setPpkNomor(ppkNomor);
                orderData.setStatus(State.INPROGRESS.value());
                orderData.setNasabahJenis("Perusahaan".equals(data.getJenisCustomer())? 1 : 2);
                orderData.setPpkTanggal(ppkTanggal);
                orderData.setNasabahNama(nsbNama);
                orderData.setSkSubstitusi(null);//TODO
                orderData.setTanggal(rawData.getOrderDate());
                orderData.setType(OrderType.REGULAR.value());
                orderData.setCabang(cabang);
                orderData.setFidusiaType(data.getFiduciaType());
                orderData.setFidusiaOnlineId("Perusahaan".equals(data.getJenisCustomer())? data.getFiduciaOnlineIdCompany() : data.getFiduciaOnlineIdPersonal());
                orderData.setAktaNo(data.getAktaNo());
//                orderData.setAktaDate(LocalDate.parse(data.getAktaDate()));
                orderData.setAktaDate(DateUtils.parseZonedDateTime(data.getAktaDate(), DateUtils.FORMATTER_ID_LOCAL_DATETIME_V2));
                
                Berkas berkas = createBerkas(ppkNomor, ppkTanggal, nsbNama, rawData.getNsbJenis(), cabang);
                orderData.setBerkas(berkas);

                orderDataRepository.save(orderData);
                
                generatePpk(rawData, data);
                return "OK";
            }
            
		} catch (Exception e) {
            log.error("Error on OrderDataInsertProcessor.process() for " + rawData.getUniqueKey() + " - " + rawData.getOrderDate(), e);
            
//            OrderData orderData = new OrderData();
//            orderData.setRawData(rawData);
//            orderData.setStatus(State.INVALID.value());
            return null;
		}
    	
    }

    private Berkas createBerkas(String ppkNomor, LocalDate ppkTanggal, String nsbNama, Integer nsbJenis, Cabang cabang) {
        Berkas berkas = new Berkas();
        berkas.setPpkNomor(ppkNomor);
        berkas.setTanggalPpk(ppkTanggal);
        berkas.setNamaNasabah(nsbNama);
        berkas.setNsbJenis(nsbJenis);
        berkas.setCabang(cabang);
        berkas.setBerkasStatus(BerkasStatus.NONE.value());
        berkas.setMetodeKirim(1);
        log.debug("berkas :{} status:{}",berkas.getPpkNomor(), berkas.getBerkasStatus());
        
        berkasRepository.save(berkas);
        return berkas;
    }
    
    private Ppk generatePpk(RawData rawData, ResponseArrayGetDataDTO data) {
        LocalDate ppkTanggal = LocalDate.parse(data.getTanggalKontrak());
        Ppk ppk = new Ppk()
                .nsbNamaDebitur(data.getNamaDebitur())
                .nsbJenis(rawData.getNsbJenis())
        		.nomor(data.getAgreementNumber())
        		.nama("Pembiayaan");
        
        String kota = null;
        String provinsi= null;

        List<Kabupaten> kabupatens = kabupatenRepository.findByNamaLike(data.getKota());
        
        if (kabupatens.size() > 0) {
            Kabupaten kabupaten = kabupatens.get(0);
        	kota = kabupaten.getName();
        	provinsi = kabupaten.getProvinsi().getName().toUpperCase();
        } else {
        	kota = data.getKota().toUpperCase();
        	//TODO notif
        }
        
        if ("Perorangan".equals(data.getJenisCustomer())) {
//        	TODO Perorangan
            Orang orang = new Orang()
                    .nama(data.getNamaDebitur())
                    .jenisKelamin(data.getJenisKelamin())
                    .penggunaan("2")
                    .statusKawin(data.getStatusPernikahan())
                    .kelahiran(data.getTempatLahir())
                    .tglLahir(LocalDate.parse(data.getTanggalLahir()))
                    .pekerjaan(data.getPekerjaan())
                    .wargaNegara(data.getKewarganegaraan())
                    .jenisId(data.getiDType())
                    .noId(data.getIdNumber())
                    .alamat(data.getAlamatDebitur())
                    .kelurahan(data.getKelurahan())
                    .kecamatan(data.getKecamatan())
                    .kota(kota)
                    .provinsi(provinsi); // TODO

            String kodePos = data.getKodepos().replaceAll("[^\\d.]", "");
            orang.kodePos((kodePos!=null && kodePos.length()>=5) ? kodePos : String.format("%-5s", kodePos).replace(' ', '7'));

            String orangRtRw = data.getRtRw();
            Matcher matcherOrangRtRw = PATTERN_RTRW.matcher(orangRtRw);
            orang.setRt(matcherOrangRtRw.matches() ? matcherOrangRtRw.group(1) : "000");
            orang.setRw(matcherOrangRtRw.matches() ? matcherOrangRtRw.group(2) : "000");
            
            String orangRtRw1 = data.getRtRwPemegangBpkb();
            Matcher matcherOrangRtRw1 = PATTERN_RTRW.matcher(orangRtRw1);
            orang.setRt(matcherOrangRtRw1.matches() ? matcherOrangRtRw1.group(1) : "000");
            orang.setRw(matcherOrangRtRw1.matches() ? matcherOrangRtRw1.group(2) : "000");
            
            if (data.getNamaDebitur().equals(data.getNamaPemegangBpkb())) {
            	
    			orang.setIdNumberPemegangBpkb(data.getIdNumber());
    			orang.setAlamatPemegangBpkb(data.getAlamatDebitur());
    			orang.setKecamatanPemegangBpkb(data.getKecamatan());
    			orang.setNamaPemegangBpkb(data.getNamaPemegangBpkb());
    			orang.setKelurahanPemegangBpkb(data.getKelurahan());
    			orang.setKodeposPemegangBpkb(data.getKodepos());
    			orang.setKotaPemegangBpkb(kota.toUpperCase());
    			orang.setProvinsiPemegangBpkb(provinsi.toUpperCase());
    			orang.setRtPemegangBpkb(matcherOrangRtRw.matches() ? matcherOrangRtRw.group(1) : "000");
    			orang.setRwPemegangBpkb(matcherOrangRtRw.matches() ? matcherOrangRtRw.group(2) : "000");
            	
    			orang.setKelahiranPemegangBpkb(data.getTempatLahir());
    			orang.setTglLahirPemegangBpkb(LocalDate.parse(data.getTanggalLahir()));
    			orang.setJenisKelaminPemegangBpkb(data.getJenisKelamin());
    			orang.setStatusKawinPemegangBpkb(data.getStatusPernikahan());
            } else {
                List<Kabupaten> kab = kabupatenRepository.findByNamaLike(data.getKotaPemegangBpkb());
                String kotaBpkb = null;
                String provinsiBpkb= null;
                
                if (kabupatens.size() > 0) {
                    Kabupaten kabupaten = kab.get(0);
                    kotaBpkb = kabupaten.getName().toUpperCase();
                    provinsiBpkb = kabupaten.getProvinsi().getName().toUpperCase();
                } else {
                	kotaBpkb = data.getKotaPemegangBpkb().toUpperCase();
                	//TODO notif
                }
                
    			orang.setIdNumberPemegangBpkb(data.getIdNumberPemegangBpkb());
    			orang.setAlamatPemegangBpkb(data.getAlamatPemegangBpkb());
    			orang.setKecamatanPemegangBpkb(data.getKecamatanPemegangBpkb());
    			orang.setNamaPemegangBpkb(data.getNamaPemegangBpkb());
    			orang.setKelurahanPemegangBpkb(data.getKelurahanPemegangBpkb());
    			orang.setKodeposPemegangBpkb(data.getKodeposPemegangBpkb());
    			orang.setKotaPemegangBpkb(kotaBpkb);
    			orang.setProvinsiPemegangBpkb(provinsiBpkb);
    			orang.setRtPemegangBpkb(matcherOrangRtRw1.matches() ? matcherOrangRtRw1.group(1) : "0");
    			orang.setRwPemegangBpkb(matcherOrangRtRw1.matches() ? matcherOrangRtRw1.group(2) : "0");
    			orang.setJenisKelaminPemegangBpkb(data.getJenisKelaminPemegangBpkb());
    			orang.setStatusKawinPemegangBpkb(data.getStatusPernikahanPemegangBpkb());
            }
            
            orangRepository.save(orang);

            ppk.setNsbNama(data.getNamaPemegangBpkb());
            ppk.setOrang(orang);
            
        } else {
//        	TODO Perusahaan
            String kota1 = null;
            String provinsi1= null;
            
        	String jenis = data.getNamaDebitur().split(" ")[0].replace(".", "");
	   		 if (!"PT".equals(jenis) && !"CV".equals(jenis)) {
	   			jenis="OTHER";
	   		 }
	   		 
	         List<Kabupaten> kabBadan = kabupatenRepository.findByNamaLike(data.getKotaPemegangBpkb());
	         
	         if (kabupatens.size() > 0) {
	             Kabupaten kabupaten = kabBadan.get(0);
	         	 kota1 = kabupaten.getName();
	         	 provinsi1 = kabupaten.getProvinsi().getName();
	         } else {
	         	 kota1 = data.getKota();
	         	 //TODO notif
	         }
        	
            Badan badan = new Badan()
                    .nama(data.getNamaDebitur())
                    .jenis(jenis)
                    .npwp(data.getNpwp())
                    .noAkta(data.getNoAkteAkhir())
                    .tglAkta(LocalDate.parse(data.getAkteAkhirDate()))
                    .alamat(data.getAlamatDebitur())
                    .kelurahan(data.getKelurahan())
                    .kecamatan(data.getKecamatan())
                    .kota(data.getKota())
                    .provinsi(null)// TODO
                    .pjNama(data.getNamaPic())
                    .pjJenisKelamin(null)// TODO
                    .pjStatusKawin(null)// TODO
                    .pjKelahiran(null)// TODO
                    .pjTglLahir(null)// TODO
                    .pjWargaNegara(null)// TODO
                    .pjJenisId(null)//
                    .pjNoId(data.getIdNumberPic())//
                    .pjAlamat(data.getAddressPic())// TODO
                    .pjKelurahan(null)// TODO
                    .pjKecamatan(null)// TODO
                    .pjKota(null)// TODO
                    .pjProvinsi(null)// TODO
                    .pjPekerjaan(data.getJobPic())//
                    .golonganUsaha("19");
            
            badan.setAlamatPemegangBpkb(data.getAlamatPemegangBpkb());
            badan.setAlamatPenjamin(data.getAlamatPenjamin());
            badan.setIdNumberPemegangBpkb(data.getIdNumberPemegangBpkb());
            badan.setKecamatanPemegangBpkb(data.getKecamatanPemegangBpkb());
            badan.setKelurahanPemegangBpkb(data.getKelurahanPemegangBpkb());
            badan.setKodeposPemegangBpkb(data.getKodeposPemegangBpkb());
            badan.setKotaPemegangBpkb(kota1.toUpperCase());
            badan.setProvinsiPemegangBpkb(provinsi1.toUpperCase());
            // RT RW Pemegang BPKB
            String rtRwBpkb = data.getRtRwPemegangBpkb();
            Matcher matcherBadanRtRw1 = PATTERN_RTRW.matcher(rtRwBpkb);
            badan.setRtPemegangBpkb(matcherBadanRtRw1.matches() ? matcherBadanRtRw1.group(1) : "000");
            badan.setRwPemegangBpkb(matcherBadanRtRw1.matches() ? matcherBadanRtRw1.group(2) : "000");
            // RTRW Debitur
            String badanRtRw = data.getRtRw();
            Matcher matcherBadanRtRw = PATTERN_RTRW.matcher(badanRtRw);
            badan.setRt(matcherBadanRtRw.matches() ? matcherBadanRtRw.group(1) : "000");
            badan.setRw(matcherBadanRtRw.matches() ? matcherBadanRtRw.group(2) : "000");
            // RTRW PJ
            String pjRtRw = data.getRtRwPic();//
            Matcher matcherPjRtRw = PATTERN_RTRW.matcher(pjRtRw);
            badan.setPjRt(matcherPjRtRw.matches() ? matcherPjRtRw.group(1) : "000");
            badan.setPjRw(matcherPjRtRw.matches() ? matcherPjRtRw.group(2) : "000");

            
            String kodePos = data.getKodepos().replaceAll("[^\\d.]", "");
            badan.kodePos((kodePos!=null && kodePos.length()>=5) ? kodePos : String.format("%-5s", kodePos).replace(' ', '7'));
            badan.setNamaPemegangBpkb(data.getNamaPemegangBpkb());
            badanRepository.save(badan);
            
            ppk.setNsbNama(data.getNamaPemegangBpkb());
            ppk.setBadan(badan);
        	
        }
        
        ppk.setTanggal(ppkTanggal);
        ppk.setTglSk(ppkTanggal);
        ppk.setNilaiHutangPokok(Math.round(Double.parseDouble(data.getNilaiHutangPokok())));
        ppk.setNilaiPenjaminan(Math.round(Double.parseDouble(data.getNilaiObjekPenjaminan())));
        LocalDate tglCicilanAwal = LocalDate.parse(data.getStartDate());
        LocalDate tglCicilanAkhir = LocalDate.parse(data.getEndDate());
        Long periodeTenor = tenor (tglCicilanAwal, tglCicilanAkhir);
        ppk.setPeriodeTenor(periodeTenor);
        ppk.setTglCicilAwal(tglCicilanAwal);
        ppk.setTglCicilAkhir(tglCicilanAkhir);

        ppkRepository.save(ppk);
        
        String kondisi = "U".equals(data.getUsedNew())? "USED": "NEW";
//        String[] merkTipe = data.getMerkTypeKendaraan().split("/");
        
        Kendaraan kendaraan = new Kendaraan()
			.kondisi(kondisi).merk(data.getMerkTypeKendaraan()).tipe(data.getAssetDescription())
	        .warna(data.getWarna()).tahunPembuatan(data.getTahunPembuatan()).noRangka(data.getNoRangka())
	        .noMesin(data.getNoMesin()).noBpkb(data.getNoBpkb())//
	        .nilaiObjekJaminan(Math.round(Double.parseDouble(data.getNilaiObjekPenjaminan())))
	        .ppk(ppk)
	        .assetDescription(data.getAssetDescription());
        

    	if ("MOTOR".equals(data.getMerkTypeKendaraan().split("/")[1])) {
            kendaraan.setRoda(2);
    	} else {
            if(referensiService.valueOf("KENDARAAN_RODA", kendaraan.getTipe()) != null) {
//            	HINO,ISUZU,MITSUBISHI
            	if (data.getAssetDescription().contains("HINO")|| data.getAssetDescription().contains("ISUZU") || data.getAssetDescription().contains("MITSUBISHI")) {
                    kendaraan.setRoda(referensiService.intValue("KENDARAAN_RODA", kendaraan.getTipe()));
            	} else {
                    kendaraan.setRoda(4);
            	}
            } else {
                kendaraan.setRoda(4);
            }
    	}
        kendaraanRepository.save(kendaraan);
    	
    	return ppk;
    }
    
    private Long tenor (LocalDate start, LocalDate end) {
		long tenor = ChronoUnit.MONTHS.between(
				start.withDayOfMonth(1),
		        end.withDayOfMonth(1));
		
		return tenor;
    }
    
}