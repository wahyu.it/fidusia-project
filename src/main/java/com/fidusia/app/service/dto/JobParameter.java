package com.fidusia.app.service.dto;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class JobParameter {
	
	private Map<String, String> map;
	
	public JobParameter() {
		this.map = new LinkedHashMap<String, String>();
	}
	
	public JobParameter(String commaDelimitedKeyValues) {
		this.map = new LinkedHashMap<String, String>();
		
		if(commaDelimitedKeyValues != null) {
			String[] keyValues = commaDelimitedKeyValues.split(",");
			
			if(keyValues != null && keyValues.length > 0) {
				for (String keyValue : keyValues) {
					if(keyValue.contains("=")) {
						String[] split = keyValue.split("=");
						this.map.put(split[0], split[1]);
					}
				}
			}
		}
	}
	
	public JobParameter add(String key, String value) {
		this.map.put(key, value);
		return this;
	}
	
	public JobParameter add(String commaDelimitedKeyValues) {
		if(commaDelimitedKeyValues != null) {
			String[] keyValues = commaDelimitedKeyValues.split(",");
			
			if(keyValues != null && keyValues.length > 0) {
				for (String keyValue : keyValues) {
					if(keyValue.contains("=")) {
						String[] split = keyValue.split("=");
						this.map.put(split[0], split[1]);
					}
				}
			}
		}
		
		return this;
	}
	
	public String get(String key) {
		return this.map.get(key);
	}
	
	public Map<String, String> toMap() {
		return this.map;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for(Entry<String, String> e : this.map.entrySet())
			sb.append("," + e.getKey() + "=" + e.getValue());
		return sb.length() > 0 ? sb.substring(1) : null;
	}
}
