package com.fidusia.app.domain.util;

public enum State {
	DONE(2), INPROGRESS(1), NEW(0), CANCELLED(-1), INVALID(-9);
	
	private int value;
	
	State (int value) {
		this.value = value;
	}

    public int value() {
        return value;
    }
}
