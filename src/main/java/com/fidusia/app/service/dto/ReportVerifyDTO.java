package com.fidusia.app.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Objects;

import com.fidusia.app.util.DateUtils;


public class ReportVerifyDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private Long userId;

    private String userLogin;

    private String userFirstName;

    private String userLastName;

    private String userName;

    private LocalDate tanggal;

    private Integer verified;

    private Integer unverified;

    private Integer total;

    private String startTime;

    private String endTime;

    public ReportVerifyDTO() {
        super();
    }

    public ReportVerifyDTO(String userLogin, Long verified, Long unverified, ZonedDateTime startDateTime, ZonedDateTime endDateTime) {
        this.userLogin = userLogin;
        this.tanggal = startDateTime.toLocalDate();
        this.verified = verified!=null ? verified.intValue() : 0;
        this.unverified = unverified!=null ? unverified.intValue() : 0;
        this.total = this.verified + this.unverified;
        this.startTime = DateUtils.formatZonedDateTime(startDateTime, DateUtils.FORMATTER_TIME);
        this.endTime = DateUtils.formatZonedDateTime(endDateTime, DateUtils.FORMATTER_TIME);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserName() {
        return userLastName!=null && !"".equals(userLastName) ? userFirstName + " " + userLastName : userFirstName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public LocalDate getTanggal() {
        return tanggal;
    }

    public void setTanggal(LocalDate tanggal) {
        this.tanggal = tanggal;
    }

    public Integer getVerified() {
        return verified;
    }

    public void setVerified(Integer verified) {
        this.verified = verified;
    }

    public Integer getUnverified() {
        return unverified;
    }

    public void setUnverified(Integer unverified) {
        this.unverified = unverified;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReportVerifyDTO dto = (ReportVerifyDTO) o;

        if ( ! Objects.equals(id, dto.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ReportVerifyDTO{" +
            "id=" + id +
            ", userId=" + userId +
            ", userName='" + userName + '\'' +
            ", tanggal=" + tanggal +
            ", verified=" + verified +
            ", unverified=" + unverified +
            ", total=" + total +
            ", startTime='" + startTime + '\'' +
            ", endTime='" + endTime + '\'' +
            ", userLogin='" + userLogin + '\'' +
            '}';
    }
}
