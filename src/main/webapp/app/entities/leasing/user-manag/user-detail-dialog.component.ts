import { Component, OnInit, ElementRef  } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { UserService } from 'app/core/user/user.service';
import { DomSanitizer } from '@angular/platform-browser';
import { User } from 'app/core/user/user.model';


@Component({
  selector: 'jhi-user-detail-dialog',
  templateUrl: './user-detail-dialog.component.html',
  styleUrls: ['usermanage.scss']
})
export class UserDetailDialogComponent implements OnInit  {
  user?: User;

  constructor(
	  public activeModal: NgbActiveModal, 
	  private eventManager: JhiEventManager,
      protected uploadBerkasService: UserService,
      private sanitizer: DomSanitizer,
	  private playerRef: ElementRef
    ) {}

  clear(): void {
    this.activeModal.dismiss();
  }

  ngOnInit(): void {
	  
  }
}
