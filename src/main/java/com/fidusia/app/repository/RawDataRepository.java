package com.fidusia.app.repository;

import com.fidusia.app.domain.RawData;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the RawData entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RawDataRepository extends JpaRepository<RawData, Long> {

	List<RawData> findByOrderDate(LocalDate localDate);

	List<RawData> findByUniqueKey(String ppkNomor);
	
}