package com.fidusia.app.service.dto;

import java.time.LocalDate;
import java.time.ZonedDateTime;

public class AktaDTO {

	private Long idAkta;

	private LocalDate tglOrder;

	private String nomorAkta;

	private LocalDate tglAkta;

	private ZonedDateTime pukulAkta;

	private Long notarisId;

	private String notarisNama;

	private String notarisKedudukan;

	private String penghadapNama;

	private String saksiOneNama;

	private String saksiTwoNama;

	private String namaNasabah;

	private String namaBpkb;

	private Long orangId;

	private Long badanId;

	private String ppkNomor;

	public AktaDTO(Long idAkta, LocalDate tglOrder, String nomorAkta, LocalDate tglAkta, ZonedDateTime pukulAkta,
			Long notarisId, String notarisNama, String notarisKedudukan, String penghadapNama, String saksiOneNama,
			String saksiTwoNama, String namaNasabah, String namaBpkb, Long orangId, Long badanId, String ppkNomor) {
		this.idAkta = idAkta;
		this.tglOrder = tglOrder;
		this.nomorAkta = nomorAkta;
		this.tglAkta = tglAkta;
		this.pukulAkta = pukulAkta;
		this.notarisId = notarisId;
		this.notarisNama = notarisNama;
		this.notarisKedudukan = notarisKedudukan;
		this.penghadapNama = penghadapNama;
		this.saksiOneNama = saksiOneNama;
		this.saksiTwoNama = saksiTwoNama;
		this.namaNasabah = namaNasabah;
		this.namaBpkb = namaBpkb;
		this.orangId = orangId;
		this.badanId = badanId;
		this.ppkNomor = ppkNomor;
	}

	public Long getIdAkta() {
		return idAkta;
	}

	public void setIdAkta(Long idAkta) {
		this.idAkta = idAkta;
	}

	public LocalDate getTglOrder() {
		return tglOrder;
	}

	public void setTglOrder(LocalDate tglOrder) {
		this.tglOrder = tglOrder;
	}

	public String getNomorAkta() {
		return nomorAkta;
	}

	public void setNomorAkta(String nomorAkta) {
		this.nomorAkta = nomorAkta;
	}

	public LocalDate getTglAkta() {
		return tglAkta;
	}

	public void setTglAkta(LocalDate tglAkta) {
		this.tglAkta = tglAkta;
	}

	public ZonedDateTime getPukulAkta() {
		return pukulAkta;
	}

	public void setPukulAkta(ZonedDateTime pukulAkta) {
		this.pukulAkta = pukulAkta;
	}

	public Long getNotarisId() {
		return notarisId;
	}

	public void setNotarisId(Long notarisId) {
		this.notarisId = notarisId;
	}

	public String getNotarisNama() {
		return notarisNama;
	}

	public void setNotarisNama(String notarisNama) {
		this.notarisNama = notarisNama;
	}

	public String getNotarisKedudukan() {
		return notarisKedudukan;
	}

	public void setNotarisKedudukan(String notarisKedudukan) {
		this.notarisKedudukan = notarisKedudukan;
	}

	public String getPenghadapNama() {
		return penghadapNama;
	}

	public void setPenghadapNama(String penghadapNama) {
		this.penghadapNama = penghadapNama;
	}

	public String getSaksiOneNama() {
		return saksiOneNama;
	}

	public void setSaksiOneNama(String saksiOneNama) {
		this.saksiOneNama = saksiOneNama;
	}

	public String getSaksiTwoNama() {
		return saksiTwoNama;
	}

	public void setSaksiTwoNama(String saksiTwoNama) {
		this.saksiTwoNama = saksiTwoNama;
	}

	public String getNamaNasabah() {
		return namaNasabah;
	}

	public void setNamaNasabah(String namaNasabah) {
		this.namaNasabah = namaNasabah;
	}

	public String getNamaBpkb() {
		return namaBpkb;
	}

	public void setNamaBpkb(String namaBpkb) {
		this.namaBpkb = namaBpkb;
	}

	public Long getOrangId() {
		return orangId;
	}

	public void setOrangId(Long orangId) {
		this.orangId = orangId;
	}

	public Long getBadanId() {
		return badanId;
	}

	public void setBadanId(Long badanId) {
		this.badanId = badanId;
	}

	public String getPpkNomor() {
		return ppkNomor;
	}

	public void setPpkNomor(String ppkNomor) {
		this.ppkNomor = ppkNomor;
	}

	@Override
	public String toString() {
		return "AktaDTO [idAkta=" + idAkta + ", tglOrder=" + tglOrder + ", nomorAkta=" + nomorAkta + ", tglAkta="
				+ tglAkta + ", pukulAkta=" + pukulAkta + ", notarisId=" + notarisId + ", notarisNama=" + notarisNama
				+ ", notarisKedudukan=" + notarisKedudukan + ", penghadapNama=" + penghadapNama + ", saksiOneNama="
				+ saksiOneNama + ", saksiTwoNama=" + saksiTwoNama + ", namaNasabah=" + namaNasabah + ", namaBpkb="
				+ namaBpkb + ", orangId=" + orangId + ", badanId=" + badanId + ", ppkNomor=" + ppkNomor + "]";
	}

}
