package com.fidusia.app.service.dto;

import com.fidusia.app.domain.Cabang;

import java.time.LocalDate;
import java.time.ZonedDateTime;

import javax.validation.constraints.*;
import java.util.Objects;

public class SertifikatDTO {

    private Long id;

    private String kodeVoucher;

    private ZonedDateTime tglVoucher;

    private String noSertifikat;

    private ZonedDateTime tglSertifikat;

    private Long biayaPnbp;

    private String noReferensiBni;

    private Long biayaJasa;

    private Integer status;

    private String updateBy;

    private ZonedDateTime updateOn;

    private String registrationId;

    private String ppkNomor;

    private LocalDate tglOrder;

    private LocalDate ppkTanggal;

    private String ppkNsbNama;

    private Integer ppkNsbJenis;

    private Long aktaId;

    private Long aktaNotarisId;

    private String aktaNotarisNama;

    private String aktaNotarisWilayahKerja;

    private LocalDate aktaTglOrder;

    private String aktaKode;

    private String aktaNomor;

    private LocalDate aktaTanggal;

    private String aktaPenerimaKuasaNama;

    private Integer aktaPpkNilaiPenjaminan;

    private Cabang cabang;

    private Long cabangId;

    private String cabangName;

    private Long invoiceId;

    private LocalDate invoiceTanggal;

    private String ppkNsbNamaDebitur;

    private Integer partitionId;
    
    private Boolean perbaikan;
    
    private LocalDate tglPerbaikan;
    
    private Integer statusPerbaikan;
    
    private Integer statusRoya;

    private String royaRegId;
    
    private String nomorRoya;
    
    private LocalDate tglRoya;
    
    private Long idRoya;
    
    private String noRoya;
    
    private ZonedDateTime tanggalRoya;
    
    private Boolean buktiLunas;
    
    private String lampiranObject;

    public SertifikatDTO(String noSertifikat, ZonedDateTime tglSertifikat, String ppkNomor,
			LocalDate ppkTanggal, String ppkNsbNama, Integer ppkNsbJenis, String aktaNotarisNama,
			String aktaNotarisWilayahKerja, String aktaNomor, LocalDate aktaTanggal, 
			LocalDate invoiceTanggal, Integer status) {
		this.noSertifikat = noSertifikat;
		this.tglSertifikat = tglSertifikat;
		this.ppkNomor = ppkNomor;
		this.ppkTanggal = ppkTanggal;
		this.ppkNsbNama = ppkNsbNama;
		this.ppkNsbJenis = ppkNsbJenis;
		this.aktaNotarisNama = aktaNotarisNama;
		this.aktaNotarisWilayahKerja = aktaNotarisWilayahKerja;
		this.aktaNomor = aktaNomor;
		this.aktaTanggal = aktaTanggal;
		this.invoiceTanggal = invoiceTanggal;
		this.status = status;
	}

    public SertifikatDTO(String noSertifikat, ZonedDateTime tglSertifikat, String ppkNomor,
			LocalDate ppkTanggal, String ppkNsbNama, Integer ppkNsbJenis, String aktaNotarisNama,
			String aktaNotarisWilayahKerja, String aktaNomor, LocalDate aktaTanggal, 
			LocalDate tglOrder, Integer status, String kodeVoucher, ZonedDateTime tglVoucher) {
		this.noSertifikat = noSertifikat;
		this.tglSertifikat = tglSertifikat;
		this.ppkNomor = ppkNomor;
		this.ppkTanggal = ppkTanggal;
		this.ppkNsbNama = ppkNsbNama;
		this.ppkNsbJenis = ppkNsbJenis;
		this.aktaNotarisNama = aktaNotarisNama;
		this.aktaNotarisWilayahKerja = aktaNotarisWilayahKerja;
		this.aktaNomor = aktaNomor;
		this.aktaTanggal = aktaTanggal;
		this.tglOrder = tglOrder;
		this.status = status;
		this.kodeVoucher = kodeVoucher;
		this.tglVoucher = tglVoucher;
	}

    public SertifikatDTO(String noSertifikat, ZonedDateTime tglSertifikat, String ppkNomor,
			LocalDate ppkTanggal, String ppkNsbNama, Integer ppkNsbJenis, String aktaNotarisNama,
			String aktaNotarisWilayahKerja, String aktaNomor, LocalDate aktaTanggal, 
			LocalDate tglOrder, Integer status, String ppkNsbNamaDebitur) {
		this.noSertifikat = noSertifikat;
		this.tglSertifikat = tglSertifikat;
		this.ppkNomor = ppkNomor;
		this.ppkTanggal = ppkTanggal;
		this.ppkNsbNama = ppkNsbNama;
		this.ppkNsbJenis = ppkNsbJenis;
		this.aktaNotarisNama = aktaNotarisNama;
		this.aktaNotarisWilayahKerja = aktaNotarisWilayahKerja;
		this.aktaNomor = aktaNomor;
		this.aktaTanggal = aktaTanggal;
		this.tglOrder = tglOrder;
		this.status = status;
		this.ppkNsbNamaDebitur = ppkNsbNamaDebitur;
	}

	public Long getIdRoya() {
		return idRoya;
	}

	public void setIdRoya(Long idRoya) {
		this.idRoya = idRoya;
	}

	public String getNoRoya() {
		return noRoya;
	}

	public void setNoRoya(String noRoya) {
		this.noRoya = noRoya;
	}

	public ZonedDateTime getTanggalRoya() {
		return tanggalRoya;
	}

	public void setTanggalRoya(ZonedDateTime tanggalRoya) {
		this.tanggalRoya = tanggalRoya;
	}

	@Size(max = 50)
    private String noNewSertifikat;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getKodeVoucher() {
        return kodeVoucher;
    }

    public void setKodeVoucher(String kodeVoucher) {
        this.kodeVoucher = kodeVoucher;
    }
    public ZonedDateTime getTglVoucher() {
        return tglVoucher;
    }

    public void setTglVoucher(ZonedDateTime tglVoucher) {
        this.tglVoucher = tglVoucher;
    }
    public String getNoSertifikat() {
        return noSertifikat;
    }

    public void setNoSertifikat(String noSertifikat) {
        this.noSertifikat = noSertifikat;
    }
    public ZonedDateTime getTglSertifikat() {
        return tglSertifikat;
    }

    public void setTglSertifikat(ZonedDateTime tglSertifikat) {
        this.tglSertifikat = tglSertifikat;
    }
    public Long getBiayaPnbp() {
        return biayaPnbp;
    }

    public void setBiayaPnbp(Long biayaPnbp) {
        this.biayaPnbp = biayaPnbp;
    }

    public Long getBiayaJasa() {
        return biayaJasa;
    }

    public void setBiayaJasa(Long biayaJasa) {
        this.biayaJasa = biayaJasa;
    }

    public String getNoReferensiBni() {
        return noReferensiBni;
    }

    public void setNoReferensiBni(String noReferensiBni) {
        this.noReferensiBni = noReferensiBni;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }
    public ZonedDateTime getUpdateOn() {
        return updateOn;
    }

    public void setUpdateOn(ZonedDateTime updateOn) {
        this.updateOn = updateOn;
    }
    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }
    public String getPpkNomor() {
        return ppkNomor;
    }

    public void setPpkNomor(String ppkNomor) {
        this.ppkNomor = ppkNomor;
    }
    public LocalDate getTglOrder() {
        return tglOrder;
    }

    public void setTglOrder(LocalDate tglOrder) {
        this.tglOrder = tglOrder;
    }
    public LocalDate getPpkTanggal() {
        return ppkTanggal;
    }

    public void setPpkTanggal(LocalDate ppkTanggal) {
        this.ppkTanggal = ppkTanggal;
    }

    public String getPpkNsbNama() {
        return ppkNsbNama;
    }

    public void setPpkNsbNama(String ppkNsbNama) {
        this.ppkNsbNama = ppkNsbNama;
    }

    public Integer getPpkNsbJenis() {
        return ppkNsbJenis;
    }

    public void setPpkNsbJenis(Integer ppkNsbJenis) {
        this.ppkNsbJenis = ppkNsbJenis;
    }

    public Long getAktaId() {
        return aktaId;
    }

    public void setAktaId(Long aktaId) {
        this.aktaId = aktaId;
    }

    public Cabang getCabang() {
        return cabang;
    }

    public void setCabang(Cabang cabang) {
        this.cabang = cabang;
    }

    public Long getCabangId() {
        return cabangId;
    }

    public void setCabangId(Long cabangId) {
        this.cabangId = cabangId;
    }

    public String getCabangName() {
        return cabangName;
    }

    public void setCabangName(String cabangName) {
        this.cabangName = cabangName;
    }

    public Long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Long invoiceId) {
        this.invoiceId = invoiceId;
    }

    public LocalDate getInvoiceTanggal() {
        return invoiceTanggal;
    }

    public void setInvoiceTanggal(LocalDate invoiceTanggal) {
        this.invoiceTanggal = invoiceTanggal;
    }

    public String getPpkNsbNamaDebitur() {
        return ppkNsbNamaDebitur;
    }

    public void setPpkNsbNamaDebitur(String ppkNsbNamaDebitur) {
        this.ppkNsbNamaDebitur = ppkNsbNamaDebitur;
    }

    public Integer getPartitionId() {
        return partitionId;
    }

    public void setPartitionId(Integer partitionId) {
        this.partitionId = partitionId;
    }

    public Boolean getPerbaikan() {
		return perbaikan;
	}

	public void setPerbaikan(Boolean perbaikan) {
		this.perbaikan = perbaikan;
	}

	public LocalDate getTglPerbaikan() {
		return tglPerbaikan;
	}

	public void setTglPerbaikan(LocalDate tglPerbaikan) {
		this.tglPerbaikan = tglPerbaikan;
	}
	
	public Integer getStatusPerbaikan() {
		return statusPerbaikan;
	}

	public void setStatusPerbaikan(Integer statusPerbaikan) {
		this.statusPerbaikan = statusPerbaikan;
	}

	public Integer getStatusRoya() {
		return statusRoya;
	}

	public void setStatusRoya(Integer statusRoya) {
		this.statusRoya = statusRoya;
	}

	public String getRoyaRegId() {
		return royaRegId;
	}

	public void setRoyaRegId(String royaRegId) {
		this.royaRegId = royaRegId;
	}

	public String getNomorRoya() {
		return nomorRoya;
	}

	public void setNomorRoya(String nomorRoya) {
		this.nomorRoya = nomorRoya;
	}

	public LocalDate getTglRoya() {
		return tglRoya;
	}

	public void setTglRoya(LocalDate tglRoya) {
		this.tglRoya = tglRoya;
	}
	

	public String getNoNewSertifikat() {
		return noNewSertifikat;
	}

	public void setNoNewSertifikat(String noNewSertifikat) {
		this.noNewSertifikat = noNewSertifikat;
	}

	public String getLampiranObject() {
		return lampiranObject;
	}

	public void setLampiranObject(String lampiranObject) {
		this.lampiranObject = lampiranObject;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SertifikatDTO sertifikatDTO = (SertifikatDTO) o;

        if ( ! Objects.equals(id, sertifikatDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "SertifikatDTO{" +
            "id=" + id +
            ", kodeVoucher='" + kodeVoucher + "'" +
            ", tglVoucher='" + tglVoucher + "'" +
            ", noSertifikat='" + noSertifikat + "'" +
            ", tglSertifikat='" + tglSertifikat + "'" +
            ", biayaPnbp='" + biayaPnbp + "'" +
            ", noReferensiBni='" + noReferensiBni + "'" +
            ", status='" + status + "'" +
            ", updateBy='" + updateBy + "'" +
            ", updateOn='" + updateOn + "'" +
            ", registrationId='" + registrationId + "'" +
            ", ppkNomor='" + ppkNomor + "'" +
            ", tglOrder='" + tglOrder + "'" +
            ", ppkTanggal='" + ppkTanggal + "'" +
            ", biayaJasa='" + biayaJasa + "'" +
            ", perbaikan='" + perbaikan + "'" +
            ", tglPerbaikan='" + tglPerbaikan + "'" +
            ", statusPerbaikan='" + statusPerbaikan + "'" +
            ", statusRoya=" + statusRoya + 
            ", royaRegId=" + royaRegId + 
            ", nomorRoya='" + nomorRoya + "'" +
            ", tglRoya='" + tglRoya + "'" +
            ", noNewSertifikat='" + noNewSertifikat + "'" +
            ", idRoya='" + idRoya + "'" +
            ", nomorRoya=" + nomorRoya +
            ", tanggalRoya=" + tanggalRoya +
            ", lampiranObject=" + lampiranObject +
            ", buktiLunas=" + buktiLunas +
            '}';
    }

	public String getAktaNotarisNama() {
		return aktaNotarisNama;
	}

	public void setAktaNotarisNama(String aktaNotarisNama) {
		this.aktaNotarisNama = aktaNotarisNama;
	}

    public String getAktaNotarisWilayahKerja() {
        return aktaNotarisWilayahKerja;
    }

    public void setAktaNotarisWilayahKerja(String aktaNotarisWilayahKerja) {
        this.aktaNotarisWilayahKerja = aktaNotarisWilayahKerja;
    }

    public LocalDate getAktaTglOrder() {
        return aktaTglOrder;
    }

    public void setAktaTglOrder(LocalDate aktaTglOrder) {
        this.aktaTglOrder = aktaTglOrder;
    }

    public String getAktaKode() {
        return aktaKode;
    }

    public void setAktaKode(String aktaKode) {
        this.aktaKode = aktaKode;
    }

    public String getAktaNomor() {
		return aktaNomor;
	}

	public void setAktaNomor(String aktaNomor) {
		this.aktaNomor = aktaNomor;
	}

	public LocalDate getAktaTanggal() {
		return aktaTanggal;
	}

	public void setAktaTanggal(LocalDate aktaTanggal) {
		this.aktaTanggal = aktaTanggal;
	}

	public String getAktaPenerimaKuasaNama() {
		return aktaPenerimaKuasaNama;
	}

	public void setAktaPenerimaKuasaNama(String aktaPenerimaKuasaNama) {
		this.aktaPenerimaKuasaNama = aktaPenerimaKuasaNama;
	}

	public Integer getAktaPpkNilaiPenjaminan() {
		return aktaPpkNilaiPenjaminan;
	}

	public void setAktaPpkNilaiPenjaminan(Integer aktaPpkNilaiPenjaminan) {
		this.aktaPpkNilaiPenjaminan = aktaPpkNilaiPenjaminan;
	}

    public Long getAktaNotarisId() {
        return aktaNotarisId;
    }

    public void setAktaNotarisId(Long aktaNotarisId) {
        this.aktaNotarisId = aktaNotarisId;
    }

	public Boolean getBuktiLunas() {
		LocalDate cutOff = LocalDate.parse("2021-04-14");
		if (ppkTanggal.isAfter(cutOff)) {
			return true;
		} else {
			return false;
		}
	}

	public void setBuktiLunas(Boolean buktiLunas) {
		this.buktiLunas = buktiLunas;
	}
}
