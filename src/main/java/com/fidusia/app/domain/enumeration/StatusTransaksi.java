package com.fidusia.app.domain.enumeration;

/**
 * The StatusTransaksi enumeration.
 */
public enum StatusTransaksi {
    SUKSES, PROSES, GAGAL, EXPIRED, SUCCESS, REQUEST
}
