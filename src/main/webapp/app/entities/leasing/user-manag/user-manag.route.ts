import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Routes } from '@angular/router';
import { Observable, of } from 'rxjs';
import { JhiResolvePagingParams } from 'ng-jhipster';

import { User, IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { UserManagComponent } from './user-manag.component';
import { UserManagDetailComponent } from './user-manag-detail.component';
import { UserManagUpdateComponent } from './user-manag-update.component';

@Injectable({ providedIn: 'root' })
export class UserManagementResolve implements Resolve<IUser> {
  constructor(private service: UserService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IUser> {
    const id = route.params['login'] ? route.params['login'] : null;
    if (id) {
      return this.service.find(id);
    }
    return of(new User());
  }
}

export const userManagRoute: Routes = [
  {
    path: '',
    component: UserManagComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      defaultSort: 'id,asc'
    }
  },
  {
    path: ':login/view',
    component: UserManagDetailComponent,
    resolve: {
      user: UserManagementResolve
    }
  },
  {
    path: ':login/new',
    component: UserManagUpdateComponent,
    resolve: {
      user: UserManagementResolve
    }
  },
  {
    path: ':login/edit',
    component: UserManagUpdateComponent,
    resolve: {
      user: UserManagementResolve
    }
  }
];
