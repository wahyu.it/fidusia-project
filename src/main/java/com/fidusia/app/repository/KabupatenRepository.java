package com.fidusia.app.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fidusia.app.domain.Kabupaten;
import com.fidusia.app.domain.Provinsi;


@Repository
public interface KabupatenRepository extends JpaRepository<Kabupaten, Long> {

	Page<Kabupaten> findByProvinsi(Provinsi provinsi, Pageable pageable);

	@Query("select k from Kabupaten k where k.name like %:name%")
    List<Kabupaten> findByNamaLike(@Param("name") String nama);

}
