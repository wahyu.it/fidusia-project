import { Routes } from '@angular/router';
import { UploadBerkasComponent } from './uploadberkas.component';
import { JhiResolvePagingParams } from 'ng-jhipster';

export const uploadBerkasState: Routes = [{
  path: '',
  component: UploadBerkasComponent,
  resolve: {
	  pagingParams: JhiResolvePagingParams
  }
  }
];
