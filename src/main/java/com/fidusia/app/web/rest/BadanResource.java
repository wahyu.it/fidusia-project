package com.fidusia.app.web.rest;

import com.fidusia.app.domain.Badan;
import com.fidusia.app.repository.BadanRepository;
import com.fidusia.app.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * REST controller for managing {@link com.fidusia.app.domain.Badan}.
 */
@RestController
@RequestMapping("/api/badans")
@Transactional
public class BadanResource {

    private final Logger log = LoggerFactory.getLogger(BadanResource.class);

    private static final String ENTITY_NAME = "badan";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BadanRepository badanRepository;

    public BadanResource(BadanRepository badanRepository) {
        this.badanRepository = badanRepository;
    }

    /**
     * {@code POST  /badans} : Create a new badan.
     *
     * @param badan the badan to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new badan, or with status {@code 400 (Bad Request)} if the badan has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<Badan> createBadan(@RequestBody Badan badan) throws URISyntaxException {
        log.debug("REST request to save Badan : {}", badan);
        if (badan.getId() != null) {
            throw new BadRequestAlertException("A new badan cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Badan result = badanRepository.save(badan);
        return ResponseEntity
            .created(new URI("/api/badans/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /badans/:id} : Updates an existing badan.
     *
     * @param id the id of the badan to save.
     * @param badan the badan to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated badan,
     * or with status {@code 400 (Bad Request)} if the badan is not valid,
     * or with status {@code 500 (Internal Server Error)} if the badan couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<Badan> updateBadan(@PathVariable(value = "id", required = false) final Long id, @RequestBody Badan badan)
        throws URISyntaxException {
        log.debug("REST request to update Badan : {}, {}", id, badan);
        if (badan.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, badan.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!badanRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Badan result = badanRepository.save(badan);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, badan.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /badans/:id} : Partial updates given fields of an existing badan, field will ignore if it is null
     *
     * @param id the id of the badan to save.
     * @param badan the badan to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated badan,
     * or with status {@code 400 (Bad Request)} if the badan is not valid,
     * or with status {@code 404 (Not Found)} if the badan is not found,
     * or with status {@code 500 (Internal Server Error)} if the badan couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Badan> partialUpdateBadan(@PathVariable(value = "id", required = false) final Long id, @RequestBody Badan badan)
        throws URISyntaxException {
        log.debug("REST request to partial update Badan partially : {}, {}", id, badan);
        if (badan.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, badan.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!badanRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Badan> result = badanRepository
            .findById(badan.getId())
            .map(existingBadan -> {
                if (badan.getGolonganUsaha() != null) {
                    existingBadan.setGolonganUsaha(badan.getGolonganUsaha());
                }
                if (badan.getJenis() != null) {
                    existingBadan.setJenis(badan.getJenis());
                }
                if (badan.getNama() != null) {
                    existingBadan.setNama(badan.getNama());
                }
                if (badan.getKedudukan() != null) {
                    existingBadan.setKedudukan(badan.getKedudukan());
                }
                if (badan.getNoAkta() != null) {
                    existingBadan.setNoAkta(badan.getNoAkta());
                }
                if (badan.getTglAkta() != null) {
                    existingBadan.setTglAkta(badan.getTglAkta());
                }
                if (badan.getNamaNotaris() != null) {
                    existingBadan.setNamaNotaris(badan.getNamaNotaris());
                }
                if (badan.getWilKerjaNotaris() != null) {
                    existingBadan.setWilKerjaNotaris(badan.getWilKerjaNotaris());
                }
                if (badan.getSkNotaris() != null) {
                    existingBadan.setSkNotaris(badan.getSkNotaris());
                }
                if (badan.getAlamat() != null) {
                    existingBadan.setAlamat(badan.getAlamat());
                }
                if (badan.getRt() != null) {
                    existingBadan.setRt(badan.getRt());
                }
                if (badan.getRw() != null) {
                    existingBadan.setRw(badan.getRw());
                }
                if (badan.getKelurahan() != null) {
                    existingBadan.setKelurahan(badan.getKelurahan());
                }
                if (badan.getKecamatan() != null) {
                    existingBadan.setKecamatan(badan.getKecamatan());
                }
                if (badan.getKota() != null) {
                    existingBadan.setKota(badan.getKota());
                }
                if (badan.getProvinsi() != null) {
                    existingBadan.setProvinsi(badan.getProvinsi());
                }
                if (badan.getKodePos() != null) {
                    existingBadan.setKodePos(badan.getKodePos());
                }
                if (badan.getNoKontak() != null) {
                    existingBadan.setNoKontak(badan.getNoKontak());
                }
                if (badan.getNamaSk() != null) {
                    existingBadan.setNamaSk(badan.getNamaSk());
                }
                if (badan.getNomorSk() != null) {
                    existingBadan.setNomorSk(badan.getNomorSk());
                }
                if (badan.getTglSk() != null) {
                    existingBadan.setTglSk(badan.getTglSk());
                }
                if (badan.getNpwp() != null) {
                    existingBadan.setNpwp(badan.getNpwp());
                }
                if (badan.getPjNama() != null) {
                    existingBadan.setPjNama(badan.getPjNama());
                }
                if (badan.getPjJenisKelamin() != null) {
                    existingBadan.setPjJenisKelamin(badan.getPjJenisKelamin());
                }
                if (badan.getPjStatusKawin() != null) {
                    existingBadan.setPjStatusKawin(badan.getPjStatusKawin());
                }
                if (badan.getPjKelahiran() != null) {
                    existingBadan.setPjKelahiran(badan.getPjKelahiran());
                }
                if (badan.getPjTglLahir() != null) {
                    existingBadan.setPjTglLahir(badan.getPjTglLahir());
                }
                if (badan.getPjPekerjaan() != null) {
                    existingBadan.setPjPekerjaan(badan.getPjPekerjaan());
                }
                if (badan.getPjWargaNegara() != null) {
                    existingBadan.setPjWargaNegara(badan.getPjWargaNegara());
                }
                if (badan.getPjJenisId() != null) {
                    existingBadan.setPjJenisId(badan.getPjJenisId());
                }
                if (badan.getPjNoId() != null) {
                    existingBadan.setPjNoId(badan.getPjNoId());
                }
                if (badan.getPjAlamat() != null) {
                    existingBadan.setPjAlamat(badan.getPjAlamat());
                }
                if (badan.getPjRt() != null) {
                    existingBadan.setPjRt(badan.getPjRt());
                }
                if (badan.getPjRw() != null) {
                    existingBadan.setPjRw(badan.getPjRw());
                }
                if (badan.getPjKelurahan() != null) {
                    existingBadan.setPjKelurahan(badan.getPjKelurahan());
                }
                if (badan.getPjKecamatan() != null) {
                    existingBadan.setPjKecamatan(badan.getPjKecamatan());
                }
                if (badan.getPjKota() != null) {
                    existingBadan.setPjKota(badan.getPjKota());
                }
                if (badan.getPjProvinsi() != null) {
                    existingBadan.setPjProvinsi(badan.getPjProvinsi());
                }
                if (badan.getPjKodePos() != null) {
                    existingBadan.setPjKodePos(badan.getPjKodePos());
                }
                if (badan.getTemplate() != null) {
                    existingBadan.setTemplate(badan.getTemplate());
                }
                if (badan.getKomparisi() != null) {
                    existingBadan.setKomparisi(badan.getKomparisi());
                }

                return existingBadan;
            })
            .map(badanRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, badan.getId().toString())
        );
    }

    /**
     * {@code GET  /badans} : get all the badans.
     *
     * @param pageable the pagination information.
     * @param filter the filter of the request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of badans in body.
     */
    @GetMapping("")
    public ResponseEntity<List<Badan>> getAllBadans(Pageable pageable,
        @RequestParam(name = "filter", required = false) String filter
    ) {
//        if ("ppk-is-null".equals(filter)) {
//            log.debug("REST request to get all Badans where ppk is null");
//            return new ResponseEntity<>(
//                StreamSupport.stream(badanRepository.findAll().spliterator(), false).filter(badan -> badan.getPpk() == null).toList(),
//                HttpStatus.OK
//            );
//        }
        log.debug("REST request to get a page of Badans");
        Page<Badan> page = badanRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /badans/:id} : get the "id" badan.
     *
     * @param id the id of the badan to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the badan, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<Badan> getBadan(@PathVariable("id") Long id) {
        log.debug("REST request to get Badan : {}", id);
        Optional<Badan> badan = badanRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(badan);
    }

    /**
     * {@code DELETE  /badans/:id} : delete the "id" badan.
     *
     * @param id the id of the badan to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteBadan(@PathVariable("id") Long id) {
        log.debug("REST request to delete Badan : {}", id);
        badanRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
