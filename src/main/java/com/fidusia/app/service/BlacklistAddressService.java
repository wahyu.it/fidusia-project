package com.fidusia.app.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.fidusia.app.config.ApplicationProperties;

@Service
public class BlacklistAddressService {

    private final Logger log = LoggerFactory.getLogger(BlacklistAddressService.class);
    
    private static final String BLACKLIST_FILE = "blacklist.txt";
    private static final String WHITELIST_FILE = "whitelist.txt";
    private static final int MAX_UNAUTH = 3;

    private final ApplicationProperties applicationProperties;
    
    LinkedList<String> blacklist = new LinkedList<>();
    
    LinkedList<String> whitelist = new LinkedList<>();
    
    Map<String, Integer> unauthorizedMap = new HashMap<>();
    
    public BlacklistAddressService (ApplicationProperties applicationProperties) {
    	this.applicationProperties = applicationProperties;
    }
    
    public boolean isBlacklisted(String address) {
    	return address != null && !whitelist.contains(address) && blacklist.contains(address);
    }
    
    public void addBlacklistAddress(String address) {
    	log.info("adding Blacklist Address: {}", address);
    	
    	if(address == null || address.length() == 0 || whitelist.contains(address)) return;
    	
    	blacklist.add(address);
    	
    	try {
        	String s = address + System.lineSeparator();
    		File file = new File(applicationProperties.getDomain().getBaseDirectory(), BLACKLIST_FILE);
        	if(!file.exists())
        		file.createNewFile();
			Files.write(Paths.get(file.getCanonicalPath()), s.getBytes(), StandardOpenOption.APPEND);
		} catch (Exception e) {
			log.error("Error on appendBlacklistAddress due to " + e.getMessage(), e);
		}
    }
    
    public void removeBlacklistAddress(String address) {
    	log.info("removing Blacklist Address: {}", address);

    	if(address == null || address.length() == 0) return;
    	
    	blacklist.remove(address);
    	
    	try {
            File file = new File(applicationProperties.getDomain().getBaseDirectory(), BLACKLIST_FILE);
        	if(!file.exists()) {
        		file.createNewFile();
        		return;
        	}
        	
            File temp = new File("" + ".tmp");
            
    		BufferedReader reader = new BufferedReader(new FileReader(file));
    		BufferedWriter writer = new BufferedWriter(new FileWriter(temp));

            String readLine = "";
            while ((readLine = reader.readLine()) != null) {
            	if(readLine.equals(address)) continue;
            	if(readLine.length() > 0)
            		writer.write(readLine + System.lineSeparator());
            }
            
            reader.close();
            writer.close();
            
            FileUtils.deleteQuietly(file);
            FileUtils.moveFile(temp, file);
		} catch (Exception e) {
			log.error("Error on removeBlacklistAddress due to " + e.getMessage(), e);
		}
    }

    @Scheduled(cron = "0 */5 * * * ?")
    public void refreshBlacklistAddress() {
		log.debug("refresh BlacklistAddress ...");
        File file = new File(applicationProperties.getDomain().getBaseDirectory(), BLACKLIST_FILE);
    	if(!file.exists()) {
			try {
				file.createNewFile();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
    		return;
    	}

        if(System.currentTimeMillis() - file.lastModified() <= 305000L) {
    		log.debug("refresh BlacklistAddress ...");
    		blacklist = new LinkedList<>();
    		
			try {
				BufferedReader reader = new BufferedReader(new FileReader(file));
	            String readLine = "";
	            while ((readLine = reader.readLine()) != null) {
	            	if(readLine.length() > 0 && !blacklist.contains(readLine)) {
	            		log.debug("add blacklist: {}", readLine);
	            		blacklist.add(readLine);
	            	}
	            }
	            
	            reader.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
        }
    }

    @Scheduled(cron = "0 */5 * * * ?")
    public void refreshWhitelistAddress() {
        File file = new File(applicationProperties.getDomain().getBaseDirectory(), WHITELIST_FILE);
    	if(!file.exists()) {
			try {
				file.createNewFile();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
    		return;
    	}

        if(System.currentTimeMillis() - file.lastModified() <= 305000L) {
    		log.debug("refresh WhitelistAddress ...");
    		whitelist = new LinkedList<>();
    		
			try {
				BufferedReader reader = new BufferedReader(new FileReader(file));
	            String readLine = "";
	            while ((readLine = reader.readLine()) != null) {
	            	if(readLine.length() > 0 && !whitelist.contains(readLine)) {
	            		log.debug("add whitelist: {}", readLine);
	            		whitelist.add(readLine);
	            	}
	            }
	            
	            reader.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
        }
    }

    @Scheduled(cron = "0 0 0 * * ?")
    public void refreshUnauthorizedMap() {
		log.debug("refresh refreshUnauthorizedMap ...");
    	unauthorizedMap = new HashMap<>();
    }
    
    public void addUnauhtorizedCounter(String address) {
    	if(address == null || address.length() == 0 || whitelist.contains(address)) return;
    	
    	int last = 0;

    	if(unauthorizedMap.containsKey(address))
    		last = unauthorizedMap.get(address);

    	if(last >= MAX_UNAUTH) {
    		addBlacklistAddress(address);
    	} else {
    		unauthorizedMap.put(address, ++last);
    	}
    }
    
    public void resetUnauhtorizedCounter(String address) {
    	if(unauthorizedMap.containsKey(address))
    		unauthorizedMap.remove(address);
    }
}
