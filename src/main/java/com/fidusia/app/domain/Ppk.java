package com.fidusia.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

/**
 * A Ppk.
 */
@Entity
@Table(name = "ppk")
public class Ppk implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "nomor")
    private String nomor;

    @Column(name = "nama")
    private String nama;

    @Column(name = "jenis")
    private String jenis;

    @Column(name = "tanggal")
    private LocalDate tanggal;

    @Column(name = "tgl_sk")
    private LocalDate tglSk;

    @Column(name = "nilai_hutang_pokok")
    private Long nilaiHutangPokok;

    @Column(name = "nilai_penjaminan")
    private Long nilaiPenjaminan;

    @Column(name = "periode_tenor")
    private Long periodeTenor;

    @Column(name = "tgl_cicil_awal")
    private LocalDate tglCicilAwal;

    @Column(name = "tgl_cicil_akhir")
    private LocalDate tglCicilAkhir;

    @Column(name = "nsb_nama_debitur")
    private String nsbNamaDebitur;

    @Column(name = "nsb_nama")
    private String nsbNama;

    @Column(name = "nsb_jenis")
    private Integer nsbJenis;

    @OneToOne
    @JoinColumn(unique = true)
    private Orang orang;

    @OneToOne
    @JoinColumn(unique = true)
    private Badan badan;

    @Transient
    private List<Kendaraan> kendaraans;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Ppk id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomor() {
        return this.nomor;
    }

    public Ppk nomor(String nomor) {
        this.setNomor(nomor);
        return this;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public String getNama() {
        return this.nama;
    }

    public Ppk nama(String nama) {
        this.setNama(nama);
        return this;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJenis() {
        return this.jenis;
    }

    public Ppk jenis(String jenis) {
        this.setJenis(jenis);
        return this;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public LocalDate getTanggal() {
        return this.tanggal;
    }

    public Ppk tanggal(LocalDate tanggal) {
        this.setTanggal(tanggal);
        return this;
    }

    public void setTanggal(LocalDate tanggal) {
        this.tanggal = tanggal;
    }

    public LocalDate getTglSk() {
        return this.tglSk;
    }

    public Ppk tglSk(LocalDate tglSk) {
        this.setTglSk(tglSk);
        return this;
    }

    public void setTglSk(LocalDate tglSk) {
        this.tglSk = tglSk;
    }

    public Long getNilaiHutangPokok() {
        return this.nilaiHutangPokok;
    }

    public Ppk nilaiHutangPokok(Long nilaiHutangPokok) {
        this.setNilaiHutangPokok(nilaiHutangPokok);
        return this;
    }

    public void setNilaiHutangPokok(Long nilaiHutangPokok) {
        this.nilaiHutangPokok = nilaiHutangPokok;
    }

    public Long getNilaiPenjaminan() {
        return this.nilaiPenjaminan;
    }

    public Ppk nilaiPenjaminan(Long nilaiPenjaminan) {
        this.setNilaiPenjaminan(nilaiPenjaminan);
        return this;
    }

    public void setNilaiPenjaminan(Long nilaiPenjaminan) {
        this.nilaiPenjaminan = nilaiPenjaminan;
    }

    public Long getPeriodeTenor() {
        return this.periodeTenor;
    }

    public Ppk periodeTenor(Long periodeTenor) {
        this.setPeriodeTenor(periodeTenor);
        return this;
    }

    public void setPeriodeTenor(Long periodeTenor) {
        this.periodeTenor = periodeTenor;
    }

    public LocalDate getTglCicilAwal() {
        return this.tglCicilAwal;
    }

    public Ppk tglCicilAwal(LocalDate tglCicilAwal) {
        this.setTglCicilAwal(tglCicilAwal);
        return this;
    }

    public void setTglCicilAwal(LocalDate tglCicilAwal) {
        this.tglCicilAwal = tglCicilAwal;
    }

    public LocalDate getTglCicilAkhir() {
        return this.tglCicilAkhir;
    }

    public Ppk tglCicilAkhir(LocalDate tglCicilAkhir) {
        this.setTglCicilAkhir(tglCicilAkhir);
        return this;
    }

    public void setTglCicilAkhir(LocalDate tglCicilAkhir) {
        this.tglCicilAkhir = tglCicilAkhir;
    }

    public String getNsbNamaDebitur() {
        return this.nsbNamaDebitur;
    }

    public Ppk nsbNamaDebitur(String nsbNamaDebitur) {
        this.setNsbNamaDebitur(nsbNamaDebitur);
        return this;
    }

    public void setNsbNamaDebitur(String nsbNamaDebitur) {
        this.nsbNamaDebitur = nsbNamaDebitur;
    }

    public String getNsbNama() {
        return this.nsbNama;
    }

    public Ppk nsbNama(String nsbNama) {
        this.setNsbNama(nsbNama);
        return this;
    }

    public void setNsbNama(String nsbNama) {
        this.nsbNama = nsbNama;
    }

    public Integer getNsbJenis() {
        return this.nsbJenis;
    }

    public Ppk nsbJenis(Integer nsbJenis) {
        this.setNsbJenis(nsbJenis);
        return this;
    }

    public void setNsbJenis(Integer nsbJenis) {
        this.nsbJenis = nsbJenis;
    }

    public Orang getOrang() {
        return this.orang;
    }

    public void setOrang(Orang orang) {
        this.orang = orang;
    }

    public Ppk orang(Orang orang) {
        this.setOrang(orang);
        return this;
    }

    public Badan getBadan() {
        return this.badan;
    }

    public void setBadan(Badan badan) {
        this.badan = badan;
    }

    public Ppk badan(Badan badan) {
        this.setBadan(badan);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Ppk)) {
            return false;
        }
        return getId() != null && getId().equals(((Ppk) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Ppk{" +
            "id=" + getId() +
            ", nomor='" + getNomor() + "'" +
            ", nama='" + getNama() + "'" +
            ", jenis='" + getJenis() + "'" +
            ", tanggal='" + getTanggal() + "'" +
            ", tglSk='" + getTglSk() + "'" +
            ", nilaiHutangPokok=" + getNilaiHutangPokok() +
            ", nilaiPenjaminan=" + getNilaiPenjaminan() +
            ", periodeTenor=" + getPeriodeTenor() +
            ", tglCicilAwal='" + getTglCicilAwal() + "'" +
            ", tglCicilAkhir='" + getTglCicilAkhir() + "'" +
            ", nsbNamaDebitur='" + getNsbNamaDebitur() + "'" +
            ", nsbNama='" + getNsbNama() + "'" +
            ", nsbJenis=" + getNsbJenis() +
            "}";
    }

	public List<Kendaraan> getKendaraans() {
		return kendaraans;
	}

	public void setKendaraans(List<Kendaraan> kendaraans) {
		this.kendaraans = kendaraans;
	}
}
