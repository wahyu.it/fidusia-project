import { Routes } from '@angular/router';
import { CetakAktaComponent } from './cetak-akta.component';
import { JhiResolvePagingParams } from 'ng-jhipster';

export const cetakAktaState: Routes = [{
  path: '',
  component: CetakAktaComponent,
  resolve: {
	  pagingParams: JhiResolvePagingParams
  }
  }
];
