export interface IKeyCabangDTO {
        id?:number,
		nama?: string,
}

export class KeyCabangDTO implements IKeyCabangDTO {
  constructor(
	  	public id?:number,
		public nama?: string
  ) {}
}
