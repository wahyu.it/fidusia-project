import { Injectable } from '@angular/core';
import { AccountService } from 'app/core';

export interface RouteInfo {
  path: string;
  title: string;
  type: string;
  translate: string;
  icontype: string;
  collapse?: string;
  children?: ChildrenItems[];
}

export interface ChildrenItems {
  path: string;
  ab: string;
  type?: string;
  translate: string;
  title: string;
}

@Injectable({ providedIn: 'root' })
export class MenuService {
  constructor(private accountService: AccountService) {}

  getMenuItems(): RouteInfo[] {
    if (this.accountService.hasAnyAuthority(['ROLE_ADMIN'])) {
      return [
        { path: '/', title: 'Dashboard', type: 'link', translate: 'home', icontype: 'dashboard' },
        {
          path: '/',
          title: 'Administration',
          type: 'sub',
          translate: 'entities.admin',
          icontype: 'list_alt',
          collapse: 'admin',
          children: [
            {
              path: 'admin/user-management',
              title: 'User Management',
              ab: 'UM',
              translate: 'admin.userManagement'
            },
            { path: 'admin/metrics', title: 'Metrics', ab: 'MT', translate: 'admin.metrics' },
            { path: 'admin/health', title: 'Health', ab: 'HE', translate: 'admin.health' },
            {
              path: 'admin/configuration',
              title: 'Configuration',
              ab: 'CO',
              translate: 'admin.configuration'
            },
            { path: 'admin/audits', title: 'Audits', ab: 'AU', translate: 'admin.audits' },
            { path: 'admin/logs', title: 'Logs', ab: 'LO', translate: 'admin.logs' },
            { path: 'admin/docs', title: 'API', ab: 'DO', translate: 'admin.apidocs' }
          ]
        }
      ]
	} else if (this.accountService.hasAnyAuthority([ 'ROLE_OFFICER'])) {
		return [
			{ path: '/', title: 'Dashboard', type: 'link', translate: 'home', icontype: 'dashboard' },
			{ path: 'uploadberkas', title: 'Sertifikat', type: 'link', translate: 'menu.uploadberkas', icontype: 'list_alt' }
		];
	} else if (this.accountService.hasAnyAuthority(['ROLE_HEADQUARTER','ROLE_LEASING'])) {
		return [
			{ path: '/', title: 'Dashboard', type: 'link', translate: 'home', icontype: 'dashboard' },
	        {
	          path: '/',
	          title: 'Laporan',
	          type: 'sub',
	          translate: 'entities.laporan',
	          icontype: 'list_alt',
	          collapse: 'notaris',
	          children: [
	            { path: 'laporan/registrasi-fidusia', title: 'Registrasi Fidusia', ab: 'RF', translate: 'menu.registrasifidusia' },
	            { path: 'laporan/registrasi-roya', title: 'Registrasi Roya', ab: 'RR', translate: 'menu.registrasiroya' },
	            { path: 'laporan/sertifikat', title: 'Download Sertifikat', ab: 'RR', translate: 'menu.laporansertifikat' },
	            { path: 'laporan/upload-warkah', title: 'Upload berkas', ab: 'UB', translate: 'menu.laporanuploadwarkah' }
	          ]
	        },
			{ path: 'leasing/user-manag', title: 'User Management', type: 'link', translate: 'menu.user-management', icontype: 'supervised_user_circle' },
		];
	} else if (this.accountService.hasAnyAuthority(['ROLE_ADMINISTRATOR'])) {
		return [
			{ path: '/', title: 'Dashboard', type: 'link', translate: 'home', icontype: 'dashboard' },
			{ path: 'laporan/sertifikat', title: 'Sertifikat', type: 'link', translate: 'menu.laporansertifikat', icontype: 'list_alt' },
			{ path: 'uploadberkas', title: 'Sertifikat', type: 'link', translate: 'menu.uploadberkas', icontype: 'list_alt' },
	        {
	          path: '/',
	          title: 'Notaris',
	          type: 'sub',
	          translate: 'entities.notaris',
	          icontype: 'list_alt',
	          collapse: 'notaris',
	          children: [
	            { path: 'notaris/akta-notaris', title: 'Daftar Akta', ab: 'DA', translate: 'menu.daftarakta' },
	            { path: 'notaris/cetak-akta', title: 'Cetak Akta', ab: 'CA', translate: 'menu.cetakakta' }
	          ]
	        },
			{ path: 'leasing/user-manag', title: 'User Management', type: 'link', translate: 'menu.user-management', icontype: 'supervised_user_circle' },
		];
	} else if (this.accountService.hasAnyAuthority(['ROLE_OPERATOR'])) {
		return [
			{ path: '/', title: 'Dashboard', type: 'link', translate: 'home', icontype: 'dashboard' },
	        {
	          path: '/',
	          title: 'Verifikasi',
	          type: 'sub',
	          translate: 'entities.verifikasi',
	          icontype: 'list_alt',
	          collapse: 'admin',
	          children: [
	            { path: 'operation/verifyberkas', title: 'verifikasi_orang', ab: 'VP', translate: 'menu.verifyberkasorang' },
	            { path: 'operation/verifykendaraan', title: 'verifikasi_kendaraan', ab: 'VK', translate: 'menu.verifykendaraan' },
	          ]
	        },
	        {
	          path: '/',
	          title: 'Laporan',
	          type: 'sub',
	          translate: 'entities.laporan',
	          icontype: 'list_alt',
	          collapse: 'laporan',
	          children: [
	            { path: 'laporan/sertifikat', title: 'Sertifikat', ab: 'SF', translate: 'menu.sertifikat' }
	          ]
	        }
		];
	} else if (this.accountService.hasAnyAuthority(['ROLE_PARTNER'])) {
		return [
			{ path: '/', title: 'Dashboard', type: 'link', translate: 'home', icontype: 'dashboard' },
			{ path: 'notaris/biodata-notaris', title: 'Biodata', type: 'link', translate: 'menu.biodata', icontype: 'account_circle' },
			{ path: 'notaris/input-akta', title: 'Input Akta', type: 'link', translate: 'menu.inputakta', icontype: 'note_add' },
	        {
	          path: '/',
	          title: 'Minuta',
	          type: 'sub',
	          translate: 'entities.minuta',
	          icontype: 'list_alt',
	          collapse: 'minuta',
	          children: [
	            { path: 'notaris/akta-notaris', title: 'Metrics', ab: 'ca', translate: 'menu.daftarakta' },
	            { path: 'notaris/cetak-akta', title: 'Metrics', ab: 'cs', translate: 'menu.cetakakta' }
	          ]
	        }
		];
	} else {
      return [];
    }
  }
}
