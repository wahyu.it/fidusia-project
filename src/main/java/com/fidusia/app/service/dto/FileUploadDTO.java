package com.fidusia.app.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * Created by wahyu on 10/01/2015.
 */
public class FileUploadDTO implements Serializable {

    private String category;

    private String filename;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FileUploadDTO dto = (FileUploadDTO) o;

        if ( ! Objects.equals(filename, dto.filename)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(filename);
    }

    @Override
    public String toString() {
        return "FileUploadDTO{" +
            "category='" + category + "'" +
            "filename='" + filename + "'" +
            '}';
    }
}
