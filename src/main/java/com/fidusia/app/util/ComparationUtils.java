package com.fidusia.app.util;

import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ComparationUtils {
	public final static DateTimeFormatter FORMATTER_DATE = DateTimeFormatter.ofPattern("dd-MM-yyyy");
	public final static DateTimeFormatter FORMATTER_DATE2 = DateTimeFormatter.ofPattern("dd MMMM yyyy");
	public final static DateTimeFormatter FORMATTER_YYYYMMDD = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	public final static DateTimeFormatter FORMATTER_TIME = DateTimeFormatter.ofPattern("HH:mm");
	public final static DateTimeFormatter FORMATTER_TIME_V2 = DateTimeFormatter.ofPattern("HH.mm");
	public final static NumberFormat FORMATTER_LONG_ID = NumberFormat.getInstance(new Locale("ID"));

	private final static Map<String,String> mapDayOfWeekID = new HashMap<String,String>();
	private final static Map<String,String> mapMonthID = new HashMap<String,String>();
	private final static Map<Character,String> mapNumberID = new HashMap<Character,String>();
	private final static Map<String,String> mapMonthIDText = new HashMap<String,String>();

	static {
		mapDayOfWeekID.put("0", "Minggu");
		mapDayOfWeekID.put("1", "Senin");
		mapDayOfWeekID.put("2", "Selasa");
		mapDayOfWeekID.put("3", "Rabu");
		mapDayOfWeekID.put("4", "Kamis");
		mapDayOfWeekID.put("5", "Jumat");
		mapDayOfWeekID.put("6", "Sabtu");
	}

	static {
		mapMonthID.put("1", "Januari");
		mapMonthID.put("2", "Februari");
		mapMonthID.put("3", "Maret");
		mapMonthID.put("4", "April");
		mapMonthID.put("5", "Mei");
		mapMonthID.put("6", "Juni");
		mapMonthID.put("7", "Juli");
		mapMonthID.put("8", "Agustus");
		mapMonthID.put("9", "September");
		mapMonthID.put("10", "Oktober");
		mapMonthID.put("11", "November");
		mapMonthID.put("12", "Desember");
	}

	static {
		mapNumberID.put('0', "Nol");
		mapNumberID.put('1', "Satu");
		mapNumberID.put('2', "Dua");
		mapNumberID.put('3', "Tiga");
		mapNumberID.put('4', "Empat");
		mapNumberID.put('5', "Lima");
		mapNumberID.put('6', "Enam");
		mapNumberID.put('7', "Tujuh");
		mapNumberID.put('8', "Delapan");
		mapNumberID.put('9', "Sembilan");
	}

	static {
		mapMonthIDText.put("Januari", "01");
		mapMonthIDText.put("Februari", "02");
		mapMonthIDText.put("Maret", "03");
		mapMonthIDText.put("April", "04");
		mapMonthIDText.put("Mei", "05");
		mapMonthIDText.put("Juni", "06");
		mapMonthIDText.put("Juli", "07");
		mapMonthIDText.put("Agustus", "08");
		mapMonthIDText.put("September", "09");
		mapMonthIDText.put("Oktober", "10");
		mapMonthIDText.put("November", "11");
		mapMonthIDText.put("Desember", "12");
	}
	
	public static String formatDate(LocalDate localDate, DateTimeFormatter formatter) {
		return localDate.format(formatter);
	}
	
	public static String formatZonedDateTime(ZonedDateTime zonedDateTime, DateTimeFormatter formatter) {
		return zonedDateTime.format(formatter);
	}
	
	public static String getDayOfWeekID(LocalDate date) {
		return mapDayOfWeekID.get("" + date.getDayOfWeek().getValue());
	}
	
	public static String getMonthID(LocalDate date) {
		return mapMonthID.get("" + date.getMonthValue());
	}
	
	public static String getMonthIDText(String text) {
		return mapMonthIDText.get(text);
	}
	
	public static String getDateComparation(LocalDate date) {
		StringBuilder resultBuilder = new StringBuilder();
		resultBuilder.append(getNumberComparation("" + date.getDayOfMonth()).toLowerCase()).append(" ");
		resultBuilder.append(getMonthID(date)).append(" ");
		resultBuilder.append(getNumberComparation("" + date.getYear()).toLowerCase());
		return resultBuilder.toString();
	}
	
	public static String getDateTimeComparation(ZonedDateTime dateTime, boolean isTime) {
		StringBuilder resultBuilder = new StringBuilder();
		
		if(!isTime) {
			//TODO
		}

		resultBuilder.append(getNumberComparation("" + dateTime.getHour()));
		if(dateTime.getMinute() > 0)
			resultBuilder.append(" lewat " + getNumberComparation("" + dateTime.getMinute()) + " menit");
		
		return resultBuilder.toString().toLowerCase();
	}

//	public static void main(String[] args) {
//		System.out.println(ComparationUtils.getDateComparation(LocalDate.now()));
//	}

	public static String format(long number, NumberFormat formatter) {
		return formatter.format(number);
	}

	public static String getNumberComparation(String numberStr) {
		if("0".equals(numberStr))
			return "nol";
		else {
			String result = getNumberIDRecursive(numberStr);
			return result.substring(0, result.length() - 1).toLowerCase();
		}
	}

	private static String getNumberIDRecursive(String numberStr) {
		StringBuilder resultBuilder = new StringBuilder();

		if(numberStr.startsWith("0"))
			return getNumberIDRecursive(numberStr.substring(1));

		if(numberStr.length() > 12 && numberStr.length() <= 15) {
			String substr = numberStr.substring(0, numberStr.length() - 12);
			resultBuilder.append(getNumberIDRecursive(substr) + " Triliun").append(" ");
			resultBuilder.append(getNumberIDRecursive(numberStr.substring(substr.length())));
		}
		if(numberStr.length() > 9 && numberStr.length() <= 12) {
			String substr = numberStr.substring(0, numberStr.length() - 9);
			resultBuilder.append(getNumberIDRecursive(substr) + "Milyar").append(" ");
			resultBuilder.append(getNumberIDRecursive(numberStr.substring(substr.length())));
		}
		if(numberStr.length() > 6 && numberStr.length() <= 9) {
			String substr = numberStr.substring(0, numberStr.length() - 6);
			resultBuilder.append(getNumberIDRecursive(substr) + "Juta").append(" ");
			resultBuilder.append(getNumberIDRecursive(numberStr.substring(substr.length())));
		}
		if(numberStr.length() > 3 && numberStr.length() <= 6) {
			String substr = numberStr.substring(0, numberStr.length() - 3);
			resultBuilder.append("1".equals(substr) ? "Seribu" : getNumberIDRecursive(substr) + "Ribu").append(" ");
			resultBuilder.append(getNumberIDRecursive(numberStr.substring(substr.length())));
		}
		if(numberStr.length() == 3) {
			char c = numberStr.charAt(0);
			resultBuilder.append(c == '1' ? "Seratus" : mapNumberID.get(c) + " Ratus").append(" ");
			resultBuilder.append(getNumberIDRecursive(numberStr.substring(1)));
		}
		if(numberStr.length() == 2) {
			char c = numberStr.charAt(0);
			if(c == '1') {
				char c0 = numberStr.charAt(1);
				resultBuilder.append(c0 == '0' ? "Sepuluh" : (c0 == '1' ? "Sebelas" : mapNumberID.get(c0) + " Belas")).append(" ");
			} else {
				resultBuilder.append(mapNumberID.get(c) + " Puluh").append(" ");
				resultBuilder.append(getNumberIDRecursive(numberStr.substring(1)));
			}
		}
		if(numberStr.length() == 1) {
			resultBuilder.append(mapNumberID.get(numberStr.charAt(0))).append(" ");
		}

		return resultBuilder.toString();
	}
}
