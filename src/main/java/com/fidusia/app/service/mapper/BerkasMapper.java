package com.fidusia.app.service.mapper;


import org.mapstruct.*;

import com.fidusia.app.domain.Berkas;
import com.fidusia.app.domain.Cabang;
import com.fidusia.app.domain.Remark;
import com.fidusia.app.service.dto.BerkasDTO;

import java.util.List;

/**
 * Mapper for the entity Berkas and its DTO BerkasDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BerkasMapper {

    @Mapping(source = "cabang.id", target = "cabangId")
    @Mapping(source = "cabang.nama", target = "namaCabang")
    @Mapping(source = "remark.id", target = "remarkId")
    @Mapping(source = "remark.name", target = "remarkName")
    BerkasDTO berkasToBerkasDTO(Berkas berkas);

    List<BerkasDTO> berkasToBerkasDTOs(List<Berkas> berkas);

    @Mapping(source = "cabangId", target = "cabang")
    @Mapping(source = "remarkId", target = "remark")
    Berkas berkasDTOToBerkas(BerkasDTO berkasDTO);

    List<Berkas> berkasDTOsToBerkas(List<BerkasDTO> berkasDTOs);

    default Cabang cabangFromId(Long id) {
        if (id == null) {
            return null;
        }
        Cabang cabang = new Cabang();
        cabang.setId(id);
        return cabang;
    }

    default Remark remarkFromId(Long id) {
        if (id == null) {
            return null;
        }
        Remark remark = new Remark();
        remark.setId(id);
        return remark;
    }
}
