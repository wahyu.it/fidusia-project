import { Component, OnInit, ElementRef  } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { VerifyKendaraanService } from './verifykendaraan.service';
import { DomSanitizer } from '@angular/platform-browser';
import { KendaraankDTO } from 'app/shared/model/kendaraanDTO.model';


@Component({
  selector: 'jhi-verifykendaraan-dialog',
  templateUrl: './verifykendaraan-dialog.component.html',
  styleUrls: ['verifykendaraan.scss']
})
export class VerifyKendaraanDialogComponent implements OnInit  {
	url !: any;
	filename !: string;
	image !: any;
	ext !: any;
	blob !: any;
	kendaraan!: KendaraankDTO;

  constructor(
	  public activeModal: NgbActiveModal, 
	  private eventManager: JhiEventManager,
      protected verifyKendaraanService: VerifyKendaraanService,
      private sanitizer: DomSanitizer,
	  private playerRef: ElementRef
    ) {}

  clear(): void {
    this.activeModal.dismiss();
  }

  ngOnInit(): void {
	 this.verifyKendaraanService.viewBerkas2(this.filename).subscribe(res => {
	  this.ext = this.filename.split(".")[1];
	  	 try{
            this.blob = new Blob([res], { type: "image/"+ this.ext});
            const imageUrl = window.URL.createObjectURL(this.blob);
            
   		   this.image = this.sanitizer.bypassSecurityTrustResourceUrl(imageUrl);
   		   
         } catch (ex) {
             console.log(ex);
         }
	 });
	  
  }
}
