import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { SERVER_API_URL } from 'app/app.constants';
import { LoginModalService } from 'app/core/login/login-modal.service';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import { JhiEventManager } from 'ng-jhipster';
import { NotarisDTO } from 'app/shared/model/notarisDTO.model';
import { ICabangDTO } from 'app/shared/model/cabangDTO.model';
import { UploadBerkasService } from '../entities/leasing/uploadberkas/uploadberkas.service';

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.scss']
})
export class HomeComponent implements OnInit {
  account: Account | null = null;
  notaris: NotarisDTO |any;
  authSubscription?: Subscription;
  date!: any;
  lastMonth!: any;
  auth!: any;
  orderLastMonth!: any;
  orderNow!: any;
  orderMonth!: any;
  aktaNow!: any;
  aktaMonth!: any;
  cabang?: ICabangDTO;
  
  public resourceUrl= SERVER_API_URL + 'api/sertifikats/order/';
  public resourceNotarisUrl= SERVER_API_URL + 'api/notarises/';

  constructor(
    private accountService: AccountService,
    private loginModalService: LoginModalService,
    private eventManager: JhiEventManager,
    protected uploadBerkasService: UploadBerkasService,
    protected http: HttpClient
  ) {
	 this.accountService.identity().subscribe();
  }

  ngOnInit(): void {
    this.authSubscription = this.accountService.getAuthenticationState().subscribe(account => {
      if (account) {
        this.account = account;
      }
      this.account = account;
    });
    this.datenow();
    this.lastMonth2();
    this.registerAuthenticationSuccess();
    
    if (this.account!.authorities[1] === "ROLE_ADMINISTRATOR"){
		this.http.get(this.resourceUrl + "month/last").subscribe(order => {
			this.orderLastMonth = order;
		});
		this.http.get(this.resourceUrl + "day/now").subscribe(order => {
			this.orderNow = order;
		});
		this.http.get(this.resourceUrl + "month/now").subscribe(order => {
			this.orderMonth = order;
		});
	} else if (this.account!.authorities[1] === "ROLE_PARTNER") {
		this.http.get(this.resourceUrl + "akta/now").subscribe(order => {
			this.aktaNow = order;
		});
		this.http.get(this.resourceUrl + "akta/month").subscribe(order => {
			this.aktaMonth = order;
		});
		this.http.get(this.resourceNotarisUrl + "getByLogin").subscribe(notaris => {
			this.notaris = notaris;
		});
	} else if (this.account!.authorities[1] === "ROLE_OFFICER") {
		this.uploadBerkasService.cabangLogin().subscribe(cabang => {
	      this.cabang = cabang;
	    });
	}
    
  }

  registerAuthenticationSuccess(): void {
    this.eventManager.subscribe('authenticationSuccess', () => {
      this.accountService.identity().subscribe(account => {
        this.account = account;
      });
    });
  }

  datenow(): void {
    this.date = new Date();
  }

  lastMonth2(): void {
    const x = new Date();
    this.lastMonth = x.setMonth(x.getMonth()-1);
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  login(): void {
    this.loginModalService.open();
  }

  getImageUrl(): any {
    return this.accountService.getImageUrl();
  }
}
