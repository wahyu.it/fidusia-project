import { Component, OnInit, ElementRef  } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { VerifyBerkasService } from './verifyberkas.service';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'jhi-verifyberkas-dialog',
  templateUrl: './verifyberkas-dialog.component.html',
  styleUrls: ['verifyberkas.scss']
})
export class VerifyBerkasDialogComponent implements OnInit  {
	url !: any;
	filename !: string;
	image !: any;
	ext !: any;
	blob !: any;

  constructor(
	  public activeModal: NgbActiveModal, 
	  private eventManager: JhiEventManager,
      protected verifyBerkasService: VerifyBerkasService,
      private sanitizer: DomSanitizer,
	  private playerRef: ElementRef
    ) {}

  clear(): void {
    this.activeModal.dismiss();
  }

  ngOnInit(): void {
	 this.verifyBerkasService.viewBerkas2(this.filename).subscribe(res => {
	  this.ext = this.filename.split(".")[1];
	  	 try{
            this.blob = new Blob([res], { type: "image/"+ this.ext});
            const imageUrl = window.URL.createObjectURL(this.blob);
            
   		   this.image = this.sanitizer.bypassSecurityTrustResourceUrl(imageUrl);
   		   
         } catch (ex) {
             console.log(ex);
         }
	 });
	  
  }
}
