package com.fidusia.app.web.rest;

import com.fidusia.app.domain.Remark;
import com.fidusia.app.repository.RemarkRepository;
import com.fidusia.app.security.AuthoritiesConstants;
import com.fidusia.app.service.dto.RemarkDTO;
import com.fidusia.app.service.mapper.RemarkMapper;
import com.fidusia.app.web.rest.errors.BadRequestAlertException;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.fidusia.app.domain.Remark}.
 */
@RestController
@RequestMapping("/api/remarks")
@Transactional
public class RemarkResource {

    private final Logger log = LoggerFactory.getLogger(RemarkResource.class);

    private static final String ENTITY_NAME = "remark";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RemarkRepository remarkRepository;

    private final RemarkMapper remarkMapper;

    public RemarkResource(RemarkRepository remarkRepository, RemarkMapper remarkMapper) {
        this.remarkRepository = remarkRepository;
        this.remarkMapper = remarkMapper;
    }

    /**
     * {@code POST  /remarks} : Create a new remark.
     *
     * @param remark the remark to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new remark, or with status {@code 400 (Bad Request)} if the remark has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<Remark> createRemark(@RequestBody Remark remark) throws URISyntaxException {
        log.debug("REST request to save Remark : {}", remark);
        if (remark.getId() != null) {
            throw new BadRequestAlertException("A new remark cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Remark result = remarkRepository.save(remark);
        return ResponseEntity
            .created(new URI("/api/remarks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /remarks/:id} : Updates an existing remark.
     *
     * @param id the id of the remark to save.
     * @param remark the remark to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated remark,
     * or with status {@code 400 (Bad Request)} if the remark is not valid,
     * or with status {@code 500 (Internal Server Error)} if the remark couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<Remark> updateRemark(@PathVariable(value = "id", required = false) final Long id, @RequestBody Remark remark)
        throws URISyntaxException {
        log.debug("REST request to update Remark : {}, {}", id, remark);
        if (remark.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, remark.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!remarkRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Remark result = remarkRepository.save(remark);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, remark.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /remarks/:id} : Partial updates given fields of an existing remark, field will ignore if it is null
     *
     * @param id the id of the remark to save.
     * @param remark the remark to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated remark,
     * or with status {@code 400 (Bad Request)} if the remark is not valid,
     * or with status {@code 404 (Not Found)} if the remark is not found,
     * or with status {@code 500 (Internal Server Error)} if the remark couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Remark> partialUpdateRemark(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Remark remark
    ) throws URISyntaxException {
        log.debug("REST request to partial update Remark partially : {}, {}", id, remark);
        if (remark.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, remark.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!remarkRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Remark> result = remarkRepository
            .findById(remark.getId())
            .map(existingRemark -> {
                if (remark.getCategory() != null) {
                    existingRemark.setCategory(remark.getCategory());
                }
                if (remark.getName() != null) {
                    existingRemark.setName(remark.getName());
                }
                if (remark.getDescription() != null) {
                    existingRemark.setDescription(remark.getDescription());
                }
                if (remark.getBerkasRevisi() != null) {
                    existingRemark.setBerkasRevisi(remark.getBerkasRevisi());
                }

                return existingRemark;
            })
            .map(remarkRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, remark.getId().toString())
        );
    }

    /**
     * {@code GET  /remarks} : get all the remarks.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of remarks in body.
     */
    @GetMapping("")
    public ResponseEntity<List<Remark>> getAllRemarks(Pageable pageable) {
        log.debug("REST request to get a page of Remarks");
        Page<Remark> page = remarkRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /remarks/:id} : get the "id" remark.
     *
     * @param id the id of the remark to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the remark, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<Remark> getRemark(@PathVariable("id") Long id) {
        log.debug("REST request to get Remark : {}", id);
        Optional<Remark> remark = remarkRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(remark);
    }

    /**
     * {@code DELETE  /remarks/:id} : delete the "id" remark.
     *
     * @param id the id of the remark to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteRemark(@PathVariable("id") Long id) {
        log.debug("REST request to delete Remark : {}", id);
        remarkRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }

    @GetMapping("/findByCategory/{category}")
    @Secured({AuthoritiesConstants.ADMIN, AuthoritiesConstants.OPERATOR, AuthoritiesConstants.PARTNER, AuthoritiesConstants.PARTNER_STAFF})
    public ResponseEntity<List<RemarkDTO>> findByCategory(@PathVariable String category) throws URISyntaxException {
        log.debug("REST request to findByCategory of Remarks {}", category);

        List<Remark> remarks = new ArrayList<>();
        if(category != null)
            remarks = remarkRepository.findByCategory(category.toUpperCase());

        return new ResponseEntity<>(remarkMapper.remarksToRemarkDTOs(remarks), HttpStatus.OK);
    }
}
