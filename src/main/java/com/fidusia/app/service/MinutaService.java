package com.fidusia.app.service;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.fidusia.app.config.ApplicationProperties;
import com.fidusia.app.domain.*;
import com.fidusia.app.domain.util.ResponseStatus;
import com.fidusia.app.repository.AktaRepository;
import com.fidusia.app.repository.KendaraanRepository;
import com.fidusia.app.repository.PpkRepository;
import com.fidusia.app.service.dto.JobParameter;
import com.fidusia.app.util.CustomDashedLineSeparator;
import com.fidusia.app.util.AddParagraph;
import com.fidusia.app.util.ComparationUtils;
import com.fidusia.app.util.DateUtils;
import com.fidusia.app.util.ExceptionUtils;
import com.fidusia.app.util.LogUtils;
import com.fidusia.app.util.NumberUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import com.fidusia.app.util.StringUtils;
import com.fidusia.app.web.rest.util.CommonResponse;

import java.io.File;
import java.io.FileOutputStream;
import java.time.LocalDate;
import java.util.List;


@Service
public class MinutaService {

    private final Logger log = LoggerFactory.getLogger(MinutaService.class);

    private final static float DOC_LEFT = 91.5f, DOC_RIGHT = 30.5f, DOC_TOP = 53.33f , DOC_BUTTOM = 64.73f,
            WIDTH = 473f, WIDTH_V2 = 473f, SPACE_CHAR_RATIO = 2.1f, LINE_RATIO = 16.5f;

    private final KendaraanRepository kendaraanRepository;
    
    private final ApplicationProperties applicationProperties;
    
    private final AktaRepository aktaRepository;
	
    private final JobInstanceService jobInstanceService;
    
    private final SlackInhookService slackInhookService;
    
    private final PpkRepository ppkRepository;
    
    public MinutaService(
    		PpkRepository ppkRepository,
    		ApplicationProperties applicationProperties,
    		KendaraanRepository kendaraanRepository,
    		AktaRepository aktaRepository,
    		JobInstanceService jobInstanceService,
    		SlackInhookService slackInhookService
    		) {
    	this.ppkRepository = ppkRepository;
    	this.kendaraanRepository = kendaraanRepository;
    	this.applicationProperties = applicationProperties;
    	this.aktaRepository = aktaRepository;
    	this.jobInstanceService = jobInstanceService;
    	this.slackInhookService = slackInhookService;
    }
    
    public void generateMinutaAsync(JobParameter parameters) throws Exception {
		String tglorder = parameters.get("tglorder");
		LocalDate date = tglorder != null ? LocalDate.parse(tglorder) : LocalDate.now();
		
        String service = "Generate Minuta";
        String parameter = "tglorder:" + date;
        CommonResponse response = new CommonResponse(service, parameter);
        
        try {
        	StringBuilder sb = new StringBuilder();
			List<Akta> aktaList = aktaRepository.findByTglOrder(date);
			int count=0;
            
			for (Akta akta : aktaList) {
				String result = generateMinutaFile(akta);
				if (result != null)
					count++;
			}
        	
    		sb.append("\n* Generate Minuta").append(" *");
    		sb.append("\ntotal        ").append(aktaList.size() > 0 ? aktaList.size() : 0);
    		sb.append("\nsuccess      ").append(count > 0 ? count : 0);
    		sb.append("\npending      ").append(aktaList.size()-count);

            LogUtils.debug(response = response.complete(ResponseStatus.COMPLETED.toString(), sb.toString()), log, slackInhookService);
            jobInstanceService.updateStatus(parameters.get("jobid"), "SUCCESS", parameters.toString(), "");
		} catch (Exception e) {
            e.printStackTrace();
            log.error(ExceptionUtils.getMessage(e));
            
            LogUtils.debug(response = response.complete(ResponseStatus.FAILED.toString(), ExceptionUtils.getMessage(e), ExceptionUtils.getStackTrace(e)), log, slackInhookService);
            jobInstanceService.updateStatus(parameters.get("jobid"), "ERROR", parameters.toString(), ExceptionUtils.getStackTrace(e));
            return;
		}
    }

    public String generateMinutaFile(Akta akta) throws Exception {
    	
        String filePath = applicationProperties.getDomain().getBaseDirectory() + applicationProperties.getDirectory().getMinuta()
                +  akta.getNotaris().getId() + "/" + akta.getTglOrder() + "/" + akta.getPpk().getNomor() + "-minuta.pdf";
        File file = new File(filePath);

        if(!file.exists()) {
        	file = generateMinutaPerorangan2024(akta);
        } else {
        	file = generateMinutaPerorangan2024(akta);
        }
    	
    	return filePath;
    }
    
    public File generateMinutaPerorangan2024(Akta akta) throws Exception {
        log.debug("CREATEAKTA-MINUTA generate minuta perorangan 2024 PPK {} by notaris {}", akta.getPpk().getNomor(), akta.getNotaris().getId());

        String baseDirectory = applicationProperties.getDomain().getBaseDirectory() + applicationProperties.getDirectory().getMinuta();
        String fontFile = applicationProperties.getDomain().getBaseDirectory() + applicationProperties.getDirectory().getFont() + applicationProperties.getFont().getMinutaFont();
        
        Notaris notaris = akta.getNotaris();
        Ppk ppk = akta.getPpk();

		Long nilaiHutangPokok = ppk.getNilaiHutangPokok();
		Long nilaiPenjaminan = ppk.getNilaiPenjaminan();

        List<Kendaraan> kendaraans = kendaraanRepository.findByPpk(ppk);
        PenerimaKuasa penerimaKuasa = akta.getPenerimaKuasa();
        
        boolean nasabahBadan = ppk.getNsbJenis().intValue() == 1;

        String directory = baseDirectory + notaris.getId();
        File basedir = new File(directory);
        if(!basedir.exists())
            basedir.mkdir();

        String fileName = directory + "/" + akta.getTglOrder().toString() + "/" + ppk.getNomor() + "-minuta.pdf";

        File dir = new File(directory + "/" + akta.getTglOrder().toString());
        if(!dir.exists())
            dir.mkdir();

        File file = new File(fileName);
        if(file.exists()) {
            log.debug("file already exists. deleting ...");
            file.delete();
        }

        try {
            Document document = new Document(PageSize.A4, DOC_LEFT, DOC_RIGHT, DOC_TOP, DOC_BUTTOM);
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(new File(fileName)));

            document.open();
            writer.setSpaceCharRatio(SPACE_CHAR_RATIO);

            BaseFont b_font = BaseFont.createFont(fontFile, BaseFont.CP1257, BaseFont.EMBEDDED);
            Font f_bold = new Font(b_font, 10, Font.BOLD, BaseColor.BLACK);
            Font f_normal = new Font(b_font, 10, Font.NORMAL, BaseColor.BLACK);
            Chunk separator = new Chunk(new CustomDashedLineSeparator());

            Paragraph p1 = new Paragraph("AKTA JAMINAN FIDUSIA", f_bold);
            p1.setLeading(LINE_RATIO, 0);
            p1.setAlignment(Element.ALIGN_CENTER);
            document.add(p1);

            Paragraph p2 = new Paragraph("Nomor : " + akta.getNomor() + ".-", f_bold);
            p2.setLeading(LINE_RATIO, 0);
            p2.setAlignment(Element.ALIGN_CENTER);
            document.add(p2);
            document.add(Chunk.NEWLINE);

            String[] dayAndDate = DateUtils.getDayAndDate(akta.getTanggal());

            AddParagraph.addParagraph(document,0,0,null,
                    "-Pada hari ini, " + dayAndDate[0] + ", tanggal " + dayAndDate[1] + " (" + ComparationUtils.getDateComparation(akta.getTanggal())+ ").",
                    f_normal, separator);

            AddParagraph.addParagraph(document,0,0,null,
                    "-Pukul " + ComparationUtils.formatZonedDateTime(akta.getPukulAwal(), ComparationUtils.FORMATTER_TIME_V2) + " WIB (" + ComparationUtils.getDateTimeComparation(akta.getPukulAwal(), true).toLowerCase() + " Waktu Indonesia Barat).", f_normal, separator);

            AddParagraph.addParagraphV2(document,0,0,null,new Chunk[] {
                    new Chunk("-Berhadapan dengan saya,", f_normal),
                    new Chunk(" " + (notaris.getTitelLongOne() != null && notaris.getTitelLongOne().length() > 0 ? notaris.getTitelLongOne() + " " : "")
                            + notaris.getNama() + (notaris.getTitelLongTwo() != null && notaris.getTitelLongTwo().length() > 0 ? ", " + notaris.getTitelLongTwo() : ""), f_bold),
                    new Chunk(", Notaris di " + notaris.getKedudukan() + ", dengan dihadiri oleh saksi-saksi yang dikenal oleh saya, Notaris dan akan disebut pada bagian akhir akta ini:", f_normal)}, separator);
//			agustini --> penghadap MTF
            AddParagraph.addParagraphV2(document,0,18,new Chunk("-",f_normal),new Chunk[] {
                    new Chunk(penerimaKuasa.getTitel() + " " + penerimaKuasa.getNama().toUpperCase(), f_bold),
                    new Chunk(", lahir di " + penerimaKuasa.getTts() + ", " + penerimaKuasa.getKomparisi() + ".", f_normal)}, separator);

            AddParagraph.addParagraph(document,18,0,"",
                    "- Untuk keperluan akta ini sementara waktu berada di Kabupaten Tangerang.", f_normal, separator);
            
//            [Nama KONSUMAN], lahir di Subang, pada tanggal 31-12-1969 (tiga puluh satu Desember seribu sembilan ratus enam puluh sembilan ), 
//            Warga Negara  	 Indonesia, bertempat tinggal di Tangerang,  Cluster Garnet Resdc, Gt I     I/16 Phg Rt 003 Rw 002 Kelurahan 
//            Curug Sangereng Kecamatan Kelapa Dua 	 Tangerang Provinsi Banten, Rukun Tetangga 003 , Rukun Warga 002 ,  	 
//            Kelurahan Curug Sangerang , Kecamatan Kelapa Dua  , pemegang Kartu Tanda Penduduk dengan Nomor Induk Kependudukan: 3603287112690018.  
            
            if (nasabahBadan) {
                Badan badan = ppk.getBadan();

                try {
                	String panggilan = "";
                	if (badan.getPjStatusKawin() != null && badan.getPjJenisKelamin() != null) {
                		panggilan = "LAKI-LAKI".equals(badan.getJenisKelaminPemegangBpkb().toUpperCase()) ? "\" Tuan \"": "SINGLE".equals(badan.getStatusKawinPemegangBpkb().toUpperCase()) ? "\" Nona \"" : "\" Nyonya \"";
                	}
                	
                    AddParagraph.addParagraphV2(document,18,18,new Chunk("a.",f_normal),new Chunk[] {
                      		new Chunk("Menurut keterangannya dalam hal ini bertindak selaku kuasa berdasarkan Surat Kuasa yang dibuat di bawah tangan, bermaterai cukup, "
                      				+ "fotokopi diperlihatkan kepada saya, Notaris, dan oleh karena itu bertindak untuk dan atas nama serta sah mewakili :", f_normal),
                      		new Chunk( panggilan + badan.getPjNama().toUpperCase(), f_bold),
                      		new Chunk( badan.getPjKelahiran() != null ? ", lahir di " + StringUtils.toCamelCase(badan.getPjKelahiran()) : ", lahir di ... ", f_normal),
                      		new Chunk( badan.getPjTglLahir() != null ? ", pada tanggal " + ComparationUtils.formatDate(badan.getPjTglLahir(), ComparationUtils.FORMATTER_DATE) : ", pada tanggal ... ", f_normal),
                      		new Chunk( ", Warga Negara Indonesia, bertempat tinggal di " + StringUtils.toCamelCase(badan.getPjAlamat()), f_normal),
                      		new Chunk( badan.getPjRt() != null ? ", Rukun Tetangga " + badan.getPjRt() : ", Rukun Tetangga ... ", f_normal),
                      		new Chunk(badan.getPjRw() != null ? ", Rukun Warga " + badan.getPjRw() : ", Rukun Warga ... ", f_normal),
                      		new Chunk(badan.getPjKelurahan() != null ? ", Kelurahan " + StringUtils.toCamelCase(badan.getPjKelurahan()) : ", Kelurahan ... ", f_normal),
                      		new Chunk(badan.getPjKecamatan() != null ? ", Kecamatan " + StringUtils.toCamelCase(badan.getPjKecamatan()) : ", Kecamatan ... ", f_normal),
                      		new Chunk(badan.getPjKota() != null ? (badan.getPjKota().startsWith("KAB.")?"Kabupaten "+StringUtils.toCamelCase(badan.getPjKota().substring(4)):StringUtils.toCamelCase(badan.getPjKota())):  "Kabupaten ...", f_normal),
                      		new Chunk(badan.getPjProvinsi() != null ? (badan.getPjProvinsi().startsWith("PROV.")?"Provinsi "+StringUtils.toCamelCase(badan.getPjProvinsi().substring(5)):StringUtils.toCamelCase(badan.getPjProvinsi())) : "Provinsi ...", f_normal),
                      		new Chunk( ". Pemegang Kartu Tanda Penduduk Nomor: " + (badan.getIdNumberPemegangBpkb() != null ? badan.getIdNumberPemegangBpkb() : "...") + ".", f_normal)}, separator);
				} catch (Exception e) {
					log.debug("Create salinan badan Error {}", e);
		            document.close();
		            file.delete();
					return null;
				}

                AddParagraph.addParagraphV2(document,18,18,null,new Chunk[] { new Chunk("- Yang hal ini dalam jabatannya sebagai" + " " + badan.getPjPekerjaan() + 
                		" dari dan oleh karenanya mewakili DIREKSI dan dari sebagaimana demikian untuk dan atas nama"  +" " + badan.getNama() + " berkedudukan di"
                		+ " " + badan.getKota() , f_normal)}, separator);
                
            } else {
                Orang orang = ppk.getOrang();
                if (ppk.getNsbNama().equals(ppk.getNsbNamaDebitur())) {
	                  AddParagraph.addParagraphV2(document,18,18,new Chunk("a.",f_normal),new Chunk[] {
	            		new Chunk("Menurut keterangannya dalam hal ini bertindak selaku kuasa berdasarkan Surat Kuasa yang dibuat di bawah tangan, bermaterai cukup, "
	            				+ "fotokopi diperlihatkan kepada saya, Notaris, dan oleh karena itu bertindak untuk dan atas nama serta sah mewakili :", f_normal),
//	            		new Chunk(("LAKI-LAKI".equals(orang.getJenisKelamin().toUpperCase()) ? " Tuan ":("SINGLE".equals(orang.getStatusKawin().toUpperCase()) ? " Nona " : " Nyonya ")) + ppk.getNsbNamaDebitur().toUpperCase(), f_bold),
	            		new Chunk(ppk.getNsbNamaDebitur().toUpperCase(), f_bold),
	                    new Chunk(", lahir di " + StringUtils.toCamelCase(orang.getKelahiran()) + ", pada tanggal " + ComparationUtils.formatDate(orang.getTglLahir(), ComparationUtils.FORMATTER_DATE)
	                            + " (" + ComparationUtils.getDateComparation(orang.getTglLahir()) + "), Warga Negara Indonesia, bertempat tinggal di " + StringUtils.toCamelCase(orang.getAlamat())
	                            + ", Rukun Tetangga " + orang.getRt() + ", Rukun Warga " + orang.getRw() + ", Kelurahan " + StringUtils.toCamelCase(orang.getKelurahan()) + ", Kecamatan " + StringUtils.toCamelCase(orang.getKecamatan()) + ", " + (orang.getKota().startsWith("KAB.")?"Kabupaten "+StringUtils.toCamelCase(orang.getKota().substring(4)):StringUtils.toCamelCase(orang.getKota()))
	                            + ", " + (orang.getProvinsi().startsWith("PROV.")?"Provinsi "+StringUtils.toCamelCase(orang.getProvinsi().substring(5)):StringUtils.toCamelCase(orang.getProvinsi())) + ". Pemegang Kartu Tanda Penduduk Nomor: " + orang.getNoId()+ ".", f_normal),
	                    new Chunk(".", f_normal)}, separator);
                	
                } else {
                	try {
                    	String panggilan = "";
                    	if (orang.getStatusKawinPemegangBpkb() != null && orang.getJenisKelaminPemegangBpkb() != null) {
                    		panggilan = "LAKI-LAKI".equals(orang.getJenisKelaminPemegangBpkb().toUpperCase()) ? "\" Tuan \"": "SINGLE".equals(orang.getStatusKawinPemegangBpkb().toUpperCase()) ? "\" Nona \"" : "\" Nyonya \"";
                    	}
                    	
                    	AddParagraph.addParagraphV2(document,18,18,new Chunk("a.",f_normal),new Chunk[] {
                    		new Chunk("Menurut keterangannya dalam hal ini bertindak selaku kuasa berdasarkan Surat Kuasa yang dibuat di bawah tangan, bermaterai cukup, "
                    				+ "fotokopi diperlihatkan kepada saya, Notaris, dan oleh karena itu bertindak untuk dan atas nama serta sah mewakili :", f_normal),
                    		new Chunk(panggilan + orang.getNamaPemegangBpkb().toUpperCase(), f_bold),
                    		new Chunk ( ", lahir di " + (orang.getKelahiranPemegangBpkb() != null ? StringUtils.toCamelCase(orang.getKelahiranPemegangBpkb()) : "..." ) ,f_normal),
                    		new Chunk ( ", pada tanggal " + (orang.getTglLahirPemegangBpkb() != null ?  ComparationUtils.formatDate(orang.getTglLahirPemegangBpkb(), ComparationUtils.FORMATTER_DATE) : " ... "),f_normal),
                    		new Chunk ( " (" + (orang.getTglLahirPemegangBpkb() != null ? ComparationUtils.getDateComparation(orang.getTglLahirPemegangBpkb()) : " ... " ) + " )",f_normal),
                    		new Chunk ( " , Warga Negara Indonesia, bertempat tinggal di " + (orang.getAlamatPemegangBpkb() != null ?  StringUtils.toCamelCase(orang.getAlamatPemegangBpkb()) : " ... "),f_normal),
                    		new Chunk ( ", Rukun Tetangga " + (orang.getRtPemegangBpkb() != null ? orang.getRtPemegangBpkb() : " ... "),f_normal),
                    		new Chunk ( ", Rukun Warga " + (orang.getRtPemegangBpkb() != null ? orang.getRtPemegangBpkb() : " ... "),f_normal),
                    		new Chunk ( ", Kelurahan " + (orang.getKelurahanPemegangBpkb() != null ? StringUtils.toCamelCase(orang.getKelurahanPemegangBpkb()) : " ... " ),f_normal),
                    		new Chunk ( ", Kecamatan " + (orang.getKecamatanPemegangBpkb() != null ? StringUtils.toCamelCase(orang.getKecamatanPemegangBpkb()) :" ... "),f_normal),
                    		new Chunk ( ", " + ( orang.getKotaPemegangBpkb() != null ? (orang.getKotaPemegangBpkb().startsWith("KAB.")?"Kabupaten "+StringUtils.toCamelCase(orang.getKotaPemegangBpkb().substring(4)):StringUtils.toCamelCase(orang.getKotaPemegangBpkb())) :"Kabupaten ... "),f_normal),
                    		new Chunk ( ", " + (orang.getProvinsiPemegangBpkb() != null ? (orang.getProvinsiPemegangBpkb().startsWith("PROV.")?"Provinsi "+StringUtils.toCamelCase(orang.getProvinsiPemegangBpkb().substring(5)):StringUtils.toCamelCase(orang.getProvinsiPemegangBpkb())) : "Provinsi ... "),f_normal),
                    		new Chunk ( ". Pemegang Kartu Tanda Penduduk Nomor: " + ( orang.getIdNumberPemegangBpkb() != null ? orang.getIdNumberPemegangBpkb()+ "." : " ... ") ,f_normal),
                            }, separator);
					} catch (Exception e) {
    					log.debug("Create Minuta error {}", e);
    		            document.close();
    		            file.delete();
    					return null;
					}
                }
            }

            AddParagraph.addParagraphV2(document,18,18,null,new Chunk[] {
            		new Chunk("- (untuk selanjutnya disebut ", f_normal),
            		new Chunk(" \"Pemberi Fidusia\"", f_bold), new Chunk(").", f_normal)}, separator);

            AddParagraph.addParagraphV2(document,18,18,new Chunk("b.",f_normal),new Chunk[] {
            		new Chunk ("Berdasarkan surat kuasa dari Direksi Perseroan yang dibuat dibawah tangan bermaterai cukup, "
            				+ "dari dan oleh karenanya untuk dan atas nama Perseroan Terbatas PT. MANDIRI TUNAS FINANCE, berkedudukan di Jakarta, "
            				+ "yang anggaran dasarnya telah diumumkan Berita Negara Republik Indonesia berturut-turut "
            				+ "tertanggal 18-07-1989 (delapan belas Juli seribu sembilan ratus delapan puluh sembilan) nomor 57, "
            				+ "tambahan 1369, yang anggaran dasar tersebut telah beberapa kali mengalami perubahan dan terakhir "
            				+ "dirubah dengan akta nomor : 42, tanggal 26-03-2018 (dua puluh enam Maret dua ribu delapan belas), "
            				+ "dibuat oleh LENNY JANIS ISHAK, Sarjana Hukum, Notaris di Kota Administrasi Jakarta Selatan yang telah diterima "
            				+ "dan dicatat di dalam Sistem Administrasi Badan Hukum Kementerian Hukum dan Hak Asasi Manusia Republik Indonesia, "
            				+ "Nomor AHU-AH.01.030128132, tanggal 28-03-2018 (dua puluh delapan Maret dua ribu delapan belas), selanjutnya disebut juga ", f_normal),
            		new Chunk(" \"Penerima Fidusia\"", f_bold), new Chunk(" atau ", f_normal), new Chunk(" \"Pihak Pemberi Fasilitas\"", f_bold)}, separator);

            AddParagraph.addParagraph(document,18,0,"",
                    "- Para penghadap, tetap bertindak dalam tindakannya tersebut diatas, lebih dahulu dengan ini menerangkan : .", f_normal, separator);

            AddParagraph.addParagraphV2(document,18,18,new Chunk("A.",f_normal),new Chunk[] {
                    new Chunk("Bahwa diantara",f_normal), new Chunk(" Pemberi Fidusia", f_bold), new Chunk(" tersebut, yang dalam hal ini bertindak untuk "
                    		+ "dan atas nama diri sendiri selaku pihak yang menerima fasilitas kredit",f_normal),
                    new Chunk(" (untuk selanjutnya cukup disebut",f_normal), new Chunk(" \"Debitur\"", f_bold),
                    new Chunk(") dan PT. MANDIRI TUNAS FINANCE selaku",f_normal),  new Chunk(" Penerima Fidusia", f_bold),
                    new Chunk(" atau selaku pihak yang memberi fasilitas (untuk selanjutnya dalam Akta ini disebut ",f_normal), new Chunk(" \"Kreditur\"", f_bold),
                    new Chunk("), berdasarkan",f_normal), new Chunk(" Perjanjian Pembiayaan", f_bold),
                    new Chunk(" bermaterai cukup yang dibuat dibawah tangan Nomor ",f_normal),
                    new Chunk(", Nomor :",f_normal), new Chunk(" " + ppk.getNomor(), f_bold), new Chunk(", tertanggal ",f_normal),  new Chunk(" "+ppk.getTanggal().format(ComparationUtils.FORMATTER_DATE), f_bold), 
                    new Chunk(" ("+ComparationUtils.getDateComparation(ppk.getTanggal()), f_bold), new Chunk(". Untuk selanjutnya Perjanjian Pembiayaan PT. MANDIRI TUNAS FINANCE "
                    		+ "tersebut berikut segenap perubahan dan penambahannya akan disebut", f_normal),new Chunk(" \"Perjanjian Pembiayaan\"", f_bold),  new Chunk(".", f_bold)}, separator);

            AddParagraph.addParagraphV2(document,18,18,new Chunk("B.",f_normal),new Chunk[] {
                    new Chunk("Bahwa, untuk lebih menjamin kelancaran dan kepastian pembayaran kembali segala sesuatu yang terhutang dan harus dibayar oleh "
                    		+ "DEBITUR sebagaimana diatur dalam Perjanjian Pembiayaan tersebut, maka DEBITUR diwajibkan untuk memberikan Jaminan Fidusia atas "
                    		+ "kendaraan milik Pemberi Fidusia untuk kepentingan penerima Fidusia sebagaimana yang akan diuraikan dalam akta ini;",f_normal)}, separator);
            
            AddParagraph.addParagraphV2(document,18,18,new Chunk("C.",f_normal),new Chunk[] {
                    new Chunk("Bahwa, untuk memenuhi ketentuan tentang pemberian jaminan fidusia yang ditentukan dalam Perjanjian Pembiayaan tersebut di atas, "
                    		+ "maka Pemberi Fidusia dan Penerima Fidusia telah sepakat dengan ini mengadakan perjanjian Jaminan Fidusia seperti yang dimaksud "
                    		+ "dalam Undang-undang Nomor 42 Tahun 1999 (seribu sembilan ratus sembilan puluh sembilan), yaitu perjanjian tentang Jaminan Fidusia "
                    		+ "juncto Putusan Mahkamah Konstitusi Nomor 18/PUU-XVII/2019 (MK 18/PUU-XVII/2019), sebagaimana yang hendak dinyatakan sekarang dalam akta ini   ",f_normal)}, separator);

            AddParagraph.addParagraphV2(document,18,0,null,new Chunk[] {
                    new Chunk("- Selanjutnya para penghadap, tetap dalam tindakan mereka masing-masing tersebut di atas dan untuk diri sendiri, menerangkan untuk "
                    		+ "menjamin terbayarnya dengan lancar dan tepat waktu segala sesuatu yang terhutang dan harus dibayar oleh Penerima Fasilitas kepada "
                    		+ "Pemberi Fasilitas, baik karena hutang pokok, bunga dan biaya-biaya lainnya yang timbul berdasarkan Perjanjian Pembiayaan, dengan "
                    		+ "jumlah Fasilitas Dana Pembiayaan sebesar ",f_normal),new Chunk(" Rp. " + NumberUtils.format(nilaiHutangPokok, NumberUtils.FORMATTER_LONG_ID) 
                    		+ ",- (" + ComparationUtils.getNumberComparation(nilaiHutangPokok.toString()) + " rupiah)",f_bold),
                    new Chunk(" atau sejumlah uang yang akan ditentukan dikemudian hari berdasarkan Perjanjian Pembiayaan,jumlah uang mana ternyata dari Fasilitas Dana"
                    		+ " Pembiayaan Konsumen dari Pemberi Fasilitas, maka untuk dan dalam hal ini Fidusia dengan ini menyatakan memberikan Jaminan Fidusia kepada"
                    		+ " Penerima Fidusia dan karena Penerima Fidusia ini juga menyatakan  	 menerima Jaminan Fidusia dari Pemberi Fidusia sampai dengan nilai"
                    		+ " Penjaminan sebesar ",f_normal), new Chunk(" Rp. " + NumberUtils.format(nilaiPenjaminan, NumberUtils.FORMATTER_LONG_ID) + ",- (" 
                    		+ ComparationUtils.getNumberComparation(nilaiPenjaminan.toString()) + " rupiah)",f_bold),
                    new Chunk(" demikian berdasar atau sebagaimana tersebut dalam Perjanjian Pembiayaan yang dibuat dibawah tangan bermaterai, yang fotocopy perjanjian"
                    		+ " tersebut turut dilekatkan pada minuta Akta Saya Notaris, yaitu berupa: ",f_normal)}, separator);
            
            int jmlUnit = kendaraans.size();
            AddParagraph.addParagraph(document,28,0,null,"- "+ jmlUnit + " (" + ComparationUtils.getNumberComparation("" + jmlUnit).toLowerCase() + ") buah kendaraan bermotor, dengan spesifikasi sebagai berikut :", f_normal, separator);

            if(jmlUnit == 1) {
            	Kendaraan kendaraan = kendaraans.get(0);
                AddParagraph.addParagraph(document,38,144,"Merk",":   " + kendaraan.getMerk(), f_normal, separator);
                AddParagraph.addParagraph(document,38,144,"Type",":   " + kendaraan.getTipe(), f_normal, separator);
                AddParagraph.addParagraph(document,38,144,"Tahun Pembuatan",":   " + kendaraan.getTahunPembuatan(), f_normal, separator);
                AddParagraph.addParagraph(document,38,144,"Nomor Rangka",":   " + kendaraan.getNoRangka(), f_normal, separator);
                AddParagraph.addParagraph(document,38,144,"Nomor Mesin",":   " + kendaraan.getNoMesin(), f_normal, separator);
                AddParagraph.addParagraph(document,38,144,"Warna",":   " + kendaraan.getWarna(), f_normal, separator);
                String kondisi = kendaraan.getKondisi().toUpperCase();
                
                if("USED".equals(kondisi)) {
                    AddParagraph.addParagraph(document,38,144,"Kondisi",":   BEKAS", f_normal, separator);
                } else {
                    AddParagraph.addParagraph(document,38,144,"Kondisi",":   BARU", f_normal, separator);
                }
            }
            
            AddParagraph.addParagraphV2(document,28,0,null,new Chunk[] {
            	new Chunk("- Yang diuraikan lebih lanjut dalam Perjanjian Pembiayaan. (\"untuk selanjutnya dalam akta ini akan disebut ",f_normal), new Chunk(" \"Obyek Jaminan Fidusia \"", f_bold), new Chunk(").", f_normal)}, separator);
        
            AddParagraph.addParagraph(document,0,0,null,
                    "Selanjutnya para penghadap, tetap dalam tindakan mereka masing-masing tersebut di atas dan untuk diri sendiri, menerangkan pembebanan jaminan fidusia ini diterima dan dilaksanakan dengan persyaratan dan ketentuan sebagai berikut:  ", f_normal, separator);

            AddParagraph.addParagraphV3(document,"Pasal 1", f_bold, separator);
            AddParagraph.addParagraph(document,0,0,null, "Pengalihan hak kepemilikan atas Obyek Jaminan Fidusia terjadi sejak tanggal penandatanganan akta ini, sehingga Penerima Fidusia menjadi pemilik atas Obyek Jaminan Fidusia, dengan tidak mengurangi ketentuan dalam undang-undang tentang Jaminan Fidusia dan ketentuan yang tercantum dalam akta ini. Terhitung sejak beralihnya hak kepemilikan atas Obyek Jaminan Fidusia dan selama berlakunya perjanjian ini Obyek Jaminan Fidusia tersebut dikuasai oleh Pemberi Fidusia dalam hubungan pinjam pakai, dengan syarat sesuai peraturan perundangan-undangan yang berlaku dan ketentuan yang tercantum dalam akta ini.", f_normal, separator);
            
            AddParagraph.addParagraphV3(document, "Pasal 2", f_bold, separator);
            AddParagraph.addParagraph(document,0,0,null,"Pemberi Fidusia menjamin Penerima Fidusia bahwa : ", f_normal, separator);
            AddParagraph.addParagraphV2(document,0,18,new Chunk("- ",f_normal),new Chunk[] {new Chunk("Obyek Jaminan Fidusia adalah benar milik Pemberi Fidusia;",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,0,18,new Chunk("- ",f_normal),new Chunk[] {new Chunk("Obyek Jaminan Fidusia belum pernah dijual/dialihkan haknya dengan cara apapun kepada siapapun, sehingga ia berhak dan mempunyai kewenangan untuk mengalihkan hak kepemilikannya;",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,0,18,new Chunk("- ",f_normal),new Chunk[] {new Chunk("Obyek Jaminan Fidusia tidak dalam keadaan sedang dijaminkan kepada siapapun dan dengan cara apapun kepada pihak lain serta tidak tersangkut dalam suatu perkara atau disita.",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,0,18,new Chunk("- ",f_normal),new Chunk[] {new Chunk("Pemberi Fidusia dengan ini membebaskan dan/atau melepaskan Penerima Fidusia dari semua dan setiap tuntutan, gugatan atau tagihan yang mungkin diajukan oleh orang/pihak siapapun mengenai atau yang berhubungan dengan hal yang dijamin oleh Pemberi Fidusia tersebut di atas.",f_normal)}, separator);
            AddParagraph.addParagraphV3(document, "Pasal 3", f_bold, separator);
            AddParagraph.addParagraphV2(document,0,18,new Chunk("1. ",f_normal),new Chunk[] {new Chunk("Obyek Jaminan Fidusia hanya dapat dipergunakan oleh Pemberi Fidusia sesuai dengan sifat, dan peruntukannya, dengan tidak ada kewajiban bagi Pemberi Fidusia untuk membayar biaya/ganti rugi berupa apapun untuk pinjam pakai tersebut kepada Penerima Fidusia. Namun Pemberi Fidusia berkewajiban untuk memelihara Obyek Jaminan Fidusia tersebut dengan sebaik-baiknya dan semua tindakan yang diperlukan untuk pemeliharaan dan perbaikan atas Obyek Jaminan Fidusia atas biaya dan tanggungan Pemberi Fidusia sendiri, serta membayar pajak dan beban lainnya yang berkaitan dengan itu.",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,0,18,new Chunk("2. ",f_normal),new Chunk[] {new Chunk("Apabila untuk penggunaan atas Obyek Jaminan Fidusia tersebut diperlukan suatu kuasa khusus, maka Penerima Fidusia dengan ini memberi kuasa kepada Pemberi Fidusia untuk melakukan tindakan-tindakan yang diperlukan dalam rangka pinjam pakai Obyek Jaminan Fidusia tersebut.",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,0,18,new Chunk("3. ",f_normal),new Chunk[] {new Chunk("Selama berlakunya perjanjian ini Penerima Fidusia tidak bertanggung jawab kepada Pemberi Fidusia atau pihak lain berhubung dengan kerugian dan kerusakan Obyek Jaminan Fidusia (atau bagian dari padanya) atau kerugian, kerusakan yang ditimbulkan karyawan, pekerja, wakil, agen Pemberi Fidusia atau terhadap pihak ketiga yang disebabkan oleh penggunaan atau pengoperasian Obyek Jaminan Fidusia (atau bagian dari padanya).",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,0,18,new Chunk("4. ",f_normal),new Chunk[] {new Chunk("Pemberi Fidusia wajib, bilamana diminta, menjamin sepenuhnya dan melindungi Penerima Fidusia terhadap setiap tuntutan, tindakan, gugatan atau biaya (termasuk biaya penasehat hukum) yang timbul dari atau sehubungan dengan pemeliharaan, penggunaan, pengoperasian, kepemilikan atau keadaan Obyek Jaminan Fidusia ini.",f_normal)}, separator);
            
            AddParagraph.addParagraphV3(document, "Pasal 4", f_bold, separator);
            AddParagraph.addParagraphV2(document,0,18,new Chunk("1. ",f_normal),new Chunk[] {new Chunk("Penerima Fidusia atau wakilnya yang sah setiap waktu berhak dan dengan ini telah diberi kuasa dengan hak substitusi oleh Pemberi Fidusia untuk memeriksa tentang adanya dan tentang keadaan Obyek Jaminan Fidusia.",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,0,18,new Chunk("2. ",f_normal),new Chunk[] {new Chunk("Penerima Fidusia atas biaya Pemberi Fidusia berhak namun tidak diwajibkan untuk melakukan atau suruh melakukan segala sesuatu yang seharusnya dilakukan oleh Pemberi Fidusia atas Obyek Jaminan Fidusia dalam hal Pemberi Fidusia melalaikan kewajibannya untuk melaksanakan perbaikan dan/atau perawatan atas Obyek Jaminan Fidusia termasuk tetapi tidak terbatas untuk memasuki, gudang, bangunan, ruang atau tempat dimana Obyek Jaminan Fidusia disimpan atau berada.",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,0,18,new Chunk("3. ",f_normal),new Chunk[] {new Chunk("Pemberi Fidusia dan Penerima Fidusia menyatakan bahwa tindakan tersebut tidak merupakan tindakan memasuki tempat dan/atau bangunan tanpa izin (trespass).",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,0,18,new Chunk("4. ",f_normal),new Chunk[] {new Chunk("Atas permintaan tertulis dari Penerima Fidusia, Pemberi Fidusia wajib memasang suatu tanda pada Obyek Jaminan Fidusia untuk menunjukkan hak kepemilikan Penerima Fidusia atas Obyek Jaminan Fidusia dan tanda tersebut tidak boleh diubah, dirusak atau dihilangkan.",f_normal)}, separator);
            
            AddParagraph.addParagraphV3(document, "Pasal 5", f_bold, separator);
            AddParagraph.addParagraph(document,0,0,null,"Apabila bagian dari Obyek Jaminan Fidusia atau diantara Obyek Jaminan Fidusia tersebut ada yang hilang atau tidak dapat dipergunakan lagi, maka Pemberi Fidusia dengan ini berjanji dan karenanya mengikatkan diri untuk mengganti bagian dari Obyek Jaminan Fidusia yang hilang atau tidak dapat dipergunakan itu dengan Obyek Jaminan Fidusia lainnya yang sejenis yang nilainya setara dengan yang digantikan serta yang dapat disetujui Penerima Fidusia, sedang pengganti Obyek Jaminan Fidusia tersebut termasuk dalam jaminan fidusia yang dinyatakan dalam Akta ini. ", f_normal, separator);
            
            AddParagraph.addParagraphV3(document, "Pasal 6", f_bold, separator);
            AddParagraph.addParagraphV2(document,0,18,new Chunk("1. ",f_normal),new Chunk[] {new Chunk("Pemberi Fidusia tidak berhak untuk melakukan fidusia ulang atas Obyek Jaminan Fidusia.",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,0,18,new Chunk("2. ",f_normal),new Chunk[] {new Chunk("Pemberi Fidusia juga tidak berhak untuk membebankan dengan cara apapun, termasuk menggadaikan atau menjual atau mengalihkan dengan cara apapun Obyek Jaminan Fidusia kepada pihak lain tanpa persetujuan tertulis terlebih dahulu dari Penerima Fidusia",f_normal)}, separator);

            AddParagraph.addParagraphV3(document, "Pasal 7", f_bold, separator);
            AddParagraph.addParagraphV2(document,0,18,new Chunk("1. ",f_normal),new Chunk[] {new Chunk("Selama berlakunya perjanjian ini Pemberi Fidusia berjanji dan karenanya mengikatkan diri untuk mengasuransikan Obyek Jaminan Fidusia tersebut perusahaan asuransi yang ditunjuk atau disetujui oleh Penerima Fidusia terhadap bahaya kebakaran serta bahaya lainnya dan untuk suatu jumlah pertanggungan serta dengan persyaratan yang dipandang tepat oleh Penerima Fidusia.",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,0,18,new Chunk("2. ",f_normal),new Chunk[] {new Chunk("Semua uang premi asuransi harus ditanggung dan dibayar oleh Pemberi Fidusia atau Debitur.",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,0,18,new Chunk("3. ",f_normal),new Chunk[] {new Chunk("Pada polis asuransi tersebut harus dicantumkan klausula bahwa dalam hal terjadi kerugian, maka uang pengganti kerugiannya harus dibayarkan kepada Kreditur selaku satunya-satunya pihak yang berhak menuntut dan menerima uang ganti rugi asuransi dari  perusahaan asuransi yang bersangkutan, yang selanjutnya akan memperhitungkannya dengan jumlah yang masih harus dibayarkan oleh Debitur kepada Kreditur berdasarkan Perjanjian Pembiayaan tersebut, sedangkan sisanya jika masih ada harus dikembalikan oleh Kreditur kepada Pemberi Fidusia dengan tidak ada kewajiban bagi Kreditur untuk membayar bunga atau ganti kerugian berupa apapun atas jumlah sisa tersebut kepada Pemberi Fidusia.",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,0,18,new Chunk("4. ",f_normal),new Chunk[] {new Chunk("Apabila ternyata uang pengganti kerugian dari perusahaan asuransi tersebut tidak mencukupi, maka Debitur berkewajiban untuk membayar lunas sisa yang masih harus dibayar oleh Debitur kepada Kreditur.",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,0,18,new Chunk("5. ",f_normal),new Chunk[] {new Chunk("Pemberi Fidusia memberi kuasa kepada Penerima Fidusia untuk melakukan pemberitahuan kepada perusahaan asuransi, broker, underwriter, maupun perusahaan reasuransi tentang telah pengalihan hak kepemilikan atas Obyek Jaminan Fidusia secara kepercayaan sebagaimana dinyatakan dalam Akta ini.",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,0,18,new Chunk("6. ",f_normal),new Chunk[] {new Chunk("Apabila Pemberi Fidusia dan/atau Debitur lalai dan/atau tidak mengasuransikan Obyek Jaminan Fidusia tersebut, maka Penerima Fidusia berhak (namun tidak berkewajiban) dan seberapa perlu dengan ini kepadanya oleh Pemberi Fidusia diberi kuasa untuk mengasuransikan sendiri Obyek Jaminan Fidusia tersebut, dengan ketentuan bahwa premi asuransinya tetap harus dibayar oleh Pemberi Fidusia dan/atau Debitur.",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,0,18,new Chunk("7. ",f_normal),new Chunk[] {new Chunk("Asli polis asuransi dan perpanjangannya dikemudian hari serta kuitansi pembayaran premiasuransi tersebut harus diserahkan untuk disimpan oleh Penerima Fidusia dengan segera setelah diperoleh Pemberi Fidusia atau Debitur dari perusahaan asuransi tersebut.",f_normal)}, separator);

//            AddParagraph.addParagraphV2(document,0,18,new Chunk("- ",f_normal),new Chunk[] {new Chunk("Obyek Jaminan Fidusia adalah benar milik Pemberi Fidusia;",f_normal)}, separator);
            AddParagraph.addParagraphV3(document, "Pasal 8", f_bold, separator);
            AddParagraph.addParagraph(document,0,0,null," Dalam hal terjadi ketidak sepakatan tentang cidera janji atau kelalaian antara Debitur dengan Kreditur sebagaimana dimaksud dalam Perjanjian Pembiayaan serta Debitur/Pemberi Fidusia keberatan menyerahkan secara sukarela Obyek Jaminan Fidusia, keberatan tersebut dinyatakan secara tertulis oleh Debitur/Pemberi Fidusia, maka segala mekanisme dan prosedur hukum dalam pelaksanaan eksekusi Sertipikat Jaminan Fidusia harus dilakukan dan berlaku sama dengan pelaksanaan eksekusi putusan pengadilan yang telah berkekuatan tetap.", f_normal, separator);
            AddParagraph.addParagraphV3(document, "Pasal 9", f_bold, separator);
            AddParagraph.addParagraphV2(document,0,18,new Chunk("(i)",f_normal),new Chunk[] {new Chunk("Bilamana Debitur/Pemberi Fidusia dan Kreditur/Penerima Fidusia atas kesepakatan bersama serta Debitur/Pemberi Fidusia mengakui telah cidera janji yang terbukti dari pernyataan tertulis Debitur/Pemberi Fidusia, yakni tidak memenuhi dengan seksama kewajibannya menurut yang telah ditentukan dalam akta ini atau Debitur tidak memenuhi kewajiban berdasarkan Perjanjian Pembiayaan, tentang adanya pelanggaran atau kelalaian Debitur dalam memenuhi kewajiban tersebut maka dalam hal mana hak Pemberi Fidusia untuk meminjam pakai Obyek Jaminan Fidusia tersebut menjadi berakhir.",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,0,18,new Chunk("(ii)",f_normal),new Chunk[] {new Chunk("Dalam hal Penerima Fidusia mempergunakan hak-hak yang diberikan kepadanya seperti diuraikan dalam ayat (i) diatas,", f_normal),
            		new Chunk(" Pemberi Fidusia wajib dan mengikat diri sekarang ini untuk dipergunakan dikemudian hari pada waktunya, menyerahkan secara sukarela dalam keadaan terpelihara baik kepada Penerima Fidusia Obyek Jaminan",f_normal),
            		new Chunk(" Fidusia tersebut atas pemberitahuan atau teguran pertama dari Penerima Fidusia.",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,0,18,new Chunk("(iii)",f_normal),new Chunk[] {new Chunk("Dengan terjadinya hal ayat (ii) tersebut di atas, maka Penerima Fidusia atau kuasanya yang sah berhak dengan memperhatikan peraturan perundang-undangan yang berlaku,",f_normal),
            		new Chunk(" untuk menerima penyerahan secara sukarela dari Pemberi Fidusia Obyek Jaminan Fidusia dari tempat dimanapun Obyek Jaminan Fidusia tersebut berada, baik dari tangan Pemberi Fidusia maupun dari tangan pihak ketiga yang menguasainya, untuk Penerima Fidusia",f_normal),
            		new Chunk(" melakukan penjualan Benda Obyek Jaminan Fidusia. Penjualan benda yang menjadi Obyek Jaminan Fidusia atas kekuasaan Penerima Fidusia sendiri melalui pelelangan umum atau penjualan di bawah tangan yang dilakukan berdasarkan kesepakatan Pemberi dan",f_normal),
            		new Chunk(" Penerima Fidusia jika dengan cara demikian dapat diperoleh harga tertinggi yang menguntungkan para pihak.",f_normal)		
            }, separator);
            AddParagraph.addParagraphV2(document,0,18,new Chunk("(iv)",f_normal),new Chunk[] {new Chunk("Penerima Fidusia berhak untuk melakukan eksekusi Jaminan Fidusia atas dasar:",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,18,28,new Chunk("1) ",f_normal),new Chunk[] {new Chunk("titel eksekutorial;",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,18,28,new Chunk("2) ",f_normal),new Chunk[] {new Chunk("kekuasaan Penerima Fidusia sendiri melalui pelelangan dimuka umum; atau ",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,18,28,new Chunk("3) ",f_normal),new Chunk[] {new Chunk("kesepakatan Pemberi Fidusia dan Penerima Fidusia melalui penjualan di bawah tangan yang jika dengan cara demikian diperoleh harga tertinggi yang menguntungkan para pihak;",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,0,18,null,new Chunk[] {new Chunk("Penjualan dibawah tangan dilaksanakan setelah lewat waktu 1 (satu) bulan sejak diberitahukan secara tertulis oleh Pemberi dan/atau Penerima Fidusia kepada pihak-pihak yang berkepentingan dan diumumkan sedikitnya dalam 2 (dua) surat kabar harian yang berperedaran luas di wilayah Republik Indonesia.",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,0,18,null,new Chunk[] {new Chunk("Untuk keperluan eksekusi Obyek Jaminan Fidusia, Penerima Fidusia berhak:",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,18,28,new Chunk("- ",f_normal),new Chunk[] {new Chunk("menghadap dimana perlu, membuat atau suruh membuat serta menanda-tangani semua surat, Akta serta dokumen lain yang diperlukan;",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,18,28,new Chunk("- ",f_normal),new Chunk[] {new Chunk("menerima uang harga penjualan dan memberikan tanda penerimaan untuk itu;",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,18,28,new Chunk("- ",f_normal),new Chunk[] {new Chunk("menyerahkan apa yang dijual itu kepada pembelinya;",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,18,28,new Chunk("- ",f_normal),new Chunk[] {new Chunk("memperhitungkan atau mengkompensir uang harga penjualan yang diterimanya itu dengan semua apa yang wajib dibayar oleh Debitur kepada Kreditur;",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,18,28,new Chunk("- ",f_normal),new Chunk[] {new Chunk("dan selanjutnya Penerima Fidusia juga berhak untuk melakukan segala sesuatu yang dipandang perlu dan berguna dalam rangka eksekusi Obyek Jaminan Fidusia dengan tidak ada satupun yang dikecualikan.",f_normal)}, separator);

            AddParagraph.addParagraphV2(document,0,18,new Chunk("(v)",f_normal),new Chunk[] {new Chunk("Semua jumlah uang yang diterima dari pelaksanaan eksekusi Obyek Jaminan Fidusia akan dipergunakan sesuai urutan prioritas pembayaran sebagai berikut: ",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,18,28,new Chunk("1) ",f_normal),new Chunk[] {new Chunk("membayar ongkos dan biaya yang dikeluarkan berdasarkan Akta ini dengan memperhatikan peraturan perundang-undangan yang berlaku;",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,18,28,new Chunk("2) ",f_normal),new Chunk[] {new Chunk("membayar jumlah yang jatuh tempo atau yang harus dibayar berdasarkan Perjanjian Pembiayaan;",f_normal)}, separator);

            AddParagraph.addParagraphV2(document,0,18,null,new Chunk[] {new Chunk("Apabila hasil eksekusi Obyek Jaminan Fidusia tidak mencukupi untuk melunasi semua apa yang wajib dibayar oleh Debitur kepada Kreditur, maka Debitur tetap terikat membayar lunas sisa uang yang masih harus dibayar oleh Debitur kepada Kreditur.",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,0,18,null,new Chunk[] {new Chunk("Apabila hasil eksekusi Jaminan Fidusia setelah digunakan sesuai urutan pembayaran tersebut di atas masih ada kelebihan maka uang sisanya akan dikembalikan Penerima Fidusia kepada Pemberi Fidusia dengan tidak ada kewajiban bagi Penerima Fidusia untuk membayar bunga atau ganti kerugian berupa apapun juga kepada Pemberi Fidusia atau Debitur.",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,0,18,null,new Chunk[] {new Chunk("Untuk keperluan penjualan tersebut, maka Penerima Fidusia berhak menghadap dimana perlu, membuat atau suruh membuat serta menanda tangani semua surat, akta serta dokumen lain yang diperlukan,"
            		+ " menerima uang harga penjualan dan memberikan tanda penerimaan untuk itu, menyerahkan apa yang dijual itu kepada pembelinya, memperhitungkan uang harga penjualan yang diterimanya itu dengan semua apa yang wajib dibayar oleh Debitur"
            		+ " kepada Kreditur, akan tetapi dengan kewajiban bagi Penerima Fidusia untuk menyerahkan sisa uang penjualannya jika ada kepada Pemberi Fidusia, dengan tidak ada kewajiban bagi Penerima Fidusia untuk membayar bunga atau ganti"
            		+ " kerugian berupa apapun juga kepada Pemberi Fidusia atau Debitur mengenai sisa uang harga penjualan itu dan selanjutnya Penerima Fidusia juga berhak untuk melakukan segala sesuatu yang dipandang perlu dan berguna dalam rangka"
            		+ " penjualan Obyek Jaminan Fidusia tersebut dengan tidak ada satupun yang dikecualikan.",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,0,18,null,new Chunk[] {new Chunk("Apabila hasil penjualan dari Obyek Jaminan Fidusia tersebut tidak mencukupi untuk melunasi semua apa yang wajib dibayar oleh Debitur kepada Kreditur, maka Debitur tetap terikat membayar lunas sisa uang yang masih harus dibayar oleh Debitur kepada Kreditur.",f_normal)}, separator);
            AddParagraph.addParagraphV2(document,0,18,null,new Chunk[] {new Chunk("Apabila hasil eksekusi Jaminan Fidusia setelah digunakan sesuai urutan pembayaran tersebut di atas masih ada kelebihan maka uang sisanya akan dikembalikan Penerima Fidusia kepada Pemberi Fidusia dengan tidak ada kewajiban bagi Penerima Fidusia untuk membayar bunga atau ganti kerugian berupa apapun juga kepada Pemberi Fidusia atau Debitur.",f_normal)}, separator);
            
            AddParagraph.addParagraphV3(document, "Pasal 10", f_bold, separator);
            AddParagraph.addParagraph(document,0,0,null,"Pengalihan hak kepemilikan atas Obyek Jaminan Fidusia oleh Pemberi Fidusia kepada Penerima Fidusia dilakukan dengan syarat memutus (onder de ont bindende voorwaarden),"
            		+ " yaitu sampai dengan Debitur telah memenuhi/membayar lunas semua apa yang wajib dibayar oleh Debitur kepada Kreditur sebagaimana dinyatakan dalam Perjanjian tersebut, maka hak kepemilikan atas Obyek"
            		+ " Jaminan Fidusia dengan sendirinya beralih kembali kepada Pemberi Fidusia dan Kreditur harus membuat pernyataan hapusnya hutang Debitur, serta surat-surat yang berkenaan dengan Obyek Jaminan Fidusia"
            		+ " yang ada pada Penerima Fidusia wajib diserahkan kembali kepada Pemberi Fidusia.", f_normal, separator);
            AddParagraph.addParagraphV3(document, "Pasal 11", f_bold, separator);
            AddParagraph.addParagraph(document,0,0,null,"Penerima Fidusia atau kuasanya berhak untuk melaksanakan Pendaftaran Jaminan Fidusia yang dimaksudkan dalam Akta ini dan untuk keperluan tersebut menghadap dihadapan"
            		+ " pejabat atau instansi yang berwenang (termasuk Kantor Pendaftaran Fidusia), memberikan keterangan dan laporan, menandatangani surat/formulir, mendaftarkan Jaminan Fidusia atas Obyek Jaminan Fidusia"
            		+ " tersebut dengan melampirkan Pernyataan Pendaftaran Jaminan Fidusia, serta untuk mengajukan permohonan pendaftaran atas perubahan dalam hal terjadi perubahan atas data yang tercantum dalam Sertipikat"
            		+ " Jaminan Fidusia, selanjutnya menerima Sertipikat Jaminan Fidusia dan/atau Pernyataan Perubahan, serta dokumen-dokumen lain yang bertalian untuk keperluan itu membayar semua biaya dan menerima kuitansi"
            		+ " segala uang pembayaran serta selanjutnya segala tindakan yang perlu dan berguna untuk melaksanakan pendaftaran Jaminan Fidusia tersebut.", f_normal, separator);
            AddParagraph.addParagraphV3(document, "Pasal 12", f_bold, separator);
            AddParagraph.addParagraph(document,0,0,null,"Dalam rangka memenuhi ketentuan undang-undang Nomor 42 tahun 1999 (seribu sembilan ratus sembilan puluh sembilan) tentang Jaminan Fidusia, berikut dengan segala perubahan"
            		+ " dan peraturan pelaksanaannya Penerima Fidusia diberi kuasa dengan hak substitusi oleh Pemberi Fidusia untuk menjalankan dan/atau mempertahankan hak-hak Penerima Fidusia berdasarkan Akta ini, termasuk tetapi"
            		+ " tidak terbatas untuk melakukan perubahan atau penyesuaian atas ketentuan dalam Akta ini.", f_normal, separator);
            AddParagraph.addParagraph(document,0,0,null,"Pemberi Fidusia dengan ini menyanggupi pula, segera setelah menerima permintaan dari Penerima Fidusia, untuk melakukan tindakan apapun yang diperlukan guna melakukan pendaftaran,"
            		+ " serta untuk menanda tangani dan memberikan kepada Penerima Fidusia tambahan wewenang atau kuasa yang dianggap perlu atau baik oleh Penerima Fidusia untuk mempertahankan dan melaksanakan haknya berdasarkan Akta ini.", f_normal, separator);
            AddParagraph.addParagraphV3(document, "Pasal 13", f_bold, separator);
            AddParagraph.addParagraph(document,0,0,null,"Akta ini merupakan bagian yang terpenting dan tidak dapat dipisahkan dari Perjanjian tersebut, demikian pula kuasa yang diberikan dalam Akta ini merupakan bagian yang terpenting "
            		+ " serta tidak terpisahkan dari Akta ini tanpa adanya Akta ini dan kuasa tersebut, niscaya Perjanjian tersebut demikian pula Akta ini tidak akan diterima dan dilangsungkan diantara para pihak yang bersangkutan,"
            		+ " oleh karenanya kuasa ini tidak akan batal atau berakhir karena sebab yang dapat mengakhiri pemberian sesuatu kuasa, termasuk sebab yang disebutkan dalam Pasal 1813, 1814 dan 1816 Kitab Undang-Undang Hukum Perdata Indonesia.  ", f_normal, separator);
            AddParagraph.addParagraphV3(document, "Pasal 14", f_bold, separator);
            AddParagraph.addParagraph(document,0,0,null,"Segala perselisihan yang mungkin timbul di antara kedua belah pihak mengenai Akta ini yang tidak dapat diselesaikan di antara kedua belah pihak sendiri, maka kedua belah pihak akan memilih domisili hukum yang tetap dan umum di Kantor Panitera Pengadilan Negeri" + " " + notaris.getPengadilanNegeri() + ".", f_normal, separator);
            AddParagraph.addParagraph(document,0,0,null,"Pemilihan domisili hukum tersebut dilakukan dengan tidak mengurangi hak dari Penerima Fidusia untuk mengajukan tuntutan hukum terhadap Pemberi Fidusia berdasarkan Jaminan Fidusia atas Obyek Jaminan Fidusia tersebut dihadapan Pengadilan lainnya dalam wilayah Republik Indonesia, yaitu pada Pengadilan Negeri yang mempunyai yurisdiksi atas diri dari Pemberi Fidusia atau atas Obyek Jaminan Fidusia tersebut.", f_normal, separator);
            AddParagraph.addParagraphV3(document, "Pasal 15", f_bold, separator);
            AddParagraph.addParagraph(document,0,0,null,"Biaya Akta ini dan biaya lainnya yang berkenaan dengan pembuatan Akta ini dan pendaftaran fidusia ini di kantor Pendaftaran Fidusia maupun dalam melaksanakan ketentuan dalam Akta ini menjadi tanggungan dan harus dibayar oleh Pemberi Fidusia.", f_normal, separator);
            AddParagraph.addParagraphV3(document, "DEMIKIANLAH AKTA INI", f_bold, separator);
            AddParagraph.addParagraph(document,0,0,null,"Dibuat sebagai minuta dan diselesaikan di " + notaris.getKedudukan() + " pada hari dan tanggal tersebut pada bagian permulaan akta ini dengan dihadiri oleh :", f_normal, separator);

            AddParagraph.addParagraphV2(document,0,18,new Chunk("1.",f_normal),new Chunk[] {
                    new Chunk(akta.getSaksiOne().getTitel() + " " + akta.getSaksiOne().getNama(),f_bold),
                    new Chunk(", " + akta.getSaksiOne().getKomparisi() + ".", f_normal)}, separator);
            if(akta.getSaksiOne().getTts() != null && !akta.getSaksiOne().getTts().isEmpty()) {
                AddParagraph.addParagraphV2(document,18,0,null,new Chunk[] {
                        new Chunk(akta.getSaksiOne().getTts() + ".", f_normal)}, separator);
            }

            AddParagraph.addParagraphV2(document,0,18,new Chunk("2.",f_normal),new Chunk[] {
                    new Chunk(akta.getSaksiTwo().getTitel() + " " + akta.getSaksiTwo().getNama(),f_bold),
                    new Chunk(", " + akta.getSaksiTwo().getKomparisi() + ".", f_normal)}, separator);
            if(akta.getSaksiTwo().getTts() != null && !akta.getSaksiTwo().getTts().isEmpty()) {
                AddParagraph.addParagraphV2(document,18,0,null,new Chunk[] {
                        new Chunk(akta.getSaksiTwo().getTts() + ".", f_normal)}, separator);
            }

            AddParagraph.addParagraph(document,0,0,null,"- keduanya pegawai kantor Notaris yang saya Notaris kenal sebagai saksi-saksi. Undang-Undang Nomor 2 Tahun 2014 tentang Perubahan Atas Undang-Undang Nomor 30 Tahun 2004 Tentang Jabatan Notaris, akta ini tidak dibacakan oleh saya, Notaris karena penghadap dan saksi-saksi telah membaca sendiri, mengetahui dan memahami isi akta, maka akta ini ditandatangani oleh penghadap, para saksi dan saya, Notaris serta penghadap.", f_normal, separator);
            AddParagraph.addParagraph(document,0,0,null,"- Dilangsungkan dengan tanpa perubahan.", f_normal, separator);
            AddParagraph.addParagraph(document,0,0,null,"- Minuta Akta ini telah ditandatangani dengan sempurna.", f_normal, separator);

            document.add(Chunk.NEWLINE);

            Paragraph p3 = new Paragraph("PENGHADAP,", f_normal);
            p3.setLeading(LINE_RATIO, 0);
            p3.setAlignment(Element.ALIGN_CENTER);
            document.add(p3);
            document.add(Chunk.NEWLINE); document.add(Chunk.NEWLINE); document.add(Chunk.NEWLINE);document.add(Chunk.NEWLINE);
            Paragraph p4 = new Paragraph(penerimaKuasa.getNama().toUpperCase(), f_bold);
            p4.setLeading(LINE_RATIO, 0);
            p4.setAlignment(Element.ALIGN_CENTER);
            document.add(p4);

            document.add(Chunk.NEWLINE);

            Paragraph p5 = new Paragraph("SAKSI-SAKSI,", f_normal);
            p5.setLeading(LINE_RATIO, 0);
            p5.setAlignment(Element.ALIGN_CENTER);
            document.add(p5);
            document.add(Chunk.NEWLINE); document.add(Chunk.NEWLINE); document.add(Chunk.NEWLINE);
            PdfPTable table = new PdfPTable(3);
            table.setWidthPercentage(80);
            table.setTotalWidth(new float[]{ 185, 8, 185 });
            table.setLockedWidth(true);
            table.addCell(getCell(akta.getSaksiOne().getNama().toUpperCase(), f_bold, PdfPCell.ALIGN_CENTER));
            table.addCell(getCell(" ", f_bold, PdfPCell.ALIGN_CENTER));
            table.addCell(getCell(akta.getSaksiTwo().getNama().toUpperCase(), f_bold, PdfPCell.ALIGN_CENTER));
            document.add(table);

            document.add(Chunk.NEWLINE); document.add(Chunk.NEWLINE); document.add(Chunk.NEWLINE);

            Paragraph p6 = new Paragraph("NOTARIS," + " " + notaris.getKedudukan(), f_normal);
            p6.setLeading(LINE_RATIO, 0);
            p6.setAlignment(Element.ALIGN_CENTER);
            document.add(p6);
            document.add(Chunk.NEWLINE); document.add(Chunk.NEWLINE); document.add(Chunk.NEWLINE);document.add(Chunk.NEWLINE);document.add(Chunk.NEWLINE);
            Paragraph p7 = new Paragraph((notaris.getTitelShortOne() != null && notaris.getTitelShortOne().length() > 0 ? notaris.getTitelShortOne() + " " : "")
                    + notaris.getNama() + (notaris.getTitelShortTwo() != null && notaris.getTitelShortTwo().length() > 0 ? ", " + notaris.getTitelShortTwo() : ""), f_bold);
            p7.setLeading(LINE_RATIO, 0);
            p7.setAlignment(Element.ALIGN_CENTER);
            document.add(p7);

            //END

            document.close();
        } catch(Exception e) {
            log.error("Error on generateMinuta() for " + akta.getPpk().getNomor(), e);
            throw  e;
        }

        return file;
    }

    public PdfPCell getCell(String text, Font font, int alignment) {
        PdfPCell cell = new PdfPCell(new Phrase(text,font));
        cell.setPadding(0);
        cell.setHorizontalAlignment(alignment);
        cell.setBorder(PdfPCell.NO_BORDER);
        return cell;
    }
}
