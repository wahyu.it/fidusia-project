package com.fidusia.app.util;

import java.util.ArrayList;
import java.util.List;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.TabSettings;
import com.itextpdf.text.TabStop;

public class AddParagraph {
	
	private final static float WIDTH = 473f, WIDTH_V2 = 473f, LINE_RATIO = 16.5f;

    private static String BR = "<br>";

    public static void addParagraph(Document document, float left, float tab1, String bullet1, float tab2, String bullet2, String text, Font font, Chunk separator) throws DocumentException {
        java.util.List<Chunk> c = wrapLines(text, font, left + tab1 + tab2);
        for (int i = 0; i < c.size(); i++) {
            Paragraph p = new Paragraph();
            p.setLeading(LINE_RATIO, 0);
            if(left > 0)
                p.setIndentationLeft(left);
            
            if(tab1 > 0 || tab2 > 0) {
            	if(tab1 > 0 && tab2 > 0) {
            		List<TabStop> tabs = new ArrayList<>();
            		tabs.add(new TabStop(tab1));
            		tabs.add(new TabStop(tab2));
            		p.setTabSettings(new TabSettings(tabs));
            	} else if(tab1 > 0)
            		p.setTabSettings(new TabSettings(tab1));
        		else if(tab2 > 0)
        			p.setTabSettings(new TabSettings(tab2));
            }
            
            if(tab1 > 0 && bullet1 != null && i == 0) {
                p.add(new Chunk(bullet1, font));
                p.add(Chunk.TABBING);
            }
            
            if(tab2 > 0 && bullet2 != null && i == 0) {
                p.add(new Chunk(bullet2, font));
                p.add(Chunk.TABBING);
            }
            
            p.add(c.get(i));
            if(separator != null)
                p.add(separator);
            document.add(p);
        }
    }
    
//salinan
    public static void addParagraphV2(Document document, float left, float tab1, String bullet1, float tab2, String bullet2, String text, Font font, Chunk separator, ParagraphBorder border) throws DocumentException {
        java.util.List<Chunk> c = wrapLines(text, font, left + tab1 + tab2);
        for (int i = 0; i < c.size(); i++) {
            Paragraph p = new Paragraph();
            p.setLeading(LINE_RATIO, 0);
            if(left > 0)
                p.setIndentationLeft(left);
            
            if(tab1 > 0 || tab2 > 0) {
            	if(tab1 > 0 && tab2 > 0) {
            		List<TabStop> tabs = new ArrayList<>();
            		tabs.add(new TabStop(tab1));
            		tabs.add(new TabStop(tab2));
            		p.setTabSettings(new TabSettings(tabs));
            	} else if(tab1 > 0)
            		p.setTabSettings(new TabSettings(tab1));
        		else if(tab2 > 0)
        			p.setTabSettings(new TabSettings(tab2));
            }
            
            if(tab1 > 0 && bullet1 != null && i == 0) {
                p.add(new Chunk(bullet1, font));
                p.add(Chunk.TABBING);
            }
            
            if(tab2 > 0 && bullet2 != null && i == 0) {
                p.add(new Chunk(bullet2, font));
                p.add(Chunk.TABBING);
            }
            
            p.add(c.get(i));
            if(separator != null)
                p.add(separator);
            document.add(p);
        }
    }

    public static void addParagraph(Document document, float left, float tab, String bullet, String text, Font font, Chunk separator) throws DocumentException {
        java.util.List<Chunk> c = wrapLines(text, font, left + tab);
        for (int i = 0; i < c.size(); i++) {
            Paragraph p = new Paragraph();
            p.setLeading(LINE_RATIO, 0);
            if(left > 0)
                p.setIndentationLeft(left);
            if(tab > 0) {
                p.setTabSettings(new TabSettings(tab));
                if(bullet!= null && i == 0)
                    p.add(new Chunk(bullet, font));
                p.add(Chunk.TABBING);
            }
            p.add(c.get(i));
            if(separator != null)
                p.add(separator);
            document.add(p);
        }
    }

    public static void addParagraph(Document document, float left, float tab, String bullet, String text, Font font, Chunk separator, ParagraphBorder border) throws DocumentException {
        border.setIndentationLeft(left);

        java.util.List<Chunk> c = wrapLines(text, font, left + tab);
        for (int i = 0; i < c.size(); i++) {
            Paragraph p = new Paragraph();
            p.setLeading(LINE_RATIO, 0);
            if(left > 0)
                p.setIndentationLeft(left);
            if(tab > 0) {
                p.setTabSettings(new TabSettings(tab));
                if(bullet!= null) {
                    if(i == 0) {
                        p.add(new Chunk(bullet, font));
                        border.setIndentationLeft(!" ".equals(bullet) ? left : left + tab + 14); //handle specific case
                    } else {
                        border.setIndentationLeft(left + tab);
                    }
                }
                p.add(Chunk.TABBING);
            }
            p.add(c.get(i));
            if(separator != null)
                p.add(separator);
            document.add(p);
        }
    }

    public static void addParagraphV3(Document document, String text, Font font, Chunk separator) throws DocumentException {
        java.util.List<Chunk> results = new ArrayList<>();
        results.add(new Chunk(text, font));
        results.add(new Chunk(BR));
        Paragraph p = null;

        for(int i=0; i<results.size(); i++) {
            if(p == null) {
                p = new Paragraph();
                p.setLeading(LINE_RATIO, 0);
            }

            Chunk c = results.get(i);
            p.setIndentationLeft(0);

            if(c.getContent().equals(BR)) {
                p.add(separator);
                document.add(p);
                p = null;
            } else {
                p.add(separator);
                p.add(c);
            }
        }
    }
    
    public static void addParagraphV3(Document document, String text, Font font, Chunk separator, ParagraphBorder border) throws DocumentException {
        java.util.List<Chunk> results = new ArrayList<>();
        results.add(new Chunk(text, font));
        results.add(new Chunk(BR));
        Paragraph p = null;

        for(int i=0; i<results.size(); i++) {
            if(p == null) {
                p = new Paragraph();
                p.setLeading(LINE_RATIO, 0);
            }

            Chunk c = results.get(i);
            p.setIndentationLeft(0);

            if(c.getContent().equals(BR)) {
                p.add(separator);
                document.add(p);
                p = null;
            } else {
                p.add(separator);
                p.add(c);
            }
        }
    }

    public static void addParagraphV2(Document document, float left, float tab, Chunk bullet, Chunk[] chunks, Chunk separator) throws DocumentException {
        java.util.List<Chunk> results = wrapLines(left + tab, chunks);
        Paragraph p = null;

        for(int i=0; i<results.size(); i++) {
            if(p == null) {
                p = new Paragraph();
                p.setLeading(LINE_RATIO, 0);
                if(tab > 0) {
                    p.setTabSettings(new TabSettings(tab));
                    if(i == 0 && bullet !=  null) {
                        p.add(bullet);
                    }
                    p.add(Chunk.TABBING);
                }
            }

            p.setIndentationLeft(left);

            Chunk c = results.get(i);
            if(c.getContent().equals(BR)) {
                p.add(separator);
                document.add(p);
                p = null;
            } else {
                p.add(c);
            }
        }
    }

    public static void addParagraphV2(Document document, float left, float tab, Chunk bullet, Chunk[] chunks, Chunk separator, ParagraphBorder border) throws DocumentException {
        border.setIndentationLeft(bullet == null ? left + tab : left);

        java.util.List<Chunk> results = wrapLines(left + tab, chunks);
        Paragraph p = null;

        for(int i=0; i<results.size(); i++) {
            if(p == null) {
                p = new Paragraph();
                p.setLeading(LINE_RATIO, 0);
                if(tab > 0) {
                    p.setTabSettings(new TabSettings(tab));
                    if(bullet != null) {
                        if(i == 0) {
                            p.add(bullet);
                            border.setIndentationLeft(left);
                        } else {
                            border.setIndentationLeft(left + tab);
                        }
                    }

                    p.add(Chunk.TABBING);
                }
            }

            p.setIndentationLeft(left);

            Chunk c = results.get(i);
            if(c.getContent().equals(BR)) {
                p.add(separator);
                document.add(p);
                p = null;
            } else {
                p.add(c);
            }
        }
    }

    public static java.util.List<Chunk> wrapLines(float indentationLeft, Chunk[] chunks) {
        java.util.List<Chunk> results = new ArrayList<Chunk>();

        if(chunks != null && chunks.length > 0) {
            Chunk newline = new Chunk(BR);

            java.util.List<Chunk> tempChunks = new ArrayList<Chunk>();

            for (int i=0; i<chunks.length; i++) {
                Chunk c = chunks[i];

                String[] tokens = c.getContent().split(" ");
                for(int j=0; j<tokens.length; j++) {
                    if(j < (tokens.length - 1))
                        tempChunks.add(new Chunk(tokens[j] + " ", c.getFont()));
                    else
                        tempChunks.add(new Chunk(tokens[j], c.getFont()));
                }
            }
            float width = WIDTH_V2 - indentationLeft;
            boolean isNewline = false;
            float currentWidth = 0;
            for (int i=0; i<tempChunks.size(); i++) {
                Chunk c = tempChunks.get(i);

                if(currentWidth == 0)
                    currentWidth = c.getWidthPoint();

                if((i + 1) == tempChunks.size()) {
                    isNewline = true;
                } else {
                    Chunk nextChunk = tempChunks.get(i + 1);
                    String nextContent = nextChunk.getContent();
                    float nextWidth = nextChunk.getWidthPoint();

                    float tempWidth = currentWidth + nextWidth - (nextContent.endsWith(" ") ? 3.5f : 0);
                    if(tempWidth >= width) {
                        isNewline = true;
                    } else {
                        currentWidth = currentWidth + nextWidth;
                    }
                }

                if(isNewline) {
                    String content = c.getContent();
                    if(content.endsWith(" "))
                        results.add(new Chunk(content.substring(0, content.length() - 1), c.getFont()));
                    else
                        results.add(c);

                    results.add(newline);
                    currentWidth = 0;
                    isNewline = false;
                } else {
                    results.add(c);
                }
            }
        }

        return results;
    }

    public static java.util.List<Chunk> wrapLines(String text, Font font, float left) {
        if(text != null) {
            java.util.List<Chunk> chunks = new ArrayList<Chunk>();

            String[] tokens = text.split(" ");
            int length = tokens.length;
            int index = 0;
            float width = WIDTH - left;

            while(index < length) {
                Chunk c = new Chunk(tokens[index++],font);

                if (index < length) {
                    Chunk testc = new Chunk(" " + tokens[index], font);
                    while (c.getWidthPoint() + testc.getWidthPoint() <= width && index < length) {
                        c.append(" ").append(tokens[index++]);
                        if (index < length)
                            testc = new Chunk(" " + tokens[index], font);
                    }
                }
                chunks.add(c);
            }

            return chunks;
        }

        return null;
    }

}
