package com.fidusia.app.web.rest;

import com.fidusia.app.domain.LogBook;
import com.fidusia.app.repository.LogBookRepository;
import com.fidusia.app.util.DateUtils;

import io.github.jhipster.web.util.PaginationUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.time.LocalDate;
import java.util.*;


@RestController
@RequestMapping("/api")
public class LogBookResource {

    private final Logger log = LoggerFactory.getLogger(LogBookResource.class);

    private final LogBookRepository logBookRepository;

    public LogBookResource(LogBookRepository logBookRepository) {
    	this.logBookRepository = logBookRepository;
    }


    @GetMapping("/log-books")
    public ResponseEntity<List<LogBook>> getAllLogBook(
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate,
			@RequestParam(value = "kategori", required = false) String kategori,
			Pageable pageable) {
        log.debug("REST request to get a page of log book start:{} s/d end:{} kategori:{}",startDate, endDate , kategori);
        
        LocalDate pStart = ((startDate != null && !startDate.isEmpty()) ? DateUtils.parseDate(startDate, DateUtils.FORMATTER_ISO_LOCAL_DATE_V1) : LocalDate.now().withDayOfMonth(1));
        LocalDate pEnd = ((endDate != null && !endDate.isEmpty()) ? DateUtils.parseDate(endDate, DateUtils.FORMATTER_ISO_LOCAL_DATE_V1) : LocalDate.now().withDayOfMonth(LocalDate.now().lengthOfMonth()));
    	log.debug("start : " + pStart + " end: " + pEnd);
        Page<LogBook> page = logBookRepository.findByTanggalBetweenAndKategori(pStart, pEnd, kategori, pageable);
        
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
