package com.fidusia.app.repository;

import com.fidusia.app.domain.Notaris;
import com.fidusia.app.domain.Saksi;
import com.fidusia.app.domain.util.SaksiStatus;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Saksi entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SaksiRepository extends JpaRepository<Saksi, Long> {

	List<Saksi> findByRecordStatusAndNotaris(SaksiStatus status, Notaris notaris);

	List<Saksi> findByRecordStatusAndNotaris(int value, Notaris notaris);

	List<Saksi> findTop2ByNotarisAndRecordStatus(Notaris notaris, int value);}
