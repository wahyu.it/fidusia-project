package com.fidusia.app.web.rest;

import com.fidusia.app.domain.RawData;
import com.fidusia.app.repository.RawDataRepository;
import com.fidusia.app.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.fidusia.app.domain.RawData}.
 */
@RestController
@RequestMapping("/api/raw-data")
@Transactional
public class RawDataResource {

    private final Logger log = LoggerFactory.getLogger(RawDataResource.class);

    private static final String ENTITY_NAME = "rawData";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RawDataRepository rawDataRepository;

    public RawDataResource(RawDataRepository rawDataRepository) {
        this.rawDataRepository = rawDataRepository;
    }

    /**
     * {@code POST  /raw-data} : Create a new rawData.
     *
     * @param rawData the rawData to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new rawData, or with status {@code 400 (Bad Request)} if the rawData has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<RawData> createRawData(@RequestBody RawData rawData) throws URISyntaxException {
        log.debug("REST request to save RawData : {}", rawData);
        if (rawData.getId() != null) {
            throw new BadRequestAlertException("A new rawData cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RawData result = rawDataRepository.save(rawData);
        return ResponseEntity
            .created(new URI("/api/raw-data/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /raw-data/:id} : Updates an existing rawData.
     *
     * @param id the id of the rawData to save.
     * @param rawData the rawData to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated rawData,
     * or with status {@code 400 (Bad Request)} if the rawData is not valid,
     * or with status {@code 500 (Internal Server Error)} if the rawData couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<RawData> updateRawData(@PathVariable(value = "id", required = false) final Long id, @RequestBody RawData rawData)
        throws URISyntaxException {
        log.debug("REST request to update RawData : {}, {}", id, rawData);
        if (rawData.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, rawData.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!rawDataRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        RawData result = rawDataRepository.save(rawData);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, rawData.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /raw-data/:id} : Partial updates given fields of an existing rawData, field will ignore if it is null
     *
     * @param id the id of the rawData to save.
     * @param rawData the rawData to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated rawData,
     * or with status {@code 400 (Bad Request)} if the rawData is not valid,
     * or with status {@code 404 (Not Found)} if the rawData is not found,
     * or with status {@code 500 (Internal Server Error)} if the rawData couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<RawData> partialUpdateRawData(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody RawData rawData
    ) throws URISyntaxException {
        log.debug("REST request to partial update RawData partially : {}, {}", id, rawData);
        if (rawData.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, rawData.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!rawDataRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<RawData> result = rawDataRepository
            .findById(rawData.getId())
            .map(existingRawData -> {
                if (rawData.getUniqueKey() != null) {
                    existingRawData.setUniqueKey(rawData.getUniqueKey());
                }
                if (rawData.getOrderDate() != null) {
                    existingRawData.setOrderDate(rawData.getOrderDate());
                }
                if (rawData.getFileName() != null) {
                    existingRawData.setFileName(rawData.getFileName());
                }
                if (rawData.getLine() != null) {
                    existingRawData.setLine(rawData.getLine());
                }
//                if (rawData.getDelimeter() != null) {
//                    existingRawData.setDelimeter(rawData.getDelimeter());
//                }
                if (rawData.getLineNumber() != null) {
                    existingRawData.setLineNumber(rawData.getLineNumber());
                }
                if (rawData.getValid() != null) {
                    existingRawData.setValid(rawData.getValid());
                }
                if (rawData.getInvalidReason() != null) {
                    existingRawData.setInvalidReason(rawData.getInvalidReason());
                }
                if (rawData.getStatus() != null) {
                    existingRawData.setStatus(rawData.getStatus());
                }

                return existingRawData;
            })
            .map(rawDataRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, rawData.getId().toString())
        );
    }

    /**
     * {@code GET  /raw-data} : get all the rawData.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of rawData in body.
     */
    @GetMapping("")
    public ResponseEntity<List<RawData>> getAllRawData(Pageable pageable) {
        log.debug("REST request to get a page of RawData");
        Page<RawData> page = rawDataRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /raw-data/:id} : get the "id" rawData.
     *
     * @param id the id of the rawData to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the rawData, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<RawData> getRawData(@PathVariable("id") Long id) {
        log.debug("REST request to get RawData : {}", id);
        Optional<RawData> rawData = rawDataRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(rawData);
    }

    /**
     * {@code DELETE  /raw-data/:id} : delete the "id" rawData.
     *
     * @param id the id of the rawData to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteRawData(@PathVariable("id") Long id) {
        log.debug("REST request to delete RawData : {}", id);
        rawDataRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
