import { Component, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { JhiEventManager } from 'ng-jhipster';
import { HttpClient } from '@angular/common/http';

import { LoginService } from 'app/core/login/login.service';
import { StateStorageService } from 'app/core/auth/state-storage.service';
import { FormBuilder } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Login } from 'app/core/login/login.model';
import { Account } from 'app/core/user/account.model';
import { AccountService } from 'app/core/auth/account.service';
import { AuthServerProvider } from 'app/core/auth/auth-jwt.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'jhi-login',
  templateUrl: './login.component.html',
  styleUrls: ['login.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  authenticationError!: boolean;
  password!: string;
  rememberMe!: boolean;
  username!: string;
  code!: string;
  credentials: any;
  nativeElement: Node;
  hide!: boolean;
  cekCode = true;
  inCode = false;
  gagalLogin = false;
  account?: Account | null;
  email!: string;
  loginuser!: string;
  display: any;
  salahpass= false;
  userDeactive= false;

  constructor(
	private http: HttpClient,
    private element: ElementRef,
    private eventManager: JhiEventManager,
    private loginService: LoginService,
    private stateStorageService: StateStorageService,
    private elementRef: ElementRef,
    private router: Router,
    private fb: FormBuilder,
    public activeModal: NgbActiveModal,
    private authServerProvider: AuthServerProvider,
    private accountService: AccountService
  ) {
    this.nativeElement = element.nativeElement;
    this.credentials = {};
  }
  
  timer(minute: any): void {
	    // let minute = 1;
	    let seconds: number = minute;
	    let textSec: any = "0";
	    let statSec: any = 60;
	
	    const prefix = minute < 10 ? "0" : "";
	
	    const time = setInterval(() => {
	      seconds--;
	      if (statSec !== 0) statSec--;
	      else statSec = 59;
	
	      if (statSec < 10) {
	        textSec = "0" + statSec;
	      } else textSec = statSec;
	
	      this.display = `${prefix}${Math.floor(seconds / 60)}:${textSec}`;
	
	      if (seconds === 0) {
	        clearInterval(time);
	        this.inCode= false;
	        this.cancelCek();
	      }
	    }, 1000);
	    
	   /* if (!this.cekCode)
	     clearInterval(time);*/
	  }

  ngOnInit(): void {
    this.hide = true;
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('login-page');
    body.classList.add('off-canvas-sidebar');

    const cards = document.getElementsByClassName('card');
    let card: any;
    for (let i = 0; i < cards.length; i++) {
      if (cards[i].id === 'card-login') {
        card = cards[i];
      }
    }
    
    setTimeout(function(): void {
      // after 1000 ms we add the class animated to the login/register card
      if (card) {
        card.classList.remove('card-hidden');
      }
    }, 700);
  }

  ngOnDestroy(): void {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('login-page');
    body.classList.remove('off-canvas-sidebar');
  }

  cancel(): void {
    this.credentials = {
      username: null,
      password: null,
      rememberMe: true
    };
    this.authenticationError = false;
    this.email= '';
  }
  
  masuk(code: string): void {
    this.loginService.verifyOtp(code, this.loginuser).subscribe(result=>{
		if (result) {
			this.login();
		} else {
          this.authenticationError = true;
		}
	})
  }
  
  active(credentials: Login): any {
      this.loginService.status({
        username: credentials.username,
        password: credentials.password,
        rememberMe: credentials.rememberMe
      }).subscribe(rest=>{
	     console.log("rest: " + rest)
		return rest;
	})
  }

  login(): void {
    this.loginService
      .login({
        username: this.username,
        password: this.password,
        rememberMe: this.rememberMe
      })
      .then(() => {
        this.authenticationError = false;
        if (
          this.router.url === '/account/register' ||
          this.router.url.startsWith('/account/activate') ||
          this.router.url.startsWith('/account/reset/')
        ) {
          this.router.navigate(['']);
        }
        this.router.navigate(['']);
        this.eventManager.broadcast({
          name: 'authenticationSuccess',
          content: 'Sending Authentication Success'
        });

        // previousState was set in the authExpiredInterceptor before being redirected to login modal.
        // since login is successful, go to stored previousState and clear previousState
        const redirect = this.stateStorageService.getUrl();
        if (redirect) {
          this.stateStorageService.storeUrl('');
          this.router.navigate([redirect]);
        }
        // console.log("login sukses...")
      })
      .catch(() => {
        this.authenticationError = true;
      });
  }
  
  loginUserPass(): any {
    this.login2({
        username: this.username,
        password: this.password,
        rememberMe: this.rememberMe
      }) 
  }

  login2(credentials: Login): void {
	  
	this.loginService.status({
        username: credentials.username,
        password: credentials.password,
        rememberMe: credentials.rememberMe
      }).subscribe(rest=>{
	     console.log("rest: " + rest)
	   if (!rest) {
		     console.log("this.active(): " + rest + 'user blok')
		     this.cekCode= true;
			 this.blockedUser(); 
		  } else {
		     console.log("this.active(): " + rest + 'user active')
		    this.authServerProvider.login(credentials).subscribe(() => {
		          this.accountService.identity(true).subscribe((account) => {
				  this.email = account!.email;
				  this.loginuser = account!.login;
		      	  this.gagalLogin=false;
		          this.logout2();
		          });
		      });
	      }
	     
	})
  }

  pass(): void {
    this.hide = !this.hide;
  }

  register(): void {
    this.activeModal.dismiss('to state register');
    this.router.navigate(['/account/register']);
  }

  isMobileMenu(): any {
    if (window.innerWidth > 991) {
      return false;
    }
    return true;
  }
  
  ceklogin(): void {
	this.loginService.status({
        username: this.username,
        password: this.password,
        rememberMe: this.rememberMe
      }).subscribe(rest=>{
	   if (!rest) {
		     this.cekCode= true;
			 this.blockedUser(); 
		  } else {
			  this.authServerProvider.login2({
		        username: this.username,
		        password: this.password,
		        rememberMe: this.rememberMe
		      }).subscribe((result) => {
				  if (result) {
		        	  this.authenticationError = false;
					  this.loginUserPass();
			          this.cekCode=false;
				  } else {
		        	  this.authenticationError = true;
				  }
				  
		      });
	      }
	})
  }
  
  inputCode():void {
	  this.inCode=true;
	  this.loginService.sendCodeToEmail(this.loginuser).subscribe(()=>{
	  this.timer(120);
	  })
  }
  
  cancelCek():void {
	  this.cekCode=true;
	  this.inCode=false;
	  this.cancel();
  }
  
  logout2(): void {
	this.salahpass = true;
	console.log('this.salahpass : ' + this.salahpass)
    this.authServerProvider.logout().subscribe();
    this.accountService.authenticate(null);
  }
  
  logout(): void {
    this.authServerProvider.logout().subscribe();
    this.accountService.authenticate(null);
  }
  
  blockedUser(): void {
    Swal.fire({ 
	  customClass: {
	    container: 'my-swal'
	  },
      icon: 'error',
      text: 'Akun anda telah terblokir karena tercatat gagal login lebih dari 5x. Mohon menghubungi tim support kami',
    } as any);
  }
}
