package com.fidusia.app.web.rest;

import com.fidusia.app.domain.Orang;
import com.fidusia.app.repository.OrangRepository;
import com.fidusia.app.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.fidusia.app.domain.Orang}.
 */
@RestController
@RequestMapping("/api/orangs")
@Transactional
public class OrangResource {

    private final Logger log = LoggerFactory.getLogger(OrangResource.class);

    private static final String ENTITY_NAME = "orang";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OrangRepository orangRepository;

    public OrangResource(OrangRepository orangRepository) {
        this.orangRepository = orangRepository;
    }

    /**
     * {@code POST  /orangs} : Create a new orang.
     *
     * @param orang the orang to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new orang, or with status {@code 400 (Bad Request)} if the orang has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<Orang> createOrang(@RequestBody Orang orang) throws URISyntaxException {
        log.debug("REST request to save Orang : {}", orang);
        if (orang.getId() != null) {
            throw new BadRequestAlertException("A new orang cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Orang result = orangRepository.save(orang);
        return ResponseEntity
            .created(new URI("/api/orangs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /orangs/:id} : Updates an existing orang.
     *
     * @param id the id of the orang to save.
     * @param orang the orang to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated orang,
     * or with status {@code 400 (Bad Request)} if the orang is not valid,
     * or with status {@code 500 (Internal Server Error)} if the orang couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<Orang> updateOrang(@PathVariable(value = "id", required = false) final Long id, @RequestBody Orang orang)
        throws URISyntaxException {
        log.debug("REST request to update Orang : {}, {}", id, orang);
        if (orang.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, orang.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!orangRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Orang result = orangRepository.save(orang);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, orang.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /orangs/:id} : Partial updates given fields of an existing orang, field will ignore if it is null
     *
     * @param id the id of the orang to save.
     * @param orang the orang to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated orang,
     * or with status {@code 400 (Bad Request)} if the orang is not valid,
     * or with status {@code 404 (Not Found)} if the orang is not found,
     * or with status {@code 500 (Internal Server Error)} if the orang couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Orang> partialUpdateOrang(@PathVariable(value = "id", required = false) final Long id, @RequestBody Orang orang)
        throws URISyntaxException {
        log.debug("REST request to partial update Orang partially : {}, {}", id, orang);
        if (orang.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, orang.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!orangRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Orang> result = orangRepository
            .findById(orang.getId())
            .map(existingOrang -> {
                if (orang.getPenggunaan() != null) {
                    existingOrang.setPenggunaan(orang.getPenggunaan());
                }
                if (orang.getNama() != null) {
                    existingOrang.setNama(orang.getNama());
                }
                if (orang.getJenisKelamin() != null) {
                    existingOrang.setJenisKelamin(orang.getJenisKelamin());
                }
                if (orang.getStatusKawin() != null) {
                    existingOrang.setStatusKawin(orang.getStatusKawin());
                }
                if (orang.getKelahiran() != null) {
                    existingOrang.setKelahiran(orang.getKelahiran());
                }
                if (orang.getTglLahir() != null) {
                    existingOrang.setTglLahir(orang.getTglLahir());
                }
                if (orang.getPekerjaan() != null) {
                    existingOrang.setPekerjaan(orang.getPekerjaan());
                }
                if (orang.getWargaNegara() != null) {
                    existingOrang.setWargaNegara(orang.getWargaNegara());
                }
                if (orang.getJenisId() != null) {
                    existingOrang.setJenisId(orang.getJenisId());
                }
                if (orang.getNoId() != null) {
                    existingOrang.setNoId(orang.getNoId());
                }
                if (orang.getAlamat() != null) {
                    existingOrang.setAlamat(orang.getAlamat());
                }
                if (orang.getRt() != null) {
                    existingOrang.setRt(orang.getRt());
                }
                if (orang.getRw() != null) {
                    existingOrang.setRw(orang.getRw());
                }
                if (orang.getKelurahan() != null) {
                    existingOrang.setKelurahan(orang.getKelurahan());
                }
                if (orang.getKecamatan() != null) {
                    existingOrang.setKecamatan(orang.getKecamatan());
                }
                if (orang.getKota() != null) {
                    existingOrang.setKota(orang.getKota());
                }
                if (orang.getProvinsi() != null) {
                    existingOrang.setProvinsi(orang.getProvinsi());
                }
                if (orang.getKodePos() != null) {
                    existingOrang.setKodePos(orang.getKodePos());
                }
                if (orang.getNoKontak() != null) {
                    existingOrang.setNoKontak(orang.getNoKontak());
                }

                return existingOrang;
            })
            .map(orangRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, orang.getId().toString())
        );
    }

    /**
     * {@code GET  /orangs} : get all the orangs.
     *
     * @param pageable the pagination information.
     * @param filter the filter of the request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of orangs in body.
     */
    @GetMapping("")
    public ResponseEntity<List<Orang>> getAllOrangs(Pageable pageable,
        @RequestParam(name = "filter", required = false) String filter
    ) {
//        if ("ppk-is-null".equals(filter)) {
//            log.debug("REST request to get all Orangs where ppk is null");
//            return new ResponseEntity<>(
//                StreamSupport.stream(orangRepository.findAll().spliterator(), false).filter(orang -> orang.getPpk() == null).toList(),
//                HttpStatus.OK
//            );
//        }
        log.debug("REST request to get a page of Orangs");
        Page<Orang> page = orangRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /orangs/:id} : get the "id" orang.
     *
     * @param id the id of the orang to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the orang, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<Orang> getOrang(@PathVariable("id") Long id) {
        log.debug("REST request to get Orang : {}", id);
        Optional<Orang> orang = orangRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(orang);
    }

    /**
     * {@code DELETE  /orangs/:id} : delete the "id" orang.
     *
     * @param id the id of the orang to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteOrang(@PathVariable("id") Long id) {
        log.debug("REST request to delete Orang : {}", id);
        orangRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
