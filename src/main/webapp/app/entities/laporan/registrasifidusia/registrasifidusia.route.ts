import { Routes } from '@angular/router';
import { RegistrasiFidusiaComponent } from './registrasifidusia.component';
import { JhiResolvePagingParams } from 'ng-jhipster';

export const registrasiFidusiaState: Routes = [{
  path: '',
  component: RegistrasiFidusiaComponent,
  resolve: {
	  pagingParams: JhiResolvePagingParams
  }
  }
];
