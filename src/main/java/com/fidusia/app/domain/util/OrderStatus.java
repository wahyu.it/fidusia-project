package com.fidusia.app.domain.util;

public enum OrderStatus {
	CANCEL(0), NEW(1), ORDERED(2);
	
	private int value;
	
	OrderStatus (int value) {
		this.value = value;
	}

    public int value() {
        return value;
    }
}
