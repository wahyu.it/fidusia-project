package com.fidusia.app.repository;

import com.fidusia.app.domain.OrderKerja;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the OrderKerja entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrderKerjaRepository extends JpaRepository<OrderKerja, Long> {}
