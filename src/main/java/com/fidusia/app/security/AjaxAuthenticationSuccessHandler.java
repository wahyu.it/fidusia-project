package com.fidusia.app.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.fidusia.app.service.BlacklistAddressService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Spring Security success handler, specialized for Ajax requests.
 */
@Component
public class AjaxAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

	private static final String[] HEADERS_TO_TRY = {
            "X-Forwarded-For",
            "Proxy-Client-IP",
            "WL-Proxy-Client-IP",
            "HTTP_X_FORWARDED_FOR",
            "HTTP_X_FORWARDED",
            "HTTP_X_CLUSTER_CLIENT_IP",
            "HTTP_CLIENT_IP",
            "HTTP_FORWARDED_FOR",
            "HTTP_FORWARDED",
            "HTTP_VIA",
            "REMOTE_ADDR"};
    
	private final BlacklistAddressService blacklistAddressService;
	
	public AjaxAuthenticationSuccessHandler (BlacklistAddressService blacklistAddressService) {
		this.blacklistAddressService=blacklistAddressService;
	}

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
        Authentication authentication)
        throws IOException, ServletException {
    	
    	try {
    		String clientIp = getClientIpAddress(request);
    		if(blacklistAddressService.isBlacklisted(clientIp)) {
    			request.logout();
    			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
    			return;
    		}
    		
        	blacklistAddressService.resetUnauhtorizedCounter(clientIp);
    	} catch (Exception e) {}
    	
        response.setStatus(HttpServletResponse.SC_OK);
    }

    private String getClientIpAddress(HttpServletRequest request) {
        for (String header : HEADERS_TO_TRY) {
            String ip = request.getHeader(header);
            if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
                return ip;
            }
        }
        
        return request.getRemoteAddr();
    }
}
