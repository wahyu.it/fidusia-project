package com.fidusia.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Notaris.
 */
@Entity
@Table(name = "notaris")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Notaris implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "nama")
    private String nama;

    @Column(name = "nama_short")
    private String namaShort;

    @Column(name = "titel_short_one")
    private String titelShortOne;

    @Column(name = "titel_short_two")
    private String titelShortTwo;

    @Column(name = "titel_long_one")
    private String titelLongOne;

    @Column(name = "titel_long_two")
    private String titelLongTwo;

    @Column(name = "wilayah_kerja")
    private String wilayahKerja;

    @Column(name = "kedudukan")
    private String kedudukan;

    @Column(name = "sk")
    private String sk;

    @Column(name = "tgl_sk")
    private LocalDate tglSk;

    @Column(name = "jenis_hari_kerja")
    private Integer jenisHariKerja;

    @Column(name = "jam_kerja_awal_one")
    private ZonedDateTime jamKerjaAwalOne;

    @Column(name = "jam_kerja_akhir_one")
    private ZonedDateTime jamKerjaAkhirOne;

    @Column(name = "jam_kerja_awal_two")
    private ZonedDateTime jamKerjaAwalTwo;

    @Column(name = "jam_kerja_akhir_two")
    private ZonedDateTime jamKerjaAkhirTwo;

    @Column(name = "selisih_pukul")
    private Integer selisihPukul;

    @Column(name = "jam_rest_awal_one")
    private ZonedDateTime jamRestAwalOne;

    @Column(name = "jam_rest_akhir_one")
    private ZonedDateTime jamRestAkhirOne;

    @Column(name = "jam_rest_awal_two")
    private ZonedDateTime jamRestAwalTwo;

    @Column(name = "jam_rest_akhir_two")
    private ZonedDateTime jamRestAkhirTwo;

    @Column(name = "maks_akta")
    private Integer maksAkta;

    @Column(name = "maks_order_harian")
    private Integer maksOrderHarian;

    @Column(name = "lembar_sk")
    private String lembarSk;

    @Column(name = "lembar_sumpah")
    private String lembarSumpah;

    @Column(name = "alamat_kantor")
    private String alamatKantor;

    @Column(name = "alamat_email")
    private String alamatEmail;

    @Column(name = "no_kontak")
    private String noKontak;

    @Column(name = "record_status")
    private Integer recordStatus;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "update_on")
    private ZonedDateTime updateOn;

    @OneToOne
    @JoinColumn(unique = true)
    private PenerimaKuasa penerimaKuasa;

    @NotNull
    @Size(max = 100)
    @Column(name = "nama_ahu", length = 100, nullable = false)
    private String namaAhu;

    @Column(name = "pengadilan_negeri")
    private String pengadilanNegeri;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Notaris id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNama() {
        return this.nama;
    }

    public Notaris nama(String nama) {
        this.setNama(nama);
        return this;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNamaShort() {
        return this.namaShort;
    }

    public Notaris namaShort(String namaShort) {
        this.setNamaShort(namaShort);
        return this;
    }

    public void setNamaShort(String namaShort) {
        this.namaShort = namaShort;
    }

    public String getTitelShortOne() {
        return this.titelShortOne;
    }

    public Notaris titelShortOne(String titelShortOne) {
        this.setTitelShortOne(titelShortOne);
        return this;
    }

    public void setTitelShortOne(String titelShortOne) {
        this.titelShortOne = titelShortOne;
    }

    public String getTitelShortTwo() {
        return this.titelShortTwo;
    }

    public Notaris titelShortTwo(String titelShortTwo) {
        this.setTitelShortTwo(titelShortTwo);
        return this;
    }

    public void setTitelShortTwo(String titelShortTwo) {
        this.titelShortTwo = titelShortTwo;
    }

    public String getTitelLongOne() {
        return this.titelLongOne;
    }

    public Notaris titelLongOne(String titelLongOne) {
        this.setTitelLongOne(titelLongOne);
        return this;
    }

    public void setTitelLongOne(String titelLongOne) {
        this.titelLongOne = titelLongOne;
    }

    public String getTitelLongTwo() {
        return this.titelLongTwo;
    }

    public Notaris titelLongTwo(String titelLongTwo) {
        this.setTitelLongTwo(titelLongTwo);
        return this;
    }

    public void setTitelLongTwo(String titelLongTwo) {
        this.titelLongTwo = titelLongTwo;
    }

    public String getWilayahKerja() {
        return this.wilayahKerja;
    }

    public Notaris wilayahKerja(String wilayahKerja) {
        this.setWilayahKerja(wilayahKerja);
        return this;
    }

    public void setWilayahKerja(String wilayahKerja) {
        this.wilayahKerja = wilayahKerja;
    }

    public String getKedudukan() {
        return this.kedudukan;
    }

    public Notaris kedudukan(String kedudukan) {
        this.setKedudukan(kedudukan);
        return this;
    }

    public void setKedudukan(String kedudukan) {
        this.kedudukan = kedudukan;
    }

    public String getSk() {
        return this.sk;
    }

    public Notaris sk(String sk) {
        this.setSk(sk);
        return this;
    }

    public void setSk(String sk) {
        this.sk = sk;
    }

    public LocalDate getTglSk() {
        return this.tglSk;
    }

    public Notaris tglSk(LocalDate tglSk) {
        this.setTglSk(tglSk);
        return this;
    }

    public void setTglSk(LocalDate tglSk) {
        this.tglSk = tglSk;
    }

    public Integer getJenisHariKerja() {
        return this.jenisHariKerja;
    }

    public Notaris jenisHariKerja(Integer jenisHariKerja) {
        this.setJenisHariKerja(jenisHariKerja);
        return this;
    }

    public void setJenisHariKerja(Integer jenisHariKerja) {
        this.jenisHariKerja = jenisHariKerja;
    }

    public ZonedDateTime getJamKerjaAwalOne() {
        return this.jamKerjaAwalOne;
    }

    public Notaris jamKerjaAwalOne(ZonedDateTime jamKerjaAwalOne) {
        this.setJamKerjaAwalOne(jamKerjaAwalOne);
        return this;
    }

    public void setJamKerjaAwalOne(ZonedDateTime jamKerjaAwalOne) {
        this.jamKerjaAwalOne = jamKerjaAwalOne;
    }

    public ZonedDateTime getJamKerjaAkhirOne() {
        return this.jamKerjaAkhirOne;
    }

    public Notaris jamKerjaAkhirOne(ZonedDateTime jamKerjaAkhirOne) {
        this.setJamKerjaAkhirOne(jamKerjaAkhirOne);
        return this;
    }

    public void setJamKerjaAkhirOne(ZonedDateTime jamKerjaAkhirOne) {
        this.jamKerjaAkhirOne = jamKerjaAkhirOne;
    }

    public ZonedDateTime getJamKerjaAwalTwo() {
        return this.jamKerjaAwalTwo;
    }

    public Notaris jamKerjaAwalTwo(ZonedDateTime jamKerjaAwalTwo) {
        this.setJamKerjaAwalTwo(jamKerjaAwalTwo);
        return this;
    }

    public void setJamKerjaAwalTwo(ZonedDateTime jamKerjaAwalTwo) {
        this.jamKerjaAwalTwo = jamKerjaAwalTwo;
    }

    public ZonedDateTime getJamKerjaAkhirTwo() {
        return this.jamKerjaAkhirTwo;
    }

    public Notaris jamKerjaAkhirTwo(ZonedDateTime jamKerjaAkhirTwo) {
        this.setJamKerjaAkhirTwo(jamKerjaAkhirTwo);
        return this;
    }

    public void setJamKerjaAkhirTwo(ZonedDateTime jamKerjaAkhirTwo) {
        this.jamKerjaAkhirTwo = jamKerjaAkhirTwo;
    }

    public Integer getSelisihPukul() {
        return this.selisihPukul;
    }

    public Notaris selisihPukul(Integer selisihPukul) {
        this.setSelisihPukul(selisihPukul);
        return this;
    }

    public void setSelisihPukul(Integer selisihPukul) {
        this.selisihPukul = selisihPukul;
    }

    public ZonedDateTime getJamRestAwalOne() {
        return this.jamRestAwalOne;
    }

    public Notaris jamRestAwalOne(ZonedDateTime jamRestAwalOne) {
        this.setJamRestAwalOne(jamRestAwalOne);
        return this;
    }

    public void setJamRestAwalOne(ZonedDateTime jamRestAwalOne) {
        this.jamRestAwalOne = jamRestAwalOne;
    }

    public ZonedDateTime getJamRestAkhirOne() {
        return this.jamRestAkhirOne;
    }

    public Notaris jamRestAkhirOne(ZonedDateTime jamRestAkhirOne) {
        this.setJamRestAkhirOne(jamRestAkhirOne);
        return this;
    }

    public void setJamRestAkhirOne(ZonedDateTime jamRestAkhirOne) {
        this.jamRestAkhirOne = jamRestAkhirOne;
    }

    public ZonedDateTime getJamRestAwalTwo() {
        return this.jamRestAwalTwo;
    }

    public Notaris jamRestAwalTwo(ZonedDateTime jamRestAwalTwo) {
        this.setJamRestAwalTwo(jamRestAwalTwo);
        return this;
    }

    public void setJamRestAwalTwo(ZonedDateTime jamRestAwalTwo) {
        this.jamRestAwalTwo = jamRestAwalTwo;
    }

    public ZonedDateTime getJamRestAkhirTwo() {
        return this.jamRestAkhirTwo;
    }

    public Notaris jamRestAkhirTwo(ZonedDateTime jamRestAkhirTwo) {
        this.setJamRestAkhirTwo(jamRestAkhirTwo);
        return this;
    }

    public void setJamRestAkhirTwo(ZonedDateTime jamRestAkhirTwo) {
        this.jamRestAkhirTwo = jamRestAkhirTwo;
    }

    public Integer getMaksAkta() {
        return this.maksAkta;
    }

    public Notaris maksAkta(Integer maksAkta) {
        this.setMaksAkta(maksAkta);
        return this;
    }

    public void setMaksAkta(Integer maksAkta) {
        this.maksAkta = maksAkta;
    }

    public Integer getMaksOrderHarian() {
        return this.maksOrderHarian;
    }

    public Notaris maksOrderHarian(Integer maksOrderHarian) {
        this.setMaksOrderHarian(maksOrderHarian);
        return this;
    }

    public void setMaksOrderHarian(Integer maksOrderHarian) {
        this.maksOrderHarian = maksOrderHarian;
    }

    public String getLembarSk() {
        return this.lembarSk;
    }

    public Notaris lembarSk(String lembarSk) {
        this.setLembarSk(lembarSk);
        return this;
    }

    public void setLembarSk(String lembarSk) {
        this.lembarSk = lembarSk;
    }

    public String getLembarSumpah() {
        return this.lembarSumpah;
    }

    public Notaris lembarSumpah(String lembarSumpah) {
        this.setLembarSumpah(lembarSumpah);
        return this;
    }

    public void setLembarSumpah(String lembarSumpah) {
        this.lembarSumpah = lembarSumpah;
    }

    public String getAlamatKantor() {
        return this.alamatKantor;
    }

    public Notaris alamatKantor(String alamatKantor) {
        this.setAlamatKantor(alamatKantor);
        return this;
    }

    public void setAlamatKantor(String alamatKantor) {
        this.alamatKantor = alamatKantor;
    }

    public String getAlamatEmail() {
        return this.alamatEmail;
    }

    public Notaris alamatEmail(String alamatEmail) {
        this.setAlamatEmail(alamatEmail);
        return this;
    }

    public void setAlamatEmail(String alamatEmail) {
        this.alamatEmail = alamatEmail;
    }

    public String getNoKontak() {
        return this.noKontak;
    }

    public Notaris noKontak(String noKontak) {
        this.setNoKontak(noKontak);
        return this;
    }

    public void setNoKontak(String noKontak) {
        this.noKontak = noKontak;
    }

    public Integer getRecordStatus() {
        return this.recordStatus;
    }

    public Notaris recordStatus(Integer recordStatus) {
        this.setRecordStatus(recordStatus);
        return this;
    }

    public void setRecordStatus(Integer recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getUpdateBy() {
        return this.updateBy;
    }

    public Notaris updateBy(String updateBy) {
        this.setUpdateBy(updateBy);
        return this;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public ZonedDateTime getUpdateOn() {
        return this.updateOn;
    }

    public Notaris updateOn(ZonedDateTime updateOn) {
        this.setUpdateOn(updateOn);
        return this;
    }

    public void setUpdateOn(ZonedDateTime updateOn) {
        this.updateOn = updateOn;
    }

    public PenerimaKuasa getPenerimaKuasa() {
        return this.penerimaKuasa;
    }

    public void setPenerimaKuasa(PenerimaKuasa penerimaKuasa) {
        this.penerimaKuasa = penerimaKuasa;
    }

    public Notaris penerimaKuasa(PenerimaKuasa penerimaKuasa) {
        this.setPenerimaKuasa(penerimaKuasa);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    public String getNamaAhu() {
		return namaAhu;
	}

	public void setNamaAhu(String namaAhu) {
		this.namaAhu = namaAhu;
	}

	public String getPengadilanNegeri() {
		return pengadilanNegeri;
	}

	public void setPengadilanNegeri(String pengadilanNegeri) {
		this.pengadilanNegeri = pengadilanNegeri;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Notaris)) {
            return false;
        }
        return getId() != null && getId().equals(((Notaris) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Notaris{" +
            "id=" + getId() +
            ", nama='" + getNama() + "'" +
            ", namaShort='" + getNamaShort() + "'" +
            ", titelShortOne='" + getTitelShortOne() + "'" +
            ", titelShortTwo='" + getTitelShortTwo() + "'" +
            ", titelLongOne='" + getTitelLongOne() + "'" +
            ", titelLongTwo='" + getTitelLongTwo() + "'" +
            ", wilayahKerja='" + getWilayahKerja() + "'" +
            ", kedudukan='" + getKedudukan() + "'" +
            ", sk='" + getSk() + "'" +
            ", tglSk='" + getTglSk() + "'" +
            ", jenisHariKerja=" + getJenisHariKerja() +
            ", jamKerjaAwalOne='" + getJamKerjaAwalOne() + "'" +
            ", jamKerjaAkhirOne='" + getJamKerjaAkhirOne() + "'" +
            ", jamKerjaAwalTwo='" + getJamKerjaAwalTwo() + "'" +
            ", jamKerjaAkhirTwo='" + getJamKerjaAkhirTwo() + "'" +
            ", selisihPukul=" + getSelisihPukul() +
            ", jamRestAwalOne='" + getJamRestAwalOne() + "'" +
            ", jamRestAkhirOne='" + getJamRestAkhirOne() + "'" +
            ", jamRestAwalTwo='" + getJamRestAwalTwo() + "'" +
            ", jamRestAkhirTwo='" + getJamRestAkhirTwo() + "'" +
            ", maksAkta=" + getMaksAkta() +
            ", maksOrderHarian=" + getMaksOrderHarian() +
            ", lembarSk='" + getLembarSk() + "'" +
            ", lembarSumpah='" + getLembarSumpah() + "'" +
            ", alamatKantor='" + getAlamatKantor() + "'" +
            ", alamatEmail='" + getAlamatEmail() + "'" +
            ", noKontak='" + getNoKontak() + "'" +
            ", recordStatus=" + getRecordStatus() +
            ", updateBy='" + getUpdateBy() + "'" +
            ", updateOn='" + getUpdateOn() + "'" +
            "}";
    }
}
