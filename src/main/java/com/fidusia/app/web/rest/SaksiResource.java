package com.fidusia.app.web.rest;

import com.fidusia.app.domain.Saksi;
import com.fidusia.app.repository.SaksiRepository;
import com.fidusia.app.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.fidusia.app.domain.Saksi}.
 */
@RestController
@RequestMapping("/api/saksis")
@Transactional
public class SaksiResource {

    private final Logger log = LoggerFactory.getLogger(SaksiResource.class);

    private static final String ENTITY_NAME = "saksi";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SaksiRepository saksiRepository;

    public SaksiResource(SaksiRepository saksiRepository) {
        this.saksiRepository = saksiRepository;
    }

    /**
     * {@code POST  /saksis} : Create a new saksi.
     *
     * @param saksi the saksi to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new saksi, or with status {@code 400 (Bad Request)} if the saksi has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<Saksi> createSaksi(@RequestBody Saksi saksi) throws URISyntaxException {
        log.debug("REST request to save Saksi : {}", saksi);
        if (saksi.getId() != null) {
            throw new BadRequestAlertException("A new saksi cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Saksi result = saksiRepository.save(saksi);
        return ResponseEntity
            .created(new URI("/api/saksis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /saksis/:id} : Updates an existing saksi.
     *
     * @param id the id of the saksi to save.
     * @param saksi the saksi to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated saksi,
     * or with status {@code 400 (Bad Request)} if the saksi is not valid,
     * or with status {@code 500 (Internal Server Error)} if the saksi couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<Saksi> updateSaksi(@PathVariable(value = "id", required = false) final Long id, @RequestBody Saksi saksi)
        throws URISyntaxException {
        log.debug("REST request to update Saksi : {}, {}", id, saksi);
        if (saksi.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, saksi.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!saksiRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Saksi result = saksiRepository.save(saksi);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, saksi.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /saksis/:id} : Partial updates given fields of an existing saksi, field will ignore if it is null
     *
     * @param id the id of the saksi to save.
     * @param saksi the saksi to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated saksi,
     * or with status {@code 400 (Bad Request)} if the saksi is not valid,
     * or with status {@code 404 (Not Found)} if the saksi is not found,
     * or with status {@code 500 (Internal Server Error)} if the saksi couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Saksi> partialUpdateSaksi(@PathVariable(value = "id", required = false) final Long id, @RequestBody Saksi saksi)
        throws URISyntaxException {
        log.debug("REST request to partial update Saksi partially : {}, {}", id, saksi);
        if (saksi.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, saksi.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!saksiRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Saksi> result = saksiRepository
            .findById(saksi.getId())
            .map(existingSaksi -> {
                if (saksi.getTitel() != null) {
                    existingSaksi.setTitel(saksi.getTitel());
                }
                if (saksi.getNama() != null) {
                    existingSaksi.setNama(saksi.getNama());
                }
                if (saksi.getKomparisi() != null) {
                    existingSaksi.setKomparisi(saksi.getKomparisi());
                }
                if (saksi.getTts() != null) {
                    existingSaksi.setTts(saksi.getTts());
                }
                if (saksi.getRecordStatus() != null) {
                    existingSaksi.setRecordStatus(saksi.getRecordStatus());
                }
                if (saksi.getUpdateBy() != null) {
                    existingSaksi.setUpdateBy(saksi.getUpdateBy());
                }
                if (saksi.getUpdateOn() != null) {
                    existingSaksi.setUpdateOn(saksi.getUpdateOn());
                }

                return existingSaksi;
            })
            .map(saksiRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, saksi.getId().toString())
        );
    }

    /**
     * {@code GET  /saksis} : get all the saksis.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of saksis in body.
     */
    @GetMapping("")
    public ResponseEntity<List<Saksi>> getAllSaksis(Pageable pageable) {
        log.debug("REST request to get a page of Saksis");
        Page<Saksi> page = saksiRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /saksis/:id} : get the "id" saksi.
     *
     * @param id the id of the saksi to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the saksi, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<Saksi> getSaksi(@PathVariable("id") Long id) {
        log.debug("REST request to get Saksi : {}", id);
        Optional<Saksi> saksi = saksiRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(saksi);
    }

    /**
     * {@code DELETE  /saksis/:id} : delete the "id" saksi.
     *
     * @param id the id of the saksi to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteSaksi(@PathVariable("id") Long id) {
        log.debug("REST request to delete Saksi : {}", id);
        saksiRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
