package com.fidusia.app.service.dto;

public class ResponseBerkasDTO {
	
	private String ppkNomor;
	
	private String typeDoc;
	
	private String fileContent;
	
	private String fileExt;

	public String getPpkNomor() {
		return ppkNomor;
	}

	public void setPpkNomor(String ppkNomor) {
		this.ppkNomor = ppkNomor;
	}

	public String getTypeDoc() {
		return typeDoc;
	}

	public void setTypeDoc(String typeDoc) {
		this.typeDoc = typeDoc;
	}

	public String getFileContent() {
		return fileContent;
	}

	public void setFileContent(String fileContent) {
		this.fileContent = fileContent;
	}

	public String getFileExt() {
		return fileExt;
	}

	public void setFileExt(String fileExt) {
		this.fileExt = fileExt;
	}

	@Override
	public String toString() {
		return "ResponseBerkasDTO [ppkNomor=" + ppkNomor + ", typeDoc=" + typeDoc + ", fileContent=" + fileContent
				+ "]";
	}
}
