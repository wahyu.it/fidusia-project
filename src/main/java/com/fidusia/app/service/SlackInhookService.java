package com.fidusia.app.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.fidusia.app.repository.ApplicationConfigRepository;

@Service
public class SlackInhookService {
	
	public final static String DEFAULT_USER = "itops-log";

    private final Logger log = LoggerFactory.getLogger(SlackInhookService.class);

    private final ApplicationConfigRepository applicationConfigRepository;

    public SlackInhookService(ApplicationConfigRepository applicationConfigRepository) {
    	this.applicationConfigRepository = applicationConfigRepository;
    }

    @Async
    public void sendMessageAsync(String text) {
		String channel = applicationConfigRepository.findByCategoryAndName("APPLICATION_PROPERTIES", "channel_slack").getValue();
    	
    	sendMessage(DEFAULT_USER, channel, text);
    }

    @Async
    public void sendMessageAsync(String username, String channel, String text) {
    	sendMessage(username, channel, text);
    }

    public void sendMessage(String username, String channel, String text) {
    	String payload = "payload={\"channel\": \"#" + channel + "\", \"username\": \"" + username + "\", \"text\": \"" + text + "\", \"icon_emoji\": \":octocat:\"}";
    	
        log.info("Request to sendMessage username:{} channel:{}", username, channel);
    	
		URL obj;
		BufferedReader in = null;
		String url = applicationConfigRepository.findByCategoryAndName("APPLICATION_PROPERTIES", "url_slack").getValue();
		try {
			obj = new URL(url);
			
			HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

			con.setRequestMethod("POST");
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(payload);
			wr.flush();
			wr.close();

			int responseCode = con.getResponseCode();
			log.debug("\nSending 'POST' request to URL : " + url);
			if(responseCode != HttpStatus.OK.value())
				log.debug("Post parameters : " + payload);
			log.debug("Response Code : " + responseCode);

			in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			
			String inputLine;
			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
		} catch (Exception e) {
			log.error("Error on sendMessage due to " + e.getMessage());
			e.printStackTrace();
		} finally {
			if(in != null) {
				try {
					in.close();
				} catch(Exception e) {}
			}
		}
    }
}
