import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasimonitoringSharedModule } from 'app/shared/shared.module';
import { UserManagComponent } from './user-manag.component';
import { UserDetailDialogComponent } from './user-detail-dialog.component';
import { UserNewDialogComponent } from './user-new-dialog.component';
import { UserManagDetailComponent } from './user-manag-detail.component';
import { UserManagUpdateComponent } from './user-manag-update.component';
import { UserManagDeleteDialogComponent } from './user-manag-delete-dialog.component';
import { userManagRoute } from './user-manag.route';

@NgModule({
  imports: [SisteminformasimonitoringSharedModule, RouterModule.forChild(userManagRoute)],
  declarations: [
    UserManagComponent,
    UserManagDetailComponent,
    UserManagUpdateComponent,
    UserManagDeleteDialogComponent,
    UserDetailDialogComponent,
    UserNewDialogComponent
  ],
  entryComponents: [UserManagDeleteDialogComponent, UserDetailDialogComponent, UserNewDialogComponent]
})
export class UserManagModule {}
