package com.fidusia.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Saksi.
 */
@Entity
@Table(name = "saksi")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Saksi implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "titel")
    private String titel;

    @Column(name = "nama")
    private String nama;

    @Column(name = "komparisi")
    private String komparisi;

    @Column(name = "tts")
    private String tts;

    @Column(name = "record_status")
    private Integer recordStatus;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "update_on")
    private ZonedDateTime updateOn;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "penerimaKuasa" }, allowSetters = true)
    private Notaris notaris;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Saksi id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitel() {
        return this.titel;
    }

    public Saksi titel(String titel) {
        this.setTitel(titel);
        return this;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getNama() {
        return this.nama;
    }

    public Saksi nama(String nama) {
        this.setNama(nama);
        return this;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKomparisi() {
        return this.komparisi;
    }

    public Saksi komparisi(String komparisi) {
        this.setKomparisi(komparisi);
        return this;
    }

    public void setKomparisi(String komparisi) {
        this.komparisi = komparisi;
    }

    public String getTts() {
        return this.tts;
    }

    public Saksi tts(String tts) {
        this.setTts(tts);
        return this;
    }

    public void setTts(String tts) {
        this.tts = tts;
    }

    public Integer getRecordStatus() {
        return this.recordStatus;
    }

    public Saksi recordStatus(Integer recordStatus) {
        this.setRecordStatus(recordStatus);
        return this;
    }

    public void setRecordStatus(Integer recordStatus) {
        this.recordStatus = recordStatus;
    }

    public String getUpdateBy() {
        return this.updateBy;
    }

    public Saksi updateBy(String updateBy) {
        this.setUpdateBy(updateBy);
        return this;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public ZonedDateTime getUpdateOn() {
        return this.updateOn;
    }

    public Saksi updateOn(ZonedDateTime updateOn) {
        this.setUpdateOn(updateOn);
        return this;
    }

    public void setUpdateOn(ZonedDateTime updateOn) {
        this.updateOn = updateOn;
    }

    public Notaris getNotaris() {
        return this.notaris;
    }

    public void setNotaris(Notaris notaris) {
        this.notaris = notaris;
    }

    public Saksi notaris(Notaris notaris) {
        this.setNotaris(notaris);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Saksi)) {
            return false;
        }
        return getId() != null && getId().equals(((Saksi) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Saksi{" +
            "id=" + getId() +
            ", titel='" + getTitel() + "'" +
            ", nama='" + getNama() + "'" +
            ", komparisi='" + getKomparisi() + "'" +
            ", tts='" + getTts() + "'" +
            ", recordStatus=" + getRecordStatus() +
            ", updateBy='" + getUpdateBy() + "'" +
            ", updateOn='" + getUpdateOn() + "'" +
            "}";
    }
}
