package com.fidusia.app.domain;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "log_book")
public class LogBook implements Serializable  {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "tanggal", nullable = false)
    private LocalDate tanggal;

    @Size(max = 45)
    @Column(name = "kategori", length = 45)
    private String kategori;

    @Column(name = "mulai")
    private Instant mulai;

    @Column(name = "selesai")
    private Instant selesai;

    @ManyToOne
    @JsonIgnoreProperties("logbooks")
    private User user;

    @Size(max = 100)
    @Column(name = "keperluan", length = 100)
    private String keperluan;

    @Size(max = 100)
    @Column(name = "catatan", length = 100)
    private String catatan;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getTanggal() {
		return tanggal;
	}

	public void setTanggal(LocalDate tanggal) {
		this.tanggal = tanggal;
	}

	public String getKategori() {
		return kategori;
	}

	public void setKategori(String kategori) {
		this.kategori = kategori;
	}

	public Instant getMulai() {
		return mulai;
	}

	public void setMulai(Instant mulai) {
		this.mulai = mulai;
	}

	public Instant getSelesai() {
		return selesai;
	}

	public void setSelesai(Instant selesai) {
		this.selesai = selesai;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getKeperluan() {
		return keperluan;
	}

	public void setKeperluan(String keperluan) {
		this.keperluan = keperluan;
	}

	public String getCatatan() {
		return catatan;
	}

	public void setCatatan(String catatan) {
		this.catatan = catatan;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LogBook other = (LogBook) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "LogBook [id=" + id + ", tanggal=" + tanggal + ", kategori=" + kategori + ", mulai=" + mulai
				+ ", selesai=" + selesai + ", user=" + user + ", keperluan=" + keperluan + ", catatan=" + catatan + "]";
	}

}
