package com.fidusia.app.domain.util;

public class RawDataRoyaStatus {
	public final static String NEW = "NEW";
	public final static String INVALID = "INVALID";
	public final static String NOTFOUND = "NOTFOUND";
	public final static String INPROGRESS = "INPROGRESS";
	public final static String SUCCESS = "SUCCESS";
	public final static String FAILED = "FAILED";
	public final static String ERROR = "ERROR";
	
	public final static String toOutputResult(String status)  {
		if(RawDataRoyaStatus.NEW.equals(status)) return "Belum Roya";
		if(RawDataRoyaStatus.INVALID.equals(status)) return "Belum Roya";
		if(RawDataRoyaStatus.NOTFOUND.equals(status)) return "Tidak Ditemukan";
		if(RawDataRoyaStatus.INPROGRESS.equals(status)) return "Belum Roya";
		if(RawDataRoyaStatus.SUCCESS.equals(status)) return "Berhasil";
		if(RawDataRoyaStatus.FAILED.equals(status)) return "Gagal";
		if(RawDataRoyaStatus.ERROR.equals(status)) return "Belum Roya";
		
		return status;
	}
}
