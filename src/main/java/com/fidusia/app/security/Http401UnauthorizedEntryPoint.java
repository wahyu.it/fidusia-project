package com.fidusia.app.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.fidusia.app.service.BlacklistAddressService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Returns a 401 error code (Unauthorized) to the client.
 */
@Component
public class Http401UnauthorizedEntryPoint implements AuthenticationEntryPoint {

    private final Logger log = LoggerFactory.getLogger(Http401UnauthorizedEntryPoint.class);
    
	private final BlacklistAddressService blacklistAddressService;
	
	public Http401UnauthorizedEntryPoint (BlacklistAddressService blacklistAddressService) {
		this.blacklistAddressService= blacklistAddressService;
	}
    
    private static final String[] HEADERS_TO_TRY = {
            "X-Forwarded-For",
            "Proxy-Client-IP",
            "WL-Proxy-Client-IP",
            "HTTP_X_FORWARDED_FOR",
            "HTTP_X_FORWARDED",
            "HTTP_X_CLUSTER_CLIENT_IP",
            "HTTP_CLIENT_IP",
            "HTTP_FORWARDED_FOR",
            "HTTP_FORWARDED",
            "HTTP_VIA",
            "REMOTE_ADDR"};

    private String getClientIpAddress(HttpServletRequest request) {
        for (String header : HEADERS_TO_TRY) {
            String ip = request.getHeader(header);
            if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
                return ip;
            }
        }
        
        return request.getRemoteAddr();
    }

    /**
     * Always returns a 401 error code to the client.
     */
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException arg2)
        throws IOException,
        ServletException {
    	
    	String clientIp = null;
    	try {
    		clientIp = getClientIpAddress(request);
    		log.error("Client IP fails to capture" + clientIp);
    	} catch(Exception e) { log.error("Client IP fails to capture"); }
    	
        log.debug("Pre-authenticated entry point called. Rejecting access. " + (clientIp != null ? clientIp : ""));
        
        if(clientIp != null && !blacklistAddressService.isBlacklisted(clientIp))
        	blacklistAddressService.addUnauhtorizedCounter(clientIp);
        	
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Access Denied");
    }
}
