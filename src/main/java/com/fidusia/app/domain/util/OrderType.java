package com.fidusia.app.domain.util;

/**
 * Created by ataufani on 11/1/2018.
 * Tipe Order Pendaftaran
 */
public enum OrderType {
    REGULAR(0), OVERDUE(1), SYARIAH(2), MULTIUNIT(3), PERUBAHAN(4), RESTRUCTURE(5), SAPIENT(6), DIRADLB(7);

    private int value;

    OrderType (int value) {
        this.value = value;
    }

    public int value() {
        return value;
    }
    
    public final static OrderType[] toPendaftaranArray() {
    	return new OrderType[] {REGULAR, OVERDUE, SYARIAH, MULTIUNIT, SAPIENT, DIRADLB};
    }
}
