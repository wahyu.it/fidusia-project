import { Component, OnInit, ElementRef  } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { UploadBerkasService } from './uploadberkas.service';
import { DomSanitizer } from '@angular/platform-browser';
import { BerkasDTO } from 'app/shared/model/berkasDTO.model';
import { FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';


@Component({
  selector: 'jhi-upload-dialog',
  templateUrl: './upload-dialog.component.html',
  styleUrls: ['uploadberkas.scss']
})
export class UploadDialogComponent implements OnInit  {
	url !: any;
	berkas !: BerkasDTO;
	image !: any;
	ext !: any;
	blob !: any;
	identitas !: File;
    identitasName!: string;
    kk!: string;
	kkFile !: File;
    ppk!: string;
	ppkFile !: File;
    skf!: string;
	skfFile !: File;
    aktapend!: string;
	aktapendFile !: File;
    aktaperub!: string;
	aktaperubFile !: File;
    skkemen!: string;
	skkemenFile !: File;
    npwp!: string;
	npwpFile !: File;
    isSaving = false;
    files !: [];
    
   editForm = this.fb.group({
    identitas:['', [Validators.required]],
    kk:['', [Validators.required]],
    ppk:['', [Validators.required]],
    skf:['', [Validators.required]],
    aktapend:['', [Validators.required]],
    aktaperub:['', [Validators.required]],
    skkemen:['', [Validators.required]],
    npwp:['', [Validators.required]],
  });

  constructor(
	  public activeModal: NgbActiveModal, 
	  private eventManager: JhiEventManager,
      protected uploadBerkasService: UploadBerkasService,
      private sanitizer: DomSanitizer,
	  private playerRef: ElementRef,
      private fb: FormBuilder,
    ) {}

  clear(): void {
    this.activeModal.dismiss();
  }
  

  ngOnInit(): void {
	  
  }
  
  saveupload(berkas: any): void {
      const files = [];
	  if (this.identitas) files.push(this.identitas)
	  if (this.kkFile) files.push(this.kkFile)
	  if (this.ppkFile) files.push(this.ppkFile)
	  if (this.skfFile) files.push(this.skfFile)
	  if (this.npwpFile) files.push(this.npwpFile)
	  if (this.aktapendFile) files.push(this.aktapendFile)
	  if (this.aktaperubFile) files.push(this.aktaperubFile)
	  if (this.skkemenFile) files.push(this.skkemenFile)
	  
	  this.subscribeToSaveResponse(this.uploadBerkasService.createMultiFile(berkas.id, files));
  }
  
  selectFile(kategori: string,event: any): void {
	  const type = event.target.files.item(0).type.split('/')[1];
	  console.log('type: ' + type)
      const imgFile = new File([event.target.files.item(0)], kategori + '.' + type);
	  if (kategori === 'identitas') {
		  if (event.target.files.item(0).size >10000000) {
			  this.warning();
		  } else {
	    	this.identitas = imgFile;
	    	this.identitasName = event.target.files.item(0).name;
		  }
	  } else if (kategori === 'kk') {
		  if (event.target.files.item(0).size >10000000) {
			  this.warning();
		  } else {
	    	this.kkFile = imgFile;
	    	this.kk = event.target.files.item(0).name;
		  }
	  } else if (kategori === 'ppk') {
		  if (event.target.files.item(0).size >10000000) {
			  this.warning();
		  } else {
	    	this.ppkFile = imgFile;
	    	this.ppk = event.target.files.item(0).name;
		  }
	  } else if (kategori === 'skf') {
		  if (event.target.files.item(0).size >10000000) {
			  this.warning();
		  } else {
	    	this.skfFile = imgFile;
	    	this.skf = event.target.files.item(0).name;
		  }
	  } else if (kategori === 'aktapend') {
		  if (event.target.files.item(0).size >10000000) {
			  this.warning();
		  } else {
	    	this.aktapendFile = imgFile;
	    	this.aktapend = event.target.files.item(0).name;
		  }
	  } else if (kategori === 'aktaperub') {
		  if (event.target.files.item(0).size >10000000) {
			  this.warning();
		  } else {
	    	this.aktaperubFile = imgFile;
	    	this.aktaperub = event.target.files.item(0).name;
		  }
	  } else if (kategori === 'skkemen') {
		  if (event.target.files.item(0).size >10000000) {
			  this.warning();
		  } else {
	    	this.skkemenFile = imgFile;
	    	this.skkemen = event.target.files.item(0).name;
		  }
	  }  else if (kategori === 'npwp') {
		  if (event.target.files.item(0).size >10000000) {
			  this.warning();
		  } else {
	    	this.npwpFile = imgFile;
	    	this.npwp = event.target.files.item(0).name;
		  }
	  }
  }
  
  success(): void {
	  Swal.fire({
			  icon: "success",
		      text: 'Warkah Berhasil diupload',
		      showCancelButton: false,
		    } as any);
  }
  
  warning(): void {
	  Swal.fire({
		      icon: 'error',
              title: "Oops...",
		      text: 'Size warkah melebihi 10 Mb !!!',
		      showCancelButton: false,
		    } as any);
  }

  dismiss(): void {
    this.activeModal.dismiss();
  }
  
  protected subscribeToSaveResponse(result: Observable<HttpResponse<BerkasDTO>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }
  
  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.success();
    this.eventManager.broadcast('uploadBerkastListModification');
    this.dismiss();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
