package com.fidusia.app.service.dto;

public class ResponseGetDataDTO {
	
    private String errorCode;
    private String response;
    private String messageResponse;
    private String fiduciaTaskData;
    private String error;
    private String message;
    private String status;
    
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getMessageResponse() {
		return messageResponse;
	}
	public void setMessageResponse(String messageResponse) {
		this.messageResponse = messageResponse;
	}
	public String getFiduciaTaskData() {
		return fiduciaTaskData;
	}
	public void setFiduciaTaskData(String fiduciaTaskData) {
		this.fiduciaTaskData = fiduciaTaskData;
	}
	
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "ResponseGetDataDTO [errorCode=" + errorCode + ", response=" + response + ", messageResponse="
				+ messageResponse + ", fiduciaTaskData=" + fiduciaTaskData + ", error=" + error + ", message=" + message
				+ ", status=" + status + "]";
	}

}
