import { Routes } from '@angular/router';
import { VerifikasiKendaraanComponent } from './verifykendaraan.component';
import { JhiResolvePagingParams } from 'ng-jhipster';

export const verifykendaraanState: Routes = [{
  path: '',
  component: VerifikasiKendaraanComponent,
  resolve: {
	  pagingParams: JhiResolvePagingParams
  }
  }
];
