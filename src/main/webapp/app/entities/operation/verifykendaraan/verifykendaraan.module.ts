import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasimonitoringSharedModule } from 'app/shared/shared.module';
import { VerifikasiKendaraanComponent } from './verifykendaraan.component';
import { verifykendaraanState } from './verifykendaraan.route';
import { VerifyKendaraanDialogComponent } from './verifykendaraan-dialog.component';

@NgModule({
  imports: [SisteminformasimonitoringSharedModule, RouterModule.forChild(verifykendaraanState)],
  declarations: [VerifikasiKendaraanComponent, VerifyKendaraanDialogComponent],
  entryComponents: [VerifyKendaraanDialogComponent]
})
export class VerifyKendaraanModule {}
