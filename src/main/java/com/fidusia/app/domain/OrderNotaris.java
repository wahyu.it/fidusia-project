package com.fidusia.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A OrderNotaris.
 */
@Entity
@Table(name = "order_notaris")
public class OrderNotaris implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "tanggal")
    private LocalDate tanggal;

    @Column(name = "status")
    private Integer status;

    @ManyToOne
    private OrderData orderData;

    @ManyToOne
    private Notaris notaris;

    @ManyToOne
    private Akta akta;

    @ManyToOne
    private OrderKerja orderKerja;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public OrderNotaris id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getTanggal() {
        return this.tanggal;
    }

    public OrderNotaris tanggal(LocalDate tanggal) {
        this.setTanggal(tanggal);
        return this;
    }

    public void setTanggal(LocalDate tanggal) {
        this.tanggal = tanggal;
    }

    public Integer getStatus() {
        return this.status;
    }

    public OrderNotaris status(Integer status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public OrderData getOrderData() {
        return this.orderData;
    }

    public void setOrderData(OrderData orderData) {
        this.orderData = orderData;
    }

    public OrderNotaris orderData(OrderData orderData) {
        this.setOrderData(orderData);
        return this;
    }

    public Notaris getNotaris() {
        return this.notaris;
    }

    public void setNotaris(Notaris notaris) {
        this.notaris = notaris;
    }

    public OrderNotaris notaris(Notaris notaris) {
        this.setNotaris(notaris);
        return this;
    }

    public Akta getAkta() {
        return this.akta;
    }

    public void setAkta(Akta akta) {
        this.akta = akta;
    }

    public OrderNotaris akta(Akta akta) {
        this.setAkta(akta);
        return this;
    }

    public OrderKerja getOrderKerja() {
        return this.orderKerja;
    }

    public void setOrderKerja(OrderKerja orderKerja) {
        this.orderKerja = orderKerja;
    }

    public OrderNotaris orderKerja(OrderKerja orderKerja) {
        this.setOrderKerja(orderKerja);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrderNotaris)) {
            return false;
        }
        return getId() != null && getId().equals(((OrderNotaris) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OrderNotaris{" +
            "id=" + getId() +
            ", tanggal='" + getTanggal() + "'" +
            ", status=" + getStatus() +
            "}";
    }
}
