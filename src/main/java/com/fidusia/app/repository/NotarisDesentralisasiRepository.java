package com.fidusia.app.repository;


import org.springframework.data.jpa.repository.*;

import com.fidusia.app.domain.NotarisDesentralisasi;

/**
 * Spring Data JPA repository for the Notaris entity.
 */
@SuppressWarnings("unused")
public interface NotarisDesentralisasiRepository extends JpaRepository<NotarisDesentralisasi,Long> {

	NotarisDesentralisasi findOneByNama(String nama);
}
