package com.fidusia.app.web.rest;


import com.fidusia.app.domain.User;
import com.fidusia.app.repository.UserRepository;
import com.fidusia.app.security.SecurityUtils;
import com.fidusia.app.service.MailService;
import com.fidusia.app.service.UserService;
import com.fidusia.app.service.dto.PasswordChangeDTO;
import com.fidusia.app.service.dto.UserDTO;
import com.fidusia.app.web.rest.errors.*;
import com.fidusia.app.web.rest.vm.KeyAndPasswordVM;
import com.fidusia.app.web.rest.vm.LoginVM;
import com.fidusia.app.web.rest.vm.ManagedUserVM;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import java.net.URISyntaxException;
import java.util.*;

/**
 * REST controller for managing the current user's account.
 */
@RestController
@RequestMapping("/api")
public class AccountResource {

    private static class AccountResourceException extends RuntimeException {
        private AccountResourceException(String message) {
            super(message);
        }
    }

    private final Logger log = LoggerFactory.getLogger(AccountResource.class);

    private final UserRepository userRepository;

    private final UserService userService;

    private final MailService mailService;

    public AccountResource(UserRepository userRepository, UserService userService, MailService mailService) {

        this.userRepository = userRepository;
        this.userService = userService;
        this.mailService = mailService;
    }

    /**
     * {@code POST  /register} : register the user.
     *
     * @param managedUserVM the managed user View Model.
     * @throws InvalidPasswordException {@code 400 (Bad Request)} if the password is incorrect.
     * @throws EmailAlreadyUsedException {@code 400 (Bad Request)} if the email is already used.
     * @throws LoginAlreadyUsedException {@code 400 (Bad Request)} if the login is already used.
     */
    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public void registerAccount(@Valid @RequestBody ManagedUserVM managedUserVM) {
        if (!checkPasswordLength(managedUserVM.getPassword())) {
            throw new InvalidPasswordException();
        }
        User user = userService.registerUser(managedUserVM, managedUserVM.getPassword());
        mailService.sendActivationEmail(user);
    }

    /**
     * {@code GET  /activate} : activate the registered user.
     *
     * @param key the activation key.
     * @throws RuntimeException {@code 500 (Internal Server Error)} if the user couldn't be activated.
     */
    @GetMapping("/activate")
    public void activateAccount(@RequestParam(value = "key") String key) {
        Optional<User> user = userService.activateRegistration(key);
        if (!user.isPresent()) {
            throw new AccountResourceException("No user was found for this activation key");
        }
    }

    /**
     * {@code GET  /authenticate} : check if the user is authenticated, and return its login.
     *
     * @param request the HTTP request.
     * @return the login if the user is authenticated.
     */
    @GetMapping("/authenticate")
    public String isAuthenticated(HttpServletRequest request) {
        log.debug("REST request to check if the current user is authenticated");
        return request.getRemoteUser();
    }
    
    @GetMapping("/authenticate/remote")
    public String isAuthenticatedRemote(HttpServletRequest request) {
        log.debug("REST request to check if the current user is authenticated");
        return request.getRemoteHost();
    }

    /**
     * {@code GET  /account} : get the current user.
     *
     * @return the current user.
     * @throws RuntimeException {@code 500 (Internal Server Error)} if the user couldn't be returned.
     */
    @GetMapping("/account")
    public UserDTO getAccount() {
        return userService.getUserWithAuthorities()
            .map(UserDTO::new)
            .orElseThrow(() -> new AccountResourceException("User could not be found"));
    }

    /**
     * {@code POST  /account} : update the current user information.
     *
     * @param userDTO the current user information.
     * @throws EmailAlreadyUsedException {@code 400 (Bad Request)} if the email is already used.
     * @throws RuntimeException {@code 500 (Internal Server Error)} if the user login wasn't found.
     */
    @PostMapping("/account")
    public void saveAccount(@Valid @RequestBody UserDTO userDTO) {
        String userLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() -> new AccountResourceException("Current user login not found"));
        Optional<User> existingUser = userRepository.findOneByEmailIgnoreCase(userDTO.getEmail());
        if (existingUser.isPresent() && (!existingUser.get().getLogin().equalsIgnoreCase(userLogin))) {
            throw new EmailAlreadyUsedException();
        }
        Optional<User> user = userRepository.findOneByLogin(userLogin);
        if (!user.isPresent()) {
            throw new AccountResourceException("User could not be found");
        }
        userService.updateUser(userDTO.getFirstName(), userDTO.getLastName(), userDTO.getEmail(),
            userDTO.getLangKey(), userDTO.getImageUrl());
    }

    /**
     * {@code POST  /account/change-password} : changes the current user's password.
     *
     * @param passwordChangeDto current and new password.
     * @throws InvalidPasswordException {@code 400 (Bad Request)} if the new password is incorrect.
     */
    @PostMapping(path = "/account/change-password")
    public void changePassword(@RequestBody PasswordChangeDTO passwordChangeDto) {
        if (!checkPasswordLength(passwordChangeDto.getNewPassword())) {
            throw new InvalidPasswordException();
        }
        userService.changePassword(passwordChangeDto.getCurrentPassword(), passwordChangeDto.getNewPassword());
    }

    /**
     * {@code POST   /account/reset-password/init} : Send an email to reset the password of the user.
     *
     * @param mail the mail of the user.
     * @throws EmailNotFoundException {@code 400 (Bad Request)} if the email address is not registered.
     */
    @PostMapping(path = "/account/reset-password/init")
    public void requestPasswordReset(@RequestBody String mail) {
       mailService.sendPasswordResetMail(
           userService.requestPasswordReset(mail)
               .orElseThrow(EmailNotFoundException::new)
       );
    }

    /**
     * {@code POST   /account/reset-password/finish} : Finish to reset the password of the user.
     *
     * @param keyAndPassword the generated key and the new password.
     * @throws InvalidPasswordException {@code 400 (Bad Request)} if the password is incorrect.
     * @throws RuntimeException {@code 500 (Internal Server Error)} if the password could not be reset.
     */
    @PostMapping(path = "/account/reset-password/finish")
    public void finishPasswordReset(@RequestBody KeyAndPasswordVM keyAndPassword) {
        if (!checkPasswordLength(keyAndPassword.getNewPassword())) {
            throw new InvalidPasswordException();
        }
        Optional<User> user =
            userService.completePasswordReset(keyAndPassword.getNewPassword(), keyAndPassword.getKey());

        if (!user.isPresent()) {
            throw new AccountResourceException("No user was found for this reset key");
        }
    }

    private static boolean checkPasswordLength(String password) {
        return !StringUtils.isEmpty(password) &&
            password.length() >= ManagedUserVM.PASSWORD_MIN_LENGTH &&
            password.length() <= ManagedUserVM.PASSWORD_MAX_LENGTH;
    }
    
    @PostMapping("/account/authenticate/code")
    public void authenticateAccount(
            @RequestParam(value = "email", required = true) String email)  throws URISyntaxException {
    	log.debug("Rest request send OTP to email {}", email);
    	
    	if (email == null) {
            throw new BadRequestAlertException("Invalid email", null, "email invalid");
        } else {
            userService.sendOtpToEmail(email);
        }
    }

    @PostMapping("/account/authenticate/user-active")
    public Boolean getStatusAccount(@Valid @RequestBody LoginVM loginVM) {
        return userService.findStatus(loginVM.getUsername());
    }

    @PostMapping("/account/authenticate/user-password")
    public Boolean getAkunPassword(@Valid @RequestBody LoginVM loginVM) {
        return userService.findStatus(loginVM.getUsername());
    }
    
    @PostMapping("/account/authenticate/verify")
    public Boolean authenticateAccountVerify(
            @RequestParam(value = "email", required = true) String email,
            @RequestParam(value = "otp", required = true) String otp)  throws URISyntaxException {
    		User user = userRepository.findOneByLogin(email).get();
    		
    		if (user != null) {
    			if (otp.equals(user.getOtp()))
    				return true;
    		} else {
    			return false;
    		}
    		
			return false;
    }
    
    @PostMapping("/account/authenticate/email")
    public Boolean getStatusEmailAccount(
            @RequestParam(value = "email", required = true) String email)  throws URISyntaxException {
    		log.debug("userlogin : {}", email);
    		Optional<User> user = userRepository.findOneWithAuthoritiesByEmailIgnoreCase(email);
    		
    		if (user.isPresent()) {
    			return true;
    		} else {
    			return false;
    		}
    }
    
    @PostMapping("/account/authenticate/userlogin")
    public Boolean getStatusUserAccount(
            @RequestParam(value = "login", required = true) String login)  throws URISyntaxException {
    	    log.debug("userlogin : {}", login);
    	    Optional<User> user = userRepository.findByLogin(login);
    		
    		if (user.isPresent()) {
    			return true;
    		} else {
    			return false;
    		}
    }
    
    @PostMapping("/account/register-new")
    @ResponseStatus(HttpStatus.CREATED)
    public void registerNewAccount() {
    	log.debug("Rest request save user account : {}");
    	
        User user = userRepository.findByLogin("wahyu23").get();
//        mailService.sendActivationEmail(user);
        mailService.sendPasswordResetMail(
                userService.requestPasswordReset(user.getEmail())
                    .orElseThrow(EmailNotFoundException::new));
    }
    
    @PostMapping("/account/register")
    @ResponseStatus(HttpStatus.CREATED)
    public void registerNewAccount(
            @RequestParam(value = "firstname", required = true) String firstname,
            @RequestParam(value = "lastname", required = true) String lastname,
            @RequestParam(value = "username", required = true) String username,
            @RequestParam(value = "password", required = true) String password,
            @RequestParam(value = "email", required = true) String email,
            @RequestParam(value = "role", required = true) String role,
            @RequestParam(value = "cabang", required = false) String cabang,
            @RequestParam(value = "notaris", required = false) String notaris) {
    	log.debug("Rest request save user account : {}", username);
    	
        userService.registerUserNew(firstname, lastname, username, password, email, role, cabang, notaris);
//        mailService.sendActivationEmail(user);
//        mailService.sendPasswordResetMail(
//                userService.requestPasswordReset(user.getEmail())
//                    .orElseThrow(EmailNotFoundException::new));
    }
}
