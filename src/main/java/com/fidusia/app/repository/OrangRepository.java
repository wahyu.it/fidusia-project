package com.fidusia.app.repository;

import com.fidusia.app.domain.Orang;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Orang entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrangRepository extends JpaRepository<Orang, Long> {}
