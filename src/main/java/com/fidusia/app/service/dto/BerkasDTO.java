package com.fidusia.app.service.dto;

import java.time.LocalDate;
import java.time.ZonedDateTime;

import javax.validation.constraints.*;

import com.fidusia.app.domain.Cabang;
import com.fidusia.app.domain.SkSubstitusi;

import java.io.Serializable;
import java.util.Objects;


/**
 * A DTO for the Berkas entity.
 */
public class BerkasDTO implements Serializable {

    public BerkasDTO() {}

	private Long id;

    @Size(max = 150)
    private String identitas;

    @Size(max = 150)
    private String kk;

    @Size(max = 150)
    private String ppk;

    @Size(max = 150)
    private String ppk2;

    @Size(max = 150)
    private String skNasabah;

    @Max(value = 9)
    private Integer recordStatus;

    @Size(max = 50)
    private String updateBy;

    private ZonedDateTime updateOn;

    @Size(max = 50)
    private String uploadBy;

    private ZonedDateTime uploadOn;

    @NotNull
    @Size(max = 20)
    private String ppkNomor;

    @NotNull
    @Max(value = 2)
    private Integer nsbJenis;

    private Long cabangId;

    private Cabang cabang;

    private Integer berkasStatus;

    private String namaNasabah;

    private LocalDate tanggalPpk;

    private Boolean registrasiStatus;

    private Long skSubstitusiId;

    private SkSubstitusi skSubstitusi;

    private String namaCabang;

    private String noSkSubstitusi;

    @Max(value = 2)
    private Integer metodeKirim;

    private Long remarkId;

    private String remarkName;

    @Max(value = 9)
    private Integer partitionId;

    @Size(max = 50)
    private String verifyBy;

    private ZonedDateTime verifyOn;

    private LocalDate verifyDate;

    private Integer orderType;

    private String notarisNama;

    private LocalDate tglOrder;

    private String aktaNomor;

    private LocalDate aktaTanggal;

    private String aktaPendirian;
    
    private String aktaPerubahan;
    
    private String skPendirian;
    
    private String skPerubahan;
    
    private String npwp;
    
    private String suratKuasa;
    
    private String ppkPerbaikan;
    
    private String user;
    
    private ZonedDateTime rekapOn;
    
    private Boolean archive;
    
    private String berkasRevisi;

    private String refNomorPk;

    private Integer roda;

    private String merk;

    private String tipe;

    private String warna;

    private String tahunPembuatan;

    private String noRangka;

    private String noMesin;

    private String noBpkb;

    private Long nilaiObjekJaminan;

    private LocalDate tglTxt;
    
    private Boolean verifyObject;
    
    private String ktpBpkb;
    
    private String ktpp;
    
    private String docKontrak;
    
    private String skpjf;
    
    private String ktpDirut;
    
    private String tdp;
    
    private String situ;
    
    private String aktaPend;
    
    private String aktaPerub;
    
    private String siup;
    
    private String ktpDp;
    
    private String nib;
    
    public BerkasDTO(Long id, @Size(max = 150) String identitas, @Size(max = 150) String kk,
			@Size(max = 150) String ppk, @Size(max = 150) String skNasabah, ZonedDateTime updateOn,
			ZonedDateTime uploadOn, @NotNull @Size(max = 20) String ppkNomor, @NotNull @Max(2) Integer nsbJenis,
			Long cabangId, String namaNasabah, LocalDate tanggalPpk, String namaCabang, Long remarkId,
			String remarkName, Integer orderType) {
		this.id = id;
		this.identitas = identitas;
		this.kk = kk;
		this.ppk = ppk;
		this.skNasabah = skNasabah;
		this.updateOn = updateOn;
		this.uploadOn = uploadOn;
		this.ppkNomor = ppkNomor;
		this.nsbJenis = nsbJenis;
		this.cabangId = cabangId;
		this.namaNasabah = namaNasabah;
		this.tanggalPpk = tanggalPpk;
		this.namaCabang = namaCabang;
		this.remarkId = remarkId;
		this.remarkName = remarkName;
		this.orderType = orderType;
	}

	public void setRefNomorPk(String refNomorPk) {
		this.refNomorPk = refNomorPk;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getIdentitas() {
        return identitas;
    }

    public void setIdentitas(String identitas) {
        this.identitas = identitas;
    }
    public String getKk() {
        return kk;
    }

    public void setKk(String kk) {
        this.kk = kk;
    }
    public String getPpk() {
        return ppk;
    }

    public void setPpk(String ppk) {
        this.ppk = ppk;
    }
    public Integer getRecordStatus() {
        return recordStatus;
    }

    public void setRecordStatus(Integer recordStatus) {
        this.recordStatus = recordStatus;
    }
    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }
    public ZonedDateTime getUpdateOn() {
        return updateOn;
    }

    public void setUpdateOn(ZonedDateTime updateOn) {
        this.updateOn = updateOn;
    }
    public String getPpkNomor() {
        return ppkNomor;
    }

    public void setPpkNomor(String ppkNomor) {
        this.ppkNomor = ppkNomor;
    }
    public Integer getNsbJenis() {
        return nsbJenis;
    }

    public void setNsbJenis(Integer nsbJenis) {
        this.nsbJenis = nsbJenis;
    }

    public Long getCabangId() {
        return cabangId;
    }

    public void setCabangId(Long cabangId) {
        this.cabangId = cabangId;
    }

    public String getPpk2() {
        return ppk2;
    }

    public void setPpk2(String ppk2) {
        this.ppk2 = ppk2;
    }

    public String getSkNasabah() {
        return skNasabah;
    }

    public void setSkNasabah(String skNasabah) {
        this.skNasabah = skNasabah;
    }

    public String getUploadBy() {
        return uploadBy;
    }

    public void setUploadBy(String uploadBy) {
        this.uploadBy = uploadBy;
    }

    public ZonedDateTime getUploadOn() {
        return uploadOn;
    }

    public void setUploadOn(ZonedDateTime uploadOn) {
        this.uploadOn = uploadOn;
    }

    public Cabang getCabang() {
        return cabang;
    }

    public void setCabang(Cabang cabang) {
        this.cabang = cabang;
    }

    public Integer getBerkasStatus() {
        return berkasStatus;
    }

    public void setBerkasStatus(Integer berkasStatus) {
        this.berkasStatus = berkasStatus;
    }

    public String getNamaNasabah() {
        return namaNasabah;
    }

    public void setNamaNasabah(String namaNasabah) {
        this.namaNasabah = namaNasabah;
    }

    public LocalDate getTanggalPpk() {
        return tanggalPpk;
    }

    public void setTanggalPpk(LocalDate tanggalPpk) {
        this.tanggalPpk = tanggalPpk;
    }

    public Boolean getRegistrasiStatus() {
        return registrasiStatus;
    }

    public void setRegistrasiStatus(Boolean registrasiStatus) {
        this.registrasiStatus = registrasiStatus;
    }

    public Long getSkSubstitusiId() {
        return skSubstitusiId;
    }

    public void setSkSubstitusiId(Long skSubstitusiId) {
        this.skSubstitusiId = skSubstitusiId;
    }

    public SkSubstitusi getSkSubstitusi() {
        return skSubstitusi;
    }

    public void setSkSubstitusi(SkSubstitusi skSubstitusi) {
        this.skSubstitusi = skSubstitusi;
    }

    public String getNamaCabang() {
        return namaCabang;
    }

    public void setNamaCabang(String namaCabang) {
        this.namaCabang = namaCabang;
    }

    public String getNoSkSubstitusi() {
        return noSkSubstitusi;
    }

    public void setNoSkSubstitusi(String noSkSubstitusi) {
        this.noSkSubstitusi = noSkSubstitusi;
    }

    public Integer getMetodeKirim() {
        return metodeKirim;
    }

    public void setMetodeKirim(Integer metodeKirim) {
        this.metodeKirim = metodeKirim;
    }

    public Long getRemarkId() {
        return remarkId;
    }

    public void setRemarkId(Long remarkId) {
        this.remarkId = remarkId;
    }

    public String getRemarkName() {
        return remarkName;
    }

    public void setRemarkName(String remarkName) {
        this.remarkName = remarkName;
    }

    public Integer getPartitionId() {
        return partitionId;
    }

    public void setPartitionId(Integer partitionId) {
        this.partitionId = partitionId;
    }

    public String getVerifyBy() {
        return verifyBy;
    }

    public void setVerifyBy(String verifyBy) {
        this.verifyBy = verifyBy;
    }

    public ZonedDateTime getVerifyOn() {
        return verifyOn;
    }

    public void setVerifyOn(ZonedDateTime verifyOn) {
        this.verifyOn = verifyOn;
    }

    public LocalDate getVerifyDate() {
        return verifyDate;
    }

    public void setVerifyDate(LocalDate verifyDate) {
        this.verifyDate = verifyDate;
    }

    public Integer getOrderType() {
        return orderType!=null ? orderType : 0;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = (orderType!=null ? orderType : 0);
    }

    public String getNotarisNama() {
        return notarisNama;
    }

    public void setNotarisNama(String notarisNama) {
        this.notarisNama = notarisNama;
    }

    public LocalDate getTglOrder() {
        return tglOrder;
    }

    public void setTglOrder(LocalDate tglOrder) {
        this.tglOrder = tglOrder;
    }

    public String getAktaNomor() {
        return aktaNomor;
    }

    public void setAktaNomor(String aktaNomor) {
        this.aktaNomor = aktaNomor;
    }

    public LocalDate getAktaTanggal() {
        return aktaTanggal;
    }

    public void setAktaTanggal(LocalDate aktaTanggal) {
        this.aktaTanggal = aktaTanggal;
    }

	public String getAktaPendirian() {
		return aktaPendirian;
	}

	public void setAktaPendirian(String aktaPendirian) {
		this.aktaPendirian = aktaPendirian;
	}

	public String getAktaPerubahan() {
		return aktaPerubahan;
	}

	public void setAktaPerubahan(String aktaPerubahan) {
		this.aktaPerubahan = aktaPerubahan;
	}

	public String getSkPendirian() {
		return skPendirian;
	}

	public void setSkPendirian(String skPendirian) {
		this.skPendirian = skPendirian;
	}

	public String getSkPerubahan() {
		return skPerubahan;
	}

	public void setSkPerubahan(String skPerubahan) {
		this.skPerubahan = skPerubahan;
	}

	public String getNpwp() {
		return npwp;
	}

	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}

	public String getSuratKuasa() {
		return suratKuasa;
	}

	public void setSuratKuasa(String suratKuasa) {
		this.suratKuasa = suratKuasa;
	}

	public String getPpkPerbaikan() {
		return ppkPerbaikan;
	}

	public void setPpkPerbaikan(String ppkPerbaikan) {
		this.ppkPerbaikan = ppkPerbaikan;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public ZonedDateTime getRekapOn() {
		return rekapOn;
	}

	public void setRekapOn(ZonedDateTime rekapOn) {
		this.rekapOn = rekapOn;
	}

	public String getRefNomorPk() {
		return refNomorPk;
	}

	public void setRef_nomor_pk(String refNomorPk) {
		this.refNomorPk = refNomorPk;
	}

	public Boolean getArchive() {
		return archive;
	}

	public void setArchive(Boolean archive) {
		this.archive = archive;
	}

	public String getBerkasRevisi() {
		return berkasRevisi;
	}

	public void setBerkasRevisi(String berkasRevisi) {
		this.berkasRevisi = berkasRevisi;
	}

	public Integer getRoda() {
		return roda;
	}

	public void setRoda(Integer roda) {
		this.roda = roda;
	}

	public String getMerk() {
		return merk;
	}

	public void setMerk(String merk) {
		this.merk = merk;
	}

	public String getTipe() {
		return tipe;
	}

	public void setTipe(String tipe) {
		this.tipe = tipe;
	}

	public String getWarna() {
		return warna;
	}

	public void setWarna(String warna) {
		this.warna = warna;
	}

	public String getTahunPembuatan() {
		return tahunPembuatan;
	}

	public void setTahunPembuatan(String tahunPembuatan) {
		this.tahunPembuatan = tahunPembuatan;
	}

	public String getNoRangka() {
		return noRangka;
	}

	public void setNoRangka(String noRangka) {
		this.noRangka = noRangka;
	}

	public String getNoMesin() {
		return noMesin;
	}

	public void setNoMesin(String noMesin) {
		this.noMesin = noMesin;
	}

	public String getNoBpkb() {
		return noBpkb;
	}

	public void setNoBpkb(String noBpkb) {
		this.noBpkb = noBpkb;
	}

	public Long getNilaiObjekJaminan() {
		return nilaiObjekJaminan;
	}

	public void setNilaiObjekJaminan(Long nilaiObjekJaminan) {
		this.nilaiObjekJaminan = nilaiObjekJaminan;
	}

	public LocalDate getTglTxt() {
		return tglTxt;
	}

	public void setTglTxt(LocalDate tglTxt) {
		this.tglTxt = tglTxt;
	}

	public Boolean getVerifyObject() {
		return verifyObject;
	}

	public void setVerifyObject(Boolean verifyObject) {
		this.verifyObject = verifyObject;
	}public String getKtpBpkb() {
		return ktpBpkb;
	}

	public void setKtpBpkb(String ktpBpkb) {
		this.ktpBpkb = ktpBpkb;
	}

	public String getKtpp() {
		return ktpp;
	}

	public void setKtpp(String ktpp) {
		this.ktpp = ktpp;
	}

	public String getDocKontrak() {
		return docKontrak;
	}

	public void setDocKontrak(String docKontrak) {
		this.docKontrak = docKontrak;
	}

	public String getSkpjf() {
		return skpjf;
	}

	public void setSkpjf(String skpjf) {
		this.skpjf = skpjf;
	}

	public String getKtpDirut() {
		return ktpDirut;
	}

	public void setKtpDirut(String ktpDirut) {
		this.ktpDirut = ktpDirut;
	}

	public String getTdp() {
		return tdp;
	}

	public void setTdp(String tdp) {
		this.tdp = tdp;
	}

	public String getSitu() {
		return situ;
	}

	public void setSitu(String situ) {
		this.situ = situ;
	}

	public String getAktaPend() {
		return aktaPend;
	}

	public void setAktaPend(String aktaPend) {
		this.aktaPend = aktaPend;
	}

	public String getAktaPerub() {
		return aktaPerub;
	}

	public void setAktaPerub(String aktaPerub) {
		this.aktaPerub = aktaPerub;
	}

	public String getSiup() {
		return siup;
	}

	public void setSiup(String siup) {
		this.siup = siup;
	}

	public String getKtpDp() {
		return ktpDp;
	}

	public void setKtpDp(String ktpDp) {
		this.ktpDp = ktpDp;
	}

	public String getNib() {
		return nib;
	}

	public void setNib(String nib) {
		this.nib = nib;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BerkasDTO berkasDTO = (BerkasDTO) o;

        if ( ! Objects.equals(id, berkasDTO.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

	@Override
	public String toString() {
		return "BerkasDTO [id=" + id + ", identitas=" + identitas + ", kk=" + kk + ", ppk=" + ppk + ", ppk2=" + ppk2
				+ ", skNasabah=" + skNasabah + ", recordStatus=" + recordStatus + ", updateBy=" + updateBy
				+ ", updateOn=" + updateOn + ", uploadBy=" + uploadBy + ", uploadOn=" + uploadOn + ", ppkNomor="
				+ ppkNomor + ", nsbJenis=" + nsbJenis + ", cabangId=" + cabangId + ", cabang=" + cabang
				+ ", berkasStatus=" + berkasStatus + ", namaNasabah=" + namaNasabah + ", tanggalPpk=" + tanggalPpk
				+ ", registrasiStatus=" + registrasiStatus + ", skSubstitusiId=" + skSubstitusiId + ", skSubstitusi="
				+ skSubstitusi + ", namaCabang=" + namaCabang + ", noSkSubstitusi=" + noSkSubstitusi + ", metodeKirim="
				+ metodeKirim + ", remarkId=" + remarkId + ", remarkName=" + remarkName + ", partitionId=" + partitionId
				+ ", verifyBy=" + verifyBy + ", verifyOn=" + verifyOn + ", verifyDate=" + verifyDate + ", orderType="
				+ orderType + ", notarisNama=" + notarisNama + ", tglOrder=" + tglOrder + ", aktaNomor=" + aktaNomor
				+ ", aktaTanggal=" + aktaTanggal + ", aktaPendirian=" + aktaPendirian + ", aktaPerubahan="
				+ aktaPerubahan + ", skPendirian=" + skPendirian + ", skPerubahan=" + skPerubahan + ", npwp=" + npwp
				+ ", suratKuasa=" + suratKuasa + ", ppkPerbaikan=" + ppkPerbaikan + ", user=" + user + ", rekapOn="
				+ rekapOn + ", archive=" + archive + ", berkasRevisi=" + berkasRevisi + ", refNomorPk=" + refNomorPk
				+ ", roda=" + roda + ", merk=" + merk + ", tipe=" + tipe + ", warna=" + warna + ", tahunPembuatan="
				+ tahunPembuatan + ", noRangka=" + noRangka + ", noMesin=" + noMesin + ", noBpkb=" + noBpkb
				+ ", nilaiObjekJaminan=" + nilaiObjekJaminan + ", tglTxt=" + tglTxt + ", verifyObject=" + verifyObject
				+ ", ktpBpkb=" + ktpBpkb + ", ktpp=" + ktpp + ", docKontrak=" + docKontrak + ", skpjf=" + skpjf
				+ ", ktpDirut=" + ktpDirut + ", tdp=" + tdp + ", situ=" + situ + ", aktaPend=" + aktaPend
				+ ", aktaPerub=" + aktaPerub + ", siup=" + siup + ", ktpDp=" + ktpDp + ", nib=" + nib + "]";
	}
}
