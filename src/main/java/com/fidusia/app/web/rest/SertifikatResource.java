package com.fidusia.app.web.rest;

import com.fidusia.app.domain.Akta;
import com.fidusia.app.domain.Cabang;
import com.fidusia.app.domain.Kendaraan;
import com.fidusia.app.domain.Notaris;
import com.fidusia.app.domain.Ppk;
import com.fidusia.app.domain.RawDataRoya;
import com.fidusia.app.domain.Sertifikat;
import com.fidusia.app.domain.SertifikatTurunan;
import com.fidusia.app.domain.util.AktaStatus;
import com.fidusia.app.domain.util.RawDataRoyaStatus;
import com.fidusia.app.domain.util.SertifikatStatus;
import com.fidusia.app.repository.AktaRepository;
import com.fidusia.app.repository.CabangRepository;
import com.fidusia.app.repository.KendaraanRepository;
import com.fidusia.app.repository.NotarisRepository;
import com.fidusia.app.repository.RawDataRoyaRepository;
import com.fidusia.app.repository.SertifikatRepository;
import com.fidusia.app.repository.SertifikatTurunanRepository;
import com.fidusia.app.security.AuthoritiesConstants;
import com.fidusia.app.security.SecurityUtils;
import com.fidusia.app.service.AhuPendaftaranService;
import com.fidusia.app.service.AhuPenghapusanService;
import com.fidusia.app.service.CabangService;
import com.fidusia.app.service.ReferensiService;
import com.fidusia.app.service.SlackInhookService;
import com.fidusia.app.service.dto.SertifikatDTO;
import com.fidusia.app.service.dto.SertifikatRoyaDTO;
import com.fidusia.app.service.dto.TransaksiAhuDTO;
import com.fidusia.app.util.DateUtils;
import com.fidusia.app.util.ExceptionUtils;
import com.fidusia.app.util.LogUtils;
import com.fidusia.app.web.rest.errors.BadRequestAlertException;
import com.fidusia.app.web.rest.util.CommonResponse;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing {@link com.fidusia.app.domain.Sertifikat}.
 */
@RestController
@RequestMapping("/api/sertifikats")
@Transactional
public class SertifikatResource {

    private final Logger log = LoggerFactory.getLogger(SertifikatResource.class);

    private static final String ENTITY_NAME = "sertifikat";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SertifikatRepository sertifikatRepository;

    private final CabangService cabangService;

    private final AktaRepository aktaRepository;
    
    private final SlackInhookService slackInhookService;
    
    private final AhuPendaftaranService ahuPendaftaranService;
    
    private final KendaraanRepository kendaraanRepository;

    private final RawDataRoyaRepository rawDataRoyaRepository;

    private final AhuPenghapusanService ahuPenghapusanService;

    private final NotarisRepository notarisRepository;

    private final ReferensiService referensiService;

    public SertifikatResource(
    		SertifikatRepository sertifikatRepository, 
    		CabangService cabangService, 
    		AktaRepository aktaRepository,
    		AhuPendaftaranService ahuPendaftaranService,
    		KendaraanRepository kendaraanRepository,
    		SlackInhookService slackInhookService,
    		RawDataRoyaRepository rawDataRoyaRepository,
    		AhuPenghapusanService ahuPenghapusanService,
    		NotarisRepository notarisRepository,
    		ReferensiService referensiService
    		) {
        this.sertifikatRepository = sertifikatRepository;
        this.cabangService = cabangService;
        this.aktaRepository = aktaRepository;
        this.ahuPendaftaranService = ahuPendaftaranService;
        this.kendaraanRepository = kendaraanRepository;
        this.slackInhookService = slackInhookService;
        this.rawDataRoyaRepository = rawDataRoyaRepository;
        this.ahuPenghapusanService = ahuPenghapusanService;
        this.notarisRepository = notarisRepository;
        this.referensiService = referensiService;
    }

    /**
     * {@code POST  /sertifikats} : Create a new sertifikat.
     *
     * @param sertifikat the sertifikat to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new sertifikat, or with status {@code 400 (Bad Request)} if the sertifikat has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<Sertifikat> createSertifikat(@RequestBody Sertifikat sertifikat) throws URISyntaxException {
        log.debug("REST request to save Sertifikat : {}", sertifikat);
        if (sertifikat.getId() != null) {
            throw new BadRequestAlertException("A new sertifikat cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Sertifikat result = sertifikatRepository.save(sertifikat);
        return ResponseEntity
            .created(new URI("/api/sertifikats/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /sertifikats/:id} : Updates an existing sertifikat.
     *
     * @param id the id of the sertifikat to save.
     * @param sertifikat the sertifikat to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated sertifikat,
     * or with status {@code 400 (Bad Request)} if the sertifikat is not valid,
     * or with status {@code 500 (Internal Server Error)} if the sertifikat couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<Sertifikat> updateSertifikat(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Sertifikat sertifikat
    ) throws URISyntaxException {
        log.debug("REST request to update Sertifikat : {}, {}", id, sertifikat);
        if (sertifikat.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, sertifikat.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!sertifikatRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Sertifikat result = sertifikatRepository.save(sertifikat);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, sertifikat.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /sertifikats/:id} : Partial updates given fields of an existing sertifikat, field will ignore if it is null
     *
     * @param id the id of the sertifikat to save.
     * @param sertifikat the sertifikat to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated sertifikat,
     * or with status {@code 400 (Bad Request)} if the sertifikat is not valid,
     * or with status {@code 404 (Not Found)} if the sertifikat is not found,
     * or with status {@code 500 (Internal Server Error)} if the sertifikat couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Sertifikat> partialUpdateSertifikat(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Sertifikat sertifikat
    ) throws URISyntaxException {
        log.debug("REST request to partial update Sertifikat partially : {}, {}", id, sertifikat);
        if (sertifikat.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, sertifikat.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!sertifikatRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Sertifikat> result = sertifikatRepository
            .findById(sertifikat.getId())
            .map(existingSertifikat -> {
                if (sertifikat.getKodeVoucher() != null) {
                    existingSertifikat.setKodeVoucher(sertifikat.getKodeVoucher());
                }
                if (sertifikat.getTglVoucher() != null) {
                    existingSertifikat.setTglVoucher(sertifikat.getTglVoucher());
                }
                if (sertifikat.getNoSertifikat() != null) {
                    existingSertifikat.setNoSertifikat(sertifikat.getNoSertifikat());
                }
                if (sertifikat.getTglSertifikat() != null) {
                    existingSertifikat.setTglSertifikat(sertifikat.getTglSertifikat());
                }
                if (sertifikat.getBiayaPnbp() != null) {
                    existingSertifikat.setBiayaPnbp(sertifikat.getBiayaPnbp());
                }
                if (sertifikat.getNoReferensiBni() != null) {
                    existingSertifikat.setNoReferensiBni(sertifikat.getNoReferensiBni());
                }
                if (sertifikat.getStatus() != null) {
                    existingSertifikat.setStatus(sertifikat.getStatus());
                }
                if (sertifikat.getUpdateBy() != null) {
                    existingSertifikat.setUpdateBy(sertifikat.getUpdateBy());
                }
                if (sertifikat.getUpdateOn() != null) {
                    existingSertifikat.setUpdateOn(sertifikat.getUpdateOn());
                }
                if (sertifikat.getRegistrationId() != null) {
                    existingSertifikat.setRegistrationId(sertifikat.getRegistrationId());
                }
                if (sertifikat.getTglOrder() != null) {
                    existingSertifikat.setTglOrder(sertifikat.getTglOrder());
                }
                if (sertifikat.getTglCancel() != null) {
                    existingSertifikat.setTglCancel(sertifikat.getTglCancel());
                }
                if (sertifikat.getBiayaJasa() != null) {
                    existingSertifikat.setBiayaJasa(sertifikat.getBiayaJasa());
                }
                if (sertifikat.getInvoiceSubmitted() != null) {
                    existingSertifikat.setInvoiceSubmitted(sertifikat.getInvoiceSubmitted());
                }
                if (sertifikat.getReportSubmitted() != null) {
                    existingSertifikat.setReportSubmitted(sertifikat.getReportSubmitted());
                }

                return existingSertifikat;
            })
            .map(sertifikatRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, sertifikat.getId().toString())
        );
    }

    /**
     * {@code GET  /sertifikats} : get all the sertifikats.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of sertifikats in body.
     */
    @GetMapping("")
    public ResponseEntity<List<Sertifikat>> getAllSertifikats(Pageable pageable) {
        log.debug("REST request to get a page of Sertifikats");
        Page<Sertifikat> page = sertifikatRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    @GetMapping("/order/month/now")
    public Long getOrderMonthNow() {
        log.debug("REST request to get a page of getOrderMounthNow");

        LocalDate start = LocalDate.now().withDayOfMonth(1);
        LocalDate end = start.withDayOfMonth(start.lengthOfMonth());
        Long result = sertifikatRepository.countByTglOrderBetween(start, end);
        return result > 0 ? result : (long) 0;    
    }
    
    @GetMapping("/order/month/last")
    public Long getOrderMonthLast() {
        log.debug("REST request to get a page of getOrderMonthLast");

        LocalDate start = LocalDate.now().minusMonths(1).withDayOfMonth(1);
        LocalDate end = start.withDayOfMonth(start.lengthOfMonth());
        Long result = sertifikatRepository.countByTglOrderBetween(start, end);
        return result > 0 ? result : (long) 0;    
    }
    
    @GetMapping("/order/day/now")
    public Long getOrderDayNow() {
        log.debug("REST request to get a page of getOrderMounthNow");

        LocalDate start = LocalDate.now();
        Long result = sertifikatRepository.countByTglOrderBetween(start, start);
        return result > 0 ? result : (long) 0;    
    }
    
    @GetMapping("/order/akta/now")
    public Long getOrderNotarisNow() {
        log.debug("REST request to get a page of getOrderNotarisNow");
        
        boolean isPartner = SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.PARTNER);
        
        Notaris notaris = null;
        if(isPartner) {
        	notaris = cabangService.findNotarisByUserLogin();
        }
        
        LocalDate start = LocalDate.now();
        Long result = aktaRepository.countByTglOrderBetweenAndNotaris(start, start, notaris);
        return result > 0 ? result : (long) 0;    
    }
    
    @GetMapping("/order/akta/month")
    public Long getOrderNotarisMonth() {
        log.debug("REST request to get a page of getOrderNotarisMonth");
        
        boolean isPartner = SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.PARTNER);
        
        Notaris notaris = null;
        if(isPartner) {
        	notaris = cabangService.findNotarisByUserLogin();
        }
        
        LocalDate start = LocalDate.now().withDayOfMonth(1);
        LocalDate end = start.withDayOfMonth(start.lengthOfMonth());
        Long result = aktaRepository.countByTglOrderBetweenAndNotaris(start, end, notaris);
        return result > 0 ? result : (long) 0;    
    }

    /**
     * {@code GET  /sertifikats/:id} : get the "id" sertifikat.
     *
     * @param id the id of the sertifikat to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the sertifikat, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<Sertifikat> getSertifikat(@PathVariable("id") Long id) {
        log.debug("REST request to get Sertifikat : {}", id);
        Optional<Sertifikat> sertifikat = sertifikatRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(sertifikat);
    }

    /**
     * {@code DELETE  /sertifikats/:id} : delete the "id" sertifikat.
     *
     * @param id the id of the sertifikat to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteSertifikat(@PathVariable("id") Long id) {
        log.debug("REST request to delete Sertifikat : {}", id);
        sertifikatRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }

    @GetMapping("/search2")
    @Secured({AuthoritiesConstants.ADMINISTRATOR,AuthoritiesConstants.DIRECTOR,AuthoritiesConstants.OFFICER,AuthoritiesConstants.OPERATOR, AuthoritiesConstants.HEADQUARTER,AuthoritiesConstants.AREAMANAGER,AuthoritiesConstants.FINANCE,AuthoritiesConstants.ADMIN,AuthoritiesConstants.LOAN_RECOVERY})
    public ResponseEntity<List<SertifikatDTO>> search2(
    		@RequestParam(value = "skey", required = false) String skey,
    		@RequestParam(value = "svalue", required = false) String svalue,
    		@RequestParam(value = "sppknomor", required = false) String sppknomor,
    		@ApiParam Pageable pageable) throws URISyntaxException {
        log.debug("REST request to search a page by {} : {} {}", skey, svalue, sppknomor);
      
        boolean isRoleCabang = SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.OFFICER) || SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.MANAGER);
        
        Cabang cabang = isRoleCabang ? cabangService.findByUserLogin(): null ;
        
        Page<SertifikatDTO> page = null;

        if("ppkNomor".equals(skey) && svalue != null) {
          if(isRoleCabang) {
        	  page = sertifikatRepository.findByAktaPpkNomorAndCabang(svalue, cabang.getId(),pageable);
	      } else {
	          page = sertifikatRepository.findByAktaPpkNomor(svalue, pageable);
	      }
        } else if("ppdDate".equals(skey) && svalue != null && cabang == null) {
            LocalDate ppkTanggalDt = LocalDate.parse(svalue);
            page = sertifikatRepository.findByTglOrder2(ppkTanggalDt, pageable);
        } else if("ppdDate".equals(skey) && svalue != null && cabang != null) {
            LocalDate ppkTanggal = LocalDate.parse(svalue);
            page = sertifikatRepository.findByCbngIdAndPpkTanggal(cabang.getId(), ppkTanggal, pageable);
        } else if("invoiceDate".equals(skey) && svalue != null && cabang == null) {
            LocalDate invoiceDt = LocalDate.parse(svalue);
            page = sertifikatRepository.findByInvoiceTanggal(invoiceDt, pageable);
        } else if("invoiceDate".equals(skey) && svalue != null && cabang != null) {
            LocalDate invoiceDt = LocalDate.parse(svalue);
            page = sertifikatRepository.findByCabangAndInvoiceTanggal(cabang.getId(), invoiceDt, pageable);
        } else if("ppdMonth".equals(skey) && svalue != null && cabang == null) {
            LocalDate ppkTanggalFrom = LocalDate.parse(svalue + "-01");
            LocalDate ppkTanggalTo = ppkTanggalFrom.withDayOfMonth(ppkTanggalFrom.lengthOfMonth());
            page = sertifikatRepository.findByTglOrderBetween2(ppkTanggalFrom, ppkTanggalTo, pageable);
        } else if("ppdMonth".equals(skey) && svalue != null && cabang != null) {
            LocalDate ppkTanggalFrom = LocalDate.parse(svalue + "-01");
            LocalDate ppkTanggalTo = ppkTanggalFrom.withDayOfMonth(ppkTanggalFrom.lengthOfMonth());
            page = sertifikatRepository.findByCabangAndAktaPpkTanggalBetween(cabang.getId(), ppkTanggalFrom, ppkTanggalTo, pageable);
        } else {
            LocalDate ppkTanggalFrom = LocalDate.now().withDayOfMonth(1);
            LocalDate ppkTanggalTo = ppkTanggalFrom.withDayOfMonth(ppkTanggalFrom.lengthOfMonth());
            if(cabang != null) {
                page = sertifikatRepository.findByCabangAndAktaPpkTanggalBetween(cabang.getId(), ppkTanggalFrom, ppkTanggalTo, pageable);
            } else {
                page = sertifikatRepository.findByTglOrderBetween2(ppkTanggalFrom, ppkTanggalTo, pageable);
            }
        }
        
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(),page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/search")
    @Secured({AuthoritiesConstants.ADMINISTRATOR,AuthoritiesConstants.DIRECTOR,AuthoritiesConstants.OFFICER,AuthoritiesConstants.OPERATOR, AuthoritiesConstants.HEADQUARTER,AuthoritiesConstants.AREAMANAGER,AuthoritiesConstants.FINANCE,AuthoritiesConstants.ADMIN,AuthoritiesConstants.LOAN_RECOVERY})
    public ResponseEntity<List<SertifikatDTO>> search(
    		@RequestParam(value = "skey", required = false) String skey,
    		@RequestParam(value = "svalue", required = false) String svalue,
    		@RequestParam(value = "sppknomor", required = false) String sppknomor,
    		@ApiParam Pageable pageable) throws URISyntaxException {
        log.debug("REST request to search a page by {} : {} {}", skey, svalue, sppknomor);
      
        boolean isRoleCabang = SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.OFFICER) || SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.MANAGER);
        
        Cabang cabang = isRoleCabang ? cabangService.findByUserLogin(): null ;
        
        Page<SertifikatDTO> page = null;

        if("ppkNomor".equals(skey) && svalue != null) {
          if(isRoleCabang) {
        	  page = sertifikatRepository.findByAktaPpkNomorAndCabang(svalue, cabang.getId(),pageable);
	      } else {
	          page = sertifikatRepository.findByAktaPpkNomor(svalue, pageable);
	      }
        } else if("ppdDate".equals(skey) && svalue != null && cabang == null) {
            LocalDate ppkTanggalDt = LocalDate.parse(svalue);
            page = sertifikatRepository.findByAktaTglOrder(ppkTanggalDt, pageable);
        } else if("ppdDate".equals(skey) && svalue != null && cabang != null) {
            LocalDate ppkTanggal = LocalDate.parse(svalue);
            page = sertifikatRepository.findByCbngIdAndPpkTanggal(cabang.getId(), ppkTanggal, pageable);
        } else if("invoiceDate".equals(skey) && svalue != null && cabang == null) {
            LocalDate invoiceDt = LocalDate.parse(svalue);
            page = sertifikatRepository.findByInvoiceTanggal(invoiceDt, pageable);
        } else if("invoiceDate".equals(skey) && svalue != null && cabang != null) {
            LocalDate invoiceDt = LocalDate.parse(svalue);
            page = sertifikatRepository.findByCabangAndInvoiceTanggal(cabang.getId(), invoiceDt, pageable);
        } else if("ppdMonth".equals(skey) && svalue != null && cabang == null) {
            LocalDate ppkTanggalFrom = LocalDate.parse(svalue + "-01");
            LocalDate ppkTanggalTo = ppkTanggalFrom.withDayOfMonth(ppkTanggalFrom.lengthOfMonth());
            page = sertifikatRepository.findByAktaTglOrderBetween(ppkTanggalFrom, ppkTanggalTo, pageable);
        } else if("ppdMonth".equals(skey) && svalue != null && cabang != null) {
            LocalDate ppkTanggalFrom = LocalDate.parse(svalue + "-01");
            LocalDate ppkTanggalTo = ppkTanggalFrom.withDayOfMonth(ppkTanggalFrom.lengthOfMonth());
            page = sertifikatRepository.findByCabangAndAktaPpkTanggalBetween(cabang.getId(), ppkTanggalFrom, ppkTanggalTo, pageable);
        } else {
            LocalDate ppkTanggalFrom = LocalDate.now().withDayOfMonth(1);
            LocalDate ppkTanggalTo = ppkTanggalFrom.withDayOfMonth(ppkTanggalFrom.lengthOfMonth());
            if(cabang != null) {
                page = sertifikatRepository.findByCabangAndAktaPpkTanggalBetween(cabang.getId(), ppkTanggalFrom, ppkTanggalTo, pageable);
            } else {
                page = sertifikatRepository.findByAktaTglOrderBetween(ppkTanggalFrom, ppkTanggalTo, pageable);
            }
        }
        
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(),page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/search-akta")
    @Secured({AuthoritiesConstants.ADMINISTRATOR,AuthoritiesConstants.DIRECTOR,AuthoritiesConstants.OFFICER,AuthoritiesConstants.OPERATOR, AuthoritiesConstants.HEADQUARTER,AuthoritiesConstants.AREAMANAGER,AuthoritiesConstants.FINANCE,AuthoritiesConstants.ADMIN,AuthoritiesConstants.LOAN_RECOVERY})
    public ResponseEntity<List<SertifikatDTO>> searchAkta(
    		@RequestParam(value = "skey", required = false) String skey,
    		@RequestParam(value = "svalue", required = false) String svalue,
    		@RequestParam(value = "sppknomor", required = false) String sppknomor,
    		@ApiParam Pageable pageable) throws URISyntaxException {
        log.debug("REST request to search a page by {} : {} {}", skey, svalue, sppknomor);
      
        boolean isRoleCabang = SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.OFFICER) || SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.MANAGER);
        
        Cabang cabang = isRoleCabang ? cabangService.findByUserLogin(): null ;
        
        Page<SertifikatDTO> page = null;

        if("ppkNomor".equals(skey) && svalue != null) {
          if(isRoleCabang) {
        	  page = sertifikatRepository.findByAktaPpkNomorAndCabang(svalue, cabang.getId(),pageable);
	      } else {
	          page = sertifikatRepository.findByAktaPpkNomor(svalue, pageable);
	      }
        } else if("ppdDate".equals(skey) && svalue != null && cabang == null) {
            LocalDate tglOrderDt = LocalDate.parse(svalue);
            page = sertifikatRepository.findByTglOrderAkta(tglOrderDt, pageable);
        } else if("ppdDate".equals(skey) && svalue != null && cabang != null) {
            LocalDate ppkTanggal = LocalDate.parse(svalue);
            page = sertifikatRepository.findByCabangIdAndPpkTanggal(cabang.getId(), ppkTanggal, pageable);
        } else if("ppdMonth".equals(skey) && svalue != null && cabang == null) {
            LocalDate tglOrderFrom = LocalDate.parse(svalue + "-01");
            LocalDate tglOrderTo = tglOrderFrom.withDayOfMonth(tglOrderFrom.lengthOfMonth());
            page = sertifikatRepository.findByAktaSertifikatTglOrderBetween(tglOrderFrom, tglOrderTo, pageable);
        } else if("ppdMonth".equals(skey) && svalue != null && cabang != null) {
            LocalDate tglOrderFrom = LocalDate.parse(svalue + "-01");
            LocalDate tglOrderTo = tglOrderFrom.withDayOfMonth(tglOrderFrom.lengthOfMonth());
            page = sertifikatRepository.findByAktaCabangAndAktaTglOrderBetween(cabang.getId(), tglOrderFrom, tglOrderTo, pageable);
        } else {
            LocalDate tglOrderFrom = LocalDate.now().withDayOfMonth(1);
            LocalDate tglOrderTo = tglOrderFrom.withDayOfMonth(tglOrderFrom.lengthOfMonth());
            if(cabang != null) {
                page = sertifikatRepository.findByAktaCabangAndAktaTglOrderBetween(cabang.getId(), tglOrderFrom, tglOrderTo, pageable);
            } else {
                page = sertifikatRepository.findByAktaSertifikatTglOrderBetween(tglOrderFrom, tglOrderTo, pageable);
            }
        }
        
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(),page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    @GetMapping("/search-roya")
    @Secured({AuthoritiesConstants.ADMINISTRATOR,AuthoritiesConstants.DIRECTOR,AuthoritiesConstants.OFFICER,AuthoritiesConstants.OPERATOR, AuthoritiesConstants.HEADQUARTER,AuthoritiesConstants.AREAMANAGER,AuthoritiesConstants.FINANCE,AuthoritiesConstants.ADMIN,AuthoritiesConstants.LOAN_RECOVERY})
    public ResponseEntity<List<SertifikatRoyaDTO>> searchRoya(
    		@RequestParam(value = "skey", required = false) String skey,
    		@RequestParam(value = "svalue", required = false) String svalue,
    		@RequestParam(value = "sppknomor", required = false) String sppknomor,
    		@ApiParam Pageable pageable) throws URISyntaxException {
        log.debug("REST request to search roya a page by {} : {} {}", skey, svalue, sppknomor);
      
        boolean isRoleCabang = SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.OFFICER) || SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.MANAGER);
        
        Cabang cabang = isRoleCabang ? cabangService.findByUserLogin(): null ;
        
        Page<SertifikatRoyaDTO> page = null;

        if("ppkNomor".equals(skey) && svalue != null) {
          if(isRoleCabang) {
	          page = rawDataRoyaRepository.findByUniqueKey(svalue, pageable);
//        	  page = rawDataRoyaRepository.findByAktaPpkNomorAndCabang(svalue, cabang.getId(),pageable);
	      } else {
	          page = rawDataRoyaRepository.findByUniqueKey(svalue, pageable);
	      }
        } else if("ppdDate".equals(skey) && svalue != null && cabang == null) {
            LocalDate tglOrderDt = LocalDate.parse(svalue);
            page = rawDataRoyaRepository.findByRoyaTglOrderBetween(tglOrderDt, tglOrderDt, pageable);
        } else if("ppdDate".equals(skey) && svalue != null && cabang != null) {
            LocalDate ppkTanggal = LocalDate.parse(svalue);
//            page = rawDataRoyaRepository.findByCabangIdAndPpkTanggal(cabang.getId(), ppkTanggal, pageable);
            page = rawDataRoyaRepository.findByRoyaTglOrderBetween(ppkTanggal, ppkTanggal, pageable);
        } else if("ppdMonth".equals(skey) && svalue != null && cabang == null) {
            LocalDate tglOrderFrom = LocalDate.parse(svalue + "-01");
            LocalDate tglOrderTo = tglOrderFrom.withDayOfMonth(tglOrderFrom.lengthOfMonth());
            page = rawDataRoyaRepository.findByRoyaTglOrderBetween(tglOrderFrom, tglOrderTo, pageable);
        } else if("ppdMonth".equals(skey) && svalue != null && cabang != null) {
            LocalDate tglOrderFrom = LocalDate.parse(svalue + "-01");
            LocalDate tglOrderTo = tglOrderFrom.withDayOfMonth(tglOrderFrom.lengthOfMonth());
            page = rawDataRoyaRepository.findByRoyaTglOrderBetween(tglOrderFrom, tglOrderTo, pageable);
//            page = rawDataRoyaRepository.findByAktaCabangAndAktaTglOrderBetween(cabang.getId(), tglOrderFrom, tglOrderTo, pageable);
        } else {
            LocalDate tglOrderFrom = LocalDate.now().withDayOfMonth(1);
            LocalDate tglOrderTo = tglOrderFrom.withDayOfMonth(tglOrderFrom.lengthOfMonth());
            if(cabang != null) {
//                page = rawDataRoyaRepository.findByAktaCabangAndAktaTglOrderBetween(cabang.getId(), tglOrderFrom, tglOrderTo, pageable);
                page = rawDataRoyaRepository.findByAktaCabangAndAktaTglOrderBetween(tglOrderFrom, tglOrderTo, pageable);
            } else {
                page = rawDataRoyaRepository.findByRoyaTglOrderBetween(tglOrderFrom, tglOrderTo, pageable);
            }
        }
        
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(),page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<String> register(
			@RequestParam(value = "sessionid", required = true) String sessionid,
			@RequestParam(value = "ppknomor", required = false) String ppknomor,
			@RequestParam(value = "orderdate", required = false) String orderdate,
			@RequestParam(value = "limit", required = false) String limit,
			@RequestParam(value = "sublimit", required = false) String sublimit,
			@RequestParam(value = "skipfile", required = false) String skipfile,
			@RequestParam(value = "mandiri", required = false) String mandiri) {
		
        log.info("REST request to register sessionid:{} ppknomor:{} limit:{} sublimit:{} skipfile:{}", sessionid, ppknomor, limit, sublimit, skipfile);
        
        String service = "/api/sertifikat/register";
        String _sessionid = (sessionid!=null && sessionid.length()>0) ? "sessionid=" + sessionid + "&" : "";
        String _ppknomor = (ppknomor!=null && ppknomor.length()>0) ? "ppknomor=" + ppknomor + "&" : "";
        String _limit = (limit!=null && limit.length()>0) ? "limit=" + limit + "&" : "";
        String _sublimit = (sublimit!=null && sublimit.length()>0) ? "sublimit=" + sublimit + "&" : "";
        String _skipfile = (skipfile!=null && skipfile.length()>0) ? "skipfile=" + skipfile + "&" : "";
        String _mandiri = (mandiri!=null && mandiri.length()>0) ? "mandiri=" + mandiri + "&" : "";
        String parameter = _sessionid + _ppknomor + _limit + _sublimit + _skipfile + _mandiri;
        
        Boolean sMandiri = mandiri != null ? Boolean.parseBoolean(mandiri) : false;
		ZonedDateTime startTime = ZonedDateTime.now();
        CommonResponse response = new CommonResponse(service, parameter, startTime);

        
        try {
			List<Akta> aktas = new ArrayList<>();
        	int iLimit = limit!=null ? Integer.parseInt(limit) : Integer.MAX_VALUE;
        	
			if(ppknomor!=null && !ppknomor.isEmpty()) {
				Akta akta = aktaRepository.findOneByPpkNomor(ppknomor);
				
				if(akta != null) {
					List<Sertifikat> sertifikats = sertifikatRepository.findByAktaPpkNomor(ppknomor);
	                if(sertifikats!=null && !sertifikats.isEmpty()) {
	                	akta.setSertifikat(sertifikats.get(0));
	                }
					aktas.add(akta);
				}
			} else if(orderdate != null && !orderdate.isEmpty()){
                List<Akta> aPage = aktaRepository.findByStatusAndTglOrder(AktaStatus.ORDERED.value(), LocalDate.parse(orderdate));
                
                if(aPage != null) {
                	for (Akta akta : aPage) {
                		aktas.add(akta);
					}
                };
			}
    		
			List<Sertifikat> sertifikatList = new ArrayList<>();

			int bSize = 50;

			for(int i=0,l=aktas.size(); i<l; i++) {
				ZonedDateTime currentTime = ZonedDateTime.now();
				ZonedDateTime pauseTimeStart = ZonedDateTime.now().withHour(5).withMinute(15);
				ZonedDateTime pauseTimeEnd = ZonedDateTime.now().withHour(5).withMinute(45);
				if(currentTime.isAfter(pauseTimeStart) && currentTime.isBefore(pauseTimeEnd)) {
					Thread.sleep(1800000);
				}

				Akta akta = aktas.get(i);
				Sertifikat sertifikat = null;
				boolean rereg = akta.getSertifikat() != null && akta.getSertifikat().getId() != null;

				if(rereg) {
					sertifikat = akta.getSertifikat();
					sertifikat.setKodeVoucher(null);
					sertifikat.setTglVoucher(null);
					sertifikat.setNoSertifikat(null);
					sertifikat.setTglSertifikat(null);
					sertifikat.setBiayaPnbp(null);
					sertifikat.setRegistrationId(null);
				} else {
					sertifikat = new Sertifikat()
							.akta(akta)
							.cabang(akta.getCabang())
							.tglOrder(akta.getTglOrder())
							.invoiceSubmitted(false)
							.reportSubmitted(false);
				}
				
				Ppk ppk = akta.getPpk();
				List<Kendaraan> kendaraans = kendaraanRepository.findByPpk(ppk);
				ppk.setKendaraans(kendaraans);
				akta.setPpk(ppk);
				log.debug("sertifikat no akta:{}", sertifikat.getAkta().getNomor());
				
				sertifikat = ahuPendaftaranService.register(sertifikat, sessionid);
				
				if(sertifikat != null && sertifikat.getRegistrationId() != null) {
					
					String kocer = null;
					if (sMandiri) {
						kocer = ahuPendaftaranService.getVoucherMandiri(sessionid, sertifikat.getRegistrationId());
					}
					sertifikat.setBiayaJasa(null); // TODO
					sertifikat.setKodeVoucher(kocer);
					sertifikat.setTglVoucher(ZonedDateTime.now());
					sertifikat.setUpdateBy("BACKEND");
					sertifikat.setUpdateOn(ZonedDateTime.now());
					sertifikatList.add(sertifikat);
					
					sertifikat.setStatus(SertifikatStatus.REGISTERED.value());
					Long biayaPnbp = ahuPendaftaranService.getBiayaPnbp(akta.getPpk().getNilaiPenjaminan());
					
					if(biayaPnbp != null) {
						sertifikat.setBiayaPnbp(biayaPnbp);
					}

					sertifikatRepository.save(sertifikat);
					akta.setStatus(AktaStatus.REGISTERED.value());
					aktaRepository.save(akta);
					
					ahuPendaftaranService.getVoucherFile(sessionid, sertifikat.getRegistrationId(), ppk.getNomor());
				} else {
					continue;
				}
			}

			Long totalRegistered = sertifikatRepository.findTotalRegistered(startTime, SertifikatStatus.REGISTERED.value());
			Long totalPending = aktaRepository.countByStatus(AktaStatus.ORDERED.value());
			
			StringBuilder sb = new StringBuilder();
			sb.append("\n* Registrasi AHU COMPLETED").append(" *");
			sb.append("\nsuccess    ").append(totalRegistered!=null ? totalRegistered : 0);
			sb.append("\npending    ").append(totalPending!=null ? totalPending : 0);
	        LogUtils.debug(response = response.complete("COMPLETED", sb.toString()), log, slackInhookService);
	        
	        
	        return ResponseEntity.status(HttpStatus.OK).body(response.toString());
		} catch (Exception e) {
			log.error("Error on register()", e);

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response.toString());
		}
    }
    
    @RequestMapping(value = "/download", method = RequestMethod.POST)
    public ResponseEntity<String> download(
    		@RequestParam("sessionid") String sessionid,
			@RequestParam(value = "orderdate", required = false) String orderdate,
			@RequestParam(value = "ppknomor", required = false) String ppknomor) {
		
        log.info("REST request to download sessionId: {} orderdate:{} ppknomor:{} ", sessionid, orderdate, ppknomor);

        String service = "/sertifikat/download";
        String parameter = "sessionid="+sessionid+"&orderdate="+orderdate+"&ppknomor="+ppknomor;
        CommonResponse response = new CommonResponse(service, parameter);
        
		LocalDate orderDt = orderdate!=null ? LocalDate.parse(orderdate):LocalDate.now();

        try {
			log.info("online download sertifikat {} {} {}", sessionid, orderdate, ppknomor);
			if ( orderdate != null ) {
				List<Sertifikat> listSertifikat = sertifikatRepository.findByTglOrderAndStatus(orderDt, SertifikatStatus.REGISTERED.value());
				
				if (listSertifikat.size() > 0 || listSertifikat != null ) {
					for (Sertifikat sertifikat : listSertifikat) {
						ahuPendaftaranService.getSertifikat(sertifikat.getAkta().getPpk().getNomor(), sertifikat.getTglOrder(), sertifikat.getRegistrationId(), sessionid);
						
					}
				}
			} else if (ppknomor != null){
				Sertifikat sertifikat = sertifikatRepository.findByAktaPpkNomor(ppknomor).get(0);
				
				ahuPendaftaranService.getSertifikat(sertifikat.getAkta().getPpk().getNomor(), sertifikat.getTglOrder(), sertifikat.getRegistrationId(), sessionid);
				
			}

			long success = sertifikatRepository.countByTglOrderAndStatus(orderDt, SertifikatStatus.DONE.value());
			long pending = sertifikatRepository.countByTglOrderAndStatus(orderDt, SertifikatStatus.REGISTERED.value());

			StringBuilder resultSb = new StringBuilder();
			resultSb.append("\n* Download Sertifikat ").append(" *");
			resultSb.append("\nsuccess  ").append(success);
			resultSb.append("\npending  ").append(pending);
			resultSb.append("\ntotal    ").append(success + pending);
			
	        LogUtils.debug(response = response.complete("COMPLETE", resultSb.toString()), log, slackInhookService);
	        
	        return ResponseEntity.status(HttpStatus.OK).body(response.toString());
		} catch (Exception e) {
			log.error("Error on download", e);
			
	        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response.toString());
		}
    }
	
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/sertifikat/roya",
			method = RequestMethod.POST)
	public ResponseEntity<String> processRoya(
			@RequestParam(value = "sessionid", required = true) String sessionid,
			@RequestParam(value = "tglorder", required = false) String tglorder,
			@RequestParam(value = "orderstatus", required = false) String orderstatus,
			@RequestParam(value = "ppknomor", required = false) String ppknomor,
			@RequestParam(value = "desentralisasi", required = false) String desentralisasi,
			@RequestParam(value = "force", required = false) String force,
			@RequestParam(value = "limit", required = false) String limit) {
		
		log.info("REST request to processRoya sessionid:{} tglorder:{} tgllunasfrom:{} tgllunasto:{} orderstatus:{} ppknomor:{} filename:{} desentralisasi:{} force:{} limit:{}", sessionid, tglorder, orderstatus, ppknomor, desentralisasi, force, limit);
		
        String service = "/sertifikat/roya";
        String _sessionid = (sessionid!=null && sessionid.length()>0) ? "sessionid=" + sessionid + "&" : "";
        String _tglorder = (tglorder!=null && tglorder.length()>0) ? "tglorder=" + tglorder + "&" : "";
        String _orderstatus = (orderstatus!=null && orderstatus.length()>0) ? "orderstatus=" + orderstatus + "&" : "";
        String _ppknomor = (ppknomor!=null && ppknomor.length()>0) ? "ppknomor=" + ppknomor + "&" : "";
        String _desentralisasi = (desentralisasi!=null && desentralisasi.length()>0) ? "desentralisasi=" + desentralisasi + "&" : "";
        String _force = (force!=null && force.length()>0) ? "force=" + force + "&" : "";
        String _limit = (limit!=null && limit.length()>0) ? "limit=" + limit + "&" : "";
        String parameter = _sessionid + _tglorder + _orderstatus+ _ppknomor + _desentralisasi + _force + _limit;
        CommonResponse response = new CommonResponse(service, parameter);

		try {
			String[] defaultStatusList = {RawDataRoyaStatus.INPROGRESS, RawDataRoyaStatus.FAILED, RawDataRoyaStatus.ERROR};
			
			List<RawDataRoya>  list = null;
			
			if("true".equals(desentralisasi)) {
				int iLimit = limit!=null ? Integer.parseInt(limit) : Integer.MAX_VALUE;
				
				Page<RawDataRoya> raws = rawDataRoyaRepository.findByDesentralisasiAndStatusAndTglOrder("true".equals(desentralisasi), orderstatus!=null ? orderstatus.split(",") : defaultStatusList, LocalDate.parse(tglorder), PageRequest.of(0, iLimit));
				if(raws.hasContent()) {
					if(list == null) list = new ArrayList<RawDataRoya>();
					List<RawDataRoya> content = raws.getContent();
					list.addAll(content);
					iLimit = iLimit - content.size();
				}
			} else if(tglorder!=null && tglorder.length()>0) {
				int iLimit = limit!=null ? Integer.parseInt(limit) : Integer.MAX_VALUE;
				
				String[] items = tglorder.split(",");
				for (String item : items) {
					if(iLimit == 0) break;
					
					LocalDate date = LocalDate.parse(item);
					Page<RawDataRoya> raws = rawDataRoyaRepository.findByTglOrderAndStatus(date, orderstatus!=null ? orderstatus.split(",") : defaultStatusList, PageRequest.of(0, iLimit));
					if(raws.hasContent()) {
						if(list == null) list = new ArrayList<RawDataRoya>();
						List<RawDataRoya> content = raws.getContent();
						list.addAll(content);
						iLimit = iLimit - content.size();
					}
				}
			} else if(ppknomor!=null && ppknomor.length()>0) {
				list = rawDataRoyaRepository.findByUniqueKeyAndStatus(ppknomor, orderstatus!=null ? orderstatus.split(",") : defaultStatusList);
			} 
			
			if(list != null) {
				if("true".equals(desentralisasi)) {
					ahuPenghapusanService.processRoyaDesentralisasi(list, LocalDate.now(), sessionid, "true".equalsIgnoreCase(force));
				}
				
		    	LogUtils.debug(response = response.complete("COMPLETED"), log, slackInhookService);
		        return ResponseEntity.ok().body(response.toString());
			} else {
				LogUtils.debug(response = response.complete("FAILED", "Record not found"), log, slackInhookService);
		        return ResponseEntity.badRequest().body(response.toString());
			}
		} catch (Exception e) {
			log.error("Error on processRoya()", e);

        	LogUtils.debug(response = response.complete("FAILED", ExceptionUtils.getMessage(e), ExceptionUtils.getStackTrace(e)), log, slackInhookService);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response.toString());	
		}
	}
	

    
    @RequestMapping(value = "/readpdf", method = RequestMethod.POST)
    public ResponseEntity<String> readPdfRoya(
			@RequestParam(value = "orderdate", required = false) String orderdate) {
		
        log.info("REST request to readPdfRoya orderdate:{}", orderdate);

        String service = "/sertifikat/readpdf";
        String parameter = "orderdate="+orderdate;
        CommonResponse response = new CommonResponse(service, parameter);

        try {
			log.info("readpdf sertifikat roya {} {} {}", orderdate);
			if ( orderdate != null ) {
				ahuPenghapusanService.readPdfInDirectory(orderdate);
			} 
			
	        LogUtils.debug(response = response.complete("COMPLETE"), log, slackInhookService);
	        
	        return ResponseEntity.status(HttpStatus.OK).body(response.toString());
		} catch (Exception e) {
			log.error("Error on readPdfRoya", e);
			
	        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response.toString());
		}
    }
    
    @PostMapping("/monitorings/updateroya")
    public ResponseEntity<String> updateRawDataRoya(
			@RequestHeader(value="x-api-key", required = true)  String key,
			@RequestParam(value="uniquekey", required = true) String uniquekey,
			@RequestParam(value="status", required = true) String status,
			@RequestParam(value="message", required = false) String message,
			@RequestParam(value="noroya", required = false) String noroya,
			@RequestParam(value="tglroya", required = false) String tglroya,
			@RequestParam(value="regid", required = false) String regid )
        throws URISyntaxException {
        log.debug("REST request to updateRawDataRoya {}-{}-{}-{}-{}", uniquekey, status, message, noroya, tglroya);
    	String apiKey = referensiService.applicationProp1("API_KEY_INTERNAL");
    	log.debug("paramkey: " + key);
    	log.debug("apiKey: " + apiKey);
	    if (apiKey.equals(key)) { 
	    	RawDataRoya roya = rawDataRoyaRepository.findByUniqueKey(uniquekey).get(0);
	    	if (roya != null) {
	    		if (RawDataRoyaStatus.SUCCESS.equals(roya.getStatus())) {

	    	        return ResponseEntity.status(HttpStatus.OK).body(roya.getUniqueKey() + " Already save success");
	    		} else {
		    		if (RawDataRoyaStatus.SUCCESS.equals(status)) {
			    		roya.setNomorRoya(noroya);
			    		roya.setTglRoya(LocalDate.parse(tglroya));
			    		roya.setRegistrationId(regid);
		    		} else {
			    		roya.setMessage(message);
		    		}
		    		
		    		roya.setStatus(status);
		    		roya.setLastUpdateOn(ZonedDateTime.now());
		    		roya.setLastUpdateBy("UpdateByFrontEndSystem");
		    		
		    		rawDataRoyaRepository.save(roya);
	    	        return ResponseEntity.status(HttpStatus.OK).body("Save success " + uniquekey + " status " + status);
	    		}
	    	} else {
    	        return ResponseEntity.status(HttpStatus.OK).body(uniquekey + " notfound in system");
	    	}
	    } else {
	        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
	    }
    }
    
    
}
