export interface IKeyNotarisDTO {
        id?:number,
		nama?: string,
}

export class KeyNotarisDTO implements IKeyNotarisDTO {
  constructor(
	  	public id?:number,
		public nama?: string
  ) {}
}
