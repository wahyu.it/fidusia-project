package com.fidusia.app.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {
	
	 public static final String ADMIN = "ROLE_ADMIN";

	    public static final String USER = "ROLE_USER";

	    public static final String HEADQUARTER = "ROLE_HEADQUARTER";

	    public static final String AREAMANAGER = "ROLE_AREAMANAGER";

	    public static final String MANAGER = "ROLE_MANAGER";

	    public static final String OFFICER = "ROLE_OFFICER";

	    public static final String PARTNER = "ROLE_PARTNER";

	    public static final String PARTNER_STAFF = "ROLE_PARTNER_STAFF";

	    public static final String OPERATOR = "ROLE_OPERATOR";

	    public static final String OPERATORMANAGER = "ROLE_OPERATORMANAGER";

	    public static final String DIRECTOR = "ROLE_DIRECTOR";

	    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

	    public static final String LEGAL = "ROLE_LEGAL";

	    public static final String FINANCE = "ROLE_FINANCE";
	    
	    public static final String LOGISTIC = "ROLE_LOGISTIC";
	    
	    public static final String CLUSTER = "ROLE_CLUSTER";
	    
	    public static final String LOAN_RECOVERY = "ROLE_LOAN_RECOVERY";
		
		 public static final String ADMINISTRATOR = "ROLE_ADMINISTRATOR";

    private AuthoritiesConstants() {
    }
}
