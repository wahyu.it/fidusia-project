package com.fidusia.app.service;

import com.fidusia.app.config.Constants;
import com.fidusia.app.domain.Authority;
import com.fidusia.app.domain.Cabang;
import com.fidusia.app.domain.EntityUser;
import com.fidusia.app.domain.Leasing;
import com.fidusia.app.domain.Notaris;
import com.fidusia.app.domain.User;
import com.fidusia.app.repository.AuthorityRepository;
import com.fidusia.app.repository.CabangRepository;
import com.fidusia.app.repository.EntityUserRepository;
import com.fidusia.app.repository.LeasingRepository;
import com.fidusia.app.repository.NotarisRepository;
import com.fidusia.app.repository.UserRepository;
import com.fidusia.app.security.AuthoritiesConstants;
import com.fidusia.app.security.SecurityUtils;
import com.fidusia.app.service.dto.UserDTO;
import com.fidusia.app.service.dto.UserLeasingDTO;
import com.fidusia.app.util.DateUtils;
import com.fidusia.app.web.rest.errors.EmailAlreadyUsedException;
import com.fidusia.app.web.rest.errors.EmailNotFoundException;

import io.github.jhipster.security.RandomUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DecimalFormat;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service class for managing users.
 */
@Service
@Transactional
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);

    private final UserRepository userRepository;

    private final CabangRepository cabangRepository;

    private final NotarisRepository notarisRepository;

    private final PasswordEncoder passwordEncoder;

    private final AuthorityRepository authorityRepository;

    private final CacheManager cacheManager;

    private final MailService mailService;

    private final EntityUserRepository entityUserRepository;

    private final LeasingRepository leasingRepository;

    public UserService(NotarisRepository notarisRepository, CabangRepository cabangRepository, LeasingRepository leasingRepository, MailService mailService, EntityUserRepository entityUserRepository, UserRepository userRepository, PasswordEncoder passwordEncoder, AuthorityRepository authorityRepository, CacheManager cacheManager) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.authorityRepository = authorityRepository;
        this.cacheManager = cacheManager;
        this.entityUserRepository = entityUserRepository;
        this.mailService = mailService;
        this.leasingRepository = leasingRepository;
        this.cabangRepository = cabangRepository;
        this.notarisRepository = notarisRepository;
    }

    public Optional<User> activateRegistration(String key) {
        log.debug("Activating user for activation key {}", key);
        return userRepository.findOneByActivationKey(key)
            .map(user -> {
                // activate given user for the registration key.
                user.setActivated(true);
                user.setActivationKey(null);
                this.clearUserCaches(user);
                log.debug("Activated user: {}", user);
                return user;
            });
    }

    public Optional<User> completePasswordReset(String newPassword, String key) {
        log.debug("Reset user password for reset key {}", key);
        return userRepository.findOneByResetKey(key)
            .filter(user -> user.getResetDate().isAfter(Instant.now().minusSeconds(86400)))
            .map(user -> {
                user.setPassword(passwordEncoder.encode(newPassword));
                user.setResetKey(null);
                user.setResetDate(null);
                this.clearUserCaches(user);
                return user;
            });
    }

    public Optional<User> requestPasswordReset(String mail) {
        return userRepository.findOneByEmailIgnoreCase(mail)
            .filter(User::getActivated)
            .map(user -> {
                user.setResetKey(RandomUtil.generateResetKey());
                user.setResetDate(Instant.now());
                this.clearUserCaches(user);
                return user;
            });
    }

    public User registerUserNew(String firstname, String lastname, String username, String password, String email,String role, String cabangid, String notarisid) {
        userRepository.findOneByLogin(username.toLowerCase()).ifPresent(existingUser -> {
            boolean removed = removeNonActivatedUser(existingUser);
            if (!removed) {
                throw new UsernameAlreadyUsedException();
            }
        });
        userRepository.findOneByEmailIgnoreCase(email).ifPresent(existingUser -> {
            boolean removed = removeNonActivatedUser(existingUser);
            if (!removed) {
                throw new EmailAlreadyUsedException();
            }
        });
        User newUser = new User();
        
        String pass = null;
        boolean activeAccountEmail = false;
        
        if (password != null) {
        	pass = password;
        } else {
        	pass = RandomUtil.generatePassword();
        	activeAccountEmail = true;
        }
        
        String encryptedPassword = passwordEncoder.encode(pass);
        newUser.setLogin(username.toLowerCase());
        // new user gets initially a generated password
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(firstname);
        newUser.setLastName(lastname);
        if (email != null) {
            newUser.setEmail(email.toLowerCase());
        }
        newUser.setImageUrl(null);
        newUser.setLangKey("in");
        // new user is not active
        newUser.setActivated(true);
        // new user gets registration key
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        Set<Authority> authorities = new HashSet<>();
        authorityRepository.findById(AuthoritiesConstants.USER).ifPresent(authorities::add);
        newUser.setAuthorities(authorities);
        userRepository.save(newUser);
        
        Set<Authority> authorities2 = new HashSet<>();
        authorityRepository.findById(role).ifPresent(authorities2::add);
        newUser.setAuthorities(authorities2);
        userRepository.save(newUser);
    	
    	String login = SecurityUtils.getCurrentUserLogin().get();
    	Leasing leasing = leasingRepository.findById(Long.parseLong("1")).get();

    	EntityUser entityUser = new EntityUser(); 
    	entityUser.setUser(newUser);
    	entityUser.setType(1);
    	entityUser.setRecordStatus(1);
    	entityUser.setUpdateBy(login);
    	entityUser.setUpdateOn(ZonedDateTime.now());
    	entityUser.setRole(role);
        
        if (AuthoritiesConstants.OFFICER.equals(role)) {
        	if (cabangid != null) {
            	Cabang cabang = cabangRepository.findById(Long.parseLong(cabangid)).get();
            	entityUser.setCabang(cabang);
        	}

        	entityUser.setLeasing(leasing);
        } else if (AuthoritiesConstants.HEADQUARTER.equals(role)) {
        	entityUser.setLeasing(leasing);
        } else if (AuthoritiesConstants.PARTNER.equals(role)|| AuthoritiesConstants.PARTNER_STAFF.equals(role)) {
        	if (notarisid != null) {
            	Notaris notaris = notarisRepository.findById(Long.parseLong(notarisid)).get();
            	entityUser.setNotaris(notaris);
        	}
        }
        
        entityUserRepository.save(entityUser);
        
        this.clearUserCaches(newUser);
        log.debug("Created Information for User: {}", newUser);
        
        if (activeAccountEmail)
	        mailService.sendPasswordResetMail(
	              requestPasswordReset(newUser.getEmail())
	                  .orElseThrow(EmailNotFoundException::new));
        
        return newUser;
    }

    public User registerUser(UserDTO userDTO, String password) {
        userRepository.findOneByLogin(userDTO.getLogin().toLowerCase()).ifPresent(existingUser -> {
            boolean removed = removeNonActivatedUser(existingUser);
            if (!removed) {
                throw new UsernameAlreadyUsedException();
            }
        });
        userRepository.findOneByEmailIgnoreCase(userDTO.getEmail()).ifPresent(existingUser -> {
            boolean removed = removeNonActivatedUser(existingUser);
            if (!removed) {
                throw new EmailAlreadyUsedException();
            }
        });
        User newUser = new User();
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setLogin(userDTO.getLogin().toLowerCase());
        // new user gets initially a generated password
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(userDTO.getFirstName());
        newUser.setLastName(userDTO.getLastName());
        if (userDTO.getEmail() != null) {
            newUser.setEmail(userDTO.getEmail().toLowerCase());
        }
        newUser.setImageUrl(userDTO.getImageUrl());
        newUser.setLangKey(userDTO.getLangKey());
        // new user is not active
        newUser.setActivated(false);
        // new user gets registration key
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        Set<Authority> authorities = new HashSet<>();
        authorityRepository.findById(AuthoritiesConstants.USER).ifPresent(authorities::add);
        newUser.setAuthorities(authorities);
        userRepository.save(newUser);
        this.clearUserCaches(newUser);
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }

    private boolean removeNonActivatedUser(User existingUser){
        if (existingUser.getActivated()) {
             return false;
        }
        userRepository.delete(existingUser);
        userRepository.flush();
        this.clearUserCaches(existingUser);
        return true;
    }

    public User createUser(UserDTO userDTO) {
        User user = new User();
        user.setLogin(userDTO.getLogin().toLowerCase());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        if (userDTO.getEmail() != null) {
            user.setEmail(userDTO.getEmail().toLowerCase());
        }
        user.setImageUrl(userDTO.getImageUrl());
        if (userDTO.getLangKey() == null) {
            user.setLangKey(Constants.DEFAULT_LANGUAGE); // default language
        } else {
            user.setLangKey(userDTO.getLangKey());
        }
        String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
        user.setPassword(encryptedPassword);
        user.setResetKey(RandomUtil.generateResetKey());
        user.setResetDate(Instant.now());
        user.setActivated(true);
        if (userDTO.getAuthorities() != null) {
            Set<Authority> authorities = userDTO.getAuthorities().stream()
                .map(authorityRepository::findById)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());
            user.setAuthorities(authorities);
        }
        userRepository.save(user);
        this.clearUserCaches(user);
        log.debug("Created Information for User: {}", user);
        return user;
    }

    /**
     * Update basic information (first name, last name, email, language) for the current user.
     *
     * @param firstName first name of user.
     * @param lastName  last name of user.
     * @param email     email id of user.
     * @param langKey   language key.
     * @param imageUrl  image URL of user.
     */
    public void updateUser(String firstName, String lastName, String email, String langKey, String imageUrl) {
        SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin)
            .ifPresent(user -> {
                user.setFirstName(firstName);
                user.setLastName(lastName);
                if (email != null) {
	                user.setEmail(email.toLowerCase());
                }
                user.setLangKey(langKey);
                user.setImageUrl(imageUrl);
                this.clearUserCaches(user);
                log.debug("Changed Information for User: {}", user);
            });
    }

    /**
     * Update all information for a specific user, and return the modified user.
     *
     * @param userDTO user to update.
     * @return updated user.
     */
    public Optional<UserDTO> updateUser(UserDTO userDTO) {
        return Optional.of(userRepository
            .findById(userDTO.getId()))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .map(user -> {
                this.clearUserCaches(user);
                user.setLogin(userDTO.getLogin().toLowerCase());
                user.setFirstName(userDTO.getFirstName());
                user.setLastName(userDTO.getLastName());
                if (userDTO.getEmail() != null) {
                    user.setEmail(userDTO.getEmail().toLowerCase());
                }
                user.setImageUrl(userDTO.getImageUrl());
                user.setActivated(userDTO.isActivated());
                user.setLangKey(userDTO.getLangKey());
                Set<Authority> managedAuthorities = user.getAuthorities();
                managedAuthorities.clear();
                userDTO.getAuthorities().stream()
                    .map(authorityRepository::findById)
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .forEach(managedAuthorities::add);
                this.clearUserCaches(user);
                log.debug("Changed Information for User: {}", user);
                return user;
            })
            .map(UserDTO::new);
    }

    public void deleteUser(String login) {
        userRepository.findOneByLogin(login).ifPresent(user -> {
            userRepository.delete(user);
            this.clearUserCaches(user);
            log.debug("Deleted User: {}", user);
        });
    }

    public void changePassword(String currentClearTextPassword, String newPassword) {
        SecurityUtils.getCurrentUserLogin()
            .flatMap(userRepository::findOneByLogin)
            .ifPresent(user -> {
                String currentEncryptedPassword = user.getPassword();
                if (!passwordEncoder.matches(currentClearTextPassword, currentEncryptedPassword)) {
                    throw new InvalidPasswordException();
                }
                String encryptedPassword = passwordEncoder.encode(newPassword);
                user.setPassword(encryptedPassword);
                this.clearUserCaches(user);
                log.debug("Changed password for User: {}", user);
            });
    }

    @Transactional(readOnly = true)
    public Page<UserDTO> getAllManagedUsers(Pageable pageable) {
        return userRepository.findAllByLoginNot(pageable, Constants.ANONYMOUS_USER).map(UserDTO::new);
    }

    @Transactional(readOnly = true)
    public Page<UserLeasingDTO> getAllLeasingUsers(Pageable pageable) {
        return entityUserRepository.findByUserAndLeasing(pageable);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthoritiesByLogin(String login) {
        return userRepository.findOneWithAuthoritiesByLogin(login);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthorities(Long id) {
        return userRepository.findOneWithAuthoritiesById(id);
    }

    @Transactional(readOnly = true)
    public Optional<User> getUserWithAuthorities() {
        return SecurityUtils.getCurrentUserLogin().flatMap(userRepository::findOneWithAuthoritiesByLogin);
    }

    @Transactional()
    public void sendOtpToEmail(String email) {
    	User user = userRepository.findOneByLogin(email).get();
    	String to = user.getEmail();
    	String dear = user.getFirstName();
    	String otp= new DecimalFormat("000000").format(new Random().nextInt(999999));
    	ZonedDateTime exp = ZonedDateTime.now().plusMinutes(5);
    	String expText = DateUtils.formatZonedDateTime(exp, DateUtils.FORMATTER_ID_LOCAL_DATETIME);
    	
    	user.setOtp(otp);
    	user.setOtpExpired(exp);
    	userRepository.save(user);
    	
    	mailService.sendAuthentificationCode(to, dear, otp, expText);
    }
    

    /**
     * Not activated users should be automatically deleted after 3 days.
     * <p>
     * This is scheduled to get fired everyday, at 01:00 (am).
     */
    @Scheduled(cron = "0 0 1 * * ?")
    public void removeNotActivatedUsers() {
        userRepository
            .findAllByActivatedIsFalseAndActivationKeyIsNotNullAndCreatedDateBefore(Instant.now().minus(3, ChronoUnit.DAYS))
            .forEach(user -> {
                log.debug("Deleting not activated user {}", user.getLogin());
                userRepository.delete(user);
                this.clearUserCaches(user);
            });
    }

    /**
     * Gets a list of all the authorities.
     * @return a list of all the authorities.
     */
    public List<String> getAuthorities() {
        return authorityRepository.findAll().stream().map(Authority::getName).collect(Collectors.toList());
    }
    
    public List<String> getAuth() {
        return authorityRepository.findAll().stream().map(Authority::getName).collect(Collectors.toList());
    }

    private void clearUserCaches(User user) {
        Objects.requireNonNull(cacheManager.getCache(UserRepository.USERS_BY_LOGIN_CACHE)).evict(user.getLogin());
        if (user.getEmail() != null) {
            Objects.requireNonNull(cacheManager.getCache(UserRepository.USERS_BY_EMAIL_CACHE)).evict(user.getEmail());
        }
    }
    
    public void  activateBlock(String login) {
       userRepository.findOneByLogin(login)
            .ifPresent(user -> {
                user.setActivated(false);
                user.setActivationKey(null);
                this.clearUserCaches(user);
                log.debug("Block user: {}", user);
            });
    }
    
    public boolean findStatus(String login) {
       User user = userRepository.findOneByLogin(login).get();
       if (user.getActivated()) {
    	   return true;
       } else {
    	   return false;
       }
    }
}
