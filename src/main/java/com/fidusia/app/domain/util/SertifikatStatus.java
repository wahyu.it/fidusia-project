package com.fidusia.app.domain.util;

public enum SertifikatStatus {
	NEW(0), REGISTERED(1), CONFIRMED(2), PAID(3), DONE(4), CANCELLED(-1);
	
	private int value;
	
	SertifikatStatus (int value) {
		this.value = value;
	}

    public int value() {
        return value;
    }

    public static final Integer[] ACTIVE_STATUS = {SertifikatStatus.REGISTERED.value, SertifikatStatus.CONFIRMED.value, SertifikatStatus.PAID.value, SertifikatStatus.DONE.value};
    public static final Integer[] CANCEL_STATUS = {SertifikatStatus.CANCELLED.value};

}
