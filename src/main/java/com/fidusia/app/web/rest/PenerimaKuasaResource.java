package com.fidusia.app.web.rest;

import com.fidusia.app.domain.PenerimaKuasa;
import com.fidusia.app.repository.PenerimaKuasaRepository;
import com.fidusia.app.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.fidusia.app.domain.PenerimaKuasa}.
 */
@RestController
@RequestMapping("/api/penerima-kuasas")
@Transactional
public class PenerimaKuasaResource {

    private final Logger log = LoggerFactory.getLogger(PenerimaKuasaResource.class);

    private static final String ENTITY_NAME = "penerimaKuasa";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PenerimaKuasaRepository penerimaKuasaRepository;

    public PenerimaKuasaResource(PenerimaKuasaRepository penerimaKuasaRepository) {
        this.penerimaKuasaRepository = penerimaKuasaRepository;
    }

    /**
     * {@code POST  /penerima-kuasas} : Create a new penerimaKuasa.
     *
     * @param penerimaKuasa the penerimaKuasa to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new penerimaKuasa, or with status {@code 400 (Bad Request)} if the penerimaKuasa has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<PenerimaKuasa> createPenerimaKuasa(@RequestBody PenerimaKuasa penerimaKuasa) throws URISyntaxException {
        log.debug("REST request to save PenerimaKuasa : {}", penerimaKuasa);
        if (penerimaKuasa.getId() != null) {
            throw new BadRequestAlertException("A new penerimaKuasa cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PenerimaKuasa result = penerimaKuasaRepository.save(penerimaKuasa);
        return ResponseEntity
            .created(new URI("/api/penerima-kuasas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /penerima-kuasas/:id} : Updates an existing penerimaKuasa.
     *
     * @param id the id of the penerimaKuasa to save.
     * @param penerimaKuasa the penerimaKuasa to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated penerimaKuasa,
     * or with status {@code 400 (Bad Request)} if the penerimaKuasa is not valid,
     * or with status {@code 500 (Internal Server Error)} if the penerimaKuasa couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<PenerimaKuasa> updatePenerimaKuasa(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody PenerimaKuasa penerimaKuasa
    ) throws URISyntaxException {
        log.debug("REST request to update PenerimaKuasa : {}, {}", id, penerimaKuasa);
        if (penerimaKuasa.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, penerimaKuasa.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!penerimaKuasaRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PenerimaKuasa result = penerimaKuasaRepository.save(penerimaKuasa);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, penerimaKuasa.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /penerima-kuasas/:id} : Partial updates given fields of an existing penerimaKuasa, field will ignore if it is null
     *
     * @param id the id of the penerimaKuasa to save.
     * @param penerimaKuasa the penerimaKuasa to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated penerimaKuasa,
     * or with status {@code 400 (Bad Request)} if the penerimaKuasa is not valid,
     * or with status {@code 404 (Not Found)} if the penerimaKuasa is not found,
     * or with status {@code 500 (Internal Server Error)} if the penerimaKuasa couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<PenerimaKuasa> partialUpdatePenerimaKuasa(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody PenerimaKuasa penerimaKuasa
    ) throws URISyntaxException {
        log.debug("REST request to partial update PenerimaKuasa partially : {}, {}", id, penerimaKuasa);
        if (penerimaKuasa.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, penerimaKuasa.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!penerimaKuasaRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PenerimaKuasa> result = penerimaKuasaRepository
            .findById(penerimaKuasa.getId())
            .map(existingPenerimaKuasa -> {
                if (penerimaKuasa.getTitel() != null) {
                    existingPenerimaKuasa.setTitel(penerimaKuasa.getTitel());
                }
                if (penerimaKuasa.getNama() != null) {
                    existingPenerimaKuasa.setNama(penerimaKuasa.getNama());
                }
                if (penerimaKuasa.getKomparisi() != null) {
                    existingPenerimaKuasa.setKomparisi(penerimaKuasa.getKomparisi());
                }
                if (penerimaKuasa.getTts() != null) {
                    existingPenerimaKuasa.setTts(penerimaKuasa.getTts());
                }
                if (penerimaKuasa.getSk() != null) {
                    existingPenerimaKuasa.setSk(penerimaKuasa.getSk());
                }
                if (penerimaKuasa.getTanggalSk() != null) {
                    existingPenerimaKuasa.setTanggalSk(penerimaKuasa.getTanggalSk());
                }
                if (penerimaKuasa.getNoKontak() != null) {
                    existingPenerimaKuasa.setNoKontak(penerimaKuasa.getNoKontak());
                }
                if (penerimaKuasa.getRecordStatus() != null) {
                    existingPenerimaKuasa.setRecordStatus(penerimaKuasa.getRecordStatus());
                }
                if (penerimaKuasa.getUpdateBy() != null) {
                    existingPenerimaKuasa.setUpdateBy(penerimaKuasa.getUpdateBy());
                }
                if (penerimaKuasa.getUpdateOn() != null) {
                    existingPenerimaKuasa.setUpdateOn(penerimaKuasa.getUpdateOn());
                }

                return existingPenerimaKuasa;
            })
            .map(penerimaKuasaRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, penerimaKuasa.getId().toString())
        );
    }

    /**
     * {@code GET  /penerima-kuasas} : get all the penerimaKuasas.
     *
     * @param pageable the pagination information.
     * @param filter the filter of the request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of penerimaKuasas in body.
     */
    @GetMapping("")
    public ResponseEntity<List<PenerimaKuasa>> getAllPenerimaKuasas(Pageable pageable,
        @RequestParam(name = "filter", required = false) String filter
    ) {
//        if ("notaris-is-null".equals(filter)) {
//            log.debug("REST request to get all PenerimaKuasas where notaris is null");
//            return new ResponseEntity<>(
//                StreamSupport
//                    .stream(penerimaKuasaRepository.findAll().spliterator(), false)
//                    .filter(penerimaKuasa -> penerimaKuasa.getNotaris() == null)
//                    .toList(),
//                HttpStatus.OK
//            );
//        }
        log.debug("REST request to get a page of PenerimaKuasas");
        Page<PenerimaKuasa> page = penerimaKuasaRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /penerima-kuasas/:id} : get the "id" penerimaKuasa.
     *
     * @param id the id of the penerimaKuasa to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the penerimaKuasa, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<PenerimaKuasa> getPenerimaKuasa(@PathVariable("id") Long id) {
        log.debug("REST request to get PenerimaKuasa : {}", id);
        Optional<PenerimaKuasa> penerimaKuasa = penerimaKuasaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(penerimaKuasa);
    }

    /**
     * {@code DELETE  /penerima-kuasas/:id} : delete the "id" penerimaKuasa.
     *
     * @param id the id of the penerimaKuasa to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePenerimaKuasa(@PathVariable("id") Long id) {
        log.debug("REST request to delete PenerimaKuasa : {}", id);
        penerimaKuasaRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
