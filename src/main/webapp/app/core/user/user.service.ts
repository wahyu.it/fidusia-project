import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Pagination } from 'app/shared/util/request-util';
import { IUser } from './user.model';
import { IKeyCabangDTO } from  'app/shared/model/keycabangDTO.model';

@Injectable({ providedIn: 'root' })
export class UserService {
  public resourceUrl = SERVER_API_URL + 'api/users';
  public resourceUrlActived = SERVER_API_URL + 'api/users/actived';
  public resourceUrlCabang = SERVER_API_URL + 'api/cabangs';
  public resourceUrlNotaris = SERVER_API_URL + '/api/notarises';

  constructor(private http: HttpClient) {}

  create(user: IUser): Observable<IUser> {
    return this.http.post<IUser>(this.resourceUrl, user);
  }

  update(user: IUser): Observable<IUser> {
    return this.http.put<IUser>(this.resourceUrl, user);
  }

  updateActive(id: any): Observable<IUser> {
    return this.http.put<IUser>(this.resourceUrlActived +"/"+ id, { observe: 'response' });
  }
  
  /*  updateActive(id: any): Observable<HttpResponse<any>>{
    return this.http.get<any>(this.resourceUrlActived + '/' + id, { observe: 'response' });
  }*/

  find(login: string): Observable<IUser> {
    return this.http.get<IUser>(`${this.resourceUrl}/${login}`);
  }

  query(req?: Pagination): Observable<HttpResponse<IUser[]>> {
    const options = createRequestOption(req);
    return this.http.get<IUser[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  queryleasing(req?: Pagination): Observable<HttpResponse<IUser[]>> {
    const options = createRequestOption(req);
    return this.http.get<IUser[]>(this.resourceUrl + '/leasing', { params: options, observe: 'response' });
  }

  queryCabang(): Observable<IKeyCabangDTO[]> {
    return this.http.get<IKeyCabangDTO[]>(this.resourceUrlCabang + '/value');
  }

  queryNotaris(): Observable<IKeyCabangDTO[]> {
    return this.http.get<IKeyCabangDTO[]>(this.resourceUrlNotaris + '/value');
  }

  delete(login: string): Observable<{}> {
    return this.http.delete(`${this.resourceUrl}/${login}`);
  }

  authorities(): Observable<string[]> {
    return this.http.get<string[]>(SERVER_API_URL + 'api/users/authorities');
  }

  checkEmail(email: string): Observable<any> {
	  const formData = new FormData();
	  formData.append('email', email);
     return this.http.post('api/account/authenticate/email', formData);
  }
  
  checkUsername(login: string): Observable<any> {
	  const formData = new FormData();
	  formData.append('login', login);
     return this.http.post('api/account/authenticate/userlogin', formData);
  }
  
  createUser(firstname: string, lastname: string, username: string, password: string, email: string, role: string, cabang: string, notaris: string): Observable<any> {
	  const formData = new FormData();
	  formData.append('firstname', firstname);
	  formData.append('lastname', lastname);
	  formData.append('username', username);
	  formData.append('password', password);
	  formData.append('email', email);
	  formData.append('role', role);
	  formData.append('cabang', cabang);
	  formData.append('notaris', notaris);
     return this.http.post('api/account/register', formData);
  }
  
}
