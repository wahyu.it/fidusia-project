import { element, by, ElementFinder } from 'protractor';

export class BesarBayarComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-besar-bayar div table .btn-danger'));
  title = element.all(by.css('jhi-besar-bayar div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class BesarBayarUpdatePage {
  pageTitle = element(by.id('jhi-besar-bayar-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  nominalInput = element(by.id('field_nominal'));

  kelasSelect = element(by.id('field_kelas'));
  jenisBayarSelect = element(by.id('field_jenisBayar'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setNominalInput(nominal: string): Promise<void> {
    await this.nominalInput.sendKeys(nominal);
  }

  async getNominalInput(): Promise<string> {
    return await this.nominalInput.getAttribute('value');
  }

  async kelasSelectLastOption(): Promise<void> {
    await this.kelasSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async kelasSelectOption(option: string): Promise<void> {
    await this.kelasSelect.sendKeys(option);
  }

  getKelasSelect(): ElementFinder {
    return this.kelasSelect;
  }

  async getKelasSelectedOption(): Promise<string> {
    return await this.kelasSelect.element(by.css('option:checked')).getText();
  }

  async jenisBayarSelectLastOption(): Promise<void> {
    await this.jenisBayarSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async jenisBayarSelectOption(option: string): Promise<void> {
    await this.jenisBayarSelect.sendKeys(option);
  }

  getJenisBayarSelect(): ElementFinder {
    return this.jenisBayarSelect;
  }

  async getJenisBayarSelectedOption(): Promise<string> {
    return await this.jenisBayarSelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class BesarBayarDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-besarBayar-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-besarBayar'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
