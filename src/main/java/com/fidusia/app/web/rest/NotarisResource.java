package com.fidusia.app.web.rest;

import com.fidusia.app.domain.Notaris;
import com.fidusia.app.repository.NotarisRepository;
import com.fidusia.app.security.AuthoritiesConstants;
import com.fidusia.app.security.SecurityUtils;
import com.fidusia.app.service.CabangService;
import com.fidusia.app.service.dto.KeyValueDTO;
import com.fidusia.app.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.fidusia.app.domain.Notaris}.
 */
@RestController
@RequestMapping("/api/notarises")
@Transactional
public class NotarisResource {

    private final Logger log = LoggerFactory.getLogger(NotarisResource.class);

    private static final String ENTITY_NAME = "notaris";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NotarisRepository notarisRepository;

    private final CabangService cabangService;

    public NotarisResource(NotarisRepository notarisRepository, CabangService cabangService) {
        this.notarisRepository = notarisRepository;
        this.cabangService = cabangService;
    }

    /**
     * {@code POST  /notarises} : Create a new notaris.
     *
     * @param notaris the notaris to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new notaris, or with status {@code 400 (Bad Request)} if the notaris has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<Notaris> createNotaris(@RequestBody Notaris notaris) throws URISyntaxException {
        log.debug("REST request to save Notaris : {}", notaris);
        if (notaris.getId() != null) {
            throw new BadRequestAlertException("A new notaris cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Notaris result = notarisRepository.save(notaris);
        return ResponseEntity
            .created(new URI("/api/notarises/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /notarises/:id} : Updates an existing notaris.
     *
     * @param id the id of the notaris to save.
     * @param notaris the notaris to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated notaris,
     * or with status {@code 400 (Bad Request)} if the notaris is not valid,
     * or with status {@code 500 (Internal Server Error)} if the notaris couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<Notaris> updateNotaris(@PathVariable(value = "id", required = false) final Long id, @RequestBody Notaris notaris)
        throws URISyntaxException {
        log.debug("REST request to update Notaris : {}, {}", id, notaris);
        if (notaris.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, notaris.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!notarisRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Notaris result = notarisRepository.save(notaris);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, notaris.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /notarises/:id} : Partial updates given fields of an existing notaris, field will ignore if it is null
     *
     * @param id the id of the notaris to save.
     * @param notaris the notaris to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated notaris,
     * or with status {@code 400 (Bad Request)} if the notaris is not valid,
     * or with status {@code 404 (Not Found)} if the notaris is not found,
     * or with status {@code 500 (Internal Server Error)} if the notaris couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Notaris> partialUpdateNotaris(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Notaris notaris
    ) throws URISyntaxException {
        log.debug("REST request to partial update Notaris partially : {}, {}", id, notaris);
        if (notaris.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, notaris.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!notarisRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Notaris> result = notarisRepository
            .findById(notaris.getId())
            .map(existingNotaris -> {
                if (notaris.getNama() != null) {
                    existingNotaris.setNama(notaris.getNama());
                }
                if (notaris.getNamaShort() != null) {
                    existingNotaris.setNamaShort(notaris.getNamaShort());
                }
                if (notaris.getTitelShortOne() != null) {
                    existingNotaris.setTitelShortOne(notaris.getTitelShortOne());
                }
                if (notaris.getTitelShortTwo() != null) {
                    existingNotaris.setTitelShortTwo(notaris.getTitelShortTwo());
                }
                if (notaris.getTitelLongOne() != null) {
                    existingNotaris.setTitelLongOne(notaris.getTitelLongOne());
                }
                if (notaris.getTitelLongTwo() != null) {
                    existingNotaris.setTitelLongTwo(notaris.getTitelLongTwo());
                }
                if (notaris.getWilayahKerja() != null) {
                    existingNotaris.setWilayahKerja(notaris.getWilayahKerja());
                }
                if (notaris.getKedudukan() != null) {
                    existingNotaris.setKedudukan(notaris.getKedudukan());
                }
                if (notaris.getSk() != null) {
                    existingNotaris.setSk(notaris.getSk());
                }
                if (notaris.getTglSk() != null) {
                    existingNotaris.setTglSk(notaris.getTglSk());
                }
                if (notaris.getJenisHariKerja() != null) {
                    existingNotaris.setJenisHariKerja(notaris.getJenisHariKerja());
                }
                if (notaris.getJamKerjaAwalOne() != null) {
                    existingNotaris.setJamKerjaAwalOne(notaris.getJamKerjaAwalOne());
                }
                if (notaris.getJamKerjaAkhirOne() != null) {
                    existingNotaris.setJamKerjaAkhirOne(notaris.getJamKerjaAkhirOne());
                }
                if (notaris.getJamKerjaAwalTwo() != null) {
                    existingNotaris.setJamKerjaAwalTwo(notaris.getJamKerjaAwalTwo());
                }
                if (notaris.getJamKerjaAkhirTwo() != null) {
                    existingNotaris.setJamKerjaAkhirTwo(notaris.getJamKerjaAkhirTwo());
                }
                if (notaris.getSelisihPukul() != null) {
                    existingNotaris.setSelisihPukul(notaris.getSelisihPukul());
                }
                if (notaris.getJamRestAwalOne() != null) {
                    existingNotaris.setJamRestAwalOne(notaris.getJamRestAwalOne());
                }
                if (notaris.getJamRestAkhirOne() != null) {
                    existingNotaris.setJamRestAkhirOne(notaris.getJamRestAkhirOne());
                }
                if (notaris.getJamRestAwalTwo() != null) {
                    existingNotaris.setJamRestAwalTwo(notaris.getJamRestAwalTwo());
                }
                if (notaris.getJamRestAkhirTwo() != null) {
                    existingNotaris.setJamRestAkhirTwo(notaris.getJamRestAkhirTwo());
                }
                if (notaris.getMaksAkta() != null) {
                    existingNotaris.setMaksAkta(notaris.getMaksAkta());
                }
                if (notaris.getMaksOrderHarian() != null) {
                    existingNotaris.setMaksOrderHarian(notaris.getMaksOrderHarian());
                }
                if (notaris.getLembarSk() != null) {
                    existingNotaris.setLembarSk(notaris.getLembarSk());
                }
                if (notaris.getLembarSumpah() != null) {
                    existingNotaris.setLembarSumpah(notaris.getLembarSumpah());
                }
                if (notaris.getAlamatKantor() != null) {
                    existingNotaris.setAlamatKantor(notaris.getAlamatKantor());
                }
                if (notaris.getAlamatEmail() != null) {
                    existingNotaris.setAlamatEmail(notaris.getAlamatEmail());
                }
                if (notaris.getNoKontak() != null) {
                    existingNotaris.setNoKontak(notaris.getNoKontak());
                }
                if (notaris.getRecordStatus() != null) {
                    existingNotaris.setRecordStatus(notaris.getRecordStatus());
                }
                if (notaris.getUpdateBy() != null) {
                    existingNotaris.setUpdateBy(notaris.getUpdateBy());
                }
                if (notaris.getUpdateOn() != null) {
                    existingNotaris.setUpdateOn(notaris.getUpdateOn());
                }

                return existingNotaris;
            })
            .map(notarisRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, notaris.getId().toString())
        );
    }

    /**
     * {@code GET  /notarises} : get all the notarises.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of notarises in body.
     */
    @GetMapping("")
    public ResponseEntity<List<Notaris>> getAllNotarises( Pageable pageable) {
        log.debug("REST request to get a page of Notarises");
        Page<Notaris> page = notarisRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /notarises/:id} : get the "id" notaris.
     *
     * @param id the id of the notaris to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the notaris, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<Notaris> getNotaris(@PathVariable("id") Long id) {
        log.debug("REST request to get Notaris : {}", id);
        Optional<Notaris> notaris = notarisRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(notaris);
    }

    /**
     * {@code DELETE  /notarises/:id} : delete the "id" notaris.
     *
     * @param id the id of the notaris to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteNotaris(@PathVariable("id") Long id) {
        log.debug("REST request to delete Notaris : {}", id);
        notarisRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
    
    @GetMapping("/value")
    public List<KeyValueDTO> getAllNotarisValue() {
        log.debug("REST request to get a page of notaris dto");
        List<KeyValueDTO> result = notarisRepository.findByKeyNotaris();
        return result;
    }
    
    @GetMapping("/getByLogin")
    public ResponseEntity<Notaris> getNotarisByLogin() {
        log.debug("REST request to getNotarisByLogin");

        boolean isRolePartner = SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.PARTNER);

        Notaris notaris = cabangService.findNotarisByUserLogin();
        log.debug("notaris =" + notaris);

        return new ResponseEntity<>(notaris, HttpStatus.OK);
    }
}
