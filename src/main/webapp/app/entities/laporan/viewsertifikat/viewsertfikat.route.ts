import { Routes } from '@angular/router';
import { ViewSertifikatComponent } from './viewsertifikat.component';
import { JhiResolvePagingParams } from 'ng-jhipster';

export const viewSertifikatState: Routes = [{
  path: '',
  component: ViewSertifikatComponent,
  resolve: {
	  pagingParams: JhiResolvePagingParams
  }
  }
];
