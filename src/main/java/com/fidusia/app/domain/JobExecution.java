package com.fidusia.app.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "job_execution")
public class JobExecution implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="job_id")
    private JobInstance job;

    @Column(name = "starttime")
    private ZonedDateTime starttime;

    @Column(name = "endtime")
    private ZonedDateTime endtime;

    @Size(max = 10)
    @Column(name = "status", length = 10)
    private String status;

    @Size(max = 200)
    @Column(name = "input", length = 200)
    private String input;

    @Size(max = 500)
    @Column(name = "output", length = 500)
    private String output;

    @Size(max = 30)
    @Column(name = "executor", length = 30)
    private String executor;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public JobInstance getJob() {
		return job;
	}

	public void setJob(JobInstance job) {
		this.job = job;
	}

	public ZonedDateTime getStarttime() {
		return starttime;
	}

	public void setStarttime(ZonedDateTime starttime) {
		this.starttime = starttime;
	}

	public ZonedDateTime getEndtime() {
		return endtime;
	}

	public void setEndtime(ZonedDateTime endtime) {
		this.endtime = endtime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}

	public String getOutput() {
		return output;
	}

	public void setOutput(String output) {
		this.output = output;
	}

	public String getExecutor() {
		return executor;
	}

	public void setExecutor(String executor) {
		this.executor = executor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JobExecution other = (JobExecution) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "JobExecution [id=" + id + ", job=" + job + ", starttime=" + starttime + ", endtime=" + endtime + ", status=" + status + ", input=" + input
				+ ", output=" + output + ", executor=" + executor + "]";
	}
}
