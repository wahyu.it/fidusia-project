import { Moment } from 'moment';

export interface IBerkasDTO {
        id?: number;
        ktpBpkb?: string;
        identitas?: string;
        kk?: string;
        ppk?: string;
        skNasabah?: string;
        npwp?: string;
        aktaPend?: string;
        aktaPerub?: string;
        skPendirian?: string;
        skkemen?: string;
        ktpp?: string;
        docKontrak?: string;
        skpjf?: string;
        updateBy?: string;
        updateOn?: Moment;
        uploadBy?: string;
        uploadOn?: Moment;
        ppkNomor?: string;
        nsbJenis?: number;
        cabangId?: number;
        berkasStatus?: number;
        namaNasabah?: string;
        tanggalPpk?: Moment;
        metodeKirim?: number;
        remarkId?: number;
        remarkName?: string;
        partitionId?: number;
        verifyBy?: string;
        verifyOn?: Moment;
        orderType?: number;
        ktpDirut?: string;
        tdp?: string;
        ktpdp?: string;
        situ?: string;
        
}

export class BerkasDTO implements IBerkasDTO {
  constructor(
       	public id?: number,
		public ktpBpkb?: string,
		public identitas?: string,
		public ktpp?: string,
		public kk?: string,
		public npwp?: string,
		public ppk?: string,
		public skNasabah?: string,
		public aktaPend?: string,
		public aktaPerub?: string,
		public skPendirian?: string,
		public skkemen?: string,
		public docKontrak?: string,
		public skpjf?: string,
		public updateBy?: string,
		public updateOn?: Moment,
		public uploadBy?: string,
		public uploadOn?: Moment,
		public ppkNomor?: string,
		public nsbJenis?: number,
		public cabangId?: number,
		public berkasStatus?: number,
		public namaNasabah?: string,
		public tanggalPpk?: Moment,
		public metodeKirim?: number,
		public remarkId?: number,
		public remarkName?: string,
		public partitionId?: number,
		public verifyBy?: string,
		public verifyOn?: Moment,
		public orderType?: number,
		public ktpDirut?: string,
		public tdp?: string,
		public ktpdp?: string,
		public situ?: string,
  ) {}
}
