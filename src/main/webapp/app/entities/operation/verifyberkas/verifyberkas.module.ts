import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasimonitoringSharedModule } from 'app/shared/shared.module';
import { VerifikasiBerkasComponent } from './verifyberkas.component';
import { verifyBerkasState } from './verifyberkas.route';
import { VerifyBerkasDialogComponent } from './verifyberkas-dialog.component';

@NgModule({
  imports: [SisteminformasimonitoringSharedModule, RouterModule.forChild(verifyBerkasState)],
  declarations: [VerifikasiBerkasComponent, VerifyBerkasDialogComponent],
  entryComponents: [VerifyBerkasDialogComponent]
})
export class VerifyBerkasModule {}
