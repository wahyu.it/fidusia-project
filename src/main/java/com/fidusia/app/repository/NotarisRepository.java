package com.fidusia.app.repository;

import com.fidusia.app.domain.Notaris;
import com.fidusia.app.service.dto.KeyValueDTO;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Notaris entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NotarisRepository extends JpaRepository<Notaris, Long> {
	
	@Query("select new com.fidusia.app.service.dto.KeyValueDTO(n.id, n.nama) from Notaris n")
	List<KeyValueDTO> findByKeyNotaris();
	
}
