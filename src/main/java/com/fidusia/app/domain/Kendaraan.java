package com.fidusia.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import javax.persistence.*;

/**
 * A Kendaraan.
 */
@Entity
@Table(name = "kendaraan")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Kendaraan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "kondisi")
    private String kondisi;

    @Column(name = "roda")
    private Integer roda;

    @Column(name = "merk")
    private String merk;

    @Column(name = "tipe")
    private String tipe;

    @Column(name = "warna")
    private String warna;

    @Column(name = "tahun_pembuatan")
    private String tahunPembuatan;

    @Column(name = "no_rangka")
    private String noRangka;

    @Column(name = "no_mesin")
    private String noMesin;

    @Column(name = "no_bpkb")
    private String noBpkb;

    @Column(name = "nilai_objek_jaminan")
    private Long nilaiObjekJaminan;

    @Column(name = "asset_description")
    private String assetDescription;

    @Column(name = "valid")
    private Boolean valid;

    @ManyToOne
    private Ppk ppk;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Kendaraan id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKondisi() {
        return this.kondisi;
    }

    public Kendaraan kondisi(String kondisi) {
        this.setKondisi(kondisi);
        return this;
    }

    public void setKondisi(String kondisi) {
        this.kondisi = kondisi;
    }

    public Integer getRoda() {
        return this.roda;
    }

    public Kendaraan roda(Integer roda) {
        this.setRoda(roda);
        return this;
    }

    public void setRoda(Integer roda) {
        this.roda = roda;
    }

    public String getMerk() {
        return this.merk;
    }

    public Kendaraan merk(String merk) {
        this.setMerk(merk);
        return this;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public String getTipe() {
        return this.tipe;
    }

    public Kendaraan tipe(String tipe) {
        this.setTipe(tipe);
        return this;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getWarna() {
        return this.warna;
    }

    public Kendaraan warna(String warna) {
        this.setWarna(warna);
        return this;
    }

    public void setWarna(String warna) {
        this.warna = warna;
    }

    public String getTahunPembuatan() {
        return this.tahunPembuatan;
    }

    public Kendaraan tahunPembuatan(String tahunPembuatan) {
        this.setTahunPembuatan(tahunPembuatan);
        return this;
    }

    public void setTahunPembuatan(String tahunPembuatan) {
        this.tahunPembuatan = tahunPembuatan;
    }

    public String getNoRangka() {
        return this.noRangka;
    }

    public Kendaraan noRangka(String noRangka) {
        this.setNoRangka(noRangka);
        return this;
    }

    public void setNoRangka(String noRangka) {
        this.noRangka = noRangka;
    }

    public String getNoMesin() {
        return this.noMesin;
    }

    public Kendaraan noMesin(String noMesin) {
        this.setNoMesin(noMesin);
        return this;
    }

    public void setNoMesin(String noMesin) {
        this.noMesin = noMesin;
    }

    public String getNoBpkb() {
        return this.noBpkb;
    }

    public Kendaraan noBpkb(String noBpkb) {
        this.setNoBpkb(noBpkb);
        return this;
    }

    public void setNoBpkb(String noBpkb) {
        this.noBpkb = noBpkb;
    }

    public Long getNilaiObjekJaminan() {
        return this.nilaiObjekJaminan;
    }

    public Kendaraan nilaiObjekJaminan(Long nilaiObjekJaminan) {
        this.setNilaiObjekJaminan(nilaiObjekJaminan);
        return this;
    }

    public void setNilaiObjekJaminan(Long nilaiObjekJaminan) {
        this.nilaiObjekJaminan = nilaiObjekJaminan;
    }

    public Ppk getPpk() {
        return this.ppk;
    }

    public void setPpk(Ppk ppk) {
        this.ppk = ppk;
    }

    public Kendaraan ppk(Ppk ppk) {
        this.setPpk(ppk);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    public String getAssetDescription() {
		return assetDescription;
	}

    public Kendaraan assetDescription(String assetDescription) {
        this.setAssetDescription(assetDescription);
        return this;
    }

	public void setAssetDescription(String assetDescription) {
		this.assetDescription = assetDescription;
	}

	public Boolean getValid() {
		return valid;
	}

	public void setValid(Boolean valid) {
		this.valid = valid;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Kendaraan)) {
            return false;
        }
        return getId() != null && getId().equals(((Kendaraan) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

	@Override
	public String toString() {
		return "Kendaraan [id=" + id + ", kondisi=" + kondisi + ", roda=" + roda + ", merk=" + merk + ", tipe=" + tipe
				+ ", warna=" + warna + ", tahunPembuatan=" + tahunPembuatan + ", noRangka=" + noRangka + ", noMesin="
				+ noMesin + ", noBpkb=" + noBpkb + ", nilaiObjekJaminan=" + nilaiObjekJaminan + ", assetDescription="
				+ assetDescription + ", valid=" + valid + ", ppk=" + ppk + "]";
	}
}
