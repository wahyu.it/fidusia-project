package com.fidusia.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;


/**
 * Created by wahyu on 10/01/2015 Tabel Akta.
 */
@Entity
@Table(name = "akta")
public class Akta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "tgl_order")
    private LocalDate tglOrder;

    @Column(name = "order_type")
    private Integer orderType;

    @Column(name = "kode")
    private String kode;

    @Column(name = "nomor")
    private String nomor;

    @Column(name = "tanggal")
    private LocalDate tanggal;

    @Column(name = "pukul_awal")
    private ZonedDateTime pukulAwal;

    @Column(name = "pukul_akhir")
    private ZonedDateTime pukulAkhir;

    @Column(name = "status")
    private Integer status;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "update_on")
    private ZonedDateTime updateOn;

    @OneToOne
    @JoinColumn(unique = true)
    private Ppk ppk;

    @ManyToOne
    private Notaris notaris;

    @ManyToOne
    private PenerimaKuasa penerimaKuasa;

    @ManyToOne
    private Saksi saksiOne;

    @ManyToOne
    private Saksi saksiTwo;

    @ManyToOne
    private Cabang cabang;

    @ManyToOne
    private SkSubstitusi skSubstitusi;
    
    @Transient
    private Sertifikat sertifikat;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Akta id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getTglOrder() {
        return this.tglOrder;
    }

    public Akta tglOrder(LocalDate tglOrder) {
        this.setTglOrder(tglOrder);
        return this;
    }

    public void setTglOrder(LocalDate tglOrder) {
        this.tglOrder = tglOrder;
    }

    public Integer getOrderType() {
        return this.orderType;
    }

    public Akta orderType(Integer orderType) {
        this.setOrderType(orderType);
        return this;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public String getKode() {
        return this.kode;
    }

    public Akta kode(String kode) {
        this.setKode(kode);
        return this;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNomor() {
        return this.nomor;
    }

    public Akta nomor(String nomor) {
        this.setNomor(nomor);
        return this;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public LocalDate getTanggal() {
        return this.tanggal;
    }

    public Akta tanggal(LocalDate tanggal) {
        this.setTanggal(tanggal);
        return this;
    }

    public void setTanggal(LocalDate tanggal) {
        this.tanggal = tanggal;
    }

    public ZonedDateTime getPukulAwal() {
        return this.pukulAwal;
    }

    public Akta pukulAwal(ZonedDateTime pukulAwal) {
        this.setPukulAwal(pukulAwal);
        return this;
    }

    public void setPukulAwal(ZonedDateTime pukulAwal) {
        this.pukulAwal = pukulAwal;
    }

    public ZonedDateTime getPukulAkhir() {
        return this.pukulAkhir;
    }

    public Akta pukulAkhir(ZonedDateTime pukulAkhir) {
        this.setPukulAkhir(pukulAkhir);
        return this;
    }

    public void setPukulAkhir(ZonedDateTime pukulAkhir) {
        this.pukulAkhir = pukulAkhir;
    }

    public Integer getStatus() {
        return this.status;
    }

    public Akta status(Integer status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getUpdateBy() {
        return this.updateBy;
    }

    public Akta updateBy(String updateBy) {
        this.setUpdateBy(updateBy);
        return this;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public ZonedDateTime getUpdateOn() {
        return this.updateOn;
    }

    public Akta updateOn(ZonedDateTime updateOn) {
        this.setUpdateOn(updateOn);
        return this;
    }

    public void setUpdateOn(ZonedDateTime updateOn) {
        this.updateOn = updateOn;
    }

    public Ppk getPpk() {
        return this.ppk;
    }

    public void setPpk(Ppk ppk) {
        this.ppk = ppk;
    }

    public Akta ppk(Ppk ppk) {
        this.setPpk(ppk);
        return this;
    }

    public Notaris getNotaris() {
        return this.notaris;
    }

    public void setNotaris(Notaris notaris) {
        this.notaris = notaris;
    }

    public Akta notaris(Notaris notaris) {
        this.setNotaris(notaris);
        return this;
    }

    public PenerimaKuasa getPenerimaKuasa() {
        return this.penerimaKuasa;
    }

    public void setPenerimaKuasa(PenerimaKuasa penerimaKuasa) {
        this.penerimaKuasa = penerimaKuasa;
    }

    public Akta penerimaKuasa(PenerimaKuasa penerimaKuasa) {
        this.setPenerimaKuasa(penerimaKuasa);
        return this;
    }

    public Saksi getSaksiOne() {
        return this.saksiOne;
    }

    public void setSaksiOne(Saksi saksi) {
        this.saksiOne = saksi;
    }

    public Akta saksiOne(Saksi saksi) {
        this.setSaksiOne(saksi);
        return this;
    }

    public Saksi getSaksiTwo() {
        return this.saksiTwo;
    }

    public void setSaksiTwo(Saksi saksi) {
        this.saksiTwo = saksi;
    }

    public Akta saksiTwo(Saksi saksi) {
        this.setSaksiTwo(saksi);
        return this;
    }

    public Cabang getCabang() {
        return this.cabang;
    }

    public void setCabang(Cabang cabang) {
        this.cabang = cabang;
    }

    public Akta cabang(Cabang cabang) {
        this.setCabang(cabang);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    public SkSubstitusi getSkSubstitusi() {
		return skSubstitusi;
	}

    public Akta skSubstitusi(SkSubstitusi skSubstitusi) {
        this.skSubstitusi = skSubstitusi;
        return this;
    }

	public void setSkSubstitusi(SkSubstitusi skSubstitusi) {
		this.skSubstitusi = skSubstitusi;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Akta)) {
            return false;
        }
        return getId() != null && getId().equals(((Akta) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Akta{" +
            "id=" + getId() +
            ", tglOrder='" + getTglOrder() + "'" +
            ", orderType=" + getOrderType() +
            ", kode='" + getKode() + "'" +
            ", nomor='" + getNomor() + "'" +
            ", tanggal='" + getTanggal() + "'" +
            ", pukulAwal='" + getPukulAwal() + "'" +
            ", pukulAkhir='" + getPukulAkhir() + "'" +
            ", status=" + getStatus() +
            ", updateBy='" + getUpdateBy() + "'" +
            ", updateOn='" + getUpdateOn() + "'" +
            "}";
    }

	public Sertifikat getSertifikat() {
		return sertifikat;
	}

	public void setSertifikat(Sertifikat sertifikat) {
		this.sertifikat = sertifikat;
	}
    
}
