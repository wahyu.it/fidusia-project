package com.fidusia.app.repository;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.fidusia.app.domain.ApplicationConfig;
import com.fidusia.app.domain.LogBook;

/**
 * Spring Data  repository for the LogBook entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LogBookRepository extends JpaRepository<LogBook, Long>  {

	Page<LogBook> findByTanggalBetweenAndKategori(LocalDate pStart, LocalDate pEnd, String kategori, Pageable pageable);

	
}
