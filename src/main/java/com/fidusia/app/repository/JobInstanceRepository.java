package com.fidusia.app.repository;

import com.fidusia.app.domain.JobInstance;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;


public interface JobInstanceRepository extends JpaRepository<JobInstance,Long> {
	
	List<JobInstance> findByCronNotNullOrLastStatus(String lastStatus);
	
	JobInstance findOneByNameAndMethod(String name, String method);
}
