package com.fidusia.app.repository;

import com.fidusia.app.domain.SkSubstitusi;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the SkSubtitusi entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SkSubtitusiRepository extends JpaRepository<SkSubstitusi, Long> {

	SkSubstitusi findTopByOrderByTanggalDesc();}
