package com.fidusia.app.util;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class DateUtil {
	public final static DateTimeFormatter FORMATTER_DATE = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    public final static DateTimeFormatter FORMATTER_ID_DATE = DateTimeFormatter.ofPattern("dd MMMM yyyy", new Locale("ID"));
	public final static DateTimeFormatter FORMATTER_YYYYMMDD = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public final static DateTimeFormatter FORMATTER_DDMM = DateTimeFormatter.ofPattern("ddMM");
	public final static DateTimeFormatter FORMATTER_TIME = DateTimeFormatter.ofPattern("HH:mm");
    public final static DateTimeFormatter FORMATTER_HHMMSS = DateTimeFormatter.ofPattern("HH:mm:ss");
    public final static DateTimeFormatter FORMATTER_YYMM = DateTimeFormatter.ofPattern("yyMM");
    public final static DateTimeFormatter FORMATTER_DATE_REPORT = DateTimeFormatter.ofPattern("EEEEE, dd-MM-yyyy", new Locale("ID"));
	public final static DateTimeFormatter FORMATTER_ISO_DDMMYY = DateTimeFormatter.ofPattern("ddMMyy");
	private final static Map<String,String> mapDayOfWeekID = new HashMap<String,String>();
	static {
		mapDayOfWeekID.put("0", "Minggu");
		mapDayOfWeekID.put("1", "Senin");
		mapDayOfWeekID.put("2", "Selasa");
		mapDayOfWeekID.put("3", "Rabu");
		mapDayOfWeekID.put("4", "Kamis");
		mapDayOfWeekID.put("5", "Jumat");
		mapDayOfWeekID.put("6", "Sabtu");
	}
	private final static Map<String,String> mapMonthID = new HashMap<String,String>();
	static {
		mapMonthID.put("1", "Januari");
		mapMonthID.put("2", "Februari");
		mapMonthID.put("3", "Maret");
		mapMonthID.put("4", "April");
		mapMonthID.put("5", "Mei");
		mapMonthID.put("6", "Juni");
		mapMonthID.put("7", "Juli");
		mapMonthID.put("8", "Agustus");
		mapMonthID.put("9", "September");
		mapMonthID.put("10", "Oktober");
		mapMonthID.put("11", "November");
		mapMonthID.put("12", "Desember");
	}

    private final static TreeMap<Integer, String> map = new TreeMap<Integer, String>();

    static {

        map.put(1000, "M");
        map.put(900, "CM");
        map.put(500, "D");
        map.put(400, "CD");
        map.put(100, "C");
        map.put(90, "XC");
        map.put(50, "L");
        map.put(40, "XL");
        map.put(10, "X");
        map.put(9, "IX");
        map.put(5, "V");
        map.put(4, "IV");
        map.put(1, "I");

    }

	public static String formatDate(LocalDate localDate, DateTimeFormatter formatter) {
		return localDate.format(formatter);
	}

	public static String formatZonedDateTime(ZonedDateTime zonedDateTime, DateTimeFormatter formatter) {
		return zonedDateTime.format(formatter);
	}

	public static String getDayOfWeekID(LocalDate date) {
		return mapDayOfWeekID.get("" + date.getDayOfWeek().getValue());
	}

	public static String getMonthID(LocalDate date) {
		return mapMonthID.get("" + date.getMonthValue());
	}

    public final static String toRoman(int number) {
        int l =  map.floorKey(number);
        if ( number == l ) {
            return map.get(number);
        }
        return map.get(l) + toRoman(number-l);
    }

    public static int getCurrentMonth(){

        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int currentMonth = cal.get(Calendar.MONTH);

        return currentMonth;
    }

    public static int getCurrentYear() {
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int currentYear = cal.get(Calendar.YEAR);

        return currentYear;
    }

    public static ZonedDateTime parseTime(LocalDate localDate, String zonedDateTime) {
        return ZonedDateTime.parse(localDate.format(DateTimeFormatter.ISO_DATE) + "T" + zonedDateTime + ":00+07:00[Asia/Bangkok]", DateTimeFormatter.ISO_ZONED_DATE_TIME.withZone(ZoneId.systemDefault()));
    }
}
