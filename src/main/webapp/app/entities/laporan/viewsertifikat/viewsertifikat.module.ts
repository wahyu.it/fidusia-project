import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasimonitoringSharedModule } from 'app/shared/shared.module';
import { ViewSertifikatComponent } from './viewsertifikat.component';
import { viewSertifikatState } from './viewsertfikat.route';

@NgModule({
  imports: [SisteminformasimonitoringSharedModule, RouterModule.forChild(viewSertifikatState)],
  declarations: [ViewSertifikatComponent]
})
export class ViewSertifikatModule {}
