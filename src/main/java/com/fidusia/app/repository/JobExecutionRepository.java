package com.fidusia.app.repository;

import com.fidusia.app.domain.JobExecution;

import java.time.ZonedDateTime;

import org.springframework.data.jpa.repository.JpaRepository;


public interface JobExecutionRepository extends JpaRepository<JobExecution,Long> {

	JobExecution findByJobIdAndStarttimeBetweenAndStatusAndOutput(Long i, ZonedDateTime start, ZonedDateTime end, String status, String output);

}
