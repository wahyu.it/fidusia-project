import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { IBerkasDTO } from 'app/shared/model/berkasDTO.model';
import { ICabangDTO } from 'app/shared/model/cabangDTO.model';

type EntityArrayResponseType = HttpResponse<IBerkasDTO[]>;
type EntityResponseType = HttpResponse<IBerkasDTO>;

@Injectable({ providedIn: 'root' })
export class UploadBerkasService {
  	  public resourceUrlFile = SERVER_API_URL + 'api/file/';
  	  public resourceUrlBerkas = SERVER_API_URL + 'api/berkas/search';
  	  public resourceUrlCabang = SERVER_API_URL + 'api/cabangs/getByLogin';
	
	  constructor(protected http: HttpClient) {}

	  search(req?: any): Observable<EntityArrayResponseType> {
	    const options = createRequestOption(req);
	    return this.http
	      .get<IBerkasDTO[]>(this.resourceUrlBerkas, { params: options, observe: 'response' })
	      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
	  }

	  cabangLogin(): Observable<ICabangDTO> {
	    return this.http.get<ICabangDTO>(this.resourceUrlCabang);
	  }
	  
	  createMultiFile(id: any, files: any): Observable<EntityResponseType> {
		
		const formData = new FormData();
		
		for (let i = 0; i < files.length; i++) {
		    formData.append('file', files[i])
	  		console.log('files' + files[i]);
		  }
		
		const header = new HttpHeaders();
		  	header.append('Content-Type', 'multipart/form-data');
	    	return this.http.post<IBerkasDTO>(this.resourceUrlFile + '/upload/warkah/' + id, formData, { observe: 'response', headers: header });
	  } 
	  
	  create(id: any, kategori: any, file?: any): Observable<EntityResponseType> {
		const formData = new FormData();
		      formData.append('file', file);
		      formData.append('kategori', kategori);
		const header = new HttpHeaders();
		  	header.append('Content-Type', 'multipart/form-data');
	    	return this.http.post<IBerkasDTO>(this.resourceUrlFile + '/upload/warkah/' + id, formData, { observe: 'response', headers: header });
	  } 
	  
	 viewBerkas(ppkNomor: string): Observable<Blob> {
	    console.log(ppkNomor);
	    return this.http.get<Blob>(this.resourceUrlFile + 'previewberkasfile' + (ppkNomor ? `?fileName=${ppkNomor}` : ''), {
	      observe: 'body',
	      responseType: 'blob' as 'json'
	    });
	  }
	  
	 viewBerkas2(ppkNomor: string): Observable<Blob> {
	    return this.http.get<Blob>(this.resourceUrlFile + 'previewberkas' + (ppkNomor ? `?fileName=${ppkNomor}` : ''), {
	      observe: 'body',
	      responseType: 'blob' as 'json'
	    });
	  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
	    if (res.body) {
	      res.body.forEach((r: IBerkasDTO) => {
	        r.tanggalPpk = r.tanggalPpk ? moment(r.tanggalPpk) : undefined;
	        r.uploadOn = r.uploadOn ? moment(r.uploadOn) : undefined;
	      });
	    }
    return res;
  }
	
}