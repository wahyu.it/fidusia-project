import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AccountService } from 'app/core/auth/account.service';
import { AuthServerProvider } from 'app/core/auth/auth-jwt.service';
import { Login } from 'app/core/login/login.model';
import { Account } from 'app/core/user/account.model';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';

@Injectable({ providedIn: 'root' })
export class LoginService {
  public resourceUrl = SERVER_API_URL + 'api/account/';
	
  private account?: Account | null;
  constructor(private http: HttpClient, private accountService: AccountService, private authServerProvider: AuthServerProvider) {}

  login(credentials: Login, callback?: any): any {
    const cb = callback || function(): any {};

    return new Promise((resolve: any, reject: any) => {
      this.authServerProvider.login(credentials).subscribe(
        data => {
          this.accountService.identity(true).subscribe(() => {
            resolve(data);
          });
          return cb();
        },
        err => {
          this.logout();
          reject(err);
          return cb(err);
        }
      );
    });
  }
  
  login2(credentials: Login): any {
      this.authServerProvider.login(credentials).subscribe(() => {
          this.accountService.identity(true).subscribe((account) => {
		  this.account = account;
          this.logout();
          });
      });
      return this.account;
  }

  loginWithToken(jwt: any, rememberMe: any): void {
    this.authServerProvider.loginWithToken(jwt, rememberMe);
  }

  logout(): void {
    this.authServerProvider.logout().subscribe();
    this.accountService.authenticate(null);
  }

  sendCodeToEmail(email: string): Observable<any> {
	  const formData = new FormData();
	  formData.append('email', email);
     return this.http.post(this.resourceUrl + '/authenticate/code', formData);
  }

  verifyOtp(otp: string, email: string): Observable<any> {
	  const formData = new FormData();
	  formData.append('otp', otp);
	  formData.append('email', email);
     return this.http.post(this.resourceUrl + '/authenticate/verify', formData);
  }

  status(credentials: Login): Observable<any> {
     return this.http.post(SERVER_API_URL + 'api/account/authenticate/user-active', credentials);
  }
}
