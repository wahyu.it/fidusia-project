package com.fidusia.app.web.rest;

import com.fidusia.app.domain.Cabang;
import com.fidusia.app.repository.CabangRepository;
import com.fidusia.app.security.AuthoritiesConstants;
import com.fidusia.app.security.SecurityUtils;
import com.fidusia.app.service.CabangService;
import com.fidusia.app.service.dto.CabangDTO;
import com.fidusia.app.service.dto.KeyValueDTO;
import com.fidusia.app.service.mapper.CabangMapper;
import com.fidusia.app.web.rest.errors.BadRequestAlertException;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.fidusia.app.domain.Cabang}.
 */
@RestController
@RequestMapping("/api/cabangs")
@Transactional
public class CabangResource {

    private final Logger log = LoggerFactory.getLogger(CabangResource.class);

    private static final String ENTITY_NAME = "cabang";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CabangRepository cabangRepository;
    
    private final CabangService cabangService;
    
    private final CabangMapper cabangMapper;

    public CabangResource(CabangRepository cabangRepository, CabangService cabangService, CabangMapper cabangMapper) {
        this.cabangRepository = cabangRepository;
        this.cabangService = cabangService;
        this.cabangMapper = cabangMapper;
    }

    /**
     * {@code POST  /cabangs} : Create a new cabang.
     *
     * @param cabang the cabang to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new cabang, or with status {@code 400 (Bad Request)} if the cabang has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<Cabang> createCabang(@RequestBody Cabang cabang) throws URISyntaxException {
        log.debug("REST request to save Cabang : {}", cabang);
        if (cabang.getId() != null) {
            throw new BadRequestAlertException("A new cabang cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Cabang result = cabangRepository.save(cabang);
        return ResponseEntity
            .created(new URI("/api/cabangs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /cabangs/:id} : Updates an existing cabang.
     *
     * @param id the id of the cabang to save.
     * @param cabang the cabang to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cabang,
     * or with status {@code 400 (Bad Request)} if the cabang is not valid,
     * or with status {@code 500 (Internal Server Error)} if the cabang couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<Cabang> updateCabang(@PathVariable(value = "id", required = false) final Long id, @RequestBody Cabang cabang)
        throws URISyntaxException {
        log.debug("REST request to update Cabang : {}, {}", id, cabang);
        if (cabang.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, cabang.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!cabangRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Cabang result = cabangRepository.save(cabang);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, cabang.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /cabangs/:id} : Partial updates given fields of an existing cabang, field will ignore if it is null
     *
     * @param id the id of the cabang to save.
     * @param cabang the cabang to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cabang,
     * or with status {@code 400 (Bad Request)} if the cabang is not valid,
     * or with status {@code 404 (Not Found)} if the cabang is not found,
     * or with status {@code 500 (Internal Server Error)} if the cabang couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Cabang> partialUpdateCabang(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Cabang cabang
    ) throws URISyntaxException {
        log.debug("REST request to partial update Cabang partially : {}, {}", id, cabang);
        if (cabang.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, cabang.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!cabangRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Cabang> result = cabangRepository
            .findById(cabang.getId())
            .map(existingCabang -> {
                if (cabang.getKode() != null) {
                    existingCabang.setKode(cabang.getKode());
                }
                if (cabang.getNama() != null) {
                    existingCabang.setNama(cabang.getNama());
                }
                if (cabang.getAlamat() != null) {
                    existingCabang.setAlamat(cabang.getAlamat());
                }
                if (cabang.getKota() != null) {
                    existingCabang.setKota(cabang.getKota());
                }
                if (cabang.getProvinsi() != null) {
                    existingCabang.setProvinsi(cabang.getProvinsi());
                }
                if (cabang.getPengadilanNegeri() != null) {
                    existingCabang.setPengadilanNegeri(cabang.getPengadilanNegeri());
                }
                if (cabang.getNoKontak() != null) {
                    existingCabang.setNoKontak(cabang.getNoKontak());
                }
                if (cabang.getRecordStatus() != null) {
                    existingCabang.setRecordStatus(cabang.getRecordStatus());
                }
                if (cabang.getUpdateBy() != null) {
                    existingCabang.setUpdateBy(cabang.getUpdateBy());
                }
                if (cabang.getUpdateOn() != null) {
                    existingCabang.setUpdateOn(cabang.getUpdateOn());
                }
                if (cabang.getStartTanggal() != null) {
                    existingCabang.setStartTanggal(cabang.getStartTanggal());
                }

                return existingCabang;
            })
            .map(cabangRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, cabang.getId().toString())
        );
    }

    /**
     * {@code GET  /cabangs} : get all the cabangs.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cabangs in body.
     */
    @GetMapping("")
    public ResponseEntity<List<Cabang>> getAllCabangs(Pageable pageable) {
        log.debug("REST request to get a page of Cabangs");
        Page<Cabang> page = cabangRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }
    
    @GetMapping("/value")
    public List<KeyValueDTO> getAllCabangValue() {
        log.debug("REST request to get a page of Cabangs");
        List<KeyValueDTO> page = cabangRepository.findByRecordStatus2(1);
        return page;
    }

    /**
     * {@code GET  /cabangs/:id} : get the "id" cabang.
     *
     * @param id the id of the cabang to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the cabang, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<Cabang> getCabang(@PathVariable("id") Long id) {
        log.debug("REST request to get Cabang : {}", id);
        Optional<Cabang> cabang = cabangRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(cabang);
    }

    /**
     * {@code DELETE  /cabangs/:id} : delete the "id" cabang.
     *
     * @param id the id of the cabang to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCabang(@PathVariable("id") Long id) {
        log.debug("REST request to delete Cabang : {}", id);
        cabangRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
    
    @GetMapping("/getByLogin")
    public ResponseEntity<CabangDTO> getCabangByLogin() {
        log.debug("REST request to getCabangByLogin");

        boolean isRoleCabang = SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.OFFICER) || SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.MANAGER);

        Cabang cabang = cabangService.findByUserLogin();

        return new ResponseEntity<>(cabangMapper.cabangToCabangDTO(cabang), HttpStatus.OK);
    }
}
