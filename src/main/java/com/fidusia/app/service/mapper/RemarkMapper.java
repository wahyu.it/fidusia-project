package com.fidusia.app.service.mapper;

import org.mapstruct.Mapper;

import com.fidusia.app.domain.Remark;
import com.fidusia.app.service.dto.RemarkDTO;

import java.util.List;

/**
 * Mapper for the entity Remark and its DTO RemarkDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface RemarkMapper {

    RemarkDTO remarkToRemarkDTO(Remark remark);

    List<RemarkDTO> remarksToRemarkDTOs(List<Remark> remarks);

    Remark remarkDTOToRemark(RemarkDTO remarkDTO);

    List<Remark> remarkDTOsToRemarks(List<RemarkDTO> remarkDTOs);
}
