package com.fidusia.app.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.fidusia.app.domain.OrderData;
import com.fidusia.app.domain.RawData;
import com.fidusia.app.domain.util.NasabahJenis;
import com.fidusia.app.domain.util.ResponseStatus;
import com.fidusia.app.repository.ApplicationConfigRepository;
import com.fidusia.app.repository.OrderDataRepository;
import com.fidusia.app.repository.RawDataRepository;
import com.fidusia.app.service.dto.JobParameter;
import com.fidusia.app.service.dto.ResponseArrayGetDataDTO;
import com.fidusia.app.service.dto.ResponseGetDataDTO;
import com.fidusia.app.service.dto.ResponseGetTokenDTO;
import com.fidusia.app.util.ExceptionUtils;
import com.fidusia.app.util.LogUtils;
import com.fidusia.app.web.rest.util.CommonResponse;

@Service
public class RawDataService {

    private final Logger log = LoggerFactory.getLogger(RawDataService.class);

    private final ApplicationConfigRepository applicationConfigRepository;
    
    private final OrderDataInsertService orderDataInsertService;

    private final RawDataRepository rawDataRepository;
    
    private final SlackInhookService slackInhookService;
    
    private final OrderDataRepository orderDataRepository;
    
    private final HolidayService holidayService;

    private final static int connectTimeout = 60000 ;
    
    private final static int readTimeout = 60000 ;
    
    private String lineDelimiter = "|" ;
	
    private final JobInstanceService jobInstanceService;
    
    public RawDataService(HolidayService holidayService, OrderDataRepository orderDataRepository, SlackInhookService slackInhookService, JobInstanceService jobInstanceService, OrderDataInsertService orderDataInsertService, ApplicationConfigRepository applicationConfigRepository, RawDataRepository rawDataRepository) {
    	this.applicationConfigRepository = applicationConfigRepository;
    	this.rawDataRepository = rawDataRepository;
    	this.orderDataInsertService = orderDataInsertService;
    	this.jobInstanceService = jobInstanceService;
    	this.slackInhookService = slackInhookService;
    	this.orderDataRepository = orderDataRepository;
    	this.holidayService = holidayService;
    }
    
    public String requestToken() {
//    	log.debug("Run service Request token...");
    	OutputStreamWriter writer = null;
    	ResponseGetTokenDTO response = null;
		BufferedReader reader = null;
    	
		try {
			
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("partner_id", applicationConfigRepository.findByCategoryAndName("APPLICATION_PROPERTIES", "partner_id").getValue());
            jsonObject.put("secret_key", applicationConfigRepository.findByCategoryAndName("APPLICATION_PROPERTIES", "secret_key").getValue());
            
            String postData = jsonObject.toString();
    		
    		String url_mtf = applicationConfigRepository.findByCategoryAndName("APPLICATION_PROPERTIES", "URL_MTF").getValue();

    		SSLContext ssl_ctx = SSLContext.getInstance("TLS");
			TrustManager[] trust_mgr = get_trust_mgr();
			ssl_ctx.init(null, trust_mgr, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());
			
			URL url = new URL(url_mtf + "api/token");
//			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty("Accept", "*/*");
			conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
			conn.setConnectTimeout(connectTimeout);
			conn.setReadTimeout(readTimeout);
	        conn.setDoInput(true);
			conn.setDoOutput(true);
			
			writer = new OutputStreamWriter(conn.getOutputStream());
            writer.write(postData);
            writer.flush();
			
			int responseCode = conn.getResponseCode();
			
			if(responseCode == HttpStatus.OK.value()) {

				reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				
				String line;
				StringBuffer sb = new StringBuffer();
				while ((line = reader.readLine()) != null) {
					sb.append(line);
				}

				response = parseResponse(sb.toString());
				if ("success".equals(response.getStatus())) {
//					log.debug("Post send data token :{}", response.getToken());
					return response.getToken();
				} else {
		            log.error("Failed get token {} ", response.getMessage());
				}
			}
			
		} catch (Exception e) {
            log.error("Failed service Get data token Job caused by {}", e.getMessage(), e);
		} finally {
			if(writer != null)
				try {
					writer.close();
				} catch (Exception e) { }
	
			if(reader != null)
				try {
					reader.close();
				} catch (IOException e) { }
		}
    	
    	return null;
    }
    
    public void getRawDataAsync(JobParameter parameters) {
    	log.debug("Start service Request getRawDataAsync..");
		String tglorder = parameters.get("tglRequest");
		LocalDate date = tglorder != null ? LocalDate.parse(tglorder) : LocalDate.now();
        
        String service = "Get Raw Data " + date;
        String parameter = "tglorder:" + date;
        CommonResponse response = new CommonResponse(service, parameter);
        
        try {
        	StringBuilder sb = new StringBuilder();
        	if (!holidayService.isHolidayNow() || !holidayService.isSundayNow()) {
        		int k = 3;
        		for (int i=1; i< k; i++ ) {
        			LocalDate tgl = LocalDate.now().minusDays(i);
            		if (holidayService.isHoliday(tgl) || holidayService.isSunday(tgl)) {
                    	getRawData(tgl, "P");
                    	getRawData(tgl, "C");
            		}
        		}
        		
        		getRawData(date, "P");
            	getRawData(date, "C");

        		List<OrderData> totalInput = orderDataRepository.findByTanggal(date);
        		int orang = 0;
        		int badan = 0;
        		for (OrderData orderData : totalInput) {
        			if (NasabahJenis.PERORANGAN.value() == orderData.getNasabahJenis().intValue()) {
        				orang++;
        			} else {
        				badan++;
        			}
        		}
            	
        		sb.append("\n* GET RAW DATA").append(" *");
        		sb.append("\nsuccess      ").append(totalInput.size() > 0 ? totalInput.size() : 0);
        		sb.append("\norang    	").append(orang);
        		sb.append("\nbadan    	").append(badan);
        	}

            LogUtils.debug(response = response.complete(ResponseStatus.COMPLETED.toString(), sb.toString()), log, slackInhookService);
            jobInstanceService.updateStatus(parameters.get("jobid"), "SUCCESS", parameters.toString(), "");
		} catch (Exception e) {
            e.printStackTrace();
            log.error(ExceptionUtils.getMessage(e));
            
            LogUtils.debug(response = response.complete(ResponseStatus.FAILED.toString(), ExceptionUtils.getMessage(e), ExceptionUtils.getStackTrace(e)), log, slackInhookService);
            jobInstanceService.updateStatus(parameters.get("jobid"), "ERROR", parameters.toString(), ExceptionUtils.getStackTrace(e));
            return;
		}
    }
    
    public String getRawData(LocalDate orderDate, String nsbJenis) {
    	log.debug("Start service Request getRawData {} jenis nsb {}", orderDate, nsbJenis);
    	OutputStreamWriter writer = null;
    	ResponseGetDataDTO response = null;
		BufferedReader reader = null;
    	
		try {
			
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("request_date", orderDate);
            jsonObject.put("customer_type", nsbJenis);
            
            String postData = jsonObject.toString();
			log.debug("Post data get raw data :{}",postData);
			
    		String url_mtf = applicationConfigRepository.findByCategoryAndName("APPLICATION_PROPERTIES", "URL_MTF").getValue();

    		SSLContext ssl_ctx = SSLContext.getInstance("TLS");
			TrustManager[] trust_mgr = get_trust_mgr();
			ssl_ctx.init(null, trust_mgr, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(ssl_ctx.getSocketFactory());
			
			String token = requestToken();
			
			URL url = new URL(url_mtf + "api/agreements/get");
//			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection(); TODO
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestProperty("Accept", "*/*");
			conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Authorization","Bearer " + token);
			conn.setConnectTimeout(connectTimeout);
			conn.setReadTimeout(readTimeout);
	        conn.setDoInput(true);
			conn.setDoOutput(true);
			
			writer = new OutputStreamWriter(conn.getOutputStream());
            writer.write(postData);
            writer.flush();
			
			int responseCode = conn.getResponseCode();
			
			if(responseCode == HttpStatus.OK.value()) {

				reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				
				String line;
				StringBuffer sb = new StringBuffer();
				while ((line = reader.readLine()) != null) {
					sb.append(line);
				}

				response = parseResponseData(sb.toString(), orderDate, nsbJenis);
				if ("Success".equals(response.getResponse())) {
					return response.getResponse();
				} else {
		            log.error("Failed get data {} ", response.getResponse());
				}
			}
			
		} catch (Exception e) {
            log.error("Failed service Get data token Job caused by {}", e.getMessage(), e);
		} finally {
			if(writer != null)
				try {
					writer.close();
				} catch (Exception e) { }
	
			if(reader != null)
				try {
					reader.close();
				} catch (IOException e) { }
		}
    	
    	return null;
    }
    
    private ResponseGetDataDTO parseResponseData (String json,LocalDate orderDate, String nsbJenis) throws Exception  {
    	ResponseGetDataDTO response = new ResponseGetDataDTO();
		
		if(json != null && json.length() > 0) {
			try {
				JSONObject message = new JSONObject(json);
					response.setErrorCode(message.optString("ErrorCode"));
					if (json.length() < 70) {
						response.setResponse(message.getString("status"));
					} else {
						response.setResponse(message.getString("Response"));
						response.setMessageResponse(message.optString("MessageResponse"));
						List<ResponseArrayGetDataDTO> dataArray = 
								"P".equals(nsbJenis) ? parseResponseArrayDataPerorangan(message.getJSONArray("FiduciaTaskData"))
										: parseResponseArrayDataBadan(message.getJSONArray("FiduciaTaskData"));
						
//						for (ResponseArrayGetDataDTO responseArrayGetDataDTO : dataArray) {
//							log.debug(responseArrayGetDataDTO.toString());
//						}
						
						insertRawData(dataArray, orderDate, nsbJenis);
					}
			} catch (Exception e) {
				throw e;
			}
		}
    	
    	return response;
    }
    
    public void insertOrderData() {
    	
    }
//    Perorangan
    private static List<ResponseArrayGetDataDTO> parseResponseArrayDataPerorangan(JSONArray jsonArray) throws Exception  {
    	List<ResponseArrayGetDataDTO> arrayResponse = new ArrayList<ResponseArrayGetDataDTO>();
		
		if(jsonArray != null && jsonArray.length() > 0) {
			try {
					for (int i=0; i<jsonArray.length(); i++) {
						ResponseArrayGetDataDTO resp = new ResponseArrayGetDataDTO();
						JSONObject e = jsonArray.getJSONObject(i);
						
						resp.setAgreementNumber(e.getString("AgreementNumber"));
						resp.setAlamatDebitur(e.getString("AlamatDebitur"));
						resp.setAlamatPemegangBpkb(e.getString("AlamatPemegangBPKB"));
						resp.setAssetDescription(e.getString("AssetDescription"));
						resp.setAssetUsage(e.getString("AssetUsage"));
						resp.setEffectiveDate(e.getString("EffectiveDate"));
						resp.setEndDate(e.getString("EndDate"));
						resp.setFiduciaOnlineIdPersonal(e.getString("FiduciaOnlineIDPersonal"));
						resp.setFiduciaType(e.getString("FiduciaType"));
						resp.setiDType(e.getString("IDType"));
						resp.setiDTypePemegangBpkb(e.getString("IDTypePemegangBPKB"));
						resp.setIdNumber(e.getString("IdNumber"));
						resp.setIdNumberPemegangBpkb(e.getString("IdNumberPemegangBPKB"));
						resp.setJenisCustomer(e.getString("JenisCustomer"));
						resp.setJenisKelamin(e.getString("JenisKelamin"));
						resp.setJenisKelaminPemegangBpkb(e.getString("JenisKelaminPemegangBPKB"));
						resp.setKecamatan(e.getString("Kecamatan"));
						resp.setKecamatanPemegangBpkb(e.getString("KecamatanPemegangBPKB"));
						resp.setKelurahan(e.getString("Kelurahan"));
						resp.setKelurahanPemegangBpkb(e.getString("KelurahanPemegangBPKB"));
						resp.setKewarganegaraan(e.getString("Kewarganegaraan"));
						resp.setKewarganegaraanPemegangBpkb(e.getString("KewarganegaraanPemegangBPKB"));
						resp.setKodeCabang(e.getString("KodeCabang"));
						resp.setKodepos(e.getString("Kodepos"));
						resp.setKodeposPemegangBpkb(e.getString("KodeposPemegangBPKB"));
						resp.setKota(e.getString("Kota"));
						resp.setKotaPemegangBpkb(e.getString("KotaPemegangBPKB"));
						resp.setLicensePlate(e.getString("LicensePlate"));
						resp.setMerkTypeKendaraan(e.getString("MerkTypeKendaraan"));
						resp.setNamaCabang(e.getString("NamaCabang"));
						resp.setNamaDebitur(e.getString("NamaDebitur"));
						resp.setNamaNotaris(e.getString("NamaNotaris"));
						resp.setNamaPemegangBpkb(e.getString("NamaPemegangBPKB"));
						resp.setNilaiHutangPokok(e.getString("NilaiHutangPokok"));
						resp.setNilaiOtr(e.getString("NilaiOTR"));
						resp.setNilaiObjekPenjaminan(e.getString("NilaiObjekPenjaminan"));
						resp.setNoBpkb(e.getString("NoBPKB"));
						resp.setNoHp(e.getString("NoHP"));
						resp.setNoHPPemegangBpkb(e.getString("NoHPPemegangBPKB"));
						resp.setNoMesin(e.getString("NoMesin"));
						resp.setNoRangka(e.getString("NoRangka"));
						resp.setNoTelp(e.getString("NoTelp"));
						resp.setNpwp(e.getString("Npwp"));
						resp.setPekerjaan(e.getString("Pekerjaan"));
						resp.setPekerjaanPemegangBpkb(e.getString("PekerjaanPemegangBPKB"));
						resp.setRtRw(e.getString("RTRW"));
						resp.setRtRwPemegangBpkb(e.getString("RTRWPemegangBPKB"));
						resp.setSpBpkbDate(e.optString("SPBPKBDate"));
						resp.setStartDate(e.getString("StartDate"));
						resp.setStatusPernikahan(e.getString("StatusPernikahan"));
						resp.setStatusPernikahanPemegangBpkb(e.getString("StatusPernikahanPemegangBPKB"));
						resp.setSupplier(e.getString("Supplier"));
						resp.setTahunPembuatan(e.getString("TahunPembuatan"));
						resp.setTanggalKontrak(e.getString("TanggalKontrak"));
						resp.setTanggalLahir(e.getString("TanggalLahir"));
						resp.setTempatLahir(e.getString("TempatLahir"));
						resp.setTempatTanggalLahirPemegangBpkb(e.getString("TempatTanggalLahirPemegangBPKB"));
						resp.setUsedNew(e.getString("UsedNew"));
						resp.setWarna(e.getString("Warna"));
						resp.setAktaNo(e.optString("aktaNo"));
						resp.setAktaDate(e.optString("aktaDate"));
						
						arrayResponse.add(resp);
						}
			} catch (Exception e) {
				throw e;
			}
		}
    	
    	return arrayResponse;
    }
    
//    Badan
    private static List<ResponseArrayGetDataDTO> parseResponseArrayDataBadan(JSONArray jsonArray) throws Exception  {
    	List<ResponseArrayGetDataDTO> arrayResponse = new ArrayList<ResponseArrayGetDataDTO>();
		
		if(jsonArray != null && jsonArray.length() > 0) {
			try {
					for (int i=0; i<jsonArray.length(); i++) {
						ResponseArrayGetDataDTO resp = new ResponseArrayGetDataDTO();
						JSONObject e = jsonArray.getJSONObject(i);
						
						resp.setAdartDate(e.getString("ADARTDate"));
						resp.setAdartNotes(e.getString("ADARTNotes"));
						resp.setAddressPic(e.getString("AddressPIC"));
						resp.setAgreementDate(e.getString("AgreementDate"));
						resp.setAgreementNumber(e.getString("AgreementNumber"));
						resp.setAkteAkhirDate(e.getString("AkteAkhirDate"));
						resp.setAkteAkhirNotes(e.getString("AkteAkhirNotes"));
						resp.setAkteAwalDate(e.getString("AkteAwalDate"));
						resp.setAkteAwalNotes(e.getString("AkteAwalNotes"));
						resp.setAlamatDebitur(e.getString("AlamatDebitur"));
						resp.setAlamatPemegangBpkb(e.getString("AlamatPemegangBPKB"));
						resp.setAlamatPenjamin(e.getString("AlamatPenjamin"));
						resp.setAssetDescription(e.getString("AssetDescription"));
						resp.setEffectiveDate(e.getString("EffectiveDate"));
						resp.setEndDate(e.getString("EndDate"));
						resp.setFiduciaOnlineIdCompany(e.getString("FiduciaOnlineIDCompany"));
						resp.setFiduciaType(e.getString("FiduciaType"));
						resp.setIdNumberPic(e.getString("IDNumberPIC"));
						resp.setiDTypePemegangBpkb(e.getString("IDTypePemegangBPKB"));
						resp.setIdNumberPemegangBpkb(e.getString("IdNumberPemegangBPKB"));
						resp.setJenisCustomer(e.getString("JenisCustomer"));
						resp.setJobPic(e.getString("JobPIC"));
						resp.setKecamatan(e.getString("Kecamatan"));
						resp.setKecamatanPic(e.getString("KecamatanPIC"));
						resp.setKecamatanPemegangBpkb(e.getString("KecamatanPemegangBPKB"));
						resp.setKelurahan(e.getString("Kelurahan"));
						resp.setKelurahanPIC(e.getString("KelurahanPIC"));
						resp.setKelurahanPemegangBpkb(e.getString("KelurahanPemegangBPKB"));
						resp.setKodeCabang(e.getString("KodeCabang"));
						resp.setKodepos(e.getString("Kodepos"));
						resp.setKodeposPic(e.getString("KodeposPIC"));
						resp.setKodeposPemegangBpkb(e.getString("KodeposPemegangBPKB"));
						resp.setKota(e.getString("Kota"));
						resp.setKotaPic(e.getString("KotaPIC"));
						resp.setKotaPemegangBpkb(e.getString("KotaPemegangBPKB"));
						resp.setLicensePlate(e.getString("LicensePlate"));
						resp.setMerkTypeKendaraan(e.getString("MerkTypeKendaraan"));
						resp.setNoAdart(e.getString("NOADART"));
						resp.setNpwpDate(e.getString("NPWPDate"));
						resp.setNpwpNotes(e.getString("NPWPNotes"));
						resp.setNamaCabang(e.getString("NamaCabang"));
						resp.setNamaDebitur(e.getString("NamaDebitur"));
						resp.setNamaNotaris(e.getString("NamaNotaris"));
						resp.setNamaPic(e.getString("NamaPIC"));
						resp.setNamaPemegangBpkb(e.getString("NamaPemegangBPKB"));
						resp.setNilaiObjekPenjaminan(e.getString("NilaiObjekPenjaminan"));
						resp.setNilaiPenjaminan(e.getString("NilaiPenjaminan"));
						resp.setNilaiHutangPokok(e.getString("NilaiPokokHutang"));
						resp.setNoAkteAkhir(e.getString("NoAkteAkhir"));
						resp.setNoAkteAwal(e.getString("NoAkteAwal"));
						resp.setNoBpkb(e.getString("NoBPKB"));
						resp.setNoHp(e.getString("NoHP"));
						resp.setNoHpPic(e.getString("NoHPPIC"));
						resp.setNoMesin(e.getString("NoMesin"));
						resp.setNpwp(e.getString("NoNPWP"));
						resp.setNoRangka(e.getString("NoRangka"));
						resp.setNoSiup(e.getString("NoSIUP"));
						resp.setNoTdp(e.getString("NoTDP"));
						resp.setNoTelp(e.getString("NoTelp"));
						resp.setPekerjaanPemegangBpkb(e.getString("PekerjaanPemegangBPKB"));
						resp.setRtRw(e.getString("RTRW"));
						resp.setRtRwPic(e.getString("RTRWPIC"));
						resp.setRtRwPemegangBpkb(e.getString("RTRWPemegangBPKB"));
						resp.setSiupDate(e.getString("SIUPDate"));
						resp.setSiupNotes(e.getString("SIUPNotes"));
						resp.setSpBpkbDate(e.optString("SPBPKBDate"));
						resp.setStartDate(e.getString("StartDate"));
						resp.setSupplier(e.getString("Supplier"));
						resp.setTdpDate(e.getString("TDPDate"));
						resp.setTdpNotes(e.getString("TDPNotes"));
						resp.setTahunPembuatan(e.getString("TahunPembuatan"));
						resp.setTanggalKontrak(e.getString("TanggalKontrak"));
						resp.setTempatTanggalLahirPemegangBpkb(e.getString("TempatTanggalLahirPemegangBPKB"));
						resp.setUsedNew(e.getString("UsedNew"));
						resp.setWarna(e.getString("Warna"));
						resp.setAktaNo(e.optString("aktaNo"));
						resp.setAktaDate(e.optString("aktaDate"));
						
						arrayResponse.add(resp);
						}
			} catch (Exception e) {
				throw e;
			}
		}
    	
    	return arrayResponse;
    }
    
    public RawData insertRawData(List<ResponseArrayGetDataDTO> dataArray, LocalDate orderDate, String nsbJenis) {
//Perorangan    	AgreementNumber|AlamatDebitur|AlamatPemegangBPKB|AssetDescription|AssetUsage|EffectiveDate|EndDate|FiduciaOnlineIDPersonal|FiduciaType|IDType|IDTypePemegangBPKB|IdNumber|IdNumberPemegangBPKB|JenisCustomer|JenisKelamin|JenisKelaminPemegangBPKB|Kecamatan|KecamatanPemegangBPKB|Kelurahan|KelurahanPemegangBPKB|Kewarganegaraan|KewarganegaraanPemegangBPKB|KodeCabang|Kodepos|KodeposPemegangBPKB|Kota|KotaPemegangBPKB|LicensePlate|MerkTypeKendaraan|NamaCabang|NamaDebitur|NamaNotaris|NamaPemegangBPKB|NilaiHutangPokok|NilaiOTR|NilaiObjekPenjaminan|NoBPKB|NoHP|NoHPPemegangBPKB|NoMesin|NoRangka|NoTelp|Npwp|Pekerjaan|PekerjaanPemegangBPKB|RTRW|RTRWPemegangBPKB|SPBPKBDate|StartDate|StatusPernikahan|StatusPernikahanPemegangBPKB|Supplier|TahunPembuatan|TanggalKontrak|TanggalLahir|TempatLahir|TempatTanggalLahirPemegangBPKB|UsedNew|Warna"    	
		// validasi terlebih dahulu cabang, duplicate dll
    	for (ResponseArrayGetDataDTO data : dataArray) {
    		String line = "P".equals(nsbJenis) ?
    				data.getAgreementNumber() + lineDelimiter +
    					data.getAlamatDebitur() + lineDelimiter +
    					data.getAlamatPemegangBpkb() + lineDelimiter +
    					data.getAssetDescription() + lineDelimiter +
    					data.getAssetUsage() + lineDelimiter +
    					data.getEffectiveDate() + lineDelimiter +
    					data.getEndDate() + lineDelimiter +
    					data.getFiduciaOnlineIdPersonal() + lineDelimiter +
    					data.getFiduciaType() + lineDelimiter +
    					data.getiDType() + lineDelimiter +
    					data.getiDTypePemegangBpkb() + lineDelimiter +
    					data.getIdNumber() + lineDelimiter +
    					data.getIdNumberPemegangBpkb() + lineDelimiter +
    					data.getJenisCustomer() + lineDelimiter +
    					data.getJenisKelamin() + lineDelimiter +
    					data.getJenisKelaminPemegangBpkb() + lineDelimiter +
    					data.getKecamatan() + lineDelimiter +
    					data.getKecamatanPemegangBpkb() + lineDelimiter +
    					data.getKelurahan() + lineDelimiter +
    					data.getKelurahanPemegangBpkb() + lineDelimiter +
    					data.getKewarganegaraan() + lineDelimiter +
    					data.getKewarganegaraanPemegangBpkb() + lineDelimiter +
    					data.getKodeCabang() + lineDelimiter +
    					data.getKodepos() + lineDelimiter +
    					data.getKodeposPemegangBpkb() + lineDelimiter +
    					data.getKota() + lineDelimiter +
    					data.getKotaPemegangBpkb() + lineDelimiter +
    					data.getLicensePlate() + lineDelimiter +
    					data.getMerkTypeKendaraan() + lineDelimiter +
    					data.getNamaCabang() + lineDelimiter +
    					data.getNamaDebitur() + lineDelimiter +
    					data.getNamaNotaris() + lineDelimiter +
    					data.getNamaPemegangBpkb() + lineDelimiter +
    					data.getNilaiHutangPokok() + lineDelimiter +
    					data.getNilaiOtr() + lineDelimiter +
    					data.getNilaiObjekPenjaminan() + lineDelimiter +
    					data.getNoBpkb() + lineDelimiter +
    					data.getNoHp() + lineDelimiter +
    					data.getNoHPPemegangBpkb() + lineDelimiter +
    					data.getNoMesin() + lineDelimiter +
    					data.getNoRangka() + lineDelimiter +
    					data.getNoTelp() + lineDelimiter +
    					data.getNpwp() + lineDelimiter +
    					data.getPekerjaan() + lineDelimiter +
    					data.getPekerjaanPemegangBpkb() + lineDelimiter +
    					data.getRtRw() + lineDelimiter +
    					data.getRtRwPemegangBpkb() + lineDelimiter +
    					data.getSpBpkbDate() + lineDelimiter +
    					data.getStartDate() + lineDelimiter +
    					data.getStatusPernikahan() + lineDelimiter +
    					data.getStatusPernikahanPemegangBpkb() + lineDelimiter +
    					data.getSupplier() + lineDelimiter +
    					data.getTahunPembuatan() + lineDelimiter +
    					data.getTanggalKontrak() + lineDelimiter +
    					data.getTanggalLahir() + lineDelimiter +
    					data.getTempatLahir() + lineDelimiter +
    					data.getTempatTanggalLahirPemegangBpkb() + lineDelimiter +
    					data.getUsedNew() + lineDelimiter +
    					data.getWarna() + lineDelimiter +
    					data.getAktaNo() + lineDelimiter +
    					data.getAktaDate()
    					:
	    					data.getAdartDate() + lineDelimiter +
							data.getAdartNotes() + lineDelimiter +
							data.getAddressPic() + lineDelimiter +
							data.getAgreementDate() + lineDelimiter +
							data.getAgreementNumber() + lineDelimiter +
							data.getAkteAkhirDate() + lineDelimiter +
							data.getAkteAkhirNotes() + lineDelimiter +
							data.getAkteAwalDate() + lineDelimiter +
							data.getAkteAwalNotes() + lineDelimiter +
							data.getAlamatDebitur() + lineDelimiter +
							data.getAlamatPemegangBpkb() + lineDelimiter +
							data.getAlamatPenjamin() + lineDelimiter +
							data.getAssetDescription() + lineDelimiter +
							data.getEffectiveDate() + lineDelimiter +
							data.getEndDate() + lineDelimiter +
							data.getFiduciaOnlineIdCompany() + lineDelimiter +
							data.getFiduciaType() + lineDelimiter +
							data.getIdNumberPic() + lineDelimiter +
							data.getiDTypePemegangBpkb() + lineDelimiter +
							data.getIdNumberPemegangBpkb() + lineDelimiter +
							data.getJenisCustomer() + lineDelimiter +
							data.getJobPic() + lineDelimiter +
							data.getKecamatan() + lineDelimiter +
							data.getKecamatanPic() + lineDelimiter +
							data.getKecamatanPemegangBpkb() + lineDelimiter +
							data.getKelurahan() + lineDelimiter +
							data.getKelurahanPIC() + lineDelimiter +
							data.getKelurahanPemegangBpkb() + lineDelimiter +
							data.getKodeCabang() + lineDelimiter +
							data.getKodepos() + lineDelimiter +
							data.getKodeposPic() + lineDelimiter +
							data.getKodeposPemegangBpkb() + lineDelimiter +
							data.getKota() + lineDelimiter +
							data.getKotaPic() + lineDelimiter +
							data.getKotaPemegangBpkb() + lineDelimiter +
							data.getLicensePlate() + lineDelimiter +
							data.getMerkTypeKendaraan() + lineDelimiter +
							data.getNoAdart() + lineDelimiter +
							data.getNpwpDate() + lineDelimiter +
							data.getNpwpNotes() + lineDelimiter +
							data.getNamaCabang() + lineDelimiter +
							data.getNamaDebitur() + lineDelimiter +
							data.getNamaNotaris() + lineDelimiter +
							data.getNamaPic() + lineDelimiter +
							data.getNamaPemegangBpkb() + lineDelimiter +
							data.getNilaiObjekPenjaminan() + lineDelimiter +
							data.getNilaiPenjaminan() + lineDelimiter +
							data.getNilaiHutangPokok() + lineDelimiter +
							data.getNoAkteAkhir() + lineDelimiter +
							data.getNoAkteAwal() + lineDelimiter +
							data.getNoBpkb() + lineDelimiter +
							data.getNoHp() + lineDelimiter +
							data.getNoHpPic() + lineDelimiter +
							data.getNoMesin() + lineDelimiter +
							data.getNpwp() + lineDelimiter +
							data.getNoRangka() + lineDelimiter +
							data.getNoSiup() + lineDelimiter +
							data.getNoTdp() + lineDelimiter +
							data.getNoTelp() + lineDelimiter +
							data.getPekerjaanPemegangBpkb() + lineDelimiter +
							data.getRtRw() + lineDelimiter +
							data.getRtRwPic() + lineDelimiter +
							data.getRtRwPemegangBpkb() + lineDelimiter +
							data.getSiupDate() + lineDelimiter +
							data.getSiupNotes() + lineDelimiter +
							data.getSpBpkbDate() + lineDelimiter +
							data.getStartDate() + lineDelimiter +
							data.getSupplier() + lineDelimiter +
							data.getTdpDate() + lineDelimiter +
							data.getTdpNotes() + lineDelimiter +
							data.getTahunPembuatan() + lineDelimiter +
							data.getTanggalKontrak() + lineDelimiter +
							data.getTempatTanggalLahirPemegangBpkb() + lineDelimiter +
							data.getUsedNew() + lineDelimiter +
							data.getWarna() + lineDelimiter +
	    					data.getAktaNo() + lineDelimiter +
	    					data.getAktaDate();
    					
			RawData rawData = new RawData();
	        rawData.setOrderDate(orderDate);
	        rawData.setUniqueKey(data.getAgreementNumber());
	        rawData.setFileName(null);
	        rawData.setNamaNasabah(data.getNamaDebitur());
	        rawData.setLine(line);
	        rawData.setLineNumber(null);
	        rawData.setDelimeter(lineDelimiter);
	        rawData.setNsbJenis("Perusahaan".equals(data.getJenisCustomer())? 1 : 2);
//	        System.out.println(line);
	        rawDataRepository.save(rawData);
	    	
	    	try {
	    		String result = orderDataInsertService.process(rawData, data);
	    		if (result == null) continue;
			} catch (Exception e) {
				log.error("Error orderDataInsertService " + e);
			}
		}
    	return null;
    }
    
    private static ResponseGetTokenDTO parseResponse (String json) throws Exception  {
    	ResponseGetTokenDTO response = new ResponseGetTokenDTO();
		
		if(json != null && json.length() > 0) {
			try {
				JSONObject message = new JSONObject(json);
					response.setStatus(message.getString("status"));
					response.setMessage(message.getString("message"));
					response.setToken(message.getString("token"));
			} catch (Exception e) {
				throw e;
			}
		}
    	
    	return response;
    }

	private TrustManager[] get_trust_mgr() {
		TrustManager[] certs = new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			public void checkClientTrusted(X509Certificate[] certs, String t) {
			}

			public void checkServerTrusted(X509Certificate[] certs, String t) {
			}
		} };
		return certs;
	}

}
