import { Moment } from 'moment';

export interface IJobDTO {
      tglOrder?: Moment;
      jmlOdn?: number;
      jmlOndCl?: number;
      jmlOdnSc?: number;
      jmlAkta?: number;
      jmlAktaCl?: number;
      jmlAktaBlReg?: number;
      jmlAktaScReg?: number;
      jmlSerBl?: number;
      jmlSerSc?: number;
      jmlLamp?: number;
      jmlBktLns?: number;
      tglInv?: Moment;
      drive?: string;
      totalSpace?: number;
      freeSpace?: number;
}

export class JobDTO implements IJobDTO {
  constructor(
      public tglOrder?: Moment,
      public jmlOdn?: number,
      public jmlOndCl?: number,
      public jmlOdnSc?: number,
      public jmlAkta?: number,
      public jmlAktaCl?: number,
      public jmlAktaBlReg?: number,
      public jmlAktaScReg?: number,
      public jmlSerBl?: number,
      public jmlSerSc?: number,
      public jmlLamp?: number,
      public jmlBktLns?: number,
      public tglInv?: Moment,
      public drive?: string,
      public totalSpace?: number,
      public freeSpace?: number,
  ) {}
}
