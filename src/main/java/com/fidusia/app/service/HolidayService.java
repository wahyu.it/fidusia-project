package com.fidusia.app.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.fidusia.app.domain.Holiday;
import com.fidusia.app.repository.HolidayRepository;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;

@Service
public class HolidayService {

    private final Logger log = LoggerFactory.getLogger(HolidayService.class);

	private final HolidayRepository holidayRepository;
	
	public HolidayService(HolidayRepository holidayRepository) {
		this.holidayRepository = holidayRepository;
	}

    public int getJumlahHariKerjaMonthly(int jenisHariKerja, LocalDate date, List<Holiday> holidays) {

    	if(holidays == null)
    		holidays = holidayRepository.findByTanggalBetween(date.withDayOfMonth(1), date.withDayOfMonth(date.lengthOfMonth()));

		int result = date.lengthOfMonth();

		for(Holiday holiday : holidays) {
			if((jenisHariKerja == 1 && holiday.getTanggal().getDayOfWeek().getValue() <= DayOfWeek.FRIDAY.getValue())
					|| (jenisHariKerja == 2 && holiday.getTanggal().getDayOfWeek().getValue() <= DayOfWeek.SATURDAY.getValue())) {
				result--;
			}
		}

		for(int i=1; i<=date.lengthOfMonth(); i++) {
			LocalDate dt = date.withDayOfMonth(i);
			if((jenisHariKerja == 1 && dt.getDayOfWeek().getValue() > DayOfWeek.FRIDAY.getValue())
					|| (jenisHariKerja == 2 && dt.getDayOfWeek().getValue() > DayOfWeek.SATURDAY.getValue())) {
				result--;
			}
		}

		return result;
	}

	public boolean isHolidayNow() {
    	return isHoliday(LocalDate.now());
	}
	
	public boolean isSundayNow() {
    	return isSunday(LocalDate.now());
	}

	public boolean isSunday(LocalDate date) {
		if (date.getDayOfWeek().equals(DayOfWeek.SUNDAY))
			return true;
		return false;
	}

	public boolean isHoliday(LocalDate date) {
		List<Holiday> holidays = holidayRepository.findByTanggal(date);
		return holidays!=null && !holidays.isEmpty();
	}

	public LocalDate nextWeekDay(LocalDate date) {
		LocalDate next = date.plusDays(1);
		if(next.getDayOfWeek().equals(DayOfWeek.SATURDAY) ||  next.getDayOfWeek().equals(DayOfWeek.SUNDAY) || isHoliday(next))
			return nextWeekDay(next);
		
		return next;
	}

	public LocalDate prevWeekDay(LocalDate date) {
		LocalDate prev = date.minusDays(1);
		if(prev.getDayOfWeek().equals(DayOfWeek.SATURDAY) ||  prev.getDayOfWeek().equals(DayOfWeek.SUNDAY) || isHoliday(prev))
			return prevWeekDay(prev);
		
		return prev;
	}
}
