import { Routes } from '@angular/router';
import { InputAktaComponent } from './input-akta.component';
import { JhiResolvePagingParams } from 'ng-jhipster';

export const iputaktaState: Routes = [{
  path: '',
  component: InputAktaComponent,
  resolve: {
	  pagingParams: JhiResolvePagingParams
  }
  }
];
