import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SisteminformasimonitoringSharedModule } from 'app/shared/shared.module';
import { RegistrasiRoyaComponent } from './registrasiroya.component';
import { registrasiRoyaState } from './registrasiroya.route';

@NgModule({
  imports: [SisteminformasimonitoringSharedModule, RouterModule.forChild(registrasiRoyaState)],
  declarations: [RegistrasiRoyaComponent]
})
export class RegistrasiRoyaModule {}
