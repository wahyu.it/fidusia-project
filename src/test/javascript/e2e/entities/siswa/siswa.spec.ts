import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { SiswaComponentsPage, SiswaDeleteDialog, SiswaUpdatePage } from './siswa.page-object';

const expect = chai.expect;

describe('Siswa e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let siswaComponentsPage: SiswaComponentsPage;
  let siswaUpdatePage: SiswaUpdatePage;
  let siswaDeleteDialog: SiswaDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Siswas', async () => {
    await navBarPage.goToEntity('siswa');
    siswaComponentsPage = new SiswaComponentsPage();
    await browser.wait(ec.visibilityOf(siswaComponentsPage.title), 5000);
    expect(await siswaComponentsPage.getTitle()).to.eq('sisteminformasiakademikApp.siswa.home.title');
    await browser.wait(ec.or(ec.visibilityOf(siswaComponentsPage.entities), ec.visibilityOf(siswaComponentsPage.noResult)), 1000);
  });

  it('should load create Siswa page', async () => {
    await siswaComponentsPage.clickOnCreateButton();
    siswaUpdatePage = new SiswaUpdatePage();
    expect(await siswaUpdatePage.getPageTitle()).to.eq('sisteminformasiakademikApp.siswa.home.createOrEditLabel');
    await siswaUpdatePage.cancel();
  });

  it('should create and save Siswas', async () => {
    const nbButtonsBeforeCreate = await siswaComponentsPage.countDeleteButtons();

    await siswaComponentsPage.clickOnCreateButton();

    await promise.all([
      siswaUpdatePage.setNisInput('nis'),
      siswaUpdatePage.setNamaInput('nama'),
      siswaUpdatePage.setAlamatInput('alamat'),
      siswaUpdatePage.jenisKelaminSelectLastOption(),
      siswaUpdatePage.setTempatLahirInput('tempatLahir'),
      siswaUpdatePage.setTanggalLahirInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      siswaUpdatePage.setWaliMuridInput('waliMurid'),
      siswaUpdatePage.setNoTelephoneInput('noTelephone'),
      siswaUpdatePage.setTahunAjaranInput('tahunAjaran'),
      siswaUpdatePage.kelasSelectLastOption()
    ]);

    expect(await siswaUpdatePage.getNisInput()).to.eq('nis', 'Expected Nis value to be equals to nis');
    expect(await siswaUpdatePage.getNamaInput()).to.eq('nama', 'Expected Nama value to be equals to nama');
    expect(await siswaUpdatePage.getAlamatInput()).to.eq('alamat', 'Expected Alamat value to be equals to alamat');
    expect(await siswaUpdatePage.getTempatLahirInput()).to.eq('tempatLahir', 'Expected TempatLahir value to be equals to tempatLahir');
    expect(await siswaUpdatePage.getTanggalLahirInput()).to.contain(
      '2001-01-01T02:30',
      'Expected tanggalLahir value to be equals to 2000-12-31'
    );
    expect(await siswaUpdatePage.getWaliMuridInput()).to.eq('waliMurid', 'Expected WaliMurid value to be equals to waliMurid');
    expect(await siswaUpdatePage.getNoTelephoneInput()).to.eq('noTelephone', 'Expected NoTelephone value to be equals to noTelephone');
    expect(await siswaUpdatePage.getTahunAjaranInput()).to.eq('tahunAjaran', 'Expected TahunAjaran value to be equals to tahunAjaran');

    await siswaUpdatePage.save();
    expect(await siswaUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await siswaComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Siswa', async () => {
    const nbButtonsBeforeDelete = await siswaComponentsPage.countDeleteButtons();
    await siswaComponentsPage.clickOnLastDeleteButton();

    siswaDeleteDialog = new SiswaDeleteDialog();
    expect(await siswaDeleteDialog.getDialogTitle()).to.eq('sisteminformasiakademikApp.siswa.delete.question');
    await siswaDeleteDialog.clickOnConfirmButton();

    expect(await siswaComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
