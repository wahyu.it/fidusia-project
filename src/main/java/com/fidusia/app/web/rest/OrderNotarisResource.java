package com.fidusia.app.web.rest;

import com.fidusia.app.domain.OrderNotaris;
import com.fidusia.app.repository.OrderNotarisRepository;
import com.fidusia.app.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.fidusia.app.domain.OrderNotaris}.
 */
@RestController
@RequestMapping("/api/order-notarises")
@Transactional
public class OrderNotarisResource {

    private final Logger log = LoggerFactory.getLogger(OrderNotarisResource.class);

    private static final String ENTITY_NAME = "orderNotaris";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OrderNotarisRepository orderNotarisRepository;

    public OrderNotarisResource(OrderNotarisRepository orderNotarisRepository) {
        this.orderNotarisRepository = orderNotarisRepository;
    }

    /**
     * {@code POST  /order-notarises} : Create a new orderNotaris.
     *
     * @param orderNotaris the orderNotaris to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new orderNotaris, or with status {@code 400 (Bad Request)} if the orderNotaris has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<OrderNotaris> createOrderNotaris(@RequestBody OrderNotaris orderNotaris) throws URISyntaxException {
        log.debug("REST request to save OrderNotaris : {}", orderNotaris);
        if (orderNotaris.getId() != null) {
            throw new BadRequestAlertException("A new orderNotaris cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrderNotaris result = orderNotarisRepository.save(orderNotaris);
        return ResponseEntity
            .created(new URI("/api/order-notarises/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /order-notarises/:id} : Updates an existing orderNotaris.
     *
     * @param id the id of the orderNotaris to save.
     * @param orderNotaris the orderNotaris to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated orderNotaris,
     * or with status {@code 400 (Bad Request)} if the orderNotaris is not valid,
     * or with status {@code 500 (Internal Server Error)} if the orderNotaris couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<OrderNotaris> updateOrderNotaris(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody OrderNotaris orderNotaris
    ) throws URISyntaxException {
        log.debug("REST request to update OrderNotaris : {}, {}", id, orderNotaris);
        if (orderNotaris.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, orderNotaris.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!orderNotarisRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        OrderNotaris result = orderNotarisRepository.save(orderNotaris);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, orderNotaris.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /order-notarises/:id} : Partial updates given fields of an existing orderNotaris, field will ignore if it is null
     *
     * @param id the id of the orderNotaris to save.
     * @param orderNotaris the orderNotaris to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated orderNotaris,
     * or with status {@code 400 (Bad Request)} if the orderNotaris is not valid,
     * or with status {@code 404 (Not Found)} if the orderNotaris is not found,
     * or with status {@code 500 (Internal Server Error)} if the orderNotaris couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<OrderNotaris> partialUpdateOrderNotaris(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody OrderNotaris orderNotaris
    ) throws URISyntaxException {
        log.debug("REST request to partial update OrderNotaris partially : {}, {}", id, orderNotaris);
        if (orderNotaris.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, orderNotaris.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!orderNotarisRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<OrderNotaris> result = orderNotarisRepository
            .findById(orderNotaris.getId())
            .map(existingOrderNotaris -> {
                if (orderNotaris.getTanggal() != null) {
                    existingOrderNotaris.setTanggal(orderNotaris.getTanggal());
                }
                if (orderNotaris.getStatus() != null) {
                    existingOrderNotaris.setStatus(orderNotaris.getStatus());
                }

                return existingOrderNotaris;
            })
            .map(orderNotarisRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, orderNotaris.getId().toString())
        );
    }

    /**
     * {@code GET  /order-notarises} : get all the orderNotarises.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of orderNotarises in body.
     */
    @GetMapping("")
    public ResponseEntity<List<OrderNotaris>> getAllOrderNotarises(Pageable pageable) {
        log.debug("REST request to get a page of OrderNotarises");
        Page<OrderNotaris> page = orderNotarisRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /order-notarises/:id} : get the "id" orderNotaris.
     *
     * @param id the id of the orderNotaris to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the orderNotaris, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<OrderNotaris> getOrderNotaris(@PathVariable("id") Long id) {
        log.debug("REST request to get OrderNotaris : {}", id);
        Optional<OrderNotaris> orderNotaris = orderNotarisRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(orderNotaris);
    }

    /**
     * {@code DELETE  /order-notarises/:id} : delete the "id" orderNotaris.
     *
     * @param id the id of the orderNotaris to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteOrderNotaris(@PathVariable("id") Long id) {
        log.debug("REST request to delete OrderNotaris : {}", id);
        orderNotarisRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
