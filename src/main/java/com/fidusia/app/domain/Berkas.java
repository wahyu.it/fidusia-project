package com.fidusia.app.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Berkas.
 */
@Entity
@Table(name = "berkas")
public class Berkas implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "identitas")
    private String identitas;
    
    @Column(name = "kk")
    private String kk;
    
    @Column(name = "ppk")
    private String ppk;
    
    @Column(name = "skNasabah")
    private String skNasabah;

    @Column(name = "doc_kontrak")
    private String docKontrak;

    @Column(name = "skpjf")
    private String skpjf;

    @Column(name = "npwp")
    private String npwp;

    @Column(name = "ktpp")
    private String ktpp;

    @Column(name = "ktpdp")
    private String ktpdp;

    @Column(name = "ktp_dirut")
    private String ktpDirut;

    @Column(name = "tdp")
    private String tdp;

    @Column(name = "situ")
    private String situ;

    @Column(name = "akta_pend")
    private String aktaPend;

    @Column(name = "akta_perub")
    private String aktaPerub;

    @Column(name = "sk_pendirian")
    private String skPendirian;

    @Column(name = "sk_perubahan")
    private String skPerubahan;

    @Column(name = "ktp_bpkb")
    private String ktpBpkb;

    @Column(name = "siup")
    private String siup;

    @Column(name = "nib")
    private String nib;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "update_on")
    private ZonedDateTime updateOn;

    @Column(name = "upload_by")
    private String uploadBy;

    @Column(name = "upload_on")
    private ZonedDateTime uploadOn;

    @Column(name = "ppk_nomor")
    private String ppkNomor;

    @Column(name = "nsb_jenis")
    private Integer nsbJenis;

    @Column(name = "berkas_status")
    private Integer berkasStatus;

    @Column(name = "nama_nasabah")
    private String namaNasabah;

    @Column(name = "tanggal_ppk")
    private LocalDate tanggalPpk;

    @Column(name = "metode_kirim")
    private Integer metodeKirim;

    @Column(name = "partition_id")
    private Integer partitionId;

    @Column(name = "verify_by")
    private String verifyBy;

    @Column(name = "verify_on")
    private ZonedDateTime verifyOn;

    @Column(name = "order_type")
    private Integer orderType;

    @ManyToOne(fetch = FetchType.LAZY)
    private Remark remark;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "leasing" }, allowSetters = true)
    private Cabang cabang;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Berkas)) {
            return false;
        }
        return getId() != null && getId().equals(((Berkas) o).getId());
    }

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocKontrak() {
		return docKontrak;
	}

	public void setDocKontrak(String docKontrak) {
		this.docKontrak = docKontrak;
	}

	public String getSkpjf() {
		return skpjf;
	}

	public void setSkpjf(String skpjf) {
		this.skpjf = skpjf;
	}

	public String getNpwp() {
		return npwp;
	}

	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}

	public String getKtpp() {
		return ktpp;
	}

	public void setKtpp(String ktpp) {
		this.ktpp = ktpp;
	}

	public String getKtpdp() {
		return ktpdp;
	}

	public void setKtpdp(String ktpdp) {
		this.ktpdp = ktpdp;
	}

	public String getKtpDirut() {
		return ktpDirut;
	}

	public void setKtpDirut(String ktpDirut) {
		this.ktpDirut = ktpDirut;
	}

	public String getTdp() {
		return tdp;
	}

	public void setTdp(String tdp) {
		this.tdp = tdp;
	}

	public String getSitu() {
		return situ;
	}

	public void setSitu(String situ) {
		this.situ = situ;
	}

	public String getAktaPend() {
		return aktaPend;
	}

	public void setAktaPend(String aktaPend) {
		this.aktaPend = aktaPend;
	}

	public String getKtpBpkb() {
		return ktpBpkb;
	}

	public void setKtpBpkb(String ktpBpkb) {
		this.ktpBpkb = ktpBpkb;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public ZonedDateTime getUpdateOn() {
		return updateOn;
	}

	public void setUpdateOn(ZonedDateTime updateOn) {
		this.updateOn = updateOn;
	}

	public String getUploadBy() {
		return uploadBy;
	}

	public void setUploadBy(String uploadBy) {
		this.uploadBy = uploadBy;
	}

	public ZonedDateTime getUploadOn() {
		return uploadOn;
	}

	public void setUploadOn(ZonedDateTime uploadOn) {
		this.uploadOn = uploadOn;
	}

	public String getPpkNomor() {
		return ppkNomor;
	}

	public void setPpkNomor(String ppkNomor) {
		this.ppkNomor = ppkNomor;
	}

	public Integer getNsbJenis() {
		return nsbJenis;
	}

	public void setNsbJenis(Integer nsbJenis) {
		this.nsbJenis = nsbJenis;
	}

	public Integer getBerkasStatus() {
		return berkasStatus;
	}

	public void setBerkasStatus(Integer berkasStatus) {
		this.berkasStatus = berkasStatus;
	}

	public String getNamaNasabah() {
		return namaNasabah;
	}

	public void setNamaNasabah(String namaNasabah) {
		this.namaNasabah = namaNasabah;
	}

	public LocalDate getTanggalPpk() {
		return tanggalPpk;
	}

	public void setTanggalPpk(LocalDate tanggalPpk) {
		this.tanggalPpk = tanggalPpk;
	}

	public Integer getMetodeKirim() {
		return metodeKirim;
	}

	public void setMetodeKirim(Integer metodeKirim) {
		this.metodeKirim = metodeKirim;
	}

	public Integer getPartitionId() {
		return partitionId;
	}

	public void setPartitionId(Integer partitionId) {
		this.partitionId = partitionId;
	}

	public String getVerifyBy() {
		return verifyBy;
	}

	public void setVerifyBy(String verifyBy) {
		this.verifyBy = verifyBy;
	}

	public ZonedDateTime getVerifyOn() {
		return verifyOn;
	}

	public void setVerifyOn(ZonedDateTime verifyOn) {
		this.verifyOn = verifyOn;
	}

	public Integer getOrderType() {
		return orderType;
	}

	public void setOrderType(Integer orderType) {
		this.orderType = orderType;
	}

	public Remark getRemark() {
		return remark;
	}

	public void setRemark(Remark remark) {
		this.remark = remark;
	}

	public Cabang getCabang() {
		return cabang;
	}

	public void setCabang(Cabang cabang) {
		this.cabang = cabang;
	}

	public String getAktaPerub() {
		return aktaPerub;
	}

	public void setAktaPerub(String aktaPerub) {
		this.aktaPerub = aktaPerub;
	}

	public String getSiup() {
		return siup;
	}

	public void setSiup(String siup) {
		this.siup = siup;
	}

	public String getNib() {
		return nib;
	}

	public void setNib(String nib) {
		this.nib = nib;
	}

	public String getIdentitas() {
		return identitas;
	}

	public void setIdentitas(String identitas) {
		this.identitas = identitas;
	}

	public String getKk() {
		return kk;
	}

	public void setKk(String kk) {
		this.kk = kk;
	}

	public String getPpk() {
		return ppk;
	}

	public void setPpk(String ppk) {
		this.ppk = ppk;
	}

	public String getSkNasabah() {
		return skNasabah;
	}

	public void setSkNasabah(String skNasabah) {
		this.skNasabah = skNasabah;
	}

	public String getSkPendirian() {
		return skPendirian;
	}

	public void setSkPendirian(String skPendirian) {
		this.skPendirian= skPendirian;
	}

	public String getSkPerubahan() {
		return skPerubahan;
	}

	public void setSkPerub(String skPerubahan) {
		this.skPerubahan = skPerubahan;
	}

	@Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

	@Override
	public String toString() {
		return "Berkas [id=" + id + ", identitas=" + identitas + ", kk=" + kk + ", ppk=" + ppk + ", skNasabah="
				+ skNasabah + ", docKontrak=" + docKontrak + ", skpjf=" + skpjf + ", npwp=" + npwp + ", ktpp=" + ktpp
				+ ", ktpdp=" + ktpdp + ", ktpDirut=" + ktpDirut + ", tdp=" + tdp + ", situ=" + situ + ", aktaPend="
				+ aktaPend + ", aktaPerub=" + aktaPerub + ", skPend=" + skPendirian + ", skPerububahan=" + skPerubahan + ", ktpBpkb="
				+ ktpBpkb + ", siup=" + siup + ", nib=" + nib + ", updateBy=" + updateBy + ", updateOn=" + updateOn
				+ ", uploadBy=" + uploadBy + ", uploadOn=" + uploadOn + ", ppkNomor=" + ppkNomor + ", nsbJenis="
				+ nsbJenis + ", berkasStatus=" + berkasStatus + ", namaNasabah=" + namaNasabah + ", tanggalPpk="
				+ tanggalPpk + ", metodeKirim=" + metodeKirim + ", partitionId=" + partitionId + ", verifyBy="
				+ verifyBy + ", verifyOn=" + verifyOn + ", orderType=" + orderType + ", remark=" + remark + ", cabang="
				+ cabang + "]";
	}
}
