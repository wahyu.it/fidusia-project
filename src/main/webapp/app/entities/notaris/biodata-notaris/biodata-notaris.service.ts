import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SERVER_API_URL } from 'app/app.constants';
import { Observable } from 'rxjs';
import { INotarisDTO } from 'app/shared/model/notarisDTO.model';

@Injectable({ providedIn: 'root' })
export class BiodataNotarisService {
  	  public resourceUrl= SERVER_API_URL + 'api/aktas/search';
  	  public byLoginNotarisUrl= SERVER_API_URL + 'api/notarises/getByLogin';
	
	  constructor(protected http: HttpClient) {}

	  notarisLogin(): Observable<INotarisDTO> {
	    return this.http.get<INotarisDTO>(this.byLoginNotarisUrl);
	  }
	
}