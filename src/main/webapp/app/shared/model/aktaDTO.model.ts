import { Moment } from 'moment';

export interface IAktaDTO {
        idAkta?: number;
        tglOrder?: Moment;
        nomorAkta?: string;
        tglAkta?: Moment;
        pukulAkta?: Moment;
        notarisId?: number;
        notarisNama?: string;
        notarisKedudukan?: string;
        penghadapNama?: string;
        saksiOneNama?: string;
        saksiTwoNama?: string;
        namaNasabah?: string;
        namaBpkb?: string;
        orangId?: number;
        badanId?: number;
        ppkNomor?: string;
}

export class AktaDTO implements IAktaDTO {
  constructor(
        public idAkta?: number,
        public tglOrder?: Moment,
        public nomorAkta?: string,
        public tglAkta?: Moment,
        public pukulAkta?: Moment,
        public notarisId?: number,
        public notarisNama?: string,
        public notarisKedudukan?: string,
        public penghadapNama?: string,
        public saksiOneNama?: string,
        public saksiTwoNama?: string,
        public namaNasabah?: string,
        public namaBpkb?: string,
        public orangId?: number,
        public badanId?: number,
        public ppkNomor?: string
  ) {}
}
