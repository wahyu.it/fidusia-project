package com.fidusia.app.domain.enumeration;

/**
 * The MetodeTransaksi enumeration.
 */
public enum CaraBayar {
    CASH, ANGSURAN
}
