package com.fidusia.app.repository;

import com.fidusia.app.domain.Notaris;
import com.fidusia.app.domain.OrderNotaris;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the OrderNotaris entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrderNotarisRepository extends JpaRepository<OrderNotaris, Long> {

	List<OrderNotaris> findByTanggalAndNotaris(LocalDate tglOrder, Notaris notaris);}
