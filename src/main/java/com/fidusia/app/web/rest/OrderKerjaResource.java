package com.fidusia.app.web.rest;

import com.fidusia.app.domain.OrderKerja;
import com.fidusia.app.repository.OrderKerjaRepository;
import com.fidusia.app.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.fidusia.app.domain.OrderKerja}.
 */
@RestController
@RequestMapping("/api/order-kerjas")
@Transactional
public class OrderKerjaResource {

    private final Logger log = LoggerFactory.getLogger(OrderKerjaResource.class);

    private static final String ENTITY_NAME = "orderKerja";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OrderKerjaRepository orderKerjaRepository;

    public OrderKerjaResource(OrderKerjaRepository orderKerjaRepository) {
        this.orderKerjaRepository = orderKerjaRepository;
    }

    /**
     * {@code POST  /order-kerjas} : Create a new orderKerja.
     *
     * @param orderKerja the orderKerja to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new orderKerja, or with status {@code 400 (Bad Request)} if the orderKerja has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("")
    public ResponseEntity<OrderKerja> createOrderKerja(@RequestBody OrderKerja orderKerja) throws URISyntaxException {
        log.debug("REST request to save OrderKerja : {}", orderKerja);
        if (orderKerja.getId() != null) {
            throw new BadRequestAlertException("A new orderKerja cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrderKerja result = orderKerjaRepository.save(orderKerja);
        return ResponseEntity
            .created(new URI("/api/order-kerjas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /order-kerjas/:id} : Updates an existing orderKerja.
     *
     * @param id the id of the orderKerja to save.
     * @param orderKerja the orderKerja to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated orderKerja,
     * or with status {@code 400 (Bad Request)} if the orderKerja is not valid,
     * or with status {@code 500 (Internal Server Error)} if the orderKerja couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/{id}")
    public ResponseEntity<OrderKerja> updateOrderKerja(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody OrderKerja orderKerja
    ) throws URISyntaxException {
        log.debug("REST request to update OrderKerja : {}, {}", id, orderKerja);
        if (orderKerja.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, orderKerja.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!orderKerjaRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        OrderKerja result = orderKerjaRepository.save(orderKerja);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, orderKerja.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /order-kerjas/:id} : Partial updates given fields of an existing orderKerja, field will ignore if it is null
     *
     * @param id the id of the orderKerja to save.
     * @param orderKerja the orderKerja to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated orderKerja,
     * or with status {@code 400 (Bad Request)} if the orderKerja is not valid,
     * or with status {@code 404 (Not Found)} if the orderKerja is not found,
     * or with status {@code 500 (Internal Server Error)} if the orderKerja couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<OrderKerja> partialUpdateOrderKerja(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody OrderKerja orderKerja
    ) throws URISyntaxException {
        log.debug("REST request to partial update OrderKerja partially : {}, {}", id, orderKerja);
        if (orderKerja.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, orderKerja.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!orderKerjaRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<OrderKerja> result = orderKerjaRepository
            .findById(orderKerja.getId())
            .map(existingOrderKerja -> {
                if (orderKerja.getNomor() != null) {
                    existingOrderKerja.setNomor(orderKerja.getNomor());
                }
                if (orderKerja.getTanggal() != null) {
                    existingOrderKerja.setTanggal(orderKerja.getTanggal());
                }
                if (orderKerja.getKategori() != null) {
                    existingOrderKerja.setKategori(orderKerja.getKategori());
                }
                if (orderKerja.getTanggalTerima() != null) {
                    existingOrderKerja.setTanggalTerima(orderKerja.getTanggalTerima());
                }
                if (orderKerja.getJumlahAkta() != null) {
                    existingOrderKerja.setJumlahAkta(orderKerja.getJumlahAkta());
                }
                if (orderKerja.getJumlahCancel() != null) {
                    existingOrderKerja.setJumlahCancel(orderKerja.getJumlahCancel());
                }
                if (orderKerja.getJumlahCetakAkta() != null) {
                    existingOrderKerja.setJumlahCetakAkta(orderKerja.getJumlahCetakAkta());
                }
                if (orderKerja.getUploadFile() != null) {
                    existingOrderKerja.setUploadFile(orderKerja.getUploadFile());
                }
                if (orderKerja.getStatus() != null) {
                    existingOrderKerja.setStatus(orderKerja.getStatus());
                }
                if (orderKerja.getStatusSigned() != null) {
                    existingOrderKerja.setStatusSigned(orderKerja.getStatusSigned());
                }
                if (orderKerja.getUpdateBy() != null) {
                    existingOrderKerja.setUpdateBy(orderKerja.getUpdateBy());
                }
                if (orderKerja.getUpdateOn() != null) {
                    existingOrderKerja.setUpdateOn(orderKerja.getUpdateOn());
                }
                if (orderKerja.getApproveBy() != null) {
                    existingOrderKerja.setApproveBy(orderKerja.getApproveBy());
                }
                if (orderKerja.getApproveOn() != null) {
                    existingOrderKerja.setApproveOn(orderKerja.getApproveOn());
                }

                return existingOrderKerja;
            })
            .map(orderKerjaRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, orderKerja.getId().toString())
        );
    }

    /**
     * {@code GET  /order-kerjas} : get all the orderKerjas.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of orderKerjas in body.
     */
    @GetMapping("")
    public ResponseEntity<List<OrderKerja>> getAllOrderKerjas(Pageable pageable) {
        log.debug("REST request to get a page of OrderKerjas");
        Page<OrderKerja> page = orderKerjaRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /order-kerjas/:id} : get the "id" orderKerja.
     *
     * @param id the id of the orderKerja to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the orderKerja, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/{id}")
    public ResponseEntity<OrderKerja> getOrderKerja(@PathVariable("id") Long id) {
        log.debug("REST request to get OrderKerja : {}", id);
        Optional<OrderKerja> orderKerja = orderKerjaRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(orderKerja);
    }

    /**
     * {@code DELETE  /order-kerjas/:id} : delete the "id" orderKerja.
     *
     * @param id the id of the orderKerja to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteOrderKerja(@PathVariable("id") Long id) {
        log.debug("REST request to delete OrderKerja : {}", id);
        orderKerjaRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
