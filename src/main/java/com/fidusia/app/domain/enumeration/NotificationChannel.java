package com.fidusia.app.domain.enumeration;

public enum NotificationChannel {
	APP, WA, SMS, EMAIL, FIREBASE
}
